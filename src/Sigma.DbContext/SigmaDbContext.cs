﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sigma.Core.Domain;
using Sigma.DbContext.Entities;

namespace Sigma.DbContext
{
    public class SigmaDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        private readonly string _ConnectionString;
        private readonly bool _NoTrackQuery;

        public DbSet<BiReportEmailJobEntity> BiReportEmailJobs { get; set; }
        public DbSet<SmtpClientEntity> SmtpClients { get; set; }

        public SigmaDbContext (string connectionString, bool noTrackQuery = true)
        {
            _NoTrackQuery = noTrackQuery;
            _ConnectionString = connectionString;
        }

        // TODO: Investigate the usage of No track query.
        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder)
        {
            if (_NoTrackQuery)
                optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            optionsBuilder.UseNpgsql(_ConnectionString);
        }

        public override int SaveChanges (bool acceptAllChangesOnSuccess)
        {
            UpdateDates();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync (bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            UpdateDates();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void UpdateDates()
        {
            var addedEntities = ChangeTracker.Entries<IBaseDomainObject>().Where(entityEntry => entityEntry.State == EntityState.Added).ToList();
            addedEntities.ForEach(entityEntry => { entityEntry.Entity.DateCreated = DateTime.UtcNow; });
            var modifiedEntities = ChangeTracker.Entries<IBaseDomainObject>().Where(entityEntry => entityEntry.State == EntityState.Modified).ToList();
            modifiedEntities.ForEach(entityEntry => { entityEntry.Entity.DateModified = DateTime.UtcNow; });
            modifiedEntities.ForEach(entityEntry => { entityEntry.Property(property => property.DateCreated).IsModified = false; });
        }
    }
}