﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Sigma.Core.Domain;

namespace Sigma.DbContext
{
    public static class DbContextExtensions
    {
        /// <summary>
        ///   Detach entity.
        /// </summary>
        public static void DetachLocal <T> (this Microsoft.EntityFrameworkCore.DbContext context, T sqlObject) where T : BaseDomainObject
        {
            T local = context.Set<T>().Local.FirstOrDefault(entry => entry.Id.Equals(sqlObject.Id));
            if (local != null)
                context.Entry(local).State = EntityState.Detached;
        }

        /// <summary>
        ///   Checks whether the Entity is attached.
        /// </summary>
        public static bool IsAttached <T> (this Microsoft.EntityFrameworkCore.DbContext context, T baseObject) where T : BaseDomainObject
        {
            return context.Set<T>().Local.FirstOrDefault(entry => entry.Id.Equals(baseObject.Id)) != null;
        }
    }
}