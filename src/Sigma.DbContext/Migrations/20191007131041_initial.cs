﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sigma.DbContext.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TestObjects",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestObjects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TestSubObjects",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    TestObjectEntityId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestSubObjects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TestSubObjects_TestObjects_TestObjectEntityId",
                        column: x => x.TestObjectEntityId,
                        principalTable: "TestObjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TestSubObjects_TestObjectEntityId",
                table: "TestSubObjects",
                column: "TestObjectEntityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TestSubObjects");

            migrationBuilder.DropTable(
                name: "TestObjects");
        }
    }
}
