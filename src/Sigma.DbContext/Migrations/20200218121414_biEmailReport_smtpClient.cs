﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sigma.DbContext.Migrations
{
    public partial class biEmailReport_smtpClient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SmtpClients",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    Host = table.Column<string>(nullable: false),
                    Port = table.Column<int>(nullable: false),
                    IsEnabled = table.Column<bool>(nullable: false),
                    IsSsl = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    From = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmtpClients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BiReportEmailJobs",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Recipients = table.Column<List<string>>(nullable: true),
                    ReportUrls = table.Column<List<string>>(nullable: true),
                    SmtpClientId = table.Column<string>(nullable: false),
                    Cron = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BiReportEmailJobs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BiReportEmailJobs_SmtpClients_SmtpClientId",
                        column: x => x.SmtpClientId,
                        principalTable: "SmtpClients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BiReportEmailJobs_SmtpClientId",
                table: "BiReportEmailJobs",
                column: "SmtpClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BiReportEmailJobs");

            migrationBuilder.DropTable(
                name: "SmtpClients");
        }
    }
}
