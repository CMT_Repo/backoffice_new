﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Sigma.Core;

namespace Sigma.DbContext
{
    /// <summary>
    ///   Cli design Time tooling access.
    /// </summary>
    public class SigmaDbContextDesignTimeFactory : IDesignTimeDbContextFactory<SigmaDbContext>
    {
        public SigmaDbContext CreateDbContext (string[] args)
        {
            IConfigurationRoot configurationRoot = ConfigurationHelper.InitConfigurationRoot();
            string connectionString = configurationRoot["SigmaRepository:ConnectionString"];
            IDesignTimeDbContextFactoryHelper.WriteInConsoleConnectionStringUsed(connectionString);
            var optionsBuilder = new DbContextOptionsBuilder<SigmaDbContext>();
            optionsBuilder.UseNpgsql(connectionString);
            return new SigmaDbContext(connectionString);
        }
    }
}