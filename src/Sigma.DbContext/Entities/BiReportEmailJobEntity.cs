﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Sigma.Core.Domain;

namespace Sigma.DbContext.Entities
{
    public class BiReportEmailJobEntity : BaseEntity, IBiReportEmailJob<SmtpClientEntity>
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Title { get; set; }
        public List<string> Recipients { get; set; }
        public List<string> ReportUrls { get; set; }
        [Required]
        public string SmtpClientId { get; set; }
        [Required]
        public string Cron { get; set; }
        [Required]
        public SmtpClientEntity SmtpClient { get; set; }
    }
}