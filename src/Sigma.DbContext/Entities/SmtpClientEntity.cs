﻿using System.ComponentModel.DataAnnotations;
using Sigma.Core.Domain;

namespace Sigma.DbContext.Entities
{
    public class SmtpClientEntity : BaseEntity, ISmtpClient
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Host { get; set; }
        [Required]
        public int Port { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsSsl { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string From { get; set; }
    }
}