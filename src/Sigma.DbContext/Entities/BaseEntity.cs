﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sigma.Core.Domain;

namespace Sigma.DbContext.Entities
{
    [Obsolete("Not used any more, delete when clearing up testObject", false)]
    internal interface IBaseEntity : IBaseDomainObject
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new string Id { get; set; }
        [Required]
        DateTime DateCreated { get; set; }
    }

    public class BaseEntity : IBaseDomainObject
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}