﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sigma.Scheduler.Client
{
    public class ConfigurationOptionsSchedulerClient
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool IsSecured { get; set; }
        public string RootCertPath { get; set; }
        public string CertPath { get; set; }
        public string CertKeyPath { get; set; }
    }
}
