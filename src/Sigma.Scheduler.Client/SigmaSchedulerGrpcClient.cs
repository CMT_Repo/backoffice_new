﻿using sigma.scheduler;

namespace Sigma.Scheduler.Client
{
    public class SigmaSchedulerGrpcClient : SigmaSchedulerGrpc.SigmaSchedulerGrpcClient
    {
        public SigmaSchedulerGrpcClient() : base(SchedulerGrpcClientCollectionExtensions.Channel) { }
    }
}