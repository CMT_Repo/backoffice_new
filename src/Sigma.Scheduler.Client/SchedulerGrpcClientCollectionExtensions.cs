﻿using System;
using System.IO;
using Grpc.Core;
using Grpc.Core.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Sigma.Scheduler.Client
{
    public static class SchedulerGrpcClientCollectionExtensions
    {
        internal static Channel Channel;

        public static void AddSchedulerClient (this IServiceCollection services)
        {
            services.AddScoped<SigmaSchedulerGrpcClient>();
        }

        public static void UseSchedulerClient (this IApplicationBuilder applicationBuilder)
        {
            ConfigurationOptionsSchedulerClient config = applicationBuilder.ApplicationServices.GetService<IOptions<ConfigurationOptionsSchedulerClient>>().Value;
            if (config.IsSecured)
            {
                // TODO: Unified config.
                string cacert = File.ReadAllText(config.RootCertPath);
                string clientcert = File.ReadAllText(config.CertPath);
                string clientkey = File.ReadAllText(config.CertKeyPath);
                var sslCredentials = new SslCredentials(cacert, new KeyCertificatePair(clientcert, clientkey));
                Channel = new Channel(config.Host, config.Port, sslCredentials);
            }
            else
                Channel = new Channel(config.Host, config.Port, ChannelCredentials.Insecure);
            // TODO: Sentry Logger.
            GrpcEnvironment.SetLogger(new ConsoleLogger());
        }

        public static void ShutDown()
        {
            Channel.ShutdownAsync().Wait();
        }
    }
}