import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientsService } from 'src/app/services/clients.service';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { TextboxQuestion } from 'src/app/shared/dynamic-form/question/question-textbox';
import { Validators } from '@angular/forms';
import { ValidatorRegexPatten } from 'src/app/helpers/validators';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

@Component({
    selector: 'app-crm-get-full-contact-details',
    templateUrl: './crm-get-full-contact-details.component.html',
})
export class CrmGetFullContactDetailsComponent implements OnInit {

    public scrollConfig: PerfectScrollbarConfigInterface = {
        suppressScrollX: false
    };
    public data: Array<string> = new Array<string>();
    @ViewChild('dynamicForm', { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new TextboxQuestion(
            'searchQuery',
            'SearchQuery',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Search query' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.plainTextNnumbersSpaceEmail), message: 'Invalid search query' }
            ],
        ),
    ].sort((a, b) => a.order - b.order);

    constructor(private clientService: ClientsService) { }

    ngOnInit() {
        // Set questions.
        this.dynamicForm.canceBtnShowed = false;
        this.dynamicForm.questions = this.questions;
        this.dynamicForm.submit.subscribe(() => this.submit());
    }

    private submit() {
        this.data = [];
        this.dynamicForm.disable();
        this.clientService.getFullContactDetails(this.dynamicForm.form.controls['searchQuery'].value).subscribe(response => {
            for (const key in response)
                if (response.hasOwnProperty(key)) {
                    const element = response[key];
                    this.data.push(`${key}: ${(JSON.stringify(Array.from(element))).replace(/["']/g, '')}`);
                }
            this.dynamicForm.enable();
        });
    }

}
