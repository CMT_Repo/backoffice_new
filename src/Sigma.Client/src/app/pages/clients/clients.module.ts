import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CrmGetFullContactDetailsComponent } from './crm-get-full-contact-details/crm-get-full-contact-details.component';

export const routes = [
    { path: 'CRM_GetFullContactDetails', component: CrmGetFullContactDetailsComponent, data: { breadcrumb: 'CRM_GetFullContactDetails' } },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        SharedModule,
        FormsModule,
    ],
    declarations: [CrmGetFullContactDetailsComponent]
})
export class ClientsModule { }
