import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { EventsService } from 'src/app/services/events.service';
import { EventCmt } from 'src/app/models/event/event-cmt';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { EventsDialogComponent } from './events-dialog/events-dialog.component';
import { EventRegistersDialog2Component } from './event-registers-dialog/event-registers-dialog.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-events',
    templateUrl: './events.component.html',
})
export class EventsComponent implements OnInit {

    routeQueryParams$: Subscription;
    @ViewChild('paginator', { static: true }) paginator: MatPaginator;
    public columns: string[] = [
        'name',
        'urlName',
        'new',
        'approved',
        'rejected',
        'total',
        'smtpServerName',
        'dataCreated',
        'dateModified',
        'crud',
    ];
    public data: MatTableDataSource<EventCmt>;
    private pageSize: number = 15;
    private thisPageIndex: number = 0;

    constructor(private eventService: EventsService, private simgaHelper: SigmaHelper, private dialog: MatDialog, private route: ActivatedRoute, private router: Router) {
        this.routeQueryParams$ = route.queryParams.subscribe(params => {
            if (params['name'])
                this.dialog.open(EventRegistersDialog2Component, {
                    width: '97vw',
                    maxWidth: '100vm',
                    data: { eventName: params['name'] }
                }).afterClosed().subscribe(() => this.updateListEvent());
            // TODO: Add update flag,in order to update list only if there was an update.
        });
    }

    ngOnInit() {
        this.paginator.pageSize = this.pageSize;
        // Subscribe to sort paginator change.
        this.paginator.page.subscribe(event => this.updateListEvent(event));
        // Get initial data, by calling paginator.
        this.updateListEvent();
    }

    private updateListEvent(event?: PageEvent) {
        this.paginator.disabled = true;
        // In case being called outside of UI, use saved values.
        if (event == null) {
            event = new PageEvent();
            event.pageSize = this.pageSize;
            event.pageIndex = this.thisPageIndex;
        }
        this.thisPageIndex = event.pageIndex;
        this.eventService.eventCmtpagedList(event.pageIndex + 1, event.pageSize).subscribe(result => {
            this.data = new MatTableDataSource<EventCmt>(result.data);
            this.paginator = this.simgaHelper.paginator(result.metaData, this.paginator, false);
            this.paginator.disabled = false;
        });
    }

    public eventRegisters(name: string): void {
        this.router.navigate([], { queryParams: { name: name } });
    }

    public addOrUpdate(id: string): void {
        let event;
        if (id) {
            event = this.data.data.find(x => x.id === id);
            if (!event) {
                this.simgaHelper.showSnackBar('Error accrued, contat the developers..');
                return;
            }
        }
        this.paginator.disabled = true;
        this.eventService.eventCmtSmtpServerNames().subscribe(response => {
            this.paginator.disabled = false;
            this.dialog.open(EventsDialogComponent, {
                data: {
                    smtpServerNames: new Map<number, string>(response.map((v, i) => [i + 1, v] as [number, string])),
                    event: event,
                }
            }).afterClosed().subscribe(result => {
                // If true, then a user been added, refresh list.
                if (result)
                    this.updateListEvent();
            });
        });
    }

    public delete(id: string): void {
        this.simgaHelper.dialogConfirm('Deleting this event will also delete it\'s registerees ').subscribe(response => {
            if (response) {
                this.paginator.disabled = true;
                this.eventService.eventCmtDelete(id).subscribe(result => {
                    if (result) {
                        this.simgaHelper.showSnackBar('Event deleted.');
                        this.updateListEvent();
                    } else {
                        this.paginator.disabled = false;
                        this.simgaHelper.showSnackBar('An error accrued, contact development team.');
                    }
                });
            }
        });
    }

    ngOnDestroy() {
        this.routeQueryParams$.unsubscribe();
    }

}
