import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { EventCmtRegister } from 'src/app/models/event/event-cmt-register';
import { EventsService } from 'src/app/services/events.service';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { EventRegistersDialogComponent } from '../event-register-dialog/event-register-dialog.component';

@Component({
    selector: 'app-event-registers-dialog',
    templateUrl: './event-registers-dialog.component.html',
    styleUrls: ['./event-registers-dialog.component.scss']
})
export class EventRegistersDialog2Component implements OnInit {

    @ViewChild('paginator', { static: true }) paginator: MatPaginator;
    public columns: string[] = [
        'fullName',
        'email',
        'phoneNumber',
        'numberOfSeats',
        'affiliate',
        'subAffilaite',
        'cxd',
        'vvar10',
        'language',
        'dateCreated',
        'dateModified',
        'eventRegistrationStatus',
        'refresh',
    ];
    public tableData: MatTableDataSource<EventCmtRegister>;
    private pageSize: number = 12;
    private thisPageIndex: number = 0;

    constructor(public dialogRef: MatDialogRef<EventRegistersDialog2Component>, @Inject(MAT_DIALOG_DATA) public data: { eventName: string }, private router: Router, private route: ActivatedRoute, private eventService: EventsService, private simgaHelper: SigmaHelper, private dialog: MatDialog) {
        this.dialogRef.addPanelClass(['dialog-progress-bar', 'dialog-progress-bar-big']);
        dialogRef.afterClosed().subscribe(() => this.router.navigate(['marketing/events'], { relativeTo: this.route }));
    }

    ngOnInit() {
        this.paginator.pageSize = this.pageSize;
        // Subscribe to sort paginator change.
        this.paginator.page.subscribe(event => this.updateListEvent(event));
        // Get initial data, by calling paginator.
        this.updateListEvent();
    }

    public refresh() {
        this.updateListEvent();
    }

    public updateStatus(id: string, status: number) {
        this.paginator.disabled = true;
        this.eventService.eventCmtTradingUpdateStatus(id, status).subscribe(() => {
            this.updateListEvent();
            this.simgaHelper.showSnackBar('Eevent status register updated');
        });
    }

    public export() {
        this.paginator.disabled = true;
        this.eventService.eventCmtRegistersExport(this.data.eventName).subscribe(() => {
            this.paginator.disabled = false;
        });
    }

    private updateListEvent(event?: PageEvent) {
        this.paginator.disabled = true;
        // In case being called outside of UI, use saved values.
        if (event == null) {
            event = new PageEvent();
            event.pageSize = this.pageSize;
            event.pageIndex = this.thisPageIndex;
        }
        this.thisPageIndex = event.pageIndex;
        this.eventService.eventCmtRegisterspagedList(event.pageIndex + 1, event.pageSize, this.data.eventName).subscribe(result => {
            this.tableData = new MatTableDataSource<EventCmtRegister>(result.data);
            this.paginator = this.simgaHelper.paginator(result.metaData, this.paginator, false);
            this.paginator.disabled = false;
        });
    }

    private addOrUpdate(id?: string) {
        this.dialog.open(EventRegistersDialogComponent, {
            data: {
                eventName: this.data.eventName,
                eventCmt: this.tableData.data.find(x => x.id === id),
            }
        }).afterClosed().subscribe(result => {
            // If true, then a user been added, refresh list.
            if (result)
                this.updateListEvent();
        });
    }

    public delete(id: string) {
        this.eventService.eventCmtRegisterDelete(id).subscribe(result => {
            if (result) {
                this.simgaHelper.showSnackBar('Event register deleted.');
                this.updateListEvent();
            } else
                this.simgaHelper.showSnackBar('An error accrued, contact development team.');
        });
    }

}
