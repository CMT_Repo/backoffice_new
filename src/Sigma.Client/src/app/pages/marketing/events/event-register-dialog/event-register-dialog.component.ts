import { Component, OnInit, Inject, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { EventsService } from 'src/app/services/events.service';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { TextboxQuestion } from 'src/app/shared/dynamic-form/question/question-textbox';
import { Validators } from '@angular/forms';
import { EventCmtRegister } from 'src/app/models/event/event-cmt-register';
import { ValidatorRegexPatten } from 'src/app/helpers/validators';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { GrpcResponse } from 'src/app/models/shared/grpc-response';

@Component({
    selector: 'app-event-register-dialog',
    templateUrl: './event-register-dialog.component.html',
    styleUrls: ['./event-register-dialog.component.scss']
})
export class EventRegistersDialogComponent implements OnInit, AfterViewInit {

    constructor(private eventService: EventsService, private dialogRef: MatDialogRef<EventRegistersDialogComponent>, private sigmaHelper: SigmaHelper, private cd: ChangeDetectorRef, @Inject(MAT_DIALOG_DATA) public data?: { eventName: string, eventCmt: EventCmtRegister }) { }

    @ViewChild('dynamicForm', { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new TextboxQuestion(
            'fullName',
            'Full name*',
            10,
            [
                { name: 'required', validator: Validators.required, message: 'Full name required' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.plainTextNnumbersSpace), message: 'Text only' },
            ],
        ),
        new TextboxQuestion(
            'email',
            'Email*',
            30,
            [
                { name: 'required', validator: Validators.required, message: 'Email required' },
                { name: 'email', validator: Validators.email, message: 'Invalid email' },
            ],
        ),
        new TextboxQuestion(
            'phoneNumber',
            'Phone number*',
            40,
            [
                { name: 'required', validator: Validators.required, message: 'Phone number required' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.numbersOnly), message: 'Numbers only' },
            ],
        ),
        new TextboxQuestion(
            'numberOfSeats',
            'Number of seats*',
            50,
            [
                { name: 'required', validator: Validators.required, message: 'Number of seats required' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.numbersOnly), message: 'Numbers only' },
            ],
        ),
        new TextboxQuestion(
            'affiliate',
            'Affiliate',
            60,
            [
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.numbersOnly), message: 'Numbers only' },
            ],
        ),
        new TextboxQuestion(
            'subAffiliate',
            'Sub affiliate',
            70,
            [],
        ),
        new TextboxQuestion(
            'cxd',
            'CXD',
            80,
            [],
        ),
        new TextboxQuestion(
            'vvar10',
            'VVAR10',
            90,
            [],
        ),
        new TextboxQuestion(
            'langauge',
            'Langauge',
            100,
            [],
        ),
    ].sort((a, b) => a.order - b.order);
    public errors: Array<string> = new Array<string>();
    @ViewChild('perfectScroll', { static: true }) perfectScroll: PerfectScrollbarDirective;

    ngOnInit() {
        this.dialogRef.addPanelClass('dialog-progress-bar');
        // Set questions.
        this.dynamicForm.questions = this.questions;
        // Subscribe to submit event.
        this.dynamicForm.submit.subscribe(() => this.addOrUpdate());
        // Subscribe to cancel event.
        this.dynamicForm.cancel.subscribe(() => this.dialogRef.close());
        this.dynamicForm.width = '20rem';
    }

    ngAfterViewInit() {
        if (this.data.eventCmt) {
            this.dynamicForm.form.controls['fullName'].setValue(this.data.eventCmt.fullName);
            this.dynamicForm.form.controls['email'].setValue(this.data.eventCmt.emailAddress);
            this.dynamicForm.form.controls['phoneNumber'].setValue(this.data.eventCmt.phoneNumber);
            this.dynamicForm.form.controls['numberOfSeats'].setValue(this.data.eventCmt.numberOfSeats);
            this.dynamicForm.form.controls['affiliate'].setValue(this.data.eventCmt.a);
            this.dynamicForm.form.controls['subAffiliate'].setValue(this.data.eventCmt.subAffiliate);
            this.dynamicForm.form.controls['cxd'].setValue(this.data.eventCmt.cxd);
            this.dynamicForm.form.controls['vvar10'].setValue(this.data.eventCmt.vvar10);
            this.dynamicForm.form.controls['langauge'].setValue(this.data.eventCmt.language);
            this.cd.detectChanges();
        }
    }

    private addOrUpdate() {
        this.dynamicForm.disable();
        const eventCmtRegister = new EventCmtRegister();
        eventCmtRegister.fullName = this.dynamicForm.form.controls['fullName'].value;
        eventCmtRegister.eventName = this.data.eventName;
        eventCmtRegister.emailAddress = this.dynamicForm.form.controls['email'].value;
        eventCmtRegister.phoneNumber = this.dynamicForm.form.controls['phoneNumber'].value;
        eventCmtRegister.numberOfSeats = parseInt(this.dynamicForm.form.controls['numberOfSeats'].value, 10);
        eventCmtRegister.a = this.dynamicForm.form.controls['affiliate'].value === '' ? 0 : parseInt(this.dynamicForm.form.controls['affiliate'].value, 10);
        eventCmtRegister.subAffiliate = this.dynamicForm.form.controls['subAffiliate'].value;
        eventCmtRegister.cxd = this.dynamicForm.form.controls['cxd'].value;
        eventCmtRegister.vvar10 = this.dynamicForm.form.controls['vvar10'].value;
        eventCmtRegister.language = this.dynamicForm.form.controls['langauge'].value;
        eventCmtRegister.eventName = this.data.eventName;
        if (this.data.eventCmt != null) {
            // Update.
            eventCmtRegister.id = this.data.eventCmt.id;
            eventCmtRegister.dateCreated = this.data.eventCmt.dateCreated;
            this.eventService.eventCmtRegisterAdd(eventCmtRegister).subscribe(result => {
                if (result.isSuccess)
                    this.sigmaHelper.showSnackBar('Event register updated');
                this.handleAddOrUpdate(result);
            });
        } else
            // Add.
            this.eventService.eventCmtRegisterUpdate(eventCmtRegister).subscribe(result => {
                if (result.isSuccess)
                    this.sigmaHelper.showSnackBar('Event register craeted');
                this.handleAddOrUpdate(result);
            });
    }

    private handleAddOrUpdate(grpcResnpose: GrpcResponse) {
        this.dynamicForm.enable();
        if (grpcResnpose.isSuccess)
            this.dialogRef.close(true);
        else {
            this.perfectScroll.scrollToTop();
            this.errors = new Array<string>();
            for (const iterator of grpcResnpose.errors)
                this.errors.push(iterator.description);
        }
    }

}