import { Component, OnInit, Inject, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SeoRedirectsDialogComponent } from '../../seo-redirects/dialogs/seo-redirects-dialog/seo-redirects-dialog.component';
import { Validators } from '@angular/forms';
import { SelectQuestion } from 'src/app/shared/dynamic-form/question/question-select';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { TextboxQuestion } from 'src/app/shared/dynamic-form/question/question-textbox';
import { TexfieldQuestion } from 'src/app/shared/dynamic-form/question/question-textfield';
import { EventCmt } from 'src/app/models/event/event-cmt';
import { EventsService } from 'src/app/services/events.service';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { ValidatorRegexPatten } from 'src/app/helpers/validators';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { GrpcResponse } from 'src/app/models/shared/grpc-response';

@Component({
    selector: 'app-events-dialog',
    templateUrl: './events-dialog.component.html',
})
export class EventsDialogComponent implements OnInit, AfterViewInit {

    public errors = new Array<string>();
    @ViewChild('perfectScroll', { static: true }) perfectScroll: PerfectScrollbarDirective;
    @ViewChild('dynamicForm', { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new TextboxQuestion(
            'name',
            'Name*',
            1,
            [
                { name: 'required', validator: Validators.required, message: 'Name required' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.plainTextNnumbersSpace), message: 'Text only' },
            ],
            null,
        ),
        new TextboxQuestion(
            'urlName',
            'Event URL name*',
            2,
            [
                { name: 'required', validator: Validators.required, message: 'Url name required' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.plainTextNnumbers), message: 'Invlaid url' },
            ],
            null,
        ),
        new TextboxQuestion(
            'thankYouUrl',
            'URL thank you*',
            3,
            [
                { name: 'required', validator: Validators.required, message: 'Thank you URL required' },
            ],
        ),
        new SelectQuestion(
            'smtpServer',
            'SMTP server*',
            4,
            [
                { name: 'required', validator: Validators.required, message: 'SMTP is Required' },
            ],
            null,
            this.data.smtpServerNames,
        ),
        new TextboxQuestion(
            'emailThankYouSubject',
            'Email thank you subject*',
            5,
            [
                { name: 'required', validator: Validators.required, message: 'Thank you URL required' },
            ],
        ),
        new TexfieldQuestion(
            'emailThankYouBody',
            'Email thank you body*',
            6,
            [
                { name: 'required', validator: Validators.required, message: 'Thank you URL required' },
            ],
            null,
            3,
        ),
        new TextboxQuestion(
            'emailApprovedSubject',
            'Email approved subject*',
            7,
            [
                { name: 'required', validator: Validators.required, message: 'Email approved subject required' },
            ],
        ),
        new TexfieldQuestion(
            'emailApprovedBody',
            'Email approved body*',
            8,
            [
                { name: 'required', validator: Validators.required, message: 'Email approved body required' },
            ],
            null,
            3,
        ),
        new TextboxQuestion(
            'emailRejectedSubject',
            'Email rejected subject*',
            9,
            [
                { name: 'required', validator: Validators.required, message: 'Email rejected subject required' },
            ],
        ),
        new TexfieldQuestion(
            'emailRejectedBody',
            'Email rejected body*',
            10,
            [
                { name: 'required', validator: Validators.required, message: 'Email rejected body required' },
            ],
            null,
            3,
        ),
    ].sort((a, b) => a.order - b.order);

    constructor(private dialogRef: MatDialogRef<SeoRedirectsDialogComponent>, private sigmaHelper: SigmaHelper, private cd: ChangeDetectorRef, private eventService: EventsService, @Inject(MAT_DIALOG_DATA) public data: { smtpServerNames: Map<number, string>, event: EventCmt }) {
        // Add this class if you wish to use overhead mat-progress-bar.
        this.dialogRef.addPanelClass('dialog-progress-bar');
    }

    ngOnInit() {
        this.dynamicForm.questions = this.questions;
        // Subscribe to submit event.
        this.dynamicForm.submit.subscribe(() => this.addOrUpdate());
        // Subscribe to cancel event.
        this.dynamicForm.cancel.subscribe(() => this.dialogRef.close());
        this.dynamicForm.width = '25rem';
    }

    ngAfterViewInit() {
        if (this.data.event) {
            this.dynamicForm.form.controls['name'].setValue(this.data.event.name);
            this.dynamicForm.form.controls['urlName'].setValue(this.data.event.urlName);
            this.dynamicForm.form.controls['thankYouUrl'].setValue(this.data.event.thankYouUrl);
            for (const iterator of this.data.smtpServerNames.keys()) {
                if (this.data.smtpServerNames.get(iterator) === this.data.event.smtpServerName) {
                    this.dynamicForm.form.controls['smtpServer'].setValue(iterator);
                    break;
                }
            }
            this.dynamicForm.form.controls['emailThankYouSubject'].setValue(this.data.event.emailThankYouSubject);
            this.dynamicForm.form.controls['emailThankYouBody'].setValue(this.data.event.emailThankYouBody);
            this.dynamicForm.form.controls['emailApprovedSubject'].setValue(this.data.event.emailApprovedSubject);
            this.dynamicForm.form.controls['emailApprovedBody'].setValue(this.data.event.emailApprovedBody);
            this.dynamicForm.form.controls['emailRejectedSubject'].setValue(this.data.event.emailRejectedSubject);
            this.dynamicForm.form.controls['emailRejectedBody'].setValue(this.data.event.emailRejectedBody);
            this.cd.detectChanges();
        }
    }

    public addOrUpdate() {
        this.dynamicForm.disable();
        const eventCmt = new EventCmt();
        eventCmt.name = this.dynamicForm.form.controls['name'].value;
        eventCmt.urlName = this.dynamicForm.form.controls['urlName'].value;
        eventCmt.thankYouUrl = this.dynamicForm.form.controls['thankYouUrl'].value;
        eventCmt.smtpServerName = this.data.smtpServerNames.get(this.dynamicForm.form.controls['smtpServer'].value);
        eventCmt.emailThankYouSubject = this.dynamicForm.form.controls['emailThankYouSubject'].value;
        eventCmt.emailThankYouBody = this.dynamicForm.form.controls['emailThankYouBody'].value;
        eventCmt.emailApprovedSubject = this.dynamicForm.form.controls['emailApprovedSubject'].value;
        eventCmt.emailApprovedBody = this.dynamicForm.form.controls['emailApprovedBody'].value;
        eventCmt.emailRejectedSubject = this.dynamicForm.form.controls['emailRejectedSubject'].value;
        eventCmt.emailRejectedBody = this.dynamicForm.form.controls['emailRejectedBody'].value;
        if (this.data.event != null) {
            // Update.
            eventCmt.id = this.data.event.id;
            eventCmt.dateCreated = this.data.event.dateCreated;
            this.eventService.eventCmtAdd(eventCmt).subscribe(result => {
                if (result.isSuccess)
                    this.sigmaHelper.showSnackBar('Event register updated');
                this.handleAddOrUpdate(result);
            });
        } else
            // Add.
            this.eventService.eventCmtUpdate(eventCmt).subscribe(result => {
                if (result.isSuccess)
                    this.sigmaHelper.showSnackBar('Event register craeted');
                this.handleAddOrUpdate(result);
            });
    }

    private handleAddOrUpdate(grpcResnpose: GrpcResponse) {
        this.dynamicForm.enable();
        if (grpcResnpose.isSuccess)
            this.dialogRef.close(true);
        else {
            this.perfectScroll.scrollToTop();
            this.errors = new Array<string>();
            for (const iterator of grpcResnpose.errors)
                this.errors.push(iterator.description);
        }
    }

}