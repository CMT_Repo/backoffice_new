import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SeoRedirectsComponent } from './seo-redirects/seo-redirects.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SeoRedirectsDialogComponent } from './seo-redirects/dialogs/seo-redirects-dialog/seo-redirects-dialog.component';
import { EventsComponent } from './events/events.component';
import { EventRegistersDialogComponent } from './events/event-register-dialog/event-register-dialog.component';
import { EventsDialogComponent } from './events/events-dialog/events-dialog.component';
import { EventRegistersDialog2Component } from './events/event-registers-dialog/event-registers-dialog.component';

export const routes = [
    { path: 'seoRedirects', component: SeoRedirectsComponent, pathMatch: 'full', data: { breadcrumb: 'SEO Redirects' } },
    { path: 'events', component: EventsComponent, pathMatch: 'full', data: { breadcrumb: 'Events' } },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        SharedModule,
        FormsModule,
    ],
    declarations: [
        SeoRedirectsComponent,
        SeoRedirectsDialogComponent,
        EventsComponent,
        EventRegistersDialogComponent,
        EventsDialogComponent,
        EventRegistersDialog2Component,
    ],
    entryComponents: [
        SeoRedirectsDialogComponent,
        EventRegistersDialogComponent,
        EventsDialogComponent,
        EventRegistersDialog2Component,
    ],
    exports: [RouterModule]
})
export class MarketingModule { }
