import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators } from '@angular/forms';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { TextboxQuestion } from 'src/app/shared/dynamic-form/question/question-textbox';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { ToggleQuestion } from 'src/app/shared/dynamic-form/question/question-toggle';
import { TexfieldQuestion } from 'src/app/shared/dynamic-form/question/question-textfield';
import { SelectQuestion } from 'src/app/shared/dynamic-form/question/question-select';
import { SeoRedirectsService } from 'src/app/services/seo-redirects.service';
import { SeoRedirect } from 'src/app/models/seo-redirects/seo-redirect';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { Website } from 'src/app/models/seo-redirects/website';

@Component({
    selector: 'app-seo-redirects-dialog',
    templateUrl: './seo-redirects-dialog.component.html',
})
export class SeoRedirectsDialogComponent implements OnInit, AfterViewInit {

    @ViewChild('dynamicForm', { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new SelectQuestion(
            'website',
            'Website*',
            10,
            [
                { name: 'required', validator: Validators.required, message: 'Website Required' },
            ],
            null,
            this.data.websites.reduce((mapAccumulator, website) => { mapAccumulator.set(website.id, website.name); return mapAccumulator; }, new Map()),
            'row',
            'center center',
        ),
        new TextboxQuestion(
            'name',
            'Name*',
            20,
            [
                { name: 'required', validator: Validators.required, message: 'Name Required' },
            ],
        ),
        new TexfieldQuestion(
            'source',
            'Source*',
            30,
            [
                { name: 'required', validator: Validators.required, message: 'Source required' },
            ],
        ),
        new TexfieldQuestion(
            'result',
            'Result*',
            40,
            [
                { name: 'required', validator: Validators.required, message: 'Result required' },
            ],
        ),
        new ToggleQuestion(
            'isEnabled',
            'Enabled*',
            50,
        ),
        new ToggleQuestion(
            'isPermanent',
            'Permanent',
            60,
        ),
        new ToggleQuestion(
            'isMulti',
            'Multi',
            70,
        ),
        new ToggleQuestion(
            'isRegex',
            'Regex',
            80,
        ),
    ].sort((a, b) => a.order - b.order);

    constructor(private seoRedirectsService: SeoRedirectsService, private dialogRef: MatDialogRef<SeoRedirectsDialogComponent>, private sigmaHelper: SigmaHelper, private cd: ChangeDetectorRef, @Inject(MAT_DIALOG_DATA) public data?: { websites: Array<Website>, seoRedirect: SeoRedirect }) {
        // Add this class if you wish to use overhead mat-progress-bar.
        this.dialogRef.addPanelClass('dialog-progress-bar');
    }

    private addOrUpdate() {
        this.dynamicForm.disable();
        const seoRedirect = new SeoRedirect();
        seoRedirect.name = this.dynamicForm.form.controls['name'].value;
        seoRedirect.source = this.dynamicForm.form.controls['source'].value;
        seoRedirect.result = this.dynamicForm.form.controls['result'].value;
        seoRedirect.websiteId = parseInt(this.dynamicForm.form.controls['website'].value, 10);
        seoRedirect.isEnabled = this.dynamicForm.form.controls['isEnabled'].value && true || false;
        seoRedirect.isRegex = this.dynamicForm.form.controls['isRegex'].value && true || false;
        seoRedirect.isMulti = this.dynamicForm.form.controls['isMulti'].value && true || false;
        seoRedirect.isPermanent = this.dynamicForm.form.controls['isPermanent'].value && true || false;
        if (this.data.seoRedirect == null)
            this.seoRedirectsService.add(seoRedirect).subscribe(result => {
                if (result)
                    this.sigmaHelper.showSnackBar('SEO redirect added');
                else
                    this.sigmaHelper.showSnackBar('Oppps something went wrong :0');
                this.dialogRef.close(true);
            });
        else {
            seoRedirect.id = this.data.seoRedirect.id;
            this.seoRedirectsService.modify(seoRedirect).subscribe(result => {
                if (result)
                    this.sigmaHelper.showSnackBar('SEO redirect modified');
                else
                    this.sigmaHelper.showSnackBar('Oppps something went wrong :0');
                this.dialogRef.close(true);
            });
        }
    }

    ngOnInit() {
        // Set questions.
        this.dynamicForm.questions = this.questions;
        // Subscribe to submit event.
        this.dynamicForm.submit.subscribe(() => this.addOrUpdate());
        // Subscribe to cancel event.
        this.dynamicForm.cancel.subscribe(() => this.dialogRef.close());
        this.dynamicForm.width = '20rem';
    }

    ngAfterViewInit() {
        // Make sure  only one slider (isMulti / isRegex) is selected at a time.
        this.dynamicForm.form.controls['isMulti'].valueChanges.subscribe(foo => {
            if (foo && this.dynamicForm.form.controls['isRegex'].value)
                this.dynamicForm.form.controls['isRegex'].setValue(false);
        });
        this.dynamicForm.form.controls['isRegex'].valueChanges.subscribe(foo => {
            if (foo && this.dynamicForm.form.controls['isMulti'].value)
                this.dynamicForm.form.controls['isMulti'].setValue(false);
        });
        // If data is present, render it inside form.
        if (this.data.seoRedirect != null) {
            this.dynamicForm.form.controls['website'].setValue(this.data.seoRedirect.websiteId.toString());
            this.dynamicForm.form.controls['name'].setValue(this.data.seoRedirect.name);
            this.dynamicForm.form.controls['result'].setValue(this.data.seoRedirect.result);
            this.dynamicForm.form.controls['source'].setValue(this.data.seoRedirect.source);
            this.dynamicForm.form.controls['isEnabled'].setValue(this.data.seoRedirect.isEnabled);
            this.dynamicForm.form.controls['isMulti'].setValue(this.data.seoRedirect.isMulti);
            this.dynamicForm.form.controls['isPermanent'].setValue(this.data.seoRedirect.isPermanent);
            this.dynamicForm.form.controls['isRegex'].setValue(this.data.seoRedirect.isRegex);
            this.cd.detectChanges();
        }
    }

}
