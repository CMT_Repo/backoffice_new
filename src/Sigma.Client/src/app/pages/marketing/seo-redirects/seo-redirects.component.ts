import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { SeoRedirectsService } from 'src/app/services/seo-redirects.service';
import { SeoRedirect } from 'src/app/models/seo-redirects/seo-redirect';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { Website } from 'src/app/models/seo-redirects/website';
import { SeoRedirectsDialogComponent } from 'src/app/pages/marketing/seo-redirects/dialogs/seo-redirects-dialog/seo-redirects-dialog.component';

@Component({
    selector: 'app-seo-redirects',
    templateUrl: './seo-redirects.component.html',
})
export class SeoRedirectsComponent implements OnInit {

    @ViewChild('paginator', { static: true }) paginator: MatPaginator;
    @ViewChild('userTable', { static: true }) userTable: MatTable<SeoRedirect>;
    @ViewChild(MatSort) sortUserTable: MatSort;
    public columns: string[] = ['id', 'name', 'websiteId', 'dateCreated', 'dateModified', 'source', 'result', 'isEnabled', 'isPermanent', 'isMulti', 'isRegex', 'crud'];
    public dataSource: MatTableDataSource<SeoRedirect>;
    private pageSize: number = 15;
    private thisPageIndex: number = 0;
    private websites: Array<Website>;

    constructor(private seoRedirectsService: SeoRedirectsService, private simgaHelper: SigmaHelper, private dialog: MatDialog) { }

    ngOnInit() {
        // Subscribe to sort paginator change.
        this.paginator.page.subscribe(event => this.updateListEvent(event));
        // Get initial data, by calling paginator.
        this.updateListEvent();
        // Get avaliable website list.
        this.seoRedirectsService.websites().subscribe(result => {
            this.websites = result;
        });
    }

    private updateListEvent(event?: PageEvent) {
        this.paginator.disabled = true;
        // In case being called outside of UI, use saved values.
        if (event == null) {
            event = new PageEvent();
            event.pageSize = this.pageSize;
            event.pageIndex = this.thisPageIndex;
        }
        this.thisPageIndex = event.pageIndex;
        this.seoRedirectsService.pagedList(event.pageIndex + 1, event.pageSize).subscribe(result => {
            this.dataSource = new MatTableDataSource<SeoRedirect>(result.data);
            this.paginator = this.simgaHelper.paginator(result.metaData, this.paginator);
            this.paginator.disabled = false;
        });
    }

    private add() {
        this.dialog.open(SeoRedirectsDialogComponent, {
            data: { websites: this.websites }
        }).afterClosed().subscribe(result => {
            // If true, then a user been added, refresh list.
            if (result)
                this.updateListEvent();
        });
    }

    private modify(id: number) {
        this.dialog.open(SeoRedirectsDialogComponent, {
            data: {
                websites: this.websites,
                seoRedirect: this.dataSource.data.find(fa => fa.id === id)
            }
        }).afterClosed().subscribe(result => {
            // If true, then a user been added, refresh list.
            if (result)
                this.updateListEvent();
        });
    }

    private remove(id: number) {
        this.paginator.disabled = true;
        this.seoRedirectsService.remove(id).subscribe(() => this.updateListEvent());
    }

}
