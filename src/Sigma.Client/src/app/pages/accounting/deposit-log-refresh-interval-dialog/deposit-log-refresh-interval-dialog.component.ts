import { Component, OnInit, ViewChild, Inject, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { SliderQuestion } from 'src/app/shared/dynamic-form/question/question-slider';

@Component({
    selector: 'app-deposit-log-refresh-interval-dialog',
    templateUrl: './deposit-log-refresh-interval-dialog.component.html',
})
export class DepositLogRefreshIntervalDialogComponent implements OnInit, AfterViewInit {

    @ViewChild('dynamicForm', { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new SliderQuestion(
            'refreshInterval',
            'Refresh interval',
            10,
            null,
            'auto',
            5,
        ),
    ].sort((a, b) => a.order - b.order);

    constructor(private dialogRef: MatDialogRef<DepositLogRefreshIntervalDialogComponent>, private cd: ChangeDetectorRef, @Inject(MAT_DIALOG_DATA) public data?: { refreshIntervalSec: number }) { }

    ngOnInit() {
        // Set questions.
        this.dynamicForm.questions = this.questions;
        // Subscribe to submit event.
        this.dynamicForm.submit.subscribe(() => this.dialogRef.close((this.questions[0] as SliderQuestion).value));
        // Subscribe to cancel event.
        this.dynamicForm.cancel.subscribe(() => this.dialogRef.close());
    }

    ngAfterViewInit(): void {
        if (this.data) {
            (this.questions[0] as SliderQuestion).value = this.data.refreshIntervalSec;
            this.cd.detectChanges();
        }
    }

}
