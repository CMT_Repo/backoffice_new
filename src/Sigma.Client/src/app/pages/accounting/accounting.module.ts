import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepositLogComponent } from './deposit-log/deposit-log.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { DepositLogDetailsDialogComponent } from './deposit-log-details-dialog/deposit-log-details-dialog.component';
import { DepositLogRefreshIntervalDialogComponent } from './deposit-log-refresh-interval-dialog/deposit-log-refresh-interval-dialog.component';

export const routes = [
    { path: 'depositLog', component: DepositLogComponent, data: { breadcrumb: 'Deposit log' } },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        SharedModule,
        FormsModule,
    ],
    declarations:
        [
            DepositLogComponent,
            DepositLogDetailsDialogComponent,
            DepositLogRefreshIntervalDialogComponent,
        ],
    entryComponents: [
        DepositLogDetailsDialogComponent,
        DepositLogRefreshIntervalDialogComponent,
    ],
})
export class AccountingModule { }