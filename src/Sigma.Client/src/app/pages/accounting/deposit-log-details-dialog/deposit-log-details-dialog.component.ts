import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DepositAttempt } from 'src/app/models/accounting/depositAttempt';
import { DepositAttemptPspResult } from 'src/app/models/accounting/depositAttempResult';
import { MatTableDataSource } from '@angular/material/table';
import { DepositLogService } from 'src/app/services/depositLog.service';


@Component({
    selector: 'app-deposit-log-details-dialog',
    templateUrl: './deposit-log-details-dialog.component.html',
    styleUrls: ['./deposit-log-details-dialog.component.scss']
})
export class DepositLogDetailsDialogComponent implements OnInit {
    public depositAttempt: DepositAttempt;
    public data: MatTableDataSource<DepositAttemptPspResult>;
    public loading: boolean = true;
    public columns: string[] = [
        'id',
        'dateCreated',
        'pspName',
        'ppsResult',
        'message'
    ];

    constructor(private accountingService: DepositLogService, private dialogRef: MatDialogRef<DepositLogDetailsDialogComponent>, @Inject(MAT_DIALOG_DATA) public dialogData?: any) {
        this.depositAttempt = dialogData.depositAttempt;
    }

    ngOnInit() {
        this.accountingService.depositAttempPspResults(this.depositAttempt.id).subscribe(result => {
            this.data = new MatTableDataSource<DepositAttemptPspResult>(result);
            this.loading = false;
        });
    }

}
