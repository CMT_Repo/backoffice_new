
// TODO: If ever be decided to make url search, update logic filter.

import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DepositLogService } from 'src/app/services/depositLog.service';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DepositAttempt } from 'src/app/models/accounting/depositAttempt';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { DepositLogDetailsDialogComponent } from 'src/app/pages/accounting/deposit-log-details-dialog/deposit-log-details-dialog.component';
import { DepositLogRefreshIntervalDialogComponent } from '../deposit-log-refresh-interval-dialog/deposit-log-refresh-interval-dialog.component';
import { FormGroup, FormControl } from '@angular/forms';
import { DepositLogFilter } from 'src/app/models/accounting/depositLogFilter';
import { AuthService } from 'src/app/services/auth.service';
import { Permission } from 'src/app/models/auth/permission';

@Component({
    selector: 'app-deposit-log',
    templateUrl: './deposit-log.component.html',
    styleUrls: [
        './deposit-log.component.scss'
    ]
})

export class DepositLogComponent implements OnInit {
    @ViewChild('paginator', { static: true }) paginator: MatPaginator;
    public columns: string[] = [
        'id',
        'login',
        'mt4OrderId',
        'firstName',
        'lastName',
        'email',
        'dateCreated',
        'amount',
        'currency',
        'cardAssociation',
        'last4digits',
        'pspName',
        'countryIso',
        'isDodChecked',
        'inCrm',
        'savedToTradingPlatform',
        'details',
    ];
    public data: MatTableDataSource<DepositAttempt>;
    public creditCardAssociations = new Array<any>();
    public paymentProviders = new Array<any>();
    public countryISOs = new Array<any>();
    public curriences = new Array<any>();
    public filter: DepositLogFilter;
    public dodAccess: boolean;
    public depositLogFilterForm: FormGroup = new FormGroup({
        id: new FormControl(''),
        mt4Account: new FormControl(''),
        mt4Order: new FormControl(''),
        amount: new FormControl(''),
        firstName: new FormControl(''),
        lastName: new FormControl(''),
        email: new FormControl(''),
        crmTransactionId: new FormControl(''),
        curriences: new FormControl(''),
        paymentProviders: new FormControl(''),
        creditcardAssociations: new FormControl(''),
        countryISOs: new FormControl(''),
        isSuccessfulOnly: new FormControl(''),
    });
    private pageSize: number = 15;
    private thisPageIndex: number = 0;
    private autoRefreshInterval: number;
    private autoRefreshIntervalSec: number = 0;
    private uiCounter: number = 0;

    constructor(private depositLogService: DepositLogService, private el: ElementRef, private simgaHelper: SigmaHelper, private dialog: MatDialog, private authService: AuthService) {
        this.el.nativeElement.querySelector(`.mat-header-row`);
        this.depositLogFilterForm.disable();
        // Set drop downs for filter fomr.
        this.depositLogService.metadata().subscribe(result => {
            // Countries filter options.
            for (const property in result.countries)
                if (!this.countryISOs.some((arr) => arr.name === result.countries[property]))
                    this.countryISOs.push({ id: result.countries[property], name: result.countries[property] });
            // Credit card filter options.
            for (const property in result.creditCardAssociationsEnum)
                if (!this.creditCardAssociations.some((arr) => arr.name === result.creditCardAssociationsEnum[property]))
                    this.creditCardAssociations.push({ id: property, name: result.creditCardAssociationsEnum[property] });
            // PaymentProviders card filter options.
            for (const property in result.paymentProviders)
                if (!this.paymentProviders.some((arr) => arr.name === result.paymentProviders[property]))
                    this.paymentProviders.push({ id: result.paymentProviders[property], name: result.paymentProviders[property] });
            // Curriences filter options.
            for (const property in result.curriencesEnum)
                if (!this.curriences.some((arr) => arr.name === result.curriencesEnum[property]))
                    this.curriences.push({ id: property, name: result.curriencesEnum[property] });
            this.countryISOs.filter(simgaHelper.onlyUnique);
            this.depositLogFilterForm.enable();
        });
        this.dodAccess = authService.isAuthorized(Permission.DepositLogDodChange);
    }

    ngOnInit() {
        this.paginator.pageSize = this.pageSize;
        // Subscribe to sort paginator change.
        this.paginator.page.subscribe(event => this.updateDepositListEvent(event));
        // Get initial data, by calling paginator.
        this.updateDepositListEvent();
        this.paginator.pageSizeOptions = [10, 15, 20, 30, 40, 50, 70, 80, 100];
    }

    private updateDepositListEvent(event?: PageEvent) {
        if (this.paginator.disabled)
            return;
        this.depositLogFilterForm.disable();
        this.paginator.disabled = true;
        // In case being called outside of UI, use saved values.
        if (event === undefined) {
            event = new PageEvent();
            event.pageSize = this.pageSize;
            event.pageIndex = this.thisPageIndex;
        }
        this.thisPageIndex = event.pageIndex;
        this.depositLogService.depositAttemptPagedList(event.pageIndex + 1, event.pageSize, this.filter).subscribe(result => {
            this.data = new MatTableDataSource<DepositAttempt>(result.data);
            this.paginator = this.simgaHelper.paginator(result.metaData, this.paginator, false);
            this.paginator.disabled = false;
            this.depositLogFilterForm.enable();
            if (this.autoRefreshIntervalSec !== 0 && event)
                this.startCouter();
        });
    }

    private details(id: number) {
        this.dialog.open(DepositLogDetailsDialogComponent, {
            data: {
                depositAttempt: this.data.data.filter(depositAttempt => depositAttempt.id === id)[0]
            }
        }).afterClosed().subscribe(result => {
            // If true, then a user been added, refresh list.
            if (result)
                this.updateDepositListEvent();
        });
    }

    private refresh() {
        this.updateDepositListEvent();
    }

    private refreshInterval() {
        this.dialog.open(DepositLogRefreshIntervalDialogComponent, {
            data: {
                refreshIntervalSec: this.autoRefreshIntervalSec,
            }
        }).afterClosed().subscribe(result => {
            if (result !== undefined)
                if (result === 0) {
                    clearInterval(this.autoRefreshInterval);
                    this.autoRefreshIntervalSec = 0;
                } else {
                    this.autoRefreshIntervalSec = result;
                    this.startCouter();
                }
        });
    }

    public depositLogfilter() {
        this.filter = new DepositLogFilter();
        this.filter.id = ((this.depositLogFilterForm.value.id == null) ? '' : this.depositLogFilterForm.value.id);
        this.filter.mt4Account = ((this.depositLogFilterForm.value.mt4Account == null) ? '' : this.depositLogFilterForm.value.mt4Account);
        this.filter.mt4Order = ((this.depositLogFilterForm.value.mt4Order == null) ? '' : this.depositLogFilterForm.value.mt4Order);
        this.filter.amount = ((this.depositLogFilterForm.value.amount == null) ? '' : this.depositLogFilterForm.value.amount);
        this.filter.firstName = ((this.depositLogFilterForm.value.firstName == null) ? '' : this.depositLogFilterForm.value.firstName);
        this.filter.lastName = ((this.depositLogFilterForm.value.lastName == null) ? '' : this.depositLogFilterForm.value.lastName);
        this.filter.email = ((this.depositLogFilterForm.value.email == null) ? '' : this.depositLogFilterForm.value.email);
        this.filter.crmTransactionId = ((this.depositLogFilterForm.value.crmTransactionId == null) ? '' : this.depositLogFilterForm.value.crmTransactionId);
        this.filter.currencies = ((this.depositLogFilterForm.value.curriences == null) ? '' : this.depositLogFilterForm.value.curriences);
        this.filter.paymentProviders = ((this.depositLogFilterForm.value.paymentProviders == null) ? '' : this.depositLogFilterForm.value.paymentProviders);
        this.filter.creditCardAssociations = ((this.depositLogFilterForm.value.creditcardAssociations == null) ? '' : this.depositLogFilterForm.value.creditcardAssociations);
        this.filter.countryISOs = ((this.depositLogFilterForm.value.countryISOs == null) ? '' : this.depositLogFilterForm.value.countryISOs);
        this.filter.isSuccessfulOnly = ((typeof this.depositLogFilterForm.value.isSuccessfulOnly === 'boolean') ? this.depositLogFilterForm.value.isSuccessfulOnly : false);
        this.updateDepositListEvent();
    }

    public clear() {
        this.depositLogFilterForm.reset();
        this.filter = null;
        this.updateDepositListEvent();
    }

    private startCouter() {
        clearInterval(this.autoRefreshInterval);
        this.uiCounter = this.autoRefreshIntervalSec;
        this.autoRefreshInterval = window.setInterval(() => {
            this.uiCounter = this.uiCounter - 1;
            if (this.uiCounter === 0) {
                this.updateDepositListEvent();
                clearInterval(this.autoRefreshInterval);
            }
        }, 1000);
    }

    private dodUpdate(id: number, event: MatCheckboxChange) {
        this.paginator.disabled = true;
        this.depositLogFilterForm.disable();
        this.depositLogService.dodUpdate(id, event.checked).subscribe(() => {
            this.paginator.disabled = false;
            this.depositLogFilterForm.enable();
            this.simgaHelper.showSnackBar('DOD status updated.');
        });
    }

}