import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from 'src/app/app.settings';
import { Settings } from 'src/app/app.settings.model';
import { AuthService } from 'src/app/services/auth.service';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { PasswordQuestion } from 'src/app/shared/dynamic-form/question/question-password';
import { TextboxQuestion } from 'src/app/shared/dynamic-form/question/question-textbox';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { ValidatorRegexPatten } from 'src/app/helpers/validators';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, AfterViewInit {

    private settings: Settings;
    @ViewChild('dynamicForm', { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new TextboxQuestion(
            'email',
            'Email',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Email Required' },
                { name: 'email', validator: Validators.email, message: 'Invalid email' }
            ],
        ),
        new PasswordQuestion(
            'password',
            'Password',
            1,
            [
                { name: 'required', validator: Validators.required, message: 'Password Required' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.password), message: 'Invalid password' },
            ],
        ),
    ].sort((a, b) => a.order - b.order);

    constructor(public appSettings: AppSettings, public fb: FormBuilder, public router: Router, private activatedRoute: ActivatedRoute, private authService: AuthService) {
        this.settings = this.appSettings.settings;
    }

    public onSubmit(): void {
        if (this.dynamicForm.form.valid) {
            this.dynamicForm.disable();
            this.dynamicForm.errors = new Array<string>();
            const login = this.authService.login(this.dynamicForm.form.controls['email'].value, this.dynamicForm.form.controls['password'].value);
            login.subscribe(result => {
                if (result.Succeeded)
                    // Constractor return url.
                    this.router.navigate(this.activatedRoute.snapshot.queryParams['returnUrl'] ? [this.activatedRoute.snapshot.queryParams['returnUrl']] : ['/']);
                else {
                    if (result.IsLockedOut)
                        this.dynamicForm.errors.push('User loacked out');
                    if (result.IsNotAllowed)
                        this.dynamicForm.errors.push('User not allowed');
                    else
                        this.dynamicForm.errors.push('Invalid username/password combination');
                    this.dynamicForm.enable();
                }
            });
        }
        this.dynamicForm.form.markAsTouched();
    }

    ngOnInit(): void {
        // Init dynamic form.
        this.dynamicForm.questions = this.questions;
        this.dynamicForm.canceBtnShowed = false;
        this.dynamicForm.submit.subscribe(foo => {
            this.onSubmit();
        });
    }

    ngAfterViewInit(): void {
        this.settings.loadingSpinner = false;
    }

}