import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserDialogComponent } from 'src/app/pages/admin/users/user-dialog/user-dialog.component';
import { UserPermissionDialogComponent } from 'src/app/pages/admin/users/user-permission-dialog/user-permission-dialog.component';
import { UserSessionsDialogComponent } from 'src/app/pages/admin/users/user-sessions-dialog/user-sessions-dialog.component';
import { RolesComponent } from './roles/roles.component';
import { RolesDialogComponent } from './roles/role-dialog/role-dialog.component';
import { UserRolesDialogComponent } from './users/user-roles-dialog/user-roles-dialog.component';
import { RoleUserDialogComponent } from './roles/role-user-dialog/role-user-dialog.component';
import { SchedulerComponent } from './scheduler/scheduler.component';
import { SchedulerReportDialogEmailComponent } from './scheduler/scheduler-bi-report-email-dialog.component/scheduler-bi-report-email-dialog.component';
import { SmtpClientsComponent } from './smtp/smtpClients.component';
import { SmtpClientsDialogComponent } from './smtp/smtpClients-dialog/smtpClients-dialog.component';

export const routes = [
    { path: '', redirectTo: 'users', pathMatch: 'full' },
    { path: 'users', component: UsersComponent, data: { breadcrumb: 'Users' } },
    { path: 'roles', component: RolesComponent, data: { breadcrumb: 'Roles' } },
    { path: 'scheduler', component: SchedulerComponent, data: { breadcrumb: 'Scheduler' } },
    { path: 'smtp', component: SmtpClientsComponent, data: { breadcrumb: 'SMTP' } },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        SharedModule,
        FormsModule,
    ],
    declarations: [
        UsersComponent,
        RolesComponent,
        RolesDialogComponent,
        UserDialogComponent,
        UserPermissionDialogComponent,
        UserSessionsDialogComponent,
        UserRolesDialogComponent,
        RoleUserDialogComponent,
        SchedulerComponent,
        SchedulerReportDialogEmailComponent,
        SmtpClientsComponent,
        SmtpClientsDialogComponent,
    ],
    entryComponents: [
        RolesDialogComponent,
        UserDialogComponent,
        UserPermissionDialogComponent,
        UserSessionsDialogComponent,
        UserRolesDialogComponent,
        RoleUserDialogComponent,
        SchedulerReportDialogEmailComponent,
        SmtpClientsDialogComponent,
    ],
})
export class AdminModule { }
