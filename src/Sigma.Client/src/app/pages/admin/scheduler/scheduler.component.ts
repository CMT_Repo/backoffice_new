import { Component, OnInit, ViewChild } from '@angular/core';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { MatDialog } from '@angular/material/dialog';
import { SchedulerReportDialogEmailComponent } from './scheduler-bi-report-email-dialog.component/scheduler-bi-report-email-dialog.component';
import { SchedulerService } from 'src/app/services/scheduler.service';
import { BiReportEmailJob } from 'src/app/models/scheduler/biReportEmailJob';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { SmtpClientService } from 'src/app/services/SmtpClients.service';

@Component({
    selector: 'app-scheduler',
    templateUrl: './scheduler.component.html',
})

export class SchedulerComponent implements OnInit {

    @ViewChild('paginator', { static: true }) paginator: MatPaginator;
    public data: MatTableDataSource<BiReportEmailJob>;
    public columns: string[] = [
        'name',
        'smtpClient',
        'title',
        'cron',
        'dataCreated',
        'dateModified',
        'crud',
    ];
    private pageSize: number = 14;
    private pageIndex: number = 0;

    constructor(private sigmaHelper: SigmaHelper, private smtpClientsServices: SmtpClientService, private dialog: MatDialog, private schedulerService: SchedulerService) { }

    ngOnInit(): void {
        this.paginator.pageSize = this.pageSize;
        // Subscribe to sort paginator change.
        this.paginator.page.subscribe(event => this.updateBiReportEmailJobList(event));
        // Get initial data, by calling paginator.
        this.updateBiReportEmailJobList();
    }

    public addOrUpdate(id: string): void {
        this.paginator.disabled = true;
        this.smtpClientsServices.smtpClientIdNameList().subscribe(nextSmtpl => {
            this.paginator.disabled = false;
            if (id) {
                let biReportEmail;
                biReportEmail = this.data.data.find(x => x.id === id);
                if (!biReportEmail) {
                    this.sigmaHelper.showSnackBar('Error accrued, cant find BiEmailReport in list..');
                    return;
                }
                this.openDialog(nextSmtpl, biReportEmail);
            } else
                this.openDialog(nextSmtpl);
        });
    }

    public delete(id: string): void {
        this.paginator.disabled = true;
        this.schedulerService.biReportEmailJobDelete(id).subscribe(() => {
            this.sigmaHelper.showSnackBar('Deleted successfully');
            this.updateBiReportEmailJobList();
        });
    }

    public executeBiReportEmailJob(id: string) {
        this.paginator.disabled = true;
        this.schedulerService.biReportEmailJobExecute(id).subscribe(() => {
            this.sigmaHelper.showSnackBar('Executed successfully');
            this.paginator.disabled = false;
        });
    }

    private openDialog(smtpClients: Map<string, string>, biReportEmail?: BiReportEmailJob): void {
        this.dialog.open(SchedulerReportDialogEmailComponent, {
            data: {
                biReportEmail: biReportEmail,
                smtpClients: smtpClients,
            }
        }).afterClosed().subscribe(next => {
            if (next)
                this.updateBiReportEmailJobList();
        });
    }

    private updateBiReportEmailJobList(event?: PageEvent): void {
        this.paginator.disabled = true;
        // In case being called outside of UI, use saved values.
        if (event == null) {
            event = new PageEvent();
            event.pageSize = this.pageSize;
            event.pageIndex = this.pageIndex;
        }
        this.pageIndex = event.pageIndex;
        this.schedulerService.biReportEmailJobPagedList(this.pageIndex + 1, this.pageSize).subscribe(next => {
            this.data = new MatTableDataSource<BiReportEmailJob>(next.data);
            this.paginator = this.sigmaHelper.paginator(next.metaData, this.paginator, false);
            this.paginator.disabled = false;
        });
    }

}
