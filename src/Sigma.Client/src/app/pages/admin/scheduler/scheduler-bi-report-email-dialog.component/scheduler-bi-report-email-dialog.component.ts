import { Component, OnInit, ChangeDetectorRef, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { SchedulerService } from 'src/app/services/scheduler.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { TextboxQuestion } from 'src/app/shared/dynamic-form/question/question-textbox';
import { Validators } from '@angular/forms';
import { CronQuestion } from 'src/app/shared/dynamic-form/question/question-cron';
import { ChipsQuestion } from 'src/app/shared/dynamic-form/question/question-chips';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { BiReportEmailJob } from 'src/app/models/scheduler/biReportEmailJob';
import { ApiResponse } from 'src/app/models/shared/api-response';
import { SelectQuestion } from 'src/app/shared/dynamic-form/question/question-select';
import { SmtpClient } from 'src/app/models/smtp/smtpClient';

@Component({
    selector: 'app-scheduler-dialog',
    templateUrl: './scheduler-bi-report-email-dialog.component.html',
})
export class SchedulerReportDialogEmailComponent implements OnInit, AfterViewInit {

    @ViewChild(PerfectScrollbarDirective) perfectScroll: PerfectScrollbarDirective;
    @ViewChild(DynamicForm, { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new TextboxQuestion(
            'name',
            'Name*',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Name required' },
            ],
        ),
        new SelectQuestion(
            'smtpClient',
            'SMTP client*',
            1,
            [
                { name: 'required', validator: Validators.required, message: 'Website Required' },
            ],
            null,
            this.data.smtpClients,
            'row',
            'center center',
        ),
        new TextboxQuestion(
            'title',
            'Title*',
            2,
            [
                { name: 'required', validator: Validators.required, message: 'Title required' },
            ],
        ),
        new CronQuestion(
            'cron',
            'Cron*',
            3,
            [
                { name: 'required', validator: Validators.required, message: 'Cron expression is required' },
            ],
        ),
        new ChipsQuestion(
            'recipients',
            'Recipients',
            4,
            [
                { name: 'required', validator: Validators.required, message: 'Recipients required' },
            ],
        ),
        new ChipsQuestion(
            'reportUrls',
            'Report urls',
            5,
            [
                { name: 'required', validator: Validators.required, message: 'Recipients required' },
            ],
        ),
    ].sort((a, b) => a.order - b.order);
    public errors: Array<string>;

    constructor(private schedulerService: SchedulerService, private dialogRef: MatDialogRef<SchedulerReportDialogEmailComponent>, private sigmaHelper: SigmaHelper, private cd: ChangeDetectorRef, @Inject(MAT_DIALOG_DATA) public data: { biReportEmail: BiReportEmailJob, smtpClients: Map<string, string> }) { }

    public ngOnInit(): void {
        // Add progress-bar.
        this.dialogRef.addPanelClass('dialog-progress-bar');
        // Set questions.
        this.dynamicForm.questions = this.questions;
        this.dynamicForm.submit.subscribe(() => this.addOrUpdate());
        this.dynamicForm.cancel.subscribe(() => this.dialogRef.close(false));
    }

    public ngAfterViewInit(): void {
        if (!this.data.biReportEmail)
            return;
        this.dynamicForm.form.get('name').setValue(this.data.biReportEmail.name);
        this.dynamicForm.form.get('title').setValue(this.data.biReportEmail.title);
        this.dynamicForm.form.get('recipients').setValue(this.data.biReportEmail.recipients);
        this.dynamicForm.form.get('reportUrls').setValue(this.data.biReportEmail.reportUrls);
        this.dynamicForm.form.get('smtpClient').setValue(this.data.biReportEmail.smtpClient.id);
        this.dynamicForm.form = this.sigmaHelper.populateCronFromControl(this.dynamicForm.form, this.data.biReportEmail.cron);
        this.cd.detectChanges();
    }

    private addOrUpdate(): void {
        this.dynamicForm.disable();
        this.errors = new Array<string>();
        const biReportEmailJob = new BiReportEmailJob();
        biReportEmailJob.name = this.dynamicForm.form.value.name;
        biReportEmailJob.title = this.dynamicForm.form.value.title;
        biReportEmailJob.cron = `${this.dynamicForm.form.value.cron.minutes} ${this.dynamicForm.form.value.cron.hour} ${this.dynamicForm.form.value.cron.day} ${this.dynamicForm.form.value.cron.month} ${this.dynamicForm.form.value.cron.weekDay}`;
        biReportEmailJob.recipients = this.dynamicForm.form.value.recipients;
        biReportEmailJob.reportUrls = this.dynamicForm.form.value.reportUrls;
        const smtpClient = new SmtpClient();
        smtpClient.id = this.dynamicForm.form.value.smtpClient;
        biReportEmailJob.smtpClient = smtpClient;
        if (this.data.biReportEmail) {
            biReportEmailJob.id = this.data.biReportEmail.id;
            this.schedulerService.biReportEmailJobUpdate(biReportEmailJob).subscribe(next => {
                if (next.isSuccess) {
                    this.sigmaHelper.showSnackBar('Event updated');
                    this.dialogRef.close(true);
                }
                this.handleAddOrUpdate(next);
                return;
            });
        } else
            this.schedulerService.biReportEmailJobAdd(biReportEmailJob).subscribe(next => {
                if (next.isSuccess) {
                    this.sigmaHelper.showSnackBar('Event created');
                    this.dialogRef.close(true);
                }
                this.handleAddOrUpdate(next);
                return;
            });
    }

    private handleAddOrUpdate(apiResponse: ApiResponse) {
        this.errors = apiResponse.errorMessages;
        this.dynamicForm.enable();
        this.perfectScroll.scrollToTop();
    }

}