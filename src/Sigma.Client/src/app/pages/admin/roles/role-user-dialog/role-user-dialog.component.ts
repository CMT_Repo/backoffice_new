import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RolesDialogComponent } from '../role-dialog/role-dialog.component';
import { UserService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user/user';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';

@Component({
    selector: 'app-role-user-dialog',
    templateUrl: './role-user-dialog.component.html',
})
export class RoleUserDialogComponent {

    public loading: boolean = true;
    public userNames: Array<User> = new Array<User>();

    constructor(private dialogRef: MatDialogRef<RolesDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private userService: UserService, private cd: ChangeDetectorRef, private sigmaHelper: SigmaHelper) {
        // Add this class if you wish to use overhead mat-progress-bar.
        this.dialogRef.addPanelClass('dialog-progress-bar');
        userService.roleUsers(data).subscribe(roleUsers => {
            this.userNames = roleUsers;
            this.loading = false;
        });
    }

    private remove(username: string): void {
        this.sigmaHelper.dialogConfirm().subscribe(result => {
            if (result)
                this.userService.removeFromRole(username, this.data).subscribe(identityResult => {
                    if (identityResult.Succeeded) {
                        this.sigmaHelper.showSnackBar('Success, user removed form role.');
                        this.userNames = this.userNames.filter(user => user.userName !== username);
                    } else
                        this.sigmaHelper.showSnackBar(identityResult.errors.toString());
                });
        });
    }

}