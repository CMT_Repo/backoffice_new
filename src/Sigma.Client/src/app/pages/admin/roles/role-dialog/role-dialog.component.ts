import { Component, OnInit, AfterViewInit, Inject, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { TextboxQuestion } from 'src/app/shared/dynamic-form/question/question-textbox';
import { Validators } from '@angular/forms';
import { ValidatorRegexPatten } from 'src/app/helpers/validators';
import { Permission } from 'src/app/models/auth/permission';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { AutocompleteQuestion } from 'src/app/shared/dynamic-form/question/question-autoComplete';
import { Role } from 'src/app/models/roles/role';
import { RolesService } from 'src/app/services/roles.service';

@Component({
    selector: 'app-role-dialog',
    templateUrl: './role-dialog.component.html',
})
export class RolesDialogComponent implements OnInit, AfterViewInit {

    public permissions: string[] = this.sigmaHelper.intArrToEnumNames(this.data === null ? new Array<number>() : this.data.permissions, Permission);

    @ViewChild('dynamicForm', { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new TextboxQuestion(
            'name',
            'Name*',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Name Required' },
                { name: 'name', validator: Validators.pattern(ValidatorRegexPatten.plainTextNnumbersSpace), message: 'Invalid name' }
            ],
        ),
        new AutocompleteQuestion(
            'permission',
            'Permission',
            1,
            null,
            null,
            this.calculateOptions(),
        ),
    ].sort((a, b) => a.order - b.order);

    constructor(private sigmaHelper: SigmaHelper, private dialogRef: MatDialogRef<RolesDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: Role, private roleService: RolesService, private cd: ChangeDetectorRef) {
        // Add this class if you wish to use overhead mat-progress-bar.
        this.dialogRef.addPanelClass('dialog-progress-bar');
    }

    ngOnInit() {
        // Init dynamic form.
        this.dynamicForm.submitBtnFrmValid = false;
        this.dynamicForm.canceBtnShowed = false;
        this.dynamicForm.questions = this.questions;
        this.dynamicForm.submitBtnText = 'Add permission';
        this.dynamicForm.width = '20rem';
        this.dynamicForm.submit.subscribe(() => {
            this.addPermission();
        });
    }

    ngAfterViewInit(): void {
        if (this.data) {
            this.dynamicForm.form.controls['name'].setValue(this.data.name);
            this.cd.detectChanges();
        }
    }

    private calculateOptions(): string[] {
        return this.sigmaHelper.getEnumNames(Permission).filter(item => this.permissions.indexOf(item) < 0);
    }

    private updateAutoComplete(): void {
        // Update options.
        (this.questions[1] as AutocompleteQuestion).options = this.calculateOptions();
        // Clear form.
        this.dynamicForm.form.controls['permission'].setValue('');
    }

    private addPermission() {
        const permission = this.dynamicForm.form.controls['permission'].value;
        // Check if null, and if exists in permissions enum.
        if (permission && Permission[permission] != null) {
            this.permissions.push(permission);
            this.updateAutoComplete();
        }
    }

    private removePermission(pemission: string): void {
        const index = this.permissions.indexOf(pemission);
        if (index >= 0) {
            this.permissions.splice(index, 1);
            this.updateAutoComplete();
        }
    }

    public submit() {
        this.dynamicForm.form.markAllAsTouched();
        // Check if form valid.
        if (this.dynamicForm.form.valid) {
            this.dynamicForm.disable();
            const permissions: Array<Permission> = new Array<Permission>();
            for (let index = 0; index < this.permissions.length; index++)
                permissions.push(Permission[this.permissions[index]]);
            const model: Role = new Role();
            model.name = this.dynamicForm.form.controls['name'].value;
            model.permissionsString = permissions.join(',');
            if (this.data) {
                model.id = this.data.id;
                this.roleService.update(model).subscribe(reposnse => {
                    if (reposnse.Succeeded) {
                        this.sigmaHelper.showSnackBar('Success');
                        this.dialogRef.close(true);
                    } else {
                        this.sigmaHelper.showSnackBar(`${reposnse.errors[0].description}`);
                        this.dynamicForm.enable();
                    }
                });
            } else
                this.roleService.add(model).subscribe(reposnse => {
                    if (reposnse.Succeeded) {
                        this.sigmaHelper.showSnackBar('Success');
                        this.dialogRef.close(true);
                    } else {
                        this.sigmaHelper.showSnackBar(`${reposnse.errors[0].description}`);
                        this.dynamicForm.enable();
                    }
                });
        }
    }

    public cancel() {
        this.dialogRef.close();
    }
}
