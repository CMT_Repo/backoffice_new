import { Component, OnInit, ViewChild } from '@angular/core';
import { RolesService } from 'src/app/services/roles.service';
import { Role } from 'src/app/models/roles/role';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, SortDirection, Sort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { RolesDialogComponent } from './role-dialog/role-dialog.component';
import { RoleUserDialogComponent } from './role-user-dialog/role-user-dialog.component';

@Component({
    selector: 'app-roles',
    templateUrl: './roles.component.html',
})
export class RolesComponent implements OnInit {

    @ViewChild('paginator', { static: true }) paginator: MatPaginator;
    @ViewChild('rolesTable', { static: true }) rolesTable: MatTable<Role>;
    @ViewChild(MatSort, { static: true }) sortUserTable: MatSort;
    public displayedColumns: string[] = ['name', 'dateCreated', 'dateModified', 'crud'];
    public dataSource: MatTableDataSource<Role>;
    private pageSize: number = 15;
    private sortName: string = 'dateCreated';
    private sortDirection: SortDirection = 'desc';

    constructor(private service: RolesService, private sigmaHelper: SigmaHelper, private dialog: MatDialog) { }

    ngOnInit(): void {
        // Set data bindings.
        this.rolesTable.dataSource = this.dataSource;
        // Subscribe to sort paginator change.
        this.paginator.page.subscribe(event => this.updateListEvent(event));
        // Subscribe to sort change event.
        this.sortUserTable.sortChange.subscribe(sort => this.sort(sort));
        // Get initial data, by calling paginator.
        this.updateListEvent();
    }

    private sort(sort: Sort): void {
        //  if sort  direction is null and active sort ain't dateCreated, Roll to default sort, which is datecreated decs.
        if (!sort.direction && sort.active !== 'dateCreated') {
            this.sortDirection = 'desc';
            this.sortName = 'dateCreated';
        } else {
            this.sortDirection = sort.direction;
            this.sortName = sort.active;
        }
        // Call to update table using pagianator event.
        this.updateListEvent();
    }

    private updateListEvent(event?: PageEvent) {
        this.paginator.disabled = true;
        // In case being called outside of UI, use saved values.
        if (event == null) {
            event = new PageEvent();
            event.pageSize = this.pageSize;
            event.pageIndex = 0;
        }
        this.service.pagedList(event.pageIndex + 1, event.pageSize, this.sortName, this.sortDirection).subscribe(result => {
            this.dataSource = new MatTableDataSource<Role>(result.data);
            this.paginator = this.sigmaHelper.paginator(result.metaData, this.paginator);
            this.paginator.disabled = false;
        });
    }

    private create(): void {
        this.dialog.open(RolesDialogComponent).afterClosed().subscribe(result => {
            // If true, then a user been modfied, refresh list.
            if (result)
                this.updateListEvent();
        });
    }

    private update(name: string) {
        const role = this.dataSource.data.filter(s => s.name === name)[0];
        this.dialog.open(RolesDialogComponent, {
            data: role == null ? new Role() : role
        }).afterClosed().subscribe(result => {
            // If true, then a user been modfied, refresh list.
            if (result)
                this.updateListEvent();
        });
    }

    private remove(name: string): void {
        this.paginator.disabled = true;
        this.service.remove(name).subscribe(
            result => {
                this.paginator.disabled = false;
                if (result.Succeeded) {
                    this.updateListEvent();
                    this.sigmaHelper.showSnackBar('Role deleted');
                } else
                    this.sigmaHelper.showSnackBar('Error deleting role');
            }
        );
    }

    private roleUsers(roleName: string): void {
        this.dialog.open(RoleUserDialogComponent, {
            data: roleName,
            autoFocus: false,
        }).afterClosed().subscribe(result => {
            // If true, then a user been modfied, refresh list.
            if (result)
                this.updateListEvent();
        });
    }

}
