import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { SmtpClient } from 'src/app/models/smtp/smtpClient';
import { SmtpClientsDialogComponent } from './smtpClients-dialog/smtpClients-dialog.component';
import { SmtpClientService } from 'src/app/services/SmtpClients.service';

@Component({
    selector: 'app-smtp',
    templateUrl: './smtpClients.component.html',
})
export class SmtpClientsComponent implements OnInit {

    @ViewChild('paginator', { static: true }) paginator: MatPaginator;
    public data: MatTableDataSource<SmtpClient>;
    public columns: string[] = [
        'name',
        'host',
        'port',
        'isSsl',
        'dataCreated',
        'dateModified',
        'isEnabled',
        'crud',
    ];
    private pageSize: number = 15;
    private pageIndex: number = 0;

    constructor(private sigmaHelper: SigmaHelper, private dialog: MatDialog, private smtpClientService: SmtpClientService) { }

    public ngOnInit() {
        this.paginator.pageSize = this.pageSize;
        // Subscribe to sort paginator change.
        this.paginator.page.subscribe(event => this.updateBiReportEmailJobList(event));
        // Get initial data, by calling paginator.
        this.updateBiReportEmailJobList();
    }

    public addOrUpdate(id: string): void {
        if (id) {
            let smtClient;
            smtClient = this.data.data.find(x => x.id === id);
            if (!smtClient) {
                this.sigmaHelper.showSnackBar('Error accrued, cant find smtp in list..');
                return;
            }
            this.dialog.open(SmtpClientsDialogComponent, {
                data: {
                    smtClient: smtClient,
                }
            }).afterClosed().subscribe(next => {
                if (next)
                    this.updateBiReportEmailJobList();
            });
        } else
            this.dialog.open(SmtpClientsDialogComponent)
                .afterClosed().subscribe(next => {
                    if (next)
                        this.updateBiReportEmailJobList();
                });
    }

    public delete(id: string): void {
        this.paginator.disabled = true;
        this.smtpClientService.delete(id).subscribe(next => {
            if (next.isSuccess) {
                this.sigmaHelper.showSnackBar('Deleted successfully');
                this.updateBiReportEmailJobList();
                return;
            }
            this.sigmaHelper.showSnackBar(next.errorMessages[0]);
            this.paginator.disabled = false;
        });
    }

    private updateBiReportEmailJobList(event?: PageEvent): void {
        this.paginator.disabled = true;
        // In case being called outside of UI, use saved values.
        if (event == null) {
            event = new PageEvent();
            event.pageSize = this.pageSize;
            event.pageIndex = this.pageIndex;
        }
        this.pageIndex = event.pageIndex;
        this.smtpClientService.pagedList(this.pageIndex + 1, this.pageSize).subscribe(next => {
            this.data = new MatTableDataSource<SmtpClient>(next.data);
            this.paginator = this.sigmaHelper.paginator(next.metaData, this.paginator, false);
            this.paginator.disabled = false;
        });
    }

}
