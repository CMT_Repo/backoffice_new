import { Component, OnInit, AfterViewInit, ViewChild, ChangeDetectorRef, Inject } from '@angular/core';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { TextboxQuestion } from 'src/app/shared/dynamic-form/question/question-textbox';
import { Validators } from '@angular/forms';
import { ToggleQuestion } from 'src/app/shared/dynamic-form/question/question-toggle';
import { SmtpClient } from 'src/app/models/smtp/smtpClient';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { ApiResponse } from 'src/app/models/shared/api-response';
import { SmtpClientService } from 'src/app/services/SmtpClients.service';
import { ValidatorRegexPatten } from 'src/app/helpers/validators';

@Component({
    selector: 'app-smtp-dialog',
    templateUrl: './smtpClients-dialog.component.html',
})
export class SmtpClientsDialogComponent implements OnInit, AfterViewInit {

    @ViewChild(PerfectScrollbarDirective) perfectScroll: PerfectScrollbarDirective;
    @ViewChild(DynamicForm, { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new TextboxQuestion(
            'name',
            'Name*',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Name required' },
            ],
        ),
        new TextboxQuestion(
            'host',
            'Host*',
            1,
            [
                { name: 'required', validator: Validators.required, message: 'Host required' },
            ],
            undefined,
            undefined,
            undefined,
            'smtp.office365.com'
        ),
        new TextboxQuestion(
            'port',
            'Port*',
            2,
            [
                { name: 'required', validator: Validators.required, message: 'Port required' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.numbersOnly), message: 'invalid port' },
            ],
            undefined,
            undefined,
            undefined,
            '587'
        ),
        new ToggleQuestion(
            'isEnabled',
            'Is-Enabled*',
            3,
        ),
        new ToggleQuestion(
            'isSsl',
            'Is-Ssl*',
            4,
        ),
        new TextboxQuestion(
            'userName',
            'User name*',
            5,
            [
                { name: 'required', validator: Validators.required, message: 'User name required' },
            ],
            undefined,
            undefined,
            undefined,
            'crmemailrouter@cmtrading.com',
        ),
        new TextboxQuestion(
            'password',
            'Password*',
            6,
            [
                { name: 'required', validator: Validators.required, message: 'Password required' },
            ],
        ),
        new TextboxQuestion(
            'from',
            'From*',
            7,
            [
                { name: 'required', validator: Validators.required, message: 'From required' },
            ],
            undefined,
            undefined,
            undefined,
            'crmemailrouter@cmtrading.com',
        ),
    ].sort((a, b) => a.order - b.order);
    public errors: Array<string>;

    constructor(private dialogRef: MatDialogRef<SmtpClientsDialogComponent>, private sigmaHelper: SigmaHelper, private cd: ChangeDetectorRef, private smtpClientService: SmtpClientService, @Inject(MAT_DIALOG_DATA) public data: { smtClient: SmtpClient }) { }

    public ngOnInit(): void {
        // Add progress-bar.
        this.dialogRef.addPanelClass('dialog-progress-bar');
        // Set questions.
        this.dynamicForm.questions = this.questions;
        this.dynamicForm.submit.subscribe(() => this.addOrUpdate());
        this.dynamicForm.cancel.subscribe(() => this.dialogRef.close(false));
    }

    private addOrUpdate(): void {
        this.dynamicForm.disable();
        this.errors = new Array<string>();
        const smtpClient = new SmtpClient();
        smtpClient.name = this.dynamicForm.form.value.name;
        smtpClient.host = this.dynamicForm.form.value.host;
        smtpClient.port = Number(this.dynamicForm.form.value.port);
        smtpClient.isSsl = (typeof this.dynamicForm.form.value.isSsl === 'string') ? false : this.dynamicForm.form.value.isSsl;
        smtpClient.isEnabled = (typeof this.dynamicForm.form.value.isEnabled === 'string') ? false : this.dynamicForm.form.value.isEnabled;
        smtpClient.userName = this.dynamicForm.form.value.userName;
        smtpClient.password = this.dynamicForm.form.value.password;
        smtpClient.from = this.dynamicForm.form.value.from;
        if (this.data) {
            smtpClient.id = this.data.smtClient.id;
            this.smtpClientService.update(smtpClient).subscribe(next => {
                if (next.isSuccess) {
                    this.sigmaHelper.showSnackBar('SMTP client updated');
                    this.dialogRef.close(true);
                }
                this.handleAddOrUpdate(next);
                return;
            });
        } else
            this.smtpClientService.add(smtpClient).subscribe(next => {
                if (next.isSuccess) {
                    this.sigmaHelper.showSnackBar('SMTP client created');
                    this.dialogRef.close(true);
                }
                this.handleAddOrUpdate(next);
                return;
            });
    }

    public ngAfterViewInit(): void {
        if (!this.data)
            return;
        this.dynamicForm.form.get('name').setValue(this.data.smtClient.name);
        this.dynamicForm.form.get('host').setValue(this.data.smtClient.host);
        this.dynamicForm.form.get('isSsl').setValue(this.data.smtClient.isSsl);
        this.dynamicForm.form.get('port').setValue(this.data.smtClient.port);
        this.dynamicForm.form.get('isEnabled').setValue(this.data.smtClient.isEnabled);
        this.dynamicForm.form.get('userName').setValue(this.data.smtClient.userName);
        this.dynamicForm.form.get('password').setValue(this.data.smtClient.password);
        this.dynamicForm.form.get('from').setValue(this.data.smtClient.from);
        this.cd.detectChanges();
    }

    private handleAddOrUpdate(apiResponse: ApiResponse) {
        this.errors = apiResponse.errorMessages;
        this.dynamicForm.enable();
        this.perfectScroll.scrollToTop();
    }

}
