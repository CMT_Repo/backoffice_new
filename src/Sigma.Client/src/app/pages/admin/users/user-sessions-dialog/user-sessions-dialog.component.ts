import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { UserService } from 'src/app/services/users.service';
import { Session } from 'src/app/models/user/sessionModel';
import { SortDirection } from '@angular/material/sort';

@Component({
    selector: 'app-user-sessions-dialog',
    templateUrl: './user-sessions-dialog.component.html',
    styleUrls: ['./user-sessions-dialog.component.scss'],
})
export class UserSessionsDialogComponent implements OnInit {

    @ViewChild('paginator', { static: true }) paginator: MatPaginator;
    @ViewChild('sessionTable', { static: true }) sessionTable: MatTable<Session>;
    @ViewChild(MatSort, { static: true }) sortUserTable: MatSort;
    public displayedColumns: string[] = ['userAgent', 'ipAddress', 'dateCreated', 'dateLastUsed', 'signature', 'crud'];
    public dataSource: MatTableDataSource<Session>;
    private sortDirection: SortDirection = 'desc';
    private sortName: string = 'dateCreated';
    private pageSize: number = 5;
    private thisPageIndex = 0;

    constructor(private dialogRef: MatDialogRef<UserSessionsDialogComponent>, private userService: UserService, private simgaHelper: SigmaHelper, private sigmaHelper: SigmaHelper, @Inject(MAT_DIALOG_DATA) public data: any) {
        this.dialogRef.addPanelClass('dialog-progress-bar');
    }

    ngOnInit() {
        // Subscribe to sort paginator change.
        this.paginator.page.subscribe(event => this.updateListEvent(event));
        // Set datasource bindings.
        this.sessionTable.dataSource = this.dataSource;
        // Subscribe to sort change event.
        this.sortUserTable.sortChange.subscribe(sort => this.sort(sort));
        // Get initial data, by calling paginator.
        this.updateListEvent();
    }

    private sort(sort: Sort): void {
        //  if sort  direction is null and active sort ain't dateCreated, Roll to default sort, which is datecreated decs.
        if (!sort.direction && sort.active !== 'dateCreated') {
            this.sortDirection = 'desc';
            this.sortName = 'dateCreated';
        } else {
            this.sortDirection = sort.direction;
            this.sortName = sort.active;
        }
        // Call to update table using pagianator event.
        this.updateListEvent();
    }

    private updateListEvent(event?: PageEvent): void {
        this.paginator.disabled = true;
        // In case being called outside of UI, use saved values.
        if (event == null) {
            event = new PageEvent();
            event.pageSize = this.pageSize;
            event.pageIndex = this.thisPageIndex;
        }
        this.thisPageIndex = event.pageIndex;
        this.userService.sessionsPagedList(this.data.username, event.pageIndex + 1, event.pageSize, this.sortName, this.sortDirection).subscribe(
            result => {
                this.dataSource = new MatTableDataSource<Session>(Array.from(result.data));
                this.paginator = this.simgaHelper.paginator(result.metaData, this.paginator);
                this.sortUserTable.direction = this.sortDirection;
                this.paginator.disabled = false;
            }
        );
    }

    private remove(signature: string): void {
        this.paginator.disabled = true;
        this.userService.sessionRemove(signature).subscribe(response => {
            this.paginator.disabled = false;
            if (response) {
                this.updateListEvent();
                this.simgaHelper.showSnackBar('Session removed.');
            } else
                this.simgaHelper.showSnackBar('Somethign went wrong!');
        });
    }

}
