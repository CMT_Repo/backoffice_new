import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SeoRedirectsDialogComponent } from 'src/app/pages/marketing/seo-redirects/dialogs/seo-redirects-dialog/seo-redirects-dialog.component';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { AutocompleteQuestion } from 'src/app/shared/dynamic-form/question/question-autoComplete';
import { UserService } from 'src/app/services/users.service';

@Component({
    selector: 'app-user-roles-dialog',
    templateUrl: './user-roles-dialog.component.html',
    styleUrls: ['./user-roles-dialog.component.scss']
})
export class UserRolesDialogComponent implements OnInit {

    @ViewChild('dynamicForm', { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new AutocompleteQuestion(
            'roles',
            'Roles',
            0,
            null,
            null,
            [],
        ),
    ].sort((a, b) => a.order - b.order);
    private initialUserRoles: Array<string> = Array.from(this.data.userRoles);
    public userRoles: Array<string> = Array.from(this.data.userRoles);

    constructor(private dialogRef: MatDialogRef<SeoRedirectsDialogComponent>, private sigmaHelper: SigmaHelper, @Inject(MAT_DIALOG_DATA) private data: { userRoles: Array<string>, username: string, roles: Array<string> }, private userService: UserService) {
        // Add this class if you wish to use overhead mat-progress-bar.
        this.dialogRef.addPanelClass('dialog-progress-bar');
    }

    ngOnInit() {
        // Init dynamic form.
        this.dynamicForm.submitBtnFrmValid = false;
        this.dynamicForm.canceBtnShowed = false;
        this.dynamicForm.questions = this.questions;
        this.dynamicForm.submitBtnText = 'Add role';
        this.dynamicForm.width = '20rem';
        this.dynamicForm.submit.subscribe(() => {
            this.addRole();
        });
        (this.questions[0] as AutocompleteQuestion).options = this.calculateOptions();
    }

    private calculateOptions(): Array<string> {
        return this.data.roles.filter(item => this.userRoles.indexOf(item) < 0);
    }

    private updateAutoComplete(): void {
        // Update options.
        (this.questions[0] as AutocompleteQuestion).options = this.calculateOptions();
        // Clear form.
        this.dynamicForm.form.controls['roles'].setValue('');
    }

    private addRole() {
        const role = this.dynamicForm.form.controls['roles'].value;
        // Check if null, and if exists in permissions enum.
        if (role && this.data.roles.includes(role)) {
            this.userRoles.push(role);
            this.updateAutoComplete();
        }
    }

    private removeRole(role: string): void {
        const index = this.userRoles.findIndex(r => r === role);
        if (index >= 0) {
            this.userRoles.splice(index, 1);
            this.updateAutoComplete();
        }
    }

    public submit() {
        this.dynamicForm.disable();
        const rolesToAdd = this.userRoles.filter(role => !this.initialUserRoles.includes(role));
        const rolesToRemove = this.initialUserRoles.filter(role => !this.userRoles.includes(role));
        this.userService.updateRoles(this.data.username, rolesToRemove, rolesToAdd).subscribe(reposnse => {
            if (reposnse.Succeeded) {
                this.sigmaHelper.showSnackBar('Success');
                this.dialogRef.close(true);
            } else {
                this.sigmaHelper.showSnackBar(`${reposnse.errors[0].description}`);
                this.dynamicForm.enable();
            }
        });
    }

    public cancel() {
        this.dialogRef.close();
    }

}