import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from 'src/app/services/users.service';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { AutocompleteQuestion } from 'src/app/shared/dynamic-form/question/question-autoComplete';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { Permission } from 'src/app/models/auth/permission';
import { PermissionUpdateModel } from 'src/app/models/user/permissionUpdateModel';

@Component({
    selector: 'app-user-permission-dialog',
    templateUrl: './user-permission-dialog.component.html',
})
export class UserPermissionDialogComponent implements OnInit {

    public permissions: string[] = this.sigmaHelper.intArrToEnumNames(this.data.permissions, Permission);

    @ViewChild('dynamicForm', { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new AutocompleteQuestion(
            'permission',
            'Permission',
            0,
            null,
            null,
            this.calculateOptions(),
        ),
    ].sort((a, b) => a.order - b.order);

    constructor(private dialogRef: MatDialogRef<UserPermissionDialogComponent>, private userService: UserService, private sigmaHelper: SigmaHelper, @Inject(MAT_DIALOG_DATA) public data: any) {
        // Add this class if you wish to use overhead mat-progress-bar.
        this.dialogRef.addPanelClass('dialog-progress-bar');
    }

    ngOnInit() {
        // Init dynamic form.
        this.dynamicForm.width = '20rem';
        this.dynamicForm.canceBtnShowed = false;
        this.dynamicForm.questions = this.questions;
        this.dynamicForm.submitBtnText = 'Add permission';
        this.dynamicForm.submit.subscribe(() => {
            this.addPermission();
        });
    }

    private addPermission() {
        const permission = this.dynamicForm.form.controls['permission'].value;
        // Check if null, and if exists in permissions enum.
        if (permission && Permission[permission] != null) {
            this.permissions.push(permission);
            this.updateAutoComplete();
        }
    }

    private removePermission(pemission: string): void {
        const index = this.permissions.indexOf(pemission);
        if (index >= 0) {
            this.permissions.splice(index, 1);
            this.updateAutoComplete();
        }
    }

    private updateAutoComplete(): void {
        // Update options.
        (this.questions[0] as AutocompleteQuestion).options = this.calculateOptions();
        // Clear form.
        this.dynamicForm.form.controls['permission'].setValue('');
    }

    private calculateOptions(): string[] {
        return this.sigmaHelper.getEnumNames(Permission).filter(item => this.permissions.indexOf(item) < 0);
    }

    public submit() {
        this.dynamicForm.disable();
        const permissions: Array<Permission> = new Array<Permission>();
        for (let index = 0; index < this.permissions.length; index++)
            permissions.push(Permission[this.permissions[index]]);
        const model = new PermissionUpdateModel();
        model.Username = this.data.username;
        model.Permissions = permissions;
        this.userService.updatePermission(model).subscribe(reposnse => {
            if (reposnse.Succeeded) {
                this.sigmaHelper.showSnackBar('Permissions updated');
                this.dialogRef.close();
            } else {
                this.sigmaHelper.showSnackBar(`Something went wront: ${reposnse.errors[0].description}`);
                this.dynamicForm.enable();
            }
        });
    }

    public cancel() {
        this.dialogRef.close();
    }

}
