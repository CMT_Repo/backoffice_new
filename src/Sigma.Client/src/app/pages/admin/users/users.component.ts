import { Component, ViewChild, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/users.service';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, SortDirection, Sort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { User } from 'src/app/models/user/user';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';
import { UserDialogComponent } from 'src/app/pages/admin/users/user-dialog/user-dialog.component';
import { UserPermissionDialogComponent } from 'src/app/pages/admin/users/user-permission-dialog/user-permission-dialog.component';
import { UserSessionsDialogComponent } from 'src/app/pages/admin/users/user-sessions-dialog/user-sessions-dialog.component';
import { UserRolesDialogComponent } from './user-roles-dialog/user-roles-dialog.component';
import { RolesService } from 'src/app/services/roles.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
})

export class UsersComponent implements OnInit {

    @ViewChild('paginator', { static: true }) paginator: MatPaginator;
    @ViewChild('userTable', { static: true }) userTable: MatTable<User>;
    @ViewChild(MatSort, { static: true }) sortUserTable: MatSort;
    public displayedColumns: string[] = ['userName', 'email', 'firstName', 'lastName', 'dateCreated', 'crud'];
    public dataSource: MatTableDataSource<User>;
    private pageSize: number = 15;
    private sortName: string = 'dateCreated';
    private sortDirection: SortDirection = 'desc';

    constructor(private userService: UserService, private roleService: RolesService, private sigmaHelper: SigmaHelper, private dialog: MatDialog) { }

    ngOnInit(): void {
        // Set data bindings.
        this.userTable.dataSource = this.dataSource;
        // Subscribe to sort paginator change.
        this.paginator.page.subscribe(event => this.updateListEvent(event));
        // Subscribe to sort change event.
        this.sortUserTable.sortChange.subscribe(sort => this.sort(sort));
        // Get initial data, by calling paginator.
        this.updateListEvent();
    }

    private createUser() {
        this.dialog.open(UserDialogComponent).afterClosed().subscribe(result => {
            // If true, then a user been added, refresh list.
            if (result)
                this.updateListEvent();
        });
    }

    private updateListEvent(event?: PageEvent) {
        this.paginator.disabled = true;
        // In case being called outside of UI, use saved values.
        if (event == null) {
            event = new PageEvent();
            event.pageSize = this.pageSize;
            event.pageIndex = 0;
        }
        this.userService.pagedList(event.pageIndex + 1, event.pageSize, this.sortName, this.sortDirection).subscribe(
            result => {
                this.dataSource = new MatTableDataSource<User>(result.data);
                this.paginator = this.sigmaHelper.paginator(result.metaData, this.paginator);
                this.sortUserTable.direction = this.sortDirection;
                this.paginator.disabled = false;
            }
        );
    }

    private sort(sort: Sort) {
        //  if sort  direction is null and active sort ain't dateCreated, Roll to default sort, which is datecreated decs.
        if (!sort.direction && sort.active !== 'dateCreated') {
            this.sortDirection = 'desc';
            this.sortName = 'dateCreated';
        } else {
            this.sortDirection = sort.direction;
            this.sortName = sort.active;
        }
        // Call to update table using pagianator event.
        this.updateListEvent();
    }

    private modify(username: string) {
        this.dialog.open(UserDialogComponent, {
            data: this.dataSource.data.filter(foo => foo.userName === username)[0]
        }).afterClosed().subscribe(result => {
            // If true, then a user been modfied, refresh list.
            if (result)
                this.updateListEvent();
        });
    }

    private delete(username: string) {
        this.paginator.disabled = true;
        this.userService.delete(username).subscribe(
            result => {
                this.paginator.disabled = false;
                if (result.Succeeded) {
                    this.updateListEvent();
                    this.sigmaHelper.showSnackBar('User deleted');
                } else
                    this.sigmaHelper.showSnackBar('Error deleting user');
            }
        );
    }

    private permission(username: string) {
        this.userService.claims(username).subscribe(permissions => {
            this.dialog.open(UserPermissionDialogComponent, {
                data: {
                    username,
                    permissions,
                },
            }).afterClosed().subscribe(result => {
                // If true, then a user been added, refresh list.
                if (result)
                    this.updateListEvent();
            });
        });
    }

    private session(username: string) {
        this.dialog.open(UserSessionsDialogComponent, {
            data: {
                username,
            },
            autoFocus: false,
        });
    }

    private roles(username: string) {
        this.paginator.disabled = true;
        this.userService.userRolesNames(username).subscribe(
            userRoles => {
                this.roleService.roles().subscribe(roles => {
                    this.paginator.disabled = false;
                    this.dialog.open(UserRolesDialogComponent, {
                        data: {
                            userRoles: userRoles,
                            username: username,
                            roles: roles.map(role => role.name)
                        },
                    }).afterClosed().subscribe(result => {
                        // If true, then a user been added, refresh list.
                        if (result)
                            this.updateListEvent();
                    });
                });
            }
        );
    }

}