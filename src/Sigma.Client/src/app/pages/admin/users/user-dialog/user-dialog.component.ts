import { Component, OnInit, AfterViewInit, Inject, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Validators } from '@angular/forms';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { TextboxQuestion } from 'src/app/shared/dynamic-form/question/question-textbox';
import { DynamicForm } from 'src/app/shared/dynamic-form/dynamic-form/dynamic-form.component';
import { PasswordQuestion } from 'src/app/shared/dynamic-form/question/question-password';
import { ValidatorRegexPatten } from 'src/app/helpers/validators';
import { UserService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user/user';
import { IdentityResult } from 'src/app/models/user/identityResultModel';
import { Operation } from 'src/app/models/shared/enums';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';

@Component({
    selector: 'app-user-create-dialog',
    templateUrl: './user-dialog.component.html',
})
export class UserDialogComponent implements OnInit, AfterViewInit {

    @ViewChild('dynamicForm', { static: true }) dynamicForm: DynamicForm;
    private questions: QuestionBase[] = [
        new TextboxQuestion(
            'email',
            'Email*',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Name Required' },
                { name: 'email', validator: Validators.email, message: 'Invalid email' }
            ],
        ),
        new TextboxQuestion(
            'username',
            'Username*',
            1,
            [
                { name: 'required', validator: Validators.required, message: 'Username required' },
                { name: 'email', validator: Validators.email, message: 'Invalid username' }
            ],
        ),
        new TextboxQuestion(
            'firstName',
            'First name*',
            2,
            [
                { name: 'required', validator: Validators.required, message: 'Name Required' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.plainText), message: 'Text only' },
            ],
        ),
        new TextboxQuestion(
            'lastName',
            'Last name*',
            3,
            [
                { name: 'required', validator: Validators.required, message: 'Name Required' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.plainText), message: 'Text only' },
            ],
        ),
        new PasswordQuestion(
            'password',
            'Password*',
            4,
            [
                !this.data && { name: 'required', validator: Validators.required, message: 'Password Required' },
                { name: 'pattern', validator: Validators.pattern(ValidatorRegexPatten.password), message: 'Invalid password' },
            ],
            null,
            false
        ),
    ].sort((a, b) => a.order - b.order);

    constructor(private dialogRef: MatDialogRef<UserDialogComponent>, private userService: UserService, private sigmaHelper: SigmaHelper, @Inject(MAT_DIALOG_DATA) public data: User, private cd: ChangeDetectorRef) {
        // Add this class if you wish to use overhead mat-progress-bar.
        this.dialogRef.addPanelClass('dialog-progress-bar');
    }

    ngOnInit() {
        // Set questions.
        this.dynamicForm.questions = this.questions;
        this.dynamicForm.width = '20rem';
    }

    ngAfterViewInit(): void {
        // Duplicate username to email contol.
        this.dynamicForm.form.controls['email'].valueChanges.subscribe(input => {
            this.dynamicForm.form.controls['username'].setValue(input);
        });
        // Subscribe to submit event.
        this.dynamicForm.submit.subscribe(() => this.addOrUpdate());
        // Subscribe to cancel event.
        this.dynamicForm.cancel.subscribe(() => {
            this.dialogRef.close();
        });
        // If data is present, render it inside form.
        if (this.data) {
            this.dynamicForm.form.controls['email'].setValue(this.data.email);
            this.dynamicForm.form.controls['username'].setValue(this.data.userName);
            this.dynamicForm.form.controls['firstName'].setValue(this.data.firstName);
            this.dynamicForm.form.controls['lastName'].setValue(this.data.lastName);
            this.cd.detectChanges();
        }
    }

    private addOrUpdate() {
        this.dynamicForm.disable();
        const model = new User();
        model.email = this.dynamicForm.form.controls['email'].value;
        model.userName = this.dynamicForm.form.controls['username'].value;
        model.firstName = this.dynamicForm.form.controls['firstName'].value;
        model.lastName = this.dynamicForm.form.controls['lastName'].value;
        model.password = this.dynamicForm.form.controls['password'].value;
        model.id = this.data && this.data.id;
        // Edit suer.
        if (this.data)
            this.userService.update(model).subscribe(response => {
                this.invokeOperation(Operation.Update, response);
            });
        // Add user.
        else
            this.userService.add(model).subscribe(response => {
                this.invokeOperation(Operation.Add, response);
            });
    }

    private invokeOperation(operation: Operation, response: IdentityResult): void {
        if (response.Succeeded)
            if (operation === Operation.Add) {
                this.sigmaHelper.showSnackBar('User added');
                this.dialogRef.close(true);
            } else {
                this.sigmaHelper.showSnackBar('User updated');
                this.dialogRef.close(true);
            }
        else {
            this.dynamicForm.enable();
            this.dynamicForm.errors = [];
            for (let i = 0; i < response.errors.length; i++)
                this.dynamicForm.errors.push(response.errors[i].description);
        }
    }

}