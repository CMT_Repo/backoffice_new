import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { AuthService } from 'src/app/services/auth.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-error',
    templateUrl: './error.component.html'
})

export class ErrorComponent {
    public status: number;
    public errorMessage: string;
    public settings: Settings;
    private isAuth: boolean;
    constructor(public appSettings: AppSettings, public router: Router, private auth: AuthService, private activatedRoute: ActivatedRoute, private dialogRef: MatDialog) {
        // Close all dialogs.
        this.dialogRef.closeAll();
        this.settings = this.appSettings.settings;
        if (this.router.getCurrentNavigation().extras) {
            const state = (this.router.getCurrentNavigation()).extras.state;
            if (state) {
                this.status = state.status;
                this.errorMessage = state.errorMessage;
            }
        }
    }

    goHome(): void {
        this.router.navigate(['/']);
    }

    ngAfterViewInit() {
        this.settings.loadingSpinner = false;
    }

}