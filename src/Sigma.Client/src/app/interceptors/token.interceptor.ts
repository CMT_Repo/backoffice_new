import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private auth: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Set accepted MIME types.
        request = request.clone({ setHeaders: { Accept: '*/*', } });
        // Add Authorization headers request.
        request = this.auth.setAuthHeaders(request);
        return next.handle(request);
    }

}