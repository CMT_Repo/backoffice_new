import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

// TODO: Add logger.

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private router: Router, private auth: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401)
                return this.auth.handleUnauthorized(request, next);
            // On failed kikc out, prompt to login.
            if (request.url.includes('logout'))
                this.router.navigate(['/login']);
            this.router.navigate(['/error'], { state: { status: err.status, errorMessage: err.statusText } });
            if (err.error)
                return throwError(err.error.message || err.statusText);
            return throwError(err.message);
        }));
    }
}