import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { PagedList } from '../models/shared/pagedList';
import { SeoRedirect } from '../models/seo-redirects/seo-redirect';
import { Website } from '../models/seo-redirects/website';

@Injectable({ providedIn: 'root' })
export class SeoRedirectsService {

    constructor(private http: HttpClient, private auth: AuthService) { }

    public pagedList(pageNumber: number, pageSize: number): Observable<PagedList<SeoRedirect>> {
        let params = new HttpParams();
        params = params.append('pageNumber', pageNumber.toString());
        params = params.append('pageSize', pageSize.toString());
        return this.http.get<any>('seoRedirect/pagedList', { params: params })
            .pipe(
                map(response => {
                    for (const iterator of response.data) {
                        // Update dates.
                        iterator.dateCreated = new Date(iterator.dateCreated.seconds * 1000);
                        iterator.dateModified = iterator.dateModified.seconds === 0 ? null : new Date(iterator.dateModified.seconds * 1000);
                    }
                    return new PagedList<SeoRedirect>(response);
                })
            );
    }

    public websites(): Observable<Array<Website>> {
        return this.http.get<any>('seoRedirect/websites').pipe(map(response => {
            return response.websites;
        }));
    }

    public add(model: SeoRedirect): Observable<boolean> {
        return this.http.post('seoRedirect/add', model).pipe(map(() => true));
    }

    public modify(model: SeoRedirect): Observable<boolean> {
        return this.http.post('seoRedirect/modify', model).pipe(map(() => true));
    }

    public remove(id: number): Observable<boolean> {
        let params = new HttpParams();
        params = params.append('id', id.toString());
        return this.http.get<any>('seoRedirect/remove', { params: params });
    }

}
