import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PagedList } from '../models/shared/pagedList';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BiReportEmailJob } from '../models/scheduler/biReportEmailJob';
import { ApiResponse } from '../models/shared/api-response';

@Injectable({
    providedIn: 'root'
})
export class SchedulerService {

    constructor(private http: HttpClient) { }

    public biReportEmailJobAdd(model: BiReportEmailJob): Observable<ApiResponse> {
        return this.http.post('Scheduler/BiReportEmailJobAdd', model).pipe(map(response => new ApiResponse(response)));
    }

    public biReportEmailJobUpdate(model: BiReportEmailJob): Observable<ApiResponse> {
        return this.http.patch('Scheduler/BiReportEmailJobUpdate', model).pipe(map(response => new ApiResponse(response)));
    }

    public biReportEmailJobDelete(id: string): Observable<ApiResponse> {
        const params = new HttpParams().append('id', id);
        return this.http.delete('Scheduler/BiReportEmailJobDelete', { params: params }).pipe(map(response => new ApiResponse(response)));
    }

    public biReportEmailJobExecute(id: string): Observable<ApiResponse> {
        const params = new HttpParams().append('id', id);
        return this.http.get('Scheduler/BiReportEmailJobExecute', { params: params }).pipe(map(response => new ApiResponse(response)));
    }

    public biReportEmailJobPagedList(pageNumber: number, pageSize: number): Observable<PagedList<BiReportEmailJob>> {
        let params = new HttpParams();
        params = params.append('pageNumber', pageNumber.toString());
        params = params.append('pageSize', pageSize.toString());
        return this.http.get<any>('Scheduler/BiReportEmailJobPagedList', { params: params })
            .pipe(
                map(response => new PagedList<BiReportEmailJob>(response))
            );
    }
}
