import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { CronQuestion } from '../shared/dynamic-form/question/question-cron';

@Injectable({ providedIn: 'root' })
export class QuestionControlService {
    constructor() { }

    toFormGroup(questions: QuestionBase[]) {
        const group: any = {};
        questions.forEach(question => {
            if (question.controlType === 'cron') {
                group[question.key] = new FormGroup({});
                for (const subQuestion of (question as CronQuestion).questions)
                    (group[question.key] as FormGroup).addControl(subQuestion.key, new FormControl('*', this.bindValidations(subQuestion.validations)));
            } else
                group[question.key] = question.validations ? new FormControl('', this.bindValidations(question.validations)) : new FormControl('');
        });
        return new FormGroup(group);
    }

    private bindValidations(validations: any) {
        if (validations.length > 0) {
            const validList = [];
            validations.forEach(valid => {
                validList.push(valid.validator);
            });
            return Validators.compose(validList);
        }
        return null;
    }
}