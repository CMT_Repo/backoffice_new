import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { DepositAttempt } from '../models/accounting/depositAttempt';
import { PagedList } from '../models/shared/pagedList';
import { Observable } from 'rxjs';
import { DepositAttemptPspResult } from '../models/accounting/depositAttempResult';
import { DepositLogFilter } from '../models/accounting/depositLogFilter';
import 'rxjs/add/observable/empty';

@Injectable({
    providedIn: 'root'
})
export class DepositLogService {

    constructor(private http: HttpClient) { }

    public depositAttemptPagedList(pageNumber: number, pageSize: number, filter?: DepositLogFilter): Observable<PagedList<DepositAttempt>> {
        let params = new HttpParams();
        params = params.append('pageNumber', pageNumber.toString());
        params = params.append('pageSize', pageSize.toString());
        if (filter) {
            params = params.append('id', filter.id);
            params = params.append('mt4Account', filter.mt4Account);
            params = params.append('mt4Order', filter.mt4Order);
            params = params.append('amount', filter.amount);
            params = params.append('firstName', filter.firstName);
            params = params.append('lastName', filter.lastName);
            params = params.append('email', filter.email);
            params = params.append('crmTransactionId', filter.crmTransactionId);
            params = params.append('currencies', Array.prototype.join.call(filter.currencies, ','));
            params = params.append('paymentProviders', Array.prototype.join.call(filter.paymentProviders, ','));
            params = params.append('creditCardAssociations', Array.prototype.join.call(filter.creditCardAssociations, ','));
            params = params.append('countryIsos', Array.prototype.join.call(filter.countryISOs, ','));
            params = params.append('isSuccessfulOnly', filter.isSuccessfulOnly.toString());
            return this.http.get<any>('depositLog/Filter', { params: params }).pipe(map(response => this.handlePagedListConversion(response)));
        }
        return this.http.get<any>('depositLog/PagedList', { params: params }).pipe(map(response => this.handlePagedListConversion(response)));
    }

    public depositAttempPspResults(depositAttemptId: number): Observable<Array<DepositAttemptPspResult>> {
        let params = new HttpParams();
        params = params.append('depositAttemptId', depositAttemptId.toString());
        return this.http.get<any>('depositLog/DepositAttemptPspResults', { params: params })
            .pipe(
                map(response => {
                    for (const iterator of response.depositResults)
                        // Update dates.
                        iterator.dateCreated = new Date(iterator.dateCreated.seconds * 1000);
                    return response.depositResults;
                })
            );
    }

    public metadata(): Observable<any> {
        return this.http.get<any>('depositLog/MetaData')
            .pipe(
                map(response => {
                    return response;
                })
            );
    }

    public dodUpdate(id: number, value: boolean): Observable<any> {
        const formData = new FormData();
        formData.append('id', id.toString());
        formData.append('value', value.toString());
        return this.http.post('depositLog/DodUpdate', formData).pipe(map(() => Observable.empty()));
    }

    private handlePagedListConversion(rawPagedList: any) {
        for (const iterator of rawPagedList.data) {
            // Update dates.
            iterator.dateCreated = new Date(iterator.dateCreated.seconds * 1000);
            iterator.dateModified = iterator.dateModified.seconds === 0 ? null : new Date(iterator.dateModified.seconds * 1000);
            iterator.lastFourDigits = iterator.last4Digits;
        }
        return new PagedList<DepositAttempt>(rawPagedList);
    }
}

