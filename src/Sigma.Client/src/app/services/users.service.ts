import { Injectable } from '@angular/core';
import { SortDirection } from '@angular/material/sort';
import { User } from '../models/user/user';
import { Permission } from '../models/auth/permission';
import { HttpParams, HttpClient } from '@angular/common/http';
import { PagedList } from '../models/shared/pagedList';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { IdentityResult } from '../models/user/identityResultModel';
import { PermissionUpdateModel } from '../models/user/permissionUpdateModel';
import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';
import { Session } from '../models/user/sessionModel';
import { UserUpdateRolesModel } from '../models/user/updateRolesModel';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient, private auth: AuthService) { }

    public pagedList(pageNumber: number, pageSize: number, sort: string, direction: SortDirection): Observable<PagedList<User>> {
        let params = new HttpParams();
        params = params.append('pageNumber', pageNumber.toString());
        params = params.append('pageSize', pageSize.toString());
        params = params.append('sort', sort);
        params = params.append('sortDirection', direction);
        return this.http.get('users/pagedList', { params: params }).pipe(map(response => new PagedList<User>(response)));
    }

    public add(model: User): Observable<IdentityResult> {
        return this.http.post('users/add', model)
            .pipe(
                map(response => {
                    return new IdentityResult(response);
                })
            );
    }

    public update(model: User): Observable<IdentityResult> {
        return this.http.post('users/update', model)
            .pipe(
                map(response => {
                    return new IdentityResult(response);
                })
            );
    }

    public delete(userName: string): Observable<IdentityResult> {
        const params = new HttpParams().append('userName', userName);
        return this.http.delete('users/Delete', { params: params })
            .pipe(
                map(response => {
                    return new IdentityResult(response);
                })
            );
    }

    public updatePermission(model: PermissionUpdateModel): Observable<IdentityResult> {
        return this.http.post('users/updatePermission', model)
            .pipe(
                map(response => {
                    return new IdentityResult(response);
                })
            );
    }

    public sessionsPagedList(userName: string, pageNumber: number, pageSize: number, sort: string, direction: SortDirection): Observable<PagedList<Session>> {
        let params = new HttpParams();
        params = params.append('userName', userName);
        params = params.append('pageNumber', pageNumber.toString());
        params = params.append('pageSize', pageSize.toString());
        params = params.append('sort', sort);
        params = params.append('sortDirection', direction);
        return this.http.get('users/sessionsPagedList', { params: params })
            .pipe(
                map(response => {
                    return new PagedList<Session>(response);
                })
            );
    }

    public claims(userName: string): Observable<Array<number>> {
        const params = new HttpParams().append('userName', userName);
        return this.http.get('users/claimPermission', { params: params })
            .pipe(
                map(response => {
                    return <number[]>response;
                })
            );
    }

    public sessionRemove(signature: string): Observable<boolean> {
        const params = new HttpParams().append('signature', signature);
        return this.http.delete('users/sessionsRemove', { params: params })
            .pipe(
                map((response: boolean) => {
                    return response;
                }),
                catchError(error => {
                    // TODO: Log error befor returning it.
                    return throwError(error);
                })
            );
    }

    public user(): User {
        const claims = this.auth.getClaims();
        if (!claims)
            return null;
        const user = new User();
        user.id = claims.find(claim => claim.Key === 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier').Value;
        user.firstName = claims.find(claim => claim.Key === 'FirstName').Value;
        user.lastName = claims.find(claim => claim.Key === 'LastName').Value;
        user.userName = claims.find(claim => claim.Key === 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name').Value;
        user.email = claims.find(claim => claim.Key === 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name').Value;
        claims.find(claim => claim.Key === environment.constStringPermission).Value.trim().split(',').forEach(element => {
            user.permissions.push(Permission[element]);
        });
        return user;
    }

    public userRolesNames(userName: string): Observable<Array<string>> {
        const params = new HttpParams().append('userName', userName);
        return this.http.get('users/getUsersRoles', { params: params })
            .pipe(
                map(response => {
                    return <string[]>response;
                })
            );
    }

    public updateRoles(username: string, removeRoles: Array<string>, addRoles: Array<string>): Observable<IdentityResult> {
        const model = new UserUpdateRolesModel();
        model.username = username;
        model.removeRoles = removeRoles;
        model.addRoles = addRoles;
        return this.http.post('users/updateRoles', model)
            .pipe(
                map(response => {
                    return new IdentityResult(response);
                })
            );
    }

    public roleUsers(roleName: string): Observable<Array<User>> {
        const params = new HttpParams().append('roleName', roleName);
        return this.http.get('users/roleUsers', { params: params })
            .pipe(
                map(response => {
                    return <Array<User>>response;
                })
            );
    }

    public removeFromRole(username: string, roleName: string): Observable<IdentityResult> {
        let params = new HttpParams();
        params = params.append('username', username);
        params = params.append('roleName', roleName);
        return this.http.delete('users/removeRoleFromUser', { params: params })
            .pipe(
                map(response => {
                    return new IdentityResult(response);
                })
            );
    }

}