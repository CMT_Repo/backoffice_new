import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PagedList } from '../models/shared/pagedList';
import { Role } from '../models/roles/role';
import { HttpParams, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { IdentityResult } from '../models/user/identityResultModel';
import { SortDirection } from '@angular/material/sort';

@Injectable({
    providedIn: 'root'
})
export class RolesService {

    constructor(private http: HttpClient, private auth: AuthService) { }

    public pagedList(pageNumber: number, pageSize: number, sort: string, direction: SortDirection): Observable<PagedList<Role>> {
        let params = new HttpParams();
        params = params.append('pageNumber', pageNumber.toString());
        params = params.append('pageSize', pageSize.toString());
        params = params.append('sortDirection', direction);
        params = params.append('sort', sort);
        return this.http.get('roles/pagedList', { params: params }).pipe(map(response => new PagedList<Role>(response)));
    }

    public add(model: Role): Observable<IdentityResult> {
        return this.http.post('roles/add', model)
            .pipe(
                map(response => {
                    return new IdentityResult(response);
                })
            );
    }

    public update(model: Role): Observable<IdentityResult> {
        return this.http.post('roles/update', model)
            .pipe(
                map(response => {
                    return new IdentityResult(response);
                })
            );
    }

    public remove(roleName: string): Observable<IdentityResult> {
        const params = new HttpParams().append('roleName', roleName);
        return this.http.delete('roles/remove', { params: params })
            .pipe(
                map(response => {
                    return new IdentityResult(response);
                })
            );
    }

    public roles(): Observable<Array<Role>> {
        return this.http.get('roles/roles')
            .pipe(
                map(response => {
                    return <Role[]>response;
                })
            );
    }
}
