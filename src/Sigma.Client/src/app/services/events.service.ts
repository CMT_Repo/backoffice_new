import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PagedList } from '../models/shared/pagedList';
import { EventCmt } from '../models/event/event-cmt';
import { map } from 'rxjs/operators';
import { EventCmtRegister } from '../models/event/event-cmt-register';
import { GrpcResponse } from '../models/shared/grpc-response';

@Injectable({
    providedIn: 'root'
})
export class EventsService {

    constructor(private http: HttpClient) { }

    public eventCmtpagedList(pageNumber: number, pageSize: number): Observable<PagedList<EventCmt>> {
        let params = new HttpParams();
        params = params.append('pageNumber', pageNumber.toString());
        params = params.append('pageSize', pageSize.toString());
        return this.http.get<any>('eventsCmt/eventsCmtPagedList', { params: params })
            .pipe(
                map(response => {
                    for (const iterator of response.data) {
                        // Update dates.
                        iterator.dateCreated = new Date(iterator.dateCreated.seconds * 1000);
                        iterator.dateModified = iterator.dateModified.seconds === 0 ? null : new Date(iterator.dateModified.seconds * 1000);
                        // Fix naming convention.
                        iterator.emailRejectedSubject = iterator.emailRejectSubject;
                        iterator.emailRejectedBody = iterator.emailRejectBody;
                    }
                    return new PagedList<EventCmt>(response);
                })
            );
    }

    public eventCmtAdd(model: EventCmt): Observable<GrpcResponse> {
        return this.http.post('eventsCmt/EventCmtAdd', model).pipe(map(response => new GrpcResponse(response)));
    }

    public eventCmtUpdate(model: EventCmt): Observable<GrpcResponse> {
        return this.http.post('eventsCmt/EventCmtUpdate', model).pipe(map(response => new GrpcResponse(response)));
    }

    public eventCmtDelete(id: string): Observable<boolean> {
        const params = new HttpParams().append('id', id);
        return this.http.delete('eventsCmt/EventsCmtDelete', { params: params })
            .pipe(
                map((response: boolean) => {
                    return response;
                })
            );
    }

    public eventCmtSmtpServerNames(): Observable<Array<string>> {
        return this.http.get<any>('eventsCmt/eventCmtSmtpServerNames')
            .pipe(
                map(response => {
                    return response.map(smtpServerName => smtpServerName);
                })
            );
    }

    public eventCmtNames(): Observable<Array<string>> {
        return this.http.get<any>('eventsCmt/EventsCmtNames')
            .pipe(
                map(response => {
                    return response.map(eventName => eventName);
                })
            );
    }

    public eventCmtRegisterspagedList(pageNumber: number, pageSize: number, eventName: string): Observable<PagedList<EventCmtRegister>> {
        let params = new HttpParams();
        params = params.append('pageNumber', pageNumber.toString());
        params = params.append('pageSize', pageSize.toString());
        params = params.append('eventName', eventName);
        return this.http.get<any>('eventsCmt/EventsCmtRegistersPagedList', { params: params })
            .pipe(
                map(response => {
                    for (const iterator of response.data) {
                        // Update dates.
                        iterator.dateCreated = new Date(iterator.dateCreated.seconds * 1000);
                        iterator.dateModified = iterator.dateModified.seconds === 0 ? null : new Date(iterator.dateModified.seconds * 1000);
                    }
                    return new PagedList<EventCmtRegister>(response);
                })
            );
    }

    public eventCmtTradingUpdateStatus(id: string, status: number): Observable<boolean> {
        let params = new HttpParams();
        params = params.append('id', id);
        params = params.append('status', status.toString());
        return this.http.get<any>('eventsCmt/EventsCmtRegistersUpdateStatus', { params: params }).pipe(map(() => true));
    }

    public eventCmtRegistersExport(eventName: string) {
        let params = new HttpParams();
        params = params.append('eventName', eventName);
        return this.http.get('eventsCmt/EventsCmtRegistersExport', { params: params, responseType: 'blob' as 'json' }).pipe(
            map((response: any) => {
                const dataType = response.type;
                const binaryData = [];
                binaryData.push(response);
                const downloadLink = document.createElement('a');
                downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
                downloadLink.setAttribute('download', 'event-registers.csv');
                document.body.appendChild(downloadLink);
                downloadLink.click();
            })
        );
    }

    public eventCmtRegisterAdd(model: EventCmtRegister): Observable<GrpcResponse> {
        return this.http.post('eventsCmt/EventCmtRegisterAdd', model).pipe(map(response => new GrpcResponse(response)));
    }

    public eventCmtRegisterUpdate(model: EventCmtRegister): Observable<GrpcResponse> {
        return this.http.post('eventsCmt/EventCmtRegisterUpdate', model).pipe(map(response => new GrpcResponse(response)));
    }

    public eventCmtRegisterDelete(id: string): Observable<boolean> {
        const params = new HttpParams().append('id', id);
        return this.http.delete('eventsCmt/EventsCmtRegisterDelete', { params: params })
            .pipe(
                map((response: boolean) => {
                    return response;
                })
            );
    }

}
