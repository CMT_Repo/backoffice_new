import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject, throwError as observableThrowError } from 'rxjs';
import { take, filter, catchError, finalize, map, mergeMap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { SignInResultModel } from '../models/auth/signInResultModel';
import { Permission } from '../models/auth/permission';
import { Claim } from '../models/auth/claimModel';
import { SigmaHelper } from '../helpers/sigmaHelper';
import { MatDialog } from '@angular/material/dialog';

@Injectable({ providedIn: 'root' })
export class AuthService {
    private jwtHeper = new JwtHelperService();
    private refresingToken = false;
    // Refresh Token Subject tracks the current token, or is null if no token is currently, available (e.g. refresh pending).
    private refreshTokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    constructor(private http: HttpClient, private router: Router, private sigmaHelper: SigmaHelper, private dialogRef: MatDialog) { }

    /**
     * Check whther authenticated.
     */
    public isAuthenticated(): Observable<boolean> {
        // Check if JWT valid.
        if (this.isJwtValid(this.getJwt()))
            return Observable.create(function (observer) { observer.next(true); });
        else
            // Try to refresh JWT.
            return this.refreshJwt().pipe(
                map((token: string) => this.isJwtValid(token))
            );
    }

    /**
    * Attepmt to login.
    */
    // TODO: spit SignInResultModel instead of boolean.
    public login(username: string, password: string): Observable<SignInResultModel> {
        const formData = new FormData();
        formData.append('username', username);
        formData.append('password', password);
        return this.http.post('auth/login', formData)
            .pipe(
                map(response => {
                    const result = new SignInResultModel(response);
                    if (result.Succeeded) {
                        this.setJwt(result.Jwt);
                        this.setRefreshToken(result.RefreshToken);
                    }
                    return result;
                })
            );
    }

    /**
    * Hanlde 401 errors.
    */
    public handleUnauthorized(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.refresingToken)
            return this.refreshTokenSubject.pipe(
                filter(token => token != null),
                take(1),
                mergeMap(token => {
                    return next.handle(this.setAuthHeaders(request, token));
                }));
        else {
            this.refresingToken = true;
            // Reset refreshTokenSubject so that the following requests wait until the toke comes back from the refreshToken call.
            this.refreshTokenSubject.next(null);
            return this.refreshJwt().pipe(
                mergeMap((newToken: string) => {
                    if (newToken) {
                        this.refreshTokenSubject.next(newToken);
                        return next.handle(this.setAuthHeaders(request, newToken));
                    }
                    // If we don't get a new token, we are in trouble so logout.
                    return observableThrowError('');
                }),
                catchError(error => {
                    // If there is an exception calling 'refreshToken', bad news so logout.
                    this.logout();
                    return observableThrowError(error);
                }),
                finalize(() => {
                    this.refresingToken = false;
                }));
        }
    }

    /**
    * Checks whether current user has the right permission.
    */
    public isAuthorized(permission: any, isAdmin: boolean = true): boolean {
        let normalizedPermission: number;
        if (typeof permission === 'string')
            normalizedPermission = Permission[permission];
        if (typeof permission === 'number')
            normalizedPermission = permission;
        const permissionNumbers = this.getPermissionClaim().split(',').map(function (item) {
            return parseInt(item, 10);
        });
        // Try to auth using permissions.
        if (permissionNumbers.includes(normalizedPermission))
            return true;
        // if couldn't auth using permissions try to check if can auth using admin permission.
        if (isAdmin)
            return permissionNumbers.includes(Permission.Admin);
        return false;
    }

    /**
    * Get current user claims.
    */
    public getClaims(): Array<Claim> {
        const claims = new Array<Claim>();
        const decodedJwt = this.jwtHeper.decodeToken(this.getJwt());
        const claimkeys = Object.keys(decodedJwt);
        for (let index = 0; index < claimkeys.length; index++)
            claims.push(new Claim(claimkeys[index], decodedJwt[claimkeys[index]]));
        return claims;
    }

    /**
    * Logout.
    */
    public logout(): void {
        const formData = new FormData();
        this.dialogRef.closeAll();
        formData.append('refreshToken', this.getRefreshToken());
        this.http.post('auth/logout', formData).subscribe();
        this.clearTokens();
        this.router.navigate(['/login']);
    }

    /**
    * Get JWT.
    */
    public getJwt(): string {
        return localStorage.getItem('jwt');
    }

    /**
    * Set Authorization headers to request.
    */
    public setAuthHeaders(req: HttpRequest<any>, token?: string): HttpRequest<any> {
        if (token)
            return req.clone({ setHeaders: { Authorization: `Bearer ${token}` } });
        else
            return req.clone({ setHeaders: { Authorization: `Bearer ${this.getJwt()}` } });
    }

    private refreshJwt(): Observable<string> {
        // Check if refresh token is present.
        if (this.getJwt()) {
            const formData = new FormData();
            formData.append('refreshToken', this.getRefreshToken());
            return this.http.post('auth/refreshToken', formData)
                .pipe(
                    map(response => {
                        const result = new SignInResultModel(response);
                        if (result.Succeeded) {
                            const previousClaims = this.getClaims().filter(claim => claim.Key !== 'exp');
                            this.setJwt(result.Jwt);
                            const newClaims = this.getClaims().filter(claim => claim.Key !== 'exp');
                            try {
                                // Refresh page in case any of the claims have changed.
                                if (!this.sigmaHelper.isEquivalent2(previousClaims, newClaims))
                                    window.location.reload();
                            } catch (error) {
                                window.location.href = window.location.href;
                            }
                            return result.Jwt;
                        } else
                            this.logout();
                    }));
        }
        return Observable.create(function (observer) { observer.next(undefined); });
    }

    private isJwtValid(jwt: string): boolean {
        if (jwt === null || jwt === undefined)
            return false;
        const dateTokenJwt = this.jwtHeper.getTokenExpirationDate(this.getJwt());
        if (dateTokenJwt === undefined)
            return false;
        // Check if token not expired.
        if ((dateTokenJwt.valueOf() > new Date().valueOf()))
            return true;
        return false;
    }

    private setJwt(jwt: string): void {
        localStorage.setItem('jwt', jwt);
    }

    private setRefreshToken(refreshToken: string): void {
        localStorage.setItem('refreshToken', refreshToken);
    }

    private getRefreshToken(): string {
        return localStorage.getItem('refreshToken');
    }

    private clearTokens(): void {
        localStorage.clear();
    }

    private getPermissionClaim(): string {
        return this.getClaims().find(claim => claim.Key === environment.constStringPermission).Value.trim();
    }
}