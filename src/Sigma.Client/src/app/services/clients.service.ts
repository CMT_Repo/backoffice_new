import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ClientsService {

    constructor(private http: HttpClient) { }

    public getFullContactDetails(query: string): Observable<any> {
        let params = new HttpParams();
        params = params.append('searchQuery', query);
        return this.http.get<any>('clients/ForexGetFullContactDetails', { params: params })
            .pipe(
                map(response => {
                    return response;
                })
            );
    }
}
