import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SmtpClient } from '../models/smtp/smtpClient';
import { ApiResponse } from '../models/shared/api-response';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PagedList } from '../models/shared/pagedList';

@Injectable({
    providedIn: 'root'
})

export class SmtpClientService {

    constructor(private http: HttpClient) { }

    public add(model: SmtpClient): Observable<ApiResponse> {
        return this.http.post('SmtpClient/Add', model).pipe(map(response => new ApiResponse(response)));
    }

    public update(model: SmtpClient): Observable<ApiResponse> {
        return this.http.patch('SmtpClient/Update', model).pipe(map(response => new ApiResponse(response)));
    }

    public delete(id: string): Observable<ApiResponse> {
        const params = new HttpParams().append('id', id);
        return this.http.delete('SmtpClient/delete', { params: params }).pipe(map(response => new ApiResponse(response)));
    }

    public pagedList(pageNumber: number, pageSize: number): Observable<PagedList<SmtpClient>> {
        let params = new HttpParams();
        params = params.append('pageNumber', pageNumber.toString());
        params = params.append('pageSize', pageSize.toString());
        return this.http.get<any>('SmtpClient/PagedList', { params: params })
            .pipe(
                map(response => new PagedList<SmtpClient>(response))
            );
    }

    public smtpClientIdNameList(): Observable<Map<string, string>> {
        return this.http.get('SmtpClient/SmtpClientIdNameList').pipe(map(response => {
            const smptClientsIdNameMap = new Map<string, string>();
            Object.keys(response).forEach(key => {
                smptClientsIdNameMap.set(key, response[key]);
            });
            return smptClientsIdNameMap;
        }));
    }

}
