import { BaseModel } from '../shared/baseModel';

export class SmtpClient extends BaseModel {
    public name: string;
    public host: string;
    public port: number;
    public isEnabled: boolean;
    public isSsl: boolean;
    public userName: string;
    public password: string;
    public from: string;
}