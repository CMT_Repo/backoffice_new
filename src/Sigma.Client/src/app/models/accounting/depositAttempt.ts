export class DepositAttempt {
    public id: number;
    public login: number;
    public dateCreated: Date;
    public dateModified: Date;
    public isTestDeposit: boolean;
    public currency: number;
    public amountDecimal: string;
    public userIpAddress: string;
    public savedToTradingPlatform: boolean;
    public mt4OrderId: number;
    public crmTransactionId: number;
    public cardAssociation: number;
    public lastFourDigits: string;
    public cardHolderFirstName: string;
    public cardHolderLastName: string;
    public email: string;
    public address: string;
    public countryIsoSelected: string;
    public countryIsoByIp: string;
    public city: string;
    public zipCode: string;
    public phoneNumber: string;
    public debugDetails: string;
    public settledAmountDecimal: string;
    public settledCurrency: number;
    public depositedAmountDecimal: string;
    public depositedCurrency: number;
    public usdAmountDecimal: string;
    public conversionRateDecimal: string;
    public isDodChecked: boolean;
    public promoName: string;
}