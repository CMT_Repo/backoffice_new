export class DepositLogFilter {
    public id: string;
    public mt4Account: string;
    public mt4Order: string;
    public amount: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public crmTransactionId: string;
    public currencies: Array<number>;
    public paymentProviders: Array<number>;
    public creditCardAssociations: Array<number>;
    public countryISOs: Array<number>;
    public isSuccessfulOnly: boolean;
}