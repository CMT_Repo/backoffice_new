export class DepositAttemptPspResult {
    public id: number;
    public depositAttemptId: number;
    public dateCreated: Date;
    public ppsResult: string;
    public message: string;
    public pspName: string;
}