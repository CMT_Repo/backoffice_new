import { BaseModel } from '../shared/baseModel';
import { SmtpClient } from '../smtp/smtpClient';

export class BiReportEmailJob extends BaseModel {
    public title: string;
    public name: string;
    public cron: string;
    public recipients: Array<string>;
    public reportUrls: Array<string>;
    public smtpClient: SmtpClient;
}