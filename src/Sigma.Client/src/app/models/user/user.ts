import { Permission } from '../auth/permission';

export class User {
    public id: string;
    public firstName: string;
    public lastName: string;
    public userName: string;
    public email: string;
    public password: string;
    public permissions: Array<Permission> = new Array<Permission>();
    public dateCreated: Date;
    public dateModified: Date;
}