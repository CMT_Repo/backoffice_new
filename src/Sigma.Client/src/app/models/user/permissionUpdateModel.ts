export class PermissionUpdateModel {
    public Username: string;
    public Permissions: Array<number>;
}