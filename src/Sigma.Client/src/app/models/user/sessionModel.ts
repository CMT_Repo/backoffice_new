export class Session {
    public userAgent: string;
    public ipAddress: string;
    public dateCreated: Date;
    public dateLastUsed: Date;
    public signature: string;
}