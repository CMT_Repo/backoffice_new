export class UserUpdateRolesModel {
    public username: string;
    public addRoles: Array<string>;
    public removeRoles: Array<string>;
}