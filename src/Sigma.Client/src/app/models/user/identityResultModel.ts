export class IdentityResult {
    public Succeeded: boolean;
    public errors: Array<IdentityResultError>;

    constructor(data: any) {
        this.Succeeded = data.succeeded;
        if (data.errors) {
            const errors = new Array<IdentityResultError>();
            for (let i = 0; i < data.errors.length; i++)
                errors.push(new IdentityResultError(data.errors[i]));
            this.errors = errors;
        }
    }
}

export class IdentityResultError {
    public code: string;
    public description: string;

    constructor(data: any) {
        this.code = data.code;
        this.description = data.description;
    }
}