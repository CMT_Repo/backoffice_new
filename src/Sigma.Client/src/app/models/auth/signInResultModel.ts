export class SignInResultModel {
    Jwt: string;
    RefreshToken: string;
    Succeeded: boolean;
    IsLockedOut: boolean;
    IsNotAllowed: boolean;
    RequiresTwoFactor: boolean;

    constructor(source?: any) {
        this.Jwt = source.jwt;
        this.RefreshToken = source.refreshToken;
        this.IsLockedOut = source.isLockedOut;
        this.IsNotAllowed = source.isNotAllowed;
        this.Succeeded = source.succeeded;
        this.RequiresTwoFactor = source.requiresTwoFactor;
    }
}