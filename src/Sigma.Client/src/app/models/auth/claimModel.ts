export class Claim {
    public Key: string;
    public Value: string;

    constructor(key?: string, value?: string) {
        this.Key = key;
        this.Value = value;
    }
}