export class RefreshTokenModel {
    Succeeded: boolean;
    Jwt: string;
}