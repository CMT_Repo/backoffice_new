export class EventCmt {
    public id: string;
    public name: string;
    public urlName: string;
    public thankYouUrl: string;
    public smtpServerName: string;
    public emailThankYouSubject: string;
    public emailThankYouBody: string;
    public emailRejectedSubject: string;
    public emailRejectedBody: string;
    public emailApprovedSubject: string;
    public emailApprovedBody: string;
    public dateCreated: Date;
    public dateModified: Date;
    public registersNew: number;
    public registerApproved: number;
    public registersRejected: number;
}