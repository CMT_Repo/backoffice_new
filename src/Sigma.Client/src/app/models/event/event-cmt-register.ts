export class EventCmtRegister {
    public id: string;
    public eventId: string;
    public fullName: string;
    public eventName: string;
    public emailAddress: string;
    public phoneNumber: string;
    public numberOfSeats: number;
    public a: number;
    public subAffiliate: string;
    public cxd: string;
    public vvar10: string;
    public language: string;
    public eventRegistrationStatus: number;
    public dateCreated: Date;
    public dateModified: Date;
}