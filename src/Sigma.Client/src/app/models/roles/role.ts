import { Permission } from '../auth/permission';

export class Role {
    public id: string;
    public dateCreated: Date;
    public dateModified: Date;
    public permissionsString: string;
    public name: string;
    public permissions: Array<Permission> = new Array<Permission>();
}