export class BaseModel {
    id: string;
    dateCreated: Date;
    dateModified: Date;
}
