/**
 * The direction in which to sequence elements.
 */
export enum Order {
    /**
    * Ordered from low values to high values.
    */
    Ascending,
    /**
     * Ordered from high values to low values.
     */
    Descending,
}

export enum Operation {
    Add,
    Update
}