export class ApiResponse {
    public isSuccess: boolean;
    public errorMessages: Array<string>;

    constructor(source?: any) {
        this.isSuccess = source.isSuccess;
        if (source.errorMessages) {
            this.errorMessages = new Array<string>();
            for (const iterator of source.errorMessages)
                this.errorMessages.push(iterator);
        }

    }
}