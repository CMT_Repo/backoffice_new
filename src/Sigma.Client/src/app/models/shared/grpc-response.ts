export class GrpcResponse {
    public isSuccess: number;
    public errors: Array<GrpcResponseError> = new Array<GrpcResponseError>();

    constructor(source?: any) {
        this.isSuccess = source.isSuccess;
        for (const iterator of source.errors) {
            // tslint:disable-next-line: no-use-before-declare
            const error: GrpcResponseError = new GrpcResponseError();
            error.code = iterator.code;
            error.description = iterator.description;
            this.errors.push(error);
        }
    }
}

export class GrpcResponseError {
    public code: number;
    public description: string;
}
