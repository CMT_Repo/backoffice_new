/**
 * PagedList metaData.
 */
export class PagedListMetaData {
    pageCount: number;
    totalItemCount: number;
    pageNumber: number;
    pageSize: number;
    hasPreviousPage: boolean;
    hasNextPage: boolean;
    isFirstPage: boolean;
    isLastPage: boolean;
    firstItemOnPage: number;
    lastItemOnPage: number;
}

/**
 * PagedList.
 */
export class PagedList<T> {
    data: Array<T>;
    metaData: PagedListMetaData;

    constructor(source?: any) {
        this.metaData = source && Object.assign(new PagedListMetaData, source.metaData) || new PagedListMetaData();
        this.data = source && source.data || new Array<T>();
    }
}
