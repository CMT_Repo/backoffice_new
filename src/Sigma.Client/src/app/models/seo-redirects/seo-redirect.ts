export class SeoRedirect {
    public id: number;
    public name: string;
    public websiteId: number;
    public isEnabled: boolean;
    public isMulti: boolean;
    public isPermanent: boolean;
    public isRegex: boolean;
    public result: string;
    public source: string;
    public dateCreated: Date;
    public dateModified: Date;
}