import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SigmaHelper } from '../helpers/sigmaHelper';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private auth: AuthService, private sigmaHelper: SigmaHelper) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.auth.isAuthenticated().pipe(map((result: boolean) => {
            if (result) {
                const permission = route.data['permission'];
                if (permission) {
                    const isAdmin = route.data['isAdmin'];
                    if (isAdmin === undefined || isAdmin === false || isAdmin === null)
                        return this.auth.isAuthorized(permission);
                    else
                        return this.auth.isAuthorized(permission, true);
                } else
                    return result;
            }
            this.auth.logout();
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            return result;
        }
        ));
    }

    private cantActivate(url: string) {
        // Logout and, navigate to login page if not authenticated.
        this.auth.logout();
        this.router.navigate(['/login'], { queryParams: { returnUrl: url } });
    }

}