import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { QuestionControlService } from 'src/app/services/question-control.service';

@Component({
    selector: 'app-dynamic-form',
    templateUrl: './dynamic-form.component.html',
    providers: [QuestionControlService]
})
export class DynamicForm implements OnInit {
    /**
    * Your questions go here.
    */
    @Input() questions: QuestionBase[] = [];
    /**
    * Emits event when submit button is clicked.
    */
    @Output() submit: EventEmitter<FormGroupDirective> = new EventEmitter<FormGroupDirective>();
    /**
    * Emits event when cancel button is clicked.
    */
    @Output() cancel: EventEmitter<FormGroupDirective> = new EventEmitter<FormGroupDirective>();
    /**
    * Exposes the inner FormGroup.
    */
    @Input() form: FormGroup;
    /**
    * Use to to show/hide submit button.
    */
    @Input() submitBtnShowed: boolean = true;
    /**
    * Use to show/hide cancel button.
    */
    @Input() canceBtnShowed: boolean = true;
    /**
    * Use to set cancel button text.
    */
    @Input() cancelBtnText: string = 'Cancel';
    /**
    * Use to set cancel button text.
    */
    @Input() submitBtnText: string = 'Submit';
    /**
    * Use to enable/disable submit button.
    */
    @Input() submitBtnDisabled: boolean = false;
    /**
    * Use to enable/disable cancel button.
    */
    @Input() cancelBtnDisabed: boolean = false;
    /**
    * Use to enable/disable based on form.valid.
    */
    @Input() submitBtnFrmValid: boolean = true;
    /**
    * Use to check if form is enabled/disabled.
    */
    @Input() isEnabled: boolean = true;
    /**
    *  Use this to display error messages.
    */
    @Input() errors: Array<string> = new Array<string>();
    /**
    *  forms width.
    */
    @Input() width: string = 'initial';

    constructor(private qcs?: QuestionControlService) { }

    ngOnInit() {
        this.form = this.qcs.toFormGroup(this.questions);
    }

    /**
    * Use to disable forms controls.
    */
    public disable(): void {
        this.form.disable();
        this.isEnabled = false;
        this.cancelBtnDisabed = true;
        this.submitBtnDisabled = true;
    }

    /**
    * Use to enable forms controls.
    */
    public enable(): void {
        this.form.enable();
        this.isEnabled = true;
        this.cancelBtnDisabed = false;
        this.submitBtnDisabled = false;
    }

    public onSubmit(event: FormGroupDirective): void {
        this.submit.emit(event);
    }

    public onCancel(event: FormGroupDirective): void {
        this.cancel.emit(event);
    }
}