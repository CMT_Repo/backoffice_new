import { Component, Input, AfterContentInit, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { QuestionBase } from 'src/app/shared/dynamic-form/question/question-base';
import { AutocompleteQuestion } from '../question/question-autoComplete';
import { startWith, map } from 'rxjs/operators';
import { MatSliderChange } from '@angular/material/slider';
import { SliderQuestion } from '../question/question-slider';
import { ChipsQuestion } from '../question/question-chips';

@Component({
    selector: 'app-question',
    templateUrl: './dynamic-form-question.component.html'
})
export class DynamicFormQuestion implements AfterContentInit, OnInit {

    @Input() question: QuestionBase;
    @Input() form: FormGroup = new FormGroup({});
    @Output() sliderChange: EventEmitter<MatSliderChange> = new EventEmitter<MatSliderChange>();
    get isValid() { return this.form.controls[this.question.key].valid; }
    public questionForomControl: AbstractControl;

    ngOnInit(): void {
        this.questionForomControl = this.form.get(this.question.key);
    }

    ngAfterContentInit(): void {
        switch (this.question.controlType) {
            case 'autocomplete':
                Promise.resolve(null).then(() => {
                    // Filter options.
                    (this.question as AutocompleteQuestion).optionsFiltered = this.form.controls[this.question.key].valueChanges
                        .pipe(
                            startWith(''),
                            map(value => this.autoCompltefilter(value))
                        );
                });
                break;
            case 'chips':
                const chipsQuestion = (this.question as ChipsQuestion);
                if (chipsQuestion.chips)
                    this.questionForomControl.setValue(chipsQuestion.chips);
                else
                    this.questionForomControl.setValue([]);
                break;
        }
    }

    private addChip(event) {
        const value = event.value;
        // Add our item.
        if ((value || '').trim()) {
            this.questionForomControl.setValue([...this.form.get(this.question.key).value, value.trim()]);
            this.form.updateValueAndValidity();
        }
        // Reset the input value.
        event.input.value = '';
    }

    private removeChip(chip: string) {
        const index = this.questionForomControl.value.indexOf(chip);
        if (index >= 0) {
            this.questionForomControl.value.splice(index, 1);
            this.form.get(this.question.key).updateValueAndValidity();
        }
    }

    private autoCompltefilter(value: string): string[] {
        const filterValue = value.toLowerCase();
        return (this.question as AutocompleteQuestion).options.filter(option => option.toLowerCase().includes(filterValue));
    }

    private sliderChanged(matSliderChange: MatSliderChange) {
        (this.question as SliderQuestion).change.emit(matSliderChange);
    }
}