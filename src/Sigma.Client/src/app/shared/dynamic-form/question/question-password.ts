import { QuestionBase, Validator } from 'src/app/shared/dynamic-form/question/question-base';

export class PasswordQuestion extends QuestionBase {
    public hide: boolean;

    constructor(key: string, label: string, order: number, validations?: Validator[], fxFlex?: number, hide?: boolean, fxLayout?: string, fxLayoutAlign?: string) {
        super(key, label, order, 'password', validations, fxFlex, fxLayout, fxLayoutAlign);
        if (hide === undefined)
            this.hide = true;
    }
}