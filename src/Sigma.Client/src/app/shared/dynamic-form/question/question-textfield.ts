import { QuestionBase, Validator } from 'src/app/shared/dynamic-form/question/question-base';

export class TexfieldQuestion extends QuestionBase {
    controlType = 'textfield';
    public rows: number;
    public cols: number;

    constructor(key: string, label: string, order: number, validations?: Validator[], fxFlex?: number, rows?: number, cols?: number, fxLayout?: string, fxLayoutAlign?: string) {
        super(key, label, order, 'textfield', validations, fxFlex, fxLayout, fxLayoutAlign);
        this.rows = rows == null ? 2 : rows;
        this.cols = cols == null ? 20 : cols;
    }
}