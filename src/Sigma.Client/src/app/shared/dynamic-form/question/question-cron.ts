import { QuestionBase, Validator } from 'src/app/shared/dynamic-form/question/question-base';
import { Validators } from '@angular/forms';
import { TextboxQuestion } from './question-textbox';

export class CronQuestion extends QuestionBase {

    controlType = 'cron';
    public questions: QuestionBase[] = [
        new TextboxQuestion(
            'minutes',
            'Minutes',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Minutes required' },
                { name: 'pattern', validator: Validators.pattern('^((0[0-9]|[1-5][0-9]|59)|(\\*{1})|\\*\/(0[0-9]|[1-5][0-9]|59))$'), message: 'Invalid input' },
            ],
        ),
        new TextboxQuestion(
            'hour',
            'Hour',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Hour required' },
                { name: 'pattern', validator: Validators.pattern('^((0[0-9]|1[0-4]|2[0-3])|(\\*{1})|\\*\/((0[0-9]|1[0-4]|2[0-3])))$'), message: 'Invalid input' },

            ],
        ),
        new TextboxQuestion(
            'day',
            'Day',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Day required' },
                { name: 'pattern', validator: Validators.pattern('^((0[0-9]|[12][0-9]|3[01])|(\\*{1})|\\*\/(0[0-9]|[12][0-9]|3[01]))$'), message: 'Invalid input' },
            ],
        ), new TextboxQuestion(
            'month',
            'Month',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Month required' },
                { name: 'pattern', validator: Validators.pattern('^((0[1-9]|1[0-2])|(\\*{1})|\\*\/(0[1-9]|1[0-2]))$'), message: 'Invalid input' },
            ],
        ),
        new TextboxQuestion(
            'weekDay',
            'Week day',
            0,
            [
                { name: 'required', validator: Validators.required, message: 'Weekday required' },
                { name: 'pattern', validator: Validators.pattern('^(([0-6],)+[0-6]|([0-6]+(\\/|-)[0-6]+)|(\\*{1})|([0-6])|\\*\/[0-6])$'), message: 'Invalid input' },
            ],
        ),
    ].sort((a, b) => a.order - b.order);

    constructor(key: string, label: string, order: number, validations?: Validator[], fxFlex?: number, fxLayout?: string, fxLayoutAlign?: string) {
        super(key, label, order, 'textbox', validations, fxFlex, fxLayout, fxLayoutAlign);
    }
}