import { QuestionBase, Validator } from 'src/app/shared/dynamic-form/question/question-base';

export class ToggleQuestion extends QuestionBase {
    controlType = 'toggle';

    constructor(key: string, label: string, order: number, validations?: Validator[]) {
        super(key, label, order, 'toggle', validations);
    }
}