import { QuestionBase, Validator } from 'src/app/shared/dynamic-form/question/question-base';

export class SelectQuestion extends QuestionBase {
    controlType = 'select';
    public options: Map<any, string>;

    constructor(key: string, label: string, order: number, validations?: Validator[], fxFlex?: number, options?: Map<any, string>, fxLayout?: string, fxLayoutAlign?: string) {
        super(key, label, order, 'select', validations, fxFlex, fxLayout, fxLayoutAlign);
        this.options = options;
    }
}