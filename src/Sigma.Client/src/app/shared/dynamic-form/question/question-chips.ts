import { QuestionBase, Validator } from 'src/app/shared/dynamic-form/question/question-base';

export class ChipsQuestion extends QuestionBase {
    public controlType = 'chips';
    public chips: Array<string>;

    constructor(key: string, label: string, order: number, validations?: Validator[], chips?: Array<string>, fxFlex?: number, fxLayout?: string, fxLayoutAlign?: string) {
        super(key, label, order, 'select', validations, fxFlex, fxLayout, fxLayoutAlign);
        this.chips = chips;
    }
}