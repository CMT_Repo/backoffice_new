import { QuestionBase, Validator } from 'src/app/shared/dynamic-form/question/question-base';
import { Output, EventEmitter, Directive } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';

@Directive()
export class SliderQuestion extends QuestionBase {
    @Output() change: EventEmitter<MatSliderChange> = new EventEmitter<MatSliderChange>();
    public tickInterval: number | 'auto' = 'auto';
    public min: number = 0;
    public max: number = 100;
    public width: string = '15rem';
    public value: number = 0;
    public step: number = 1;

    constructor(key: string, label: string, order: number, validations?: Validator[], tickInterval?: number | 'auto', step?: number, min?: number, max?: number, width?: string, fxFlex?: number, fxLayout?: string, fxLayoutAlign?: string) {
        super(key, label, order, 'slider', validations, fxFlex, fxLayout, fxLayoutAlign);
        if (tickInterval)
            this.tickInterval = tickInterval;
        if (min)
            this.min = min;
        if (max)
            this.max = max;
        if (width)
            this.width = width;
        if (step)
            this.step = step;
    }
}