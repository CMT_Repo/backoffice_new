import { QuestionBase, Validator } from 'src/app/shared/dynamic-form/question/question-base';
import { Observable } from 'rxjs';

export class AutocompleteQuestion extends QuestionBase {

    controlType = 'autocomplete';
    public optionsFiltered: Observable<string[]>;
    public options: string[];

    constructor(key: string, label: string, order: number, validations?: Validator[], fxFlex?: number, options?: string[], optionsFiltered?: Observable<string[]>) {
        super(key, label, order, 'autocomplete', validations, fxFlex);
        this.options = options;
        this.optionsFiltered = optionsFiltered;
    }
}