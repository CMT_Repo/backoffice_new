import { QuestionBase, Validator } from 'src/app/shared/dynamic-form/question/question-base';

export class TextboxQuestion extends QuestionBase {
    controlType = 'textbox';
    public hint: string;

    constructor(key: string, label: string, order: number, validations?: Validator[], fxFlex?: number, fxLayout?: string, fxLayoutAlign?: string, hint?: string) {
        super(key, label, order, 'textbox', validations, fxFlex, fxLayout, fxLayoutAlign);
        this.hint = hint;
    }
}