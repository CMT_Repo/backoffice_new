export class QuestionBase {

    public key: string;
    public label: string;
    public order: number;
    // TODO: Use ENUM instead of string.
    public controlType: string;
    public validations: Validator[];
    public fxFlex: number;
    public fxLayout: string;
    public fxLayoutAlign: string;

    constructor(key: string, label: string, order: number, controlType: string, validations?: Validator[], fxFlex?: number, fxLayout?: string, fxLayoutAlign?: string) {
        this.key = key || '';
        this.label = label || '';
        this.order = order === undefined ? 1 : order;
        this.controlType = controlType || '';
        this.validations = validations;
        this.fxFlex = fxFlex == null ? 100 : fxFlex;
        this.fxLayout = fxLayout == null ? 'column' : fxLayout;
        this.fxLayoutAlign = null ? 'center center' : fxLayoutAlign;
    }

}

export interface Validator {
    name: string;
    validator: any;
    message: string;
}