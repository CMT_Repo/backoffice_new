import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PagedListMetaData } from '../models/shared/pagedList';
import { ConfirmDialogComponent } from '../shared/dialogs/confirm-dialog/confirm-dialog.component';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { CronQuestion } from '../shared/dynamic-form/question/question-cron';

/**
 * Helper classs.
 */
@Injectable({ providedIn: 'root' })
export class SigmaHelper {

    constructor(private snackBar?: MatSnackBar, private dialog?: MatDialog) { }

    /**
    * Opens a snackbar with a message and an optional action.
    */
    public showSnackBar(message: string): void {
        this.snackBar.open(message, null, {
            duration: 5000,
            panelClass: 'snackbar-center'
        });
    }

    /**
    * Populate paginator with metaData.
    * @param metaData - Metadata param.
    * @param source - The paginator to populate.
    */
    public paginator(metaData: PagedListMetaData, source: MatPaginator, hidePageSize: boolean = true): MatPaginator {
        source.pageSize = metaData.pageSize;
        source.length = metaData.totalItemCount;
        source.pageIndex = metaData.pageNumber - 1;
        source.showFirstLastButtons = true;
        source.hidePageSize = hidePageSize;
        return source;
    }

    /**
    * Returns enum names.
    */
    public getEnumNames(myEnum: any): string[] {
        return Object.keys(myEnum).map(key => {
            if (this.stringIsNumber(myEnum[key]))
                return key;
        }).filter(enumName => enumName !== undefined);
    }

    /**
    * Returns number array with enum values.
    */
    public getEnumValues(myEnum: any): string[] {
        return Object.keys(myEnum).map(key => {
            if (!this.stringIsNumber(myEnum[key]))
                return key;
        }).filter(enumName => enumName !== undefined);
    }

    /**
    * Returns string array with enum names.
    */
    public intArrToEnumNames(data: Array<number>, myEnum: any): string[] {
        const enumNames: string[] = new Array<string>();
        for (let index = 0; index < data.length; index++)
            enumNames.push(myEnum[data[index]]);
        return enumNames;
    }

    /**
    * Maps enum to map.
    */
    public enumToMap(e: { [s: number]: string }): Map<number, string> {
        const map = new Map<number, string>();
        for (const n in e)
            if (typeof e[n] === 'number')
                map.set(Number(e[n]), n);
        return map;
    }

    /**
    * Checks if number.
    */
    public stringIsNumber(value: any): boolean {
        return isNaN(Number(value)) === false;
    }

    /**
    * Checks if valid date.
    */
    public isValidDate(d: any) {
        let result: boolean;
        result = d instanceof Date && !isNaN(d.valueOf());
        if (!result)
            result = !isNaN(new Date(d).valueOf());
        return result;
    }

    /**
    * Populate cron FromControl.
    */
    public populateCronFromControl(source: FormGroup, cronExpression: string): FormGroup {
        const separatedCronExpression = cronExpression.split(' ');
        source.get('cron').get('minutes').setValue(separatedCronExpression[0]);
        source.get('cron').get('hour').setValue(separatedCronExpression[1]);
        source.get('cron').get('day').setValue(separatedCronExpression[2]);
        source.get('cron').get('month').setValue(separatedCronExpression[3]);
        source.get('cron').get('weekDay').setValue(separatedCronExpression[4]);
        return source;
    }

    /**
    * Checks if object isEqualiant.
    */
    public isEquivalent2(a: any, b: any) {
        a = this.unwrapStringOrNumber(a);
        b = this.unwrapStringOrNumber(b);
        if (a === b) return true; // e.g. a and b both null
        if (a === null || b === null || typeof (a) !== typeof (b)) return false;
        if (a instanceof Date)
            return b instanceof Date && a.valueOf() === b.valueOf();
        if (typeof (a) !== 'object')
            return a == b; // for boolean, number, string, xml
        const newA = (a.areEquivalent_Eq_91_2_34 === undefined);
        const newB = (b.areEquivalent_Eq_91_2_34 === undefined);
        try {
            if (newA) a.areEquivalent_Eq_91_2_34 = [];
            else if (a.areEquivalent_Eq_91_2_34.some(
                function (other) { return other === b; })) return true;
            if (newB) b.areEquivalent_Eq_91_2_34 = [];
            else if (b.areEquivalent_Eq_91_2_34.some(
                function (other) { return other === a; })) return true;
            a.areEquivalent_Eq_91_2_34.push(b);
            b.areEquivalent_Eq_91_2_34.push(a);
            const tmp = {};
            for (const prop in a)
                if (prop != 'areEquivalent_Eq_91_2_34')
                    tmp[prop] = null;
            for (const prop in b)
                if (prop != 'areEquivalent_Eq_91_2_34')
                    tmp[prop] = null;

            for (const prop in tmp)
                if (!this.isEquivalent2(a[prop], b[prop]))
                    return false;
            return true;
        } finally {
            if (newA) delete a.areEquivalent_Eq_91_2_34;
            if (newB) delete b.areEquivalent_Eq_91_2_34;
        }
    }

    private unwrapStringOrNumber(obj) {
        return (obj instanceof Number || obj instanceof String ? obj.valueOf() : obj);
    }

    public isEnum(instance: Object): boolean {
        const keys = Object.keys(instance);
        const values = [];
        for (const key of keys) {
            let value = instance[key];
            if (typeof value === 'number')
                value = value.toString();
            values.push(value);
        }
        for (const key of keys) {
            if (values.indexOf(key) < 0)
                return false;
        }
        return true;
    }

    /**
    * Spawn confirm dialog.
    */
    public dialogConfirm(message?: string): Observable<boolean> {
        return this.dialog.open(ConfirmDialogComponent, { data: message }).afterClosed();
    }

    /**
    * Unique filter.
    */
    public onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

}