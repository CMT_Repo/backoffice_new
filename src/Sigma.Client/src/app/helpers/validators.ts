
export const ValidatorRegexPatten = {
    password: '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$',
    plainText: '^[a-zA-Z]+$',
    plainTextNnumbers: '^[a-zA-Z0-9]+$',
    plainTextNnumbersSpace: '^[a-zA-Z0-9 ]+$',
    plainTextNnumbersSpaceEmail: '^[a-zA-Z0-9 @.]+$',
    numbersOnly: '^[0-9]*$',
};