import { Permission } from 'src/app/models/auth/permission';

export class Menu {
    constructor(public id: number,
        public title: string,
        public routerLink: string,
        public href: string,
        public icon: string,
        public target: string,
        public hasSubMenu: boolean,
        public parentId: number,
        public permissions: Array<Permission> = null,
        public isAdmin: boolean = true) { }
}