import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Menu } from './menu.model';
import { verticalMenuItems, horizontalMenuItems } from './menu';
import { AuthService } from 'src/app/services/auth.service';

@Injectable()
export class MenuService {

    constructor(private location: Location, private auth: AuthService) { }

    public getVerticalMenuItems(): Array<Menu> {
        return verticalMenuItems;
    }

    public getHorizontalMenuItems(): Array<Menu> {
        // Authorize which menu items can be seen, using AuthenticationService.
        return horizontalMenuItems.filter(menuItem => {
            if (menuItem.permissions)
                return menuItem.permissions.filter(permissionNeeded => {
                    if (this.auth.isAuthorized(permissionNeeded, menuItem.isAdmin))
                        return true;
                    else
                        return false;
                }).length > 0;
            else
                return true;
        });
    }

    public expandActiveSubMenu(menu: Array<Menu>) {
        const url = this.location.path();
        const routerLink = url; // url.substring(1, url.length);
        const activeMenuItem = menu.filter(item => item.routerLink === routerLink);
        if (activeMenuItem[0]) {
            let menuItem = activeMenuItem[0];
            while (menuItem.parentId !== 0) {
                const parentMenuItem = menu.filter(item => item.id === menuItem.parentId)[0];
                menuItem = parentMenuItem;
                this.toggleMenuItem(menuItem.id);
            }
        }
    }

    public toggleMenuItem(menuId) {
        const menuItem = document.getElementById('menu-item-' + menuId);
        const subMenu = document.getElementById('sub-menu-' + menuId);
        if (subMenu)
            if (subMenu.classList.contains('show')) {
                subMenu.classList.remove('show');
                menuItem.classList.remove('expanded');
            } else {
                subMenu.classList.add('show');
                menuItem.classList.add('expanded');
            }
    }

    public closeOtherSubMenus(menu: Array<Menu>, menuId) {
        const currentMenuItem = menu.filter(item => item.id === menuId)[0];
        if (currentMenuItem.parentId === 0 && !currentMenuItem.target)
            menu.forEach(item => {
                if (item.id !== menuId) {
                    const subMenu = document.getElementById('sub-menu-' + item.id);
                    const menuItem = document.getElementById('menu-item-' + item.id);
                    if (subMenu)
                        if (subMenu.classList.contains('show')) {
                            subMenu.classList.remove('show');
                            menuItem.classList.remove('expanded');
                        }
                }
            });
    }

}