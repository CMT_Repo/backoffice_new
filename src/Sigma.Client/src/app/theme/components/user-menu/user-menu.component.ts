import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user/user';

@Component({
    selector: 'app-user-menu',
    templateUrl: './user-menu.component.html',
    styleUrls: ['./user-menu.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class UserMenuComponent implements OnInit {
    public userImage = '../assets/img/users/user.jpg';
    public user: User;

    constructor(private authService: AuthService, private userService: UserService) {
        this.user = userService.user();
    }

    ngOnInit() {
    }

    logout() {
        this.authService.logout();
    }

}