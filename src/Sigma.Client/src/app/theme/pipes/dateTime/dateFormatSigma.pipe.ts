import { environment } from '../../../../environments/environment';
import { Pipe, PipeTransform, Inject, LOCALE_ID } from '@angular/core';
import { DatePipe } from '@angular/common';
import { SigmaHelper } from 'src/app/helpers/sigmaHelper';

@Pipe({
    name: 'dateFormatSigma'
})

export class DateFormat extends DatePipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private localeId: string, private simgaHelper: SigmaHelper) {
        super(localeId);
    }

    transform(value: any): any {
        if (this.simgaHelper.isValidDate(value))
            return super.transform(value, environment.dateTimeFormat);
        else
            return '';
    }
}