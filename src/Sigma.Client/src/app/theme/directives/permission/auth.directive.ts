import { Directive, Input, ElementRef, TemplateRef, ViewContainerRef, OnInit, AfterContentInit, Renderer2 } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Directive({
    selector: '[auth]'
})
export class AuthDirective implements OnInit, AfterContentInit {

    private c_disabled: string = 'disabled';
    private c_hidden: string = 'hidden';
    private requiredPermissions: any;
    private action: string = this.c_disabled;
    private authResult: boolean = false;
    private isAdmin: boolean = true;
    private isDisabled: boolean = false;
    private elementNative: any;

    @Input() set auth(value: string | string[]) {
        this.requiredPermissions = value;
    }

    @Input() set authAction(value: string) {
        this.action = value;
    }

    @Input() set authIsAdmin(value: boolean) {
        this.isAdmin = value;
    }

    @Input() set authDisabled(value: boolean) {
        this.isDisabled = value;
        if (value !== null && this.authResult && this.action === this.c_disabled && this.elementNative)
            this.elementNative.disabled = value;
    }

    constructor(private elementRef: ElementRef, private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef, private authService: AuthService, private renderer: Renderer2) {
        this.viewContainer.createEmbeddedView(this.templateRef);
        this.elementNative = this.elementRef.nativeElement.previousElementSibling;
    }

    ngOnInit() {
        // Get auth result.
        if (this.requiredPermissions instanceof Array) {
            for (const permission of (this.requiredPermissions as Array<any>))
                if (this.authService.isAuthorized(permission, this.isAdmin)) {
                    this.authResult = true;
                    break;
                }
        } else
            this.authResult = this.authService.isAuthorized(this.requiredPermissions, this.isAdmin);
    }

    ngAfterContentInit() {
        // Auth false and action set to hidden.
        if (this.action === this.c_hidden && !this.auth) {
            this.viewContainer.clear();
            return;
        }
        // action set to disabled.
        if (this.action === this.c_disabled)
            if (this.authResult) {
                this.elementNative.disabled = this.isDisabled;
                if (this.isDisabled)
                    this.renderer.setAttribute(this.elementNative, 'disabled', 'true');
                return;
            } else
                this.renderer.setAttribute(this.elementNative, 'disabled', 'true');
    }

}