import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetectScrollDirective } from './detect-scroll/detect-scroll.directive';
import { AuthDirective } from './permission/auth.directive';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        DetectScrollDirective,
        AuthDirective,
    ],
    exports: [
        DetectScrollDirective,
        AuthDirective,
    ]
})
export class DirectivesModule { }
