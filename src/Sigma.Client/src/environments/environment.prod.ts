export const environment = {
    production: true,
    apiRoot: 'http://3.124.130.27:61173/api/',
    dateTimeFormat: 'dd.MM.yyyy HH:MM',
    constStringPermission: 'Permissions',
};
