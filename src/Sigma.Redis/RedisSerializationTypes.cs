﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf.Meta;
using Sigma.Core.Extensions;

namespace Sigma.Redis
{
    /// <summary>
    /// Provides protobuf serialization support for a number of types that can be defined at runtime.
    /// </summary>
    public static class RedisSerializationTypes
    {
        /// <summary>
        /// Adds support for an additional type in this model, optionally/// applying inbuilt patterns. If the type is already known to the/// model, the existing type is returned **without** applying/// any additional behaviour.
        /// </summary>
        public static void Add <T> (IEnumerable<Type> subTypes = null)
        {
            // If already can serialize, don't add the type.
            if (RuntimeTypeModel.Default.CanSerialize(typeof (T)))
                return;
            MetaType metaType = RuntimeTypeModel.Default.Add(typeof (T), true).Add(typeof (T).GetDeclaredProperties());
            if (subTypes == null)
                return;
            // According to proto serialization protocol.
            int propertiesCount = typeof (T).GetProperties().Length;
            for (var i = 0; i < subTypes.Count(); i++)
            {
                ++propertiesCount;
                metaType.AddSubType(propertiesCount, subTypes.ElementAt(i));
            }
            metaType.CompileInPlace();
        }
    }
}