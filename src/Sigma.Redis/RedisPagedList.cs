﻿using System;
using System.Collections;
using System.Collections.Generic;
using X.PagedList;

namespace Sigma.Redis
{
    public sealed class RedisPagedList <T> : IPagedList<T>
    {
        public readonly RedisList<T> Subset;

        public bool IsFull => Subset.IsFull;

        public Dictionary<int, string> MissingItems => Subset.MissingItems;

        public T this [int index] => Subset[index];

        public int Count => Subset.Count;

        public int PageCount { get; }
        public int TotalItemCount { get; }
        public int PageNumber { get; }
        public int PageSize { get; }
        public bool HasPreviousPage { get; }
        public bool HasNextPage { get; }
        public bool IsFirstPage { get; }
        public bool IsLastPage { get; }
        public int FirstItemOnPage { get; }
        public int LastItemOnPage { get; }

        public RedisPagedList (RedisList<T> subset, int pageNumber, int pageSize, int totalItemCount) : this(pageNumber, pageSize, totalItemCount)
        {
            Subset = subset;
        }

        private RedisPagedList (int pageNumber, int pageSize, int totalItemCount)
        {
            if (pageNumber < 1)
                throw new ArgumentOutOfRangeException($"pageNumber = {pageNumber}. PageNumber cannot be below 1.");
            if (pageSize < 0)
                throw new ArgumentOutOfRangeException($"pageSize = {pageSize}. PageSize cannot be less than 1.");
            TotalItemCount = totalItemCount;
            PageSize = pageSize;
            PageNumber = pageNumber;
            PageCount = TotalItemCount > 0 ? (int)Math.Ceiling(TotalItemCount / (double)PageSize) : 0;
            HasPreviousPage = PageNumber > 1;
            HasNextPage = PageNumber < PageCount;
            IsFirstPage = PageNumber == 1;
            IsLastPage = PageNumber == PageCount;
            FirstItemOnPage = (PageNumber - 1) * PageSize + 1;
            int num = FirstItemOnPage + PageSize - 1;
            LastItemOnPage = num > TotalItemCount ? TotalItemCount : num;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Subset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public PagedListMetaData GetMetaData()
        {
            return new PagedListMetaData(this);
        }
    }
}