﻿using System;

namespace Sigma.Redis
{
    public sealed class RedisSort <T>
    {
        public string SortName { get; }
        public Func<T, double> SortFunc { get; }

        public RedisSort (string sortName, Func<T, double> sortFunc)
        {
            SortFunc = sortFunc;
            SortName = sortName;
        }
    }
}