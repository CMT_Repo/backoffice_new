﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Sigma.Redis
{
    public class RedisResult <T> : ICollection<T> where T : IRedisObject
    {
        private readonly ICollection<T> _Items;
        private IEnumerable<string> _MissingRedisObjectKeys { get; set; }
        public IEnumerable<string> MissingRedisObjectKeys => _MissingRedisObjectKeys;
        private bool _IsFull { get; set; }
        public bool IsFull => _IsFull;
        public int Count => _Items.Count;
        public bool IsReadOnly => true;

        public T this [int index] { get => _Items.ElementAt(index); set => _Items.ToList()[index] = value; }

        public RedisResult()
        {
            _Items = new List<T>();
        }

        public RedisResult (ICollection<T> collection)
        {
            _Items = collection;
            UpdateInternals();
        }

        public void Add (T item)
        {
            _Items.Add(item);
            UpdateInternals();
        }

        public void Clear()
        {
            _Items.Clear();
            UpdateInternals();
        }

        public bool Contains (T item)
        {
            return _Items.Any(redisObject => redisObject.Id == item.Id);
        }

        public void CopyTo (T[] array, int arrayIndex)
        {
            _Items.CopyTo(array, arrayIndex);
        }

        public bool Remove (T item)
        {
            bool result = _Items.Remove(item);
            UpdateInternals();
            return result;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        public bool ContainsValue (T item)
        {
            return !_Items.FirstOrDefault(redisObject => redisObject.Id == item.Id).IsNull;
        }

        public bool ContainsValue (string id)
        {
            return !_Items.FirstOrDefault(redisObject => redisObject.Id == id).IsNull;
        }

        private void UpdateInternals()
        {
            _MissingRedisObjectKeys = _Items.Where(redisObject => redisObject.IsNull).Select(redisObject => redisObject.Id.ToString());
            _IsFull = !MissingRedisObjectKeys.Any();
        }
    }
}