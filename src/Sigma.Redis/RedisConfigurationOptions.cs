﻿namespace Sigma.Redis
{
    public class RedisConfigurationOptions
    {
        public string EndPoint { get; set; }
        public int Port { get; set; }
    }
}