﻿using System.Collections.Generic;
using System.Linq;

namespace Sigma.Redis
{
    public class RedisList <T> : List<T>
    {
        public Dictionary<int, string> MissingItems = new Dictionary<int, string>();
        public bool IsFull => !MissingItems.Any();
    }
}