﻿using System;

namespace Sigma.Redis
{
    internal class RedisObject : IRedisObject
    {
        public string Id { get; set; }
        public DateTime DateInserted { get; set; }
        public bool IsNull { get; set; }
    }

    public interface IRedisObject
    {
        string Id { get; set; }
        DateTime DateInserted { get; set; }
        bool IsNull { get; set; }
    }
}