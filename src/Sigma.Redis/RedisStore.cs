﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ProtoBuf;
using Sigma.Core.Domain;
using StackExchange.Redis;

namespace Sigma.Redis
{
    /// The reason why no await is used and compiler warring 4014 being surpassed is due to the following behaviour.
    /// https://stackoverflow.com/questions/25976231/stackexchange-redis-transaction-methods-freezes
    /// Index with a score of 0 means that the item isn't present in redis, with a score of 1 means that it's present in redis.
    /// <summary>
    ///   Redis Store.
    /// TODO: Functional tests!!!!!.
    /// </summary>
    public class RedisStore : IDisposable
    {
        private readonly TimeSpan _DefaultTtl = TimeSpan.FromDays(30);
        private readonly IConnectionMultiplexer _IConnectionMultiplexer;

        public RedisStore (IConnectionMultiplexer connectionMultiplexer)
        {
            _IConnectionMultiplexer = connectionMultiplexer;
        }

        /// <summary>
        ///   Set indexes.
        /// </summary>
        public async void SetIndexesAsync <T> (IEnumerable<string> indexes)
        {
            await _IConnectionMultiplexer.GetDatabase().SortedSetAddAsync(KeyGen.Index<T>(), indexes.Select(index => new SortedSetEntry(index, 0)).ToArray(), 0);
        }

        public async Task<bool> SetSortedSetAsync <T> (Dictionary<string, double> dictionary, string sortName)
        {
#pragma warning disable 4014
            ITransaction transaction = _IConnectionMultiplexer.GetDatabase().CreateTransaction();
            ICollection<SortedSetEntry> sortedSetEntry = new List<SortedSetEntry>();
            foreach (KeyValuePair<string, double> keyValuePair in dictionary)
                sortedSetEntry.Add(new SortedSetEntry(keyValuePair.Key, keyValuePair.Value));
            transaction.SortedSetAddAsync(KeyGen.SortName<T>(sortName), sortedSetEntry.ToArray());
#pragma warning restore 4014
            return await transaction.ExecuteAsync();
        }

        /// <summary>
        ///   Check whenever item exists, based on indexes.
        /// </summary>
        public async Task<bool> IsExistsAsync <T> (string key) where T : IBaseDomainObject
        {
            return (int)await _IConnectionMultiplexer.GetDatabase().SortedSetScoreAsync(KeyGen.Index<T>(), key) == 0;
        }

        // Flag: Has Dispose already been called?
        /// <summary>
        ///   Set new redis object.
        /// </summary>
        public async Task<bool> SetAsync <T> (T value, IEnumerable<RedisSort<T>> sorts = null, TimeSpan? ttl = null) where T : class, IBaseDomainObject
        {
#pragma warning disable 4014
            if (ttl == null)
                ttl = _DefaultTtl;
            string key = KeyGen.Key<T>(value);
            ITransaction transaction = _IConnectionMultiplexer.GetDatabase().CreateTransaction();
            // Set object.
            using (var stream = new MemoryStream())
            {
                Serializer.Serialize(stream, value);
                transaction.StringSetAsync(key, stream.ToArray(), ttl);
            }
            // Set index.
            _IConnectionMultiplexer.GetDatabase().SortedSetAddAsync(KeyGen.Index<T>(), value.Id, 1);
            // Set sorts.
            foreach (RedisSort<T> redisSort in sorts ?? Enumerable.Empty<RedisSort<T>>())
                transaction.SortedSetAddAsync(KeyGen.SortName(redisSort), value.Id, redisSort.SortFunc.Invoke(value));
            // Execute transaction.
            return await transaction.ExecuteAsync();
#pragma warning disable 4014
        }

        /// <summary>
        ///   Set new redis objects.
        /// </summary>
        public async Task<bool> SetAsync <T> (IEnumerable<T> values, IEnumerable<RedisSort<T>> sorts = null, TimeSpan? ttl = null) where T : class, IBaseDomainObject
        {
#pragma warning disable 4014
            if (ttl == null)
                ttl = _DefaultTtl;
            ITransaction transaction = _IConnectionMultiplexer.GetDatabase().CreateTransaction();
            ICollection<SortedSetEntry> indexList = new List<SortedSetEntry>();
            if (sorts == null)
                foreach (T value in values)
                {
                    // Set value.
                    using (var stream = new MemoryStream())
                    {
                        Serializer.Serialize(stream, value);
                        transaction.StringSetAsync(KeyGen.Key<T>(value), stream.ToArray(), ttl);
                    }
                    // Update index list.
                    indexList.Add(new SortedSetEntry(value.Id, 1));
                }
            else
            {
                // Init sort list.
                var sortList = sorts.ToDictionary<RedisSort<T>, string, ICollection<SortedSetEntry>>(sort => sort.SortName, sort => new List<SortedSetEntry>());
                foreach (T value in values)
                {
                    // Set value.
                    using (var stream = new MemoryStream())
                    {
                        Serializer.Serialize(stream, value);
                        transaction.StringSetAsync(KeyGen.Key<T>(value), stream.ToArray(), ttl);
                    }
                    // Update index list.
                    indexList.Add(new SortedSetEntry(value.Id, 1));
                    // Update sort list.
                    foreach (RedisSort<T> sort in sorts)
                        sortList[sort.SortName].Add(new SortedSetEntry(value.Id, sort.SortFunc.Invoke(value)));
                    // Set sorts.
                    foreach (KeyValuePair<string, ICollection<SortedSetEntry>> sort in sortList)
                        transaction.SortedSetAddAsync(KeyGen.SortName<T>(sort.Key), sort.Value.ToArray());
                }
            }
            // Set index.
            transaction.SortedSetAddAsync(KeyGen.Index<T>(), indexList.ToArray());
            return await transaction.ExecuteAsync();
#pragma warning restore 4014
        }

        //TODO: Get methods with sorts and order.
        /// <summary>
        ///   Get object.
        /// </summary>
        public async Task<T> GetAsync <T> (string key)
        {
            RedisValue value = await _IConnectionMultiplexer.GetDatabase().StringGetAsync(KeyGen.Key<T>(key));
            if (value.IsNull)
                return default;
            using (var stream = new MemoryStream(value))
                return Serializer.Deserialize<T>(stream);
        }

        /// <summary>
        ///   Get objects.
        /// </summary>
        public async Task<RedisList<T>> GetAsync <T> (string[] keys)
        {
            RedisValue[] redisValues = await _IConnectionMultiplexer.GetDatabase().StringGetAsync(keys.Select(key => (RedisKey)KeyGen.Key<T>(key)).ToArray());
            var resultArr = new RedisList<T>();
            for (var i = 0; i < keys.Length; i++)
            {
                RedisValue redisValue = redisValues[i];
                if (redisValue.IsNull)
                {
                    // Fill missing data.
                    resultArr.Add(default);
                    resultArr.MissingItems.Add(i, keys[i]);
                }
                else
                    using (var stream = new MemoryStream(redisValue))
                        resultArr.Add(Serializer.Deserialize<T>(stream));
            }
            return resultArr;
        }

        /// <summary>
        ///   Remove object.
        /// </summary>
        public async Task<bool> RemoveAsync <T> (string key, IEnumerable<RedisSort<T>> redisSort = null)
        {
            ITransaction transaction = _IConnectionMultiplexer.GetDatabase().CreateTransaction();
#pragma warning disable 4014
            transaction.KeyDeleteAsync(KeyGen.Key<T>(key));
            // Remove sorts.
            foreach (RedisSort<T> sort in redisSort ?? Enumerable.Empty<RedisSort<T>>())
                transaction.SortedSetRemoveAsync(KeyGen.SortName(sort), key);
            // Remove index.v
            transaction.SortedSetRemoveAsync(KeyGen.Index<T>(), key);
#pragma warning restore 4014
            return await transaction.ExecuteAsync();
        }

        /// <summary>
        ///   Get pagedList.
        /// </summary>
        public async Task<RedisPagedList<T>> GetPagedListAsync <T> (int pageNumber, int pageSize, string sortName = "", Order order = Order.Ascending) where T : IBaseDomainObject
        {
            int rangeStart = (pageNumber - 1) * pageSize;
            int numberOfLastItemOnPage = rangeStart + pageSize - 1;
            var totalObjectItems = (int)await _IConnectionMultiplexer.GetDatabase().SortedSetLengthAsync(KeyGen.Index<T>());
            int rangeStop = numberOfLastItemOnPage > totalObjectItems ? totalObjectItems : numberOfLastItemOnPage;
            RedisValue[] sortKeys = await _IConnectionMultiplexer.GetDatabase().SortedSetRangeByRankAsync(KeyGen.SortName<T>(sortName.ToLower()), rangeStart, rangeStop, order);
            RedisValue[] redisRawObjectSort = await _IConnectionMultiplexer.GetDatabase().StringGetAsync(sortKeys.Select(key => (RedisKey)KeyGen.Key<T>(key)).ToArray());
            var redisObjects = new RedisList<T>();
            for (var i = 0; i < redisRawObjectSort.Length; i++)
            {
                RedisValue redisValue = redisRawObjectSort[i];
                if (redisValue.IsNull)
                {
                    redisObjects.Add(default);
                    redisObjects.MissingItems.Add(i, sortKeys[i]);
                }
                else
                    using (var stream = new MemoryStream(redisValue))
                        redisObjects.Add(Serializer.Deserialize<T>(stream));
            }
            return new RedisPagedList<T>(redisObjects, pageNumber, pageSize, totalObjectItems);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}