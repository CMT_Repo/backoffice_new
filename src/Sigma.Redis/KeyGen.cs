﻿using System;
using Sigma.Core.Domain;

namespace Sigma.Redis
{
    public static class KeyGen
    {
        private const string c_objectPrefix = "object";
        private const string c_sortPrefix = "sort";
        private const string c_indexPrefix = "index";
        private const string c_refreshToken = "refreshToken";

        public static string Key <T> (string id)
        {
            return $"{typeof (T).Name}:{c_objectPrefix}:{id}".ToLower();
        }

        public static string Key <T> (Guid id)
        {
            return $"{typeof (T).Name}:{c_objectPrefix}:{id.ToString()}".ToLower();
        }

        public static string Key <T> (IBaseDomainObject redisObject)
        {
            return $"{typeof (T).Name}:{c_objectPrefix}:{redisObject.Id}".ToLower();
        }

        public static string SortName <T> (RedisSort<T> sort)
        {
            return $"{typeof (T).Name}:{c_sortPrefix}:{sort.SortName}".ToLower();
        }

        public static string SortName <T> (string sortName)
        {
            return $"{typeof (T).Name}:{c_sortPrefix}:{sortName}".ToLower();
        }

        public static string Index <T>()
        {
            return $"{typeof (T).Name}:{c_indexPrefix}".ToLower();
        }

        public static string RefreshToken (string id)
        {
            return $"{c_refreshToken}:{id}";
        }
    }
}