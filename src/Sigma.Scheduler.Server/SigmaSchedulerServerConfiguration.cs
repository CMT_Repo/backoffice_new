﻿using System;
using System.Net;
using Microsoft.Extensions.Configuration;
using Sigma.Core;

namespace Sigma.Scheduler.Server
{
    public static class SigmaSchedulerServerConfiguration
    {
        public static string SentryDns => ConfigurationRoot["Sentry:Dsn"];
        public static string GrpcHost => ConfigurationRoot["Grpc:Host"];
        public static int GrpcPort => Convert.ToInt32(ConfigurationRoot["Grpc:Port"]);
        public static string GrpcRootCertPath => ConfigurationRoot["Grpc:RootCertPath"];
        public static string GrpcCertPath => ConfigurationRoot["Grpc:CertPath"];
        public static string GrpcCertKeyPath => ConfigurationRoot["Grpc:CertKeyPath"];
        public static IConfigurationRoot ConfigurationRoot { get; }
        public static bool IsDocker => ConfigurationHelper.IsDocker();
        public static IConfigurationSection LoggingConfiguration => ConfigurationRoot.GetSection("Logging");
        public static string HangfirePostgresConnectionString => ConfigurationRoot["Hangfire:ConnectionString"];
        public static string HangfirePostgresSchemeName => ConfigurationRoot["Hangfire:SchemaName"];
        public static string HangfireUrl => ConfigurationRoot["Hangfire:Url"];
        public static string RedisAddress => ConfigurationRoot["Redis:EndPoints:Default:Address"];
        public static int RedisPort => int.Parse(ConfigurationRoot["Redis:EndPoints:Default:Port"]);
        public static EndPoint RedisEndPoint => IPAddress.TryParse(RedisAddress, out IPAddress ipAddress) ? (EndPoint)new IPEndPoint(ipAddress, RedisPort) : new DnsEndPoint(RedisAddress, RedisPort);
        public static string SigmaRepositoryConnectionString => ConfigurationRoot["SigmaRepository:ConnectionString"];
        public static bool IsInitRedis => bool.Parse(ConfigurationRoot["SigmaRepository:IsInitRedis"]);
        public static string ChromeHeadlessBinaryPath => ConfigurationRoot["ChromeHeadless:BinaryPath"];
        public static string ChromeHeadlessDriverPath => ConfigurationRoot["ChromeHeadless:DriverPath"];
        public static bool ChromeHeadlessIsHeadless => Convert.ToBoolean(ConfigurationRoot["ChromeHeadless:IsHeadless"]);
        public static string QlikUrl => ConfigurationRoot["QlikSense:Url"];
        public static string QlikUserName => ConfigurationRoot["QlikSense:UserName"];
        public static string QlikPassword => ConfigurationRoot["QlikSense:Password"];
        public static string QlikDailyReportHttpFolderUrl => ConfigurationRoot["QlikSense:DailyReportHttpFolderUrl"];
        public static string QlickDailyReportDownloadDirectory => ConfigurationRoot["QlikSense:DownloadDirectory"];


        static SigmaSchedulerServerConfiguration()
        {
            ConfigurationRoot = ConfigurationHelper.InitConfigurationRoot();
        }
    }
}