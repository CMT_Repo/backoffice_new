﻿namespace Sigma.Scheduler.Server
{
    internal class SigmaSchedulerServerConstants
    {
        internal const string QueueDefault = "default";
        internal const string QueueBiEmailReports = "bi-email-report";
    }
}