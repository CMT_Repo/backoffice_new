﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using Sigma.Core.Domain;
using Sigma.Data;
using sigma.scheduler;
using SmtpClient = System.Net.Mail.SmtpClient;

namespace Sigma.Scheduler.Server
{
    public class BiReportEmailService : SigmaSchedulerGrpc.SigmaSchedulerGrpcBase
    {
        private readonly IServiceProvider _ServiceProvider;

        public BiReportEmailService (IServiceProvider serviceProvider)
        {
            _ServiceProvider = serviceProvider;
        }

        public override Task<Empty> BiReportEmailAddOrUpdate (SigmaRecurringJob request, ServerCallContext context)
        {
            RecurringJob.AddOrUpdate(request.Id, () => BiReportEmailJobAsync(request.Id), request.Cron, TimeZoneInfo.Utc, SigmaSchedulerServerConstants.QueueBiEmailReports);
            return Task.FromResult(new Empty());
        }

        public override Task<Empty> BiReportEmailExecute (StringValue request, ServerCallContext context)
        {
            Task.Run(() => BiReportEmailJobAsync(request.Value));
            return Task.FromResult(new Empty());
        }

        public override Task<Empty> BiReportEmailDelete (StringValue request, ServerCallContext context)
        {
            RecurringJob.RemoveIfExists(request.Value);
            return Task.FromResult(new Empty());
        }

        public async Task BiReportEmailJobAsync (string id)
        {
            using IServiceScope scope = _ServiceProvider.CreateScope();
            SigmaRepository sigmaRepository = scope.ServiceProvider.GetRequiredService<SigmaRepository>();
            ILogger logger = scope.ServiceProvider.GetRequiredService<ILoggerFactory>().CreateLogger(typeof (SchedulerGrpcServerWorker));
            RepositoryResponse<BiReportEmailJob> biReportEmailJobFindById = sigmaRepository.SchedulerJobRepositorySet.BiReportEmailJobFindById(id);
            if (!biReportEmailJobFindById.IsSuccess)
            {
                logger.LogError($"Can't find {typeof(BiReportEmailJob)} with the ID of {id}");
                return;
            }
            BiReportEmailJob biReportEmailJob = biReportEmailJobFindById.Data;
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("window-size=1920,1080");
            if (SigmaSchedulerServerConfiguration.ChromeHeadlessIsHeadless)
                chromeOptions.AddArguments("headless");
            chromeOptions.BinaryLocation = SigmaSchedulerServerConfiguration.ChromeHeadlessBinaryPath;
            using var driver = new ChromeDriver(SigmaSchedulerServerConfiguration.ChromeHeadlessDriverPath, chromeOptions) { Url = SigmaSchedulerServerConfiguration.QlikUrl };
            // Login
            driver.FindElement(By.Name("username")).SendKeys(SigmaSchedulerServerConfiguration.QlikUserName);
            driver.FindElement(By.Name("pwd")).SendKeys(SigmaSchedulerServerConfiguration.QlikPassword);
            driver.FindElement(By.Id("loginbtn")).Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("q-hub")));
            try
            {
                // Start getting sheets.
                var fileNames = new List<string>();
                foreach (string reportUrl in biReportEmailJob.ReportUrls)
                {
                    driver.Url = reportUrl;
                    // Wait for when it's finished loading.
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("sheet-title-logo-img")));
                    wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("rain-loader")));
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id("grid-wrap")));
                    Thread.Sleep(15000);
                    // Taking screenshot of view port instead of full page screenshot. Hopefully by the time it's a recruitment it should be implemented https://bugs.chromium.org/p/chromedriver/issues/detail?id=294
                    Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
                    using (var memoryStream = new MemoryStream(ss.AsByteArray))
                    {
                        var originalBitmap = new Bitmap(memoryStream);
                        var rectangle = new Rectangle(0, 132, originalBitmap.Width, originalBitmap.Height);
                        var newBitmap = new Bitmap(rectangle.Width, rectangle.Height);
                        var graphics = Graphics.FromImage(newBitmap);
                        graphics.DrawImage(originalBitmap, -rectangle.X, -rectangle.Y);
                        string fileName = $@"Daily-Report_{biReportEmailJob.Name}_{DateTime.UtcNow.ToString("ddMMyyyy-HHmmss")}UTC.png";
                        newBitmap.Save($@"{SigmaSchedulerServerConfiguration.QlickDailyReportDownloadDirectory}\{fileName}", ImageFormat.Png);
                        fileNames.Add(fileName);
                    }
                }
                if (!fileNames.Any())
                    return;
                var smtpClient = new SmtpClient(biReportEmailJob.SmtpClient.Host, biReportEmailJob.SmtpClient.Port)
                                 {
                                     EnableSsl = biReportEmailJob.SmtpClient.IsSsl,
                                     Credentials = new NetworkCredential(biReportEmailJob.SmtpClient.UserName, biReportEmailJob.SmtpClient.Password)
                                 };
                var mailMessage = new MailMessage { From = new MailAddress(biReportEmailJob.SmtpClient.From), Subject = $"{biReportEmailJob.Title} {DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm")}UTC", IsBodyHtml = true };
                foreach (string recipient in biReportEmailJob.Recipients)
                    mailMessage.To.Add(recipient);
                var htmlImgContent = new StringBuilder();
                // Construct htmlImgContent, add images as attachments.
                foreach (string fileName in fileNames)
                {
                    mailMessage.Attachments.Add(new Attachment($@"{SigmaSchedulerServerConfiguration.QlickDailyReportDownloadDirectory}\{fileName}"));
                    htmlImgContent.Append($@"<img src='{SigmaSchedulerServerConfiguration.QlikDailyReportHttpFolderUrl}/{fileName}' style='height: 100%;'> <br>");
                }
                mailMessage.Body = $"<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'><head><title></title><!--[if !mso]><!-- --><meta http-equiv='X-UA-Compatible' content='IE=edge'><!--<![endif]--><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><meta name='viewport' content='width=device-width, initial-scale=1.0'></head><body>{htmlImgContent}</body></html>";
                await smtpClient.SendMailAsync(mailMessage);
            }
            catch (Exception exception)
            {
                logger.LogError(exception, $"Email biReportEmailJob ID: {biReportEmailJob.Id}");
            }
            finally
            {
                driver.Quit();
            }
        }
    }
}