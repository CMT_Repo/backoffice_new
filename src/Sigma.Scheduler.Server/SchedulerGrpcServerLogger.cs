﻿using System;
using Microsoft.Extensions.Logging;

namespace Sigma.Scheduler.Server {
    public class SchedulerGrpcServerLogger : Grpc.Core.Logging.ILogger
    {
        public readonly ILogger Logger;

        public SchedulerGrpcServerLogger (ILogger logger)
        {
            Logger = logger;
        }

        public Grpc.Core.Logging.ILogger ForType <T>()
        {
            return new SchedulerGrpcServerLogger(Logger);
        }

        public void Debug (string message)
        {
            Logger.LogDebug(message);
        }

        public void Debug (string format, params object[] formatArgs)
        {
            Logger.LogDebug(format, formatArgs);
        }

        public void Info (string message)
        {
            Logger.LogInformation(message);
        }

        public void Info (string format, params object[] formatArgs)
        {
            Logger.LogInformation(format, formatArgs);
        }

        public void Warning (string message)
        {
            Logger.LogWarning(message);
        }

        public void Warning (string format, params object[] formatArgs)
        {
            Logger.LogWarning(format, formatArgs);
        }

        public void Warning (Exception exception, string message)
        {
            Logger.LogWarning(exception, message);
        }

        public void Error (string message)
        {
            Logger.LogError(message);
        }

        public void Error (string format, params object[] formatArgs)
        {
            Logger.LogError(format, formatArgs);
        }

        public void Error (Exception exception, string message)
        {
            Logger.LogError(exception, message);
        }
    }
}