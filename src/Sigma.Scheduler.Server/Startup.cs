using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sigma.Data;

namespace Sigma.Scheduler.Server
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup (IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services)
        {
            services.AddHangfire(options => options.UsePostgreSqlStorage(SigmaSchedulerServerConfiguration.HangfirePostgresConnectionString, new PostgreSqlStorageOptions
                                                                                                                                             {
                                                                                                                                                 SchemaName = SigmaSchedulerServerConfiguration.HangfirePostgresSchemeName,
                                                                                                                                                 PrepareSchemaIfNecessary = true
                                                                                                                                             }));
            services.AddHangfireServer(options => {
                                           options.Queues = new[]
                                                            {
                                                                SigmaSchedulerServerConstants.QueueDefault,
                                                                SigmaSchedulerServerConstants.QueueBiEmailReports
                                                            };
                                       });
            // Add sigma data.
            services.Configure<ConfigurationOptionsSigmaData>(options => {
                                                                  options.ConnectionString = SigmaSchedulerServerConfiguration.SigmaRepositoryConnectionString;
                                                                  options.RedisEndpoint = SigmaSchedulerServerConfiguration.RedisEndPoint;
                                                                  options.IsInitRedis = SigmaSchedulerServerConfiguration.IsInitRedis;
                                                              });
            services.AddSigmaData();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder applicationBuilder, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                applicationBuilder.UseDeveloperExceptionPage();
            applicationBuilder.UseSigmaData();
            applicationBuilder.UseHangfireDashboard();
            applicationBuilder.UseRouting();
            applicationBuilder.UseEndpoints(endpoints => {
                                 endpoints.MapGet("/", async context => { await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909"); });
                             });
        }
    }
}