﻿using System.Diagnostics;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.Logging;

namespace Sigma.Scheduler.Server
{
    internal class GrpcRequestLoggerInterceptor : Interceptor
    {
        private const string MessageTemplate = "{RequestMethod} responded {StatusCode} in {Elapsed:0.0000} ms";
        private readonly ILogger _Logger;

        public GrpcRequestLoggerInterceptor (ILogger logger = null)
        {
            _Logger = logger;
        }

        // TODO: Add params.
        public override async Task<TResponse> UnaryServerHandler <TRequest, TResponse> (TRequest request, ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
        {
            var sw = Stopwatch.StartNew();
            TResponse response = await base.UnaryServerHandler(request, context, continuation);
            sw.Stop();
            _Logger.LogInformation(MessageTemplate, context.Method, context.Status.StatusCode, sw.Elapsed.TotalMilliseconds);
            return response;
        }
    }
}