using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Sigma.Scheduler.Server
{
    public class Program
    {
        public static void Main (string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder (string[] args) => Host.CreateDefaultBuilder(args)
                                                                            .ConfigureLogging(loggingBuilder => {
                                                                                                  loggingBuilder.ClearProviders();
                                                                                                  loggingBuilder.AddConfiguration(SigmaSchedulerServerConfiguration.LoggingConfiguration);
                                                                                                  loggingBuilder.AddConsole();
                                                                                                  loggingBuilder.AddSentry(options => {
                                                                                                                               options.MinimumEventLevel = LogLevel.Information;
                                                                                                                               options.Dsn = SigmaSchedulerServerConfiguration.SentryDns;
                                                                                                                               options.MaxQueueItems = 100;
                                                                                                                               options.ShutdownTimeout = TimeSpan.FromSeconds(60);
                                                                                                                           });

                                                                                              })
                                                                            .ConfigureWebHostDefaults(webBuilder => {
                                                                                                          webBuilder.UseUrls(SigmaSchedulerServerConfiguration.HangfireUrl);
                                                                                                          webBuilder.UseStartup<Startup>();
                                                                                                      })
                                                                            .UseWindowsService()
                                                                            .ConfigureServices((hostContext, services) => {
                                                                                                   services.AddHostedService<SchedulerGrpcServerWorker>();
                                                                                               });
    }
}