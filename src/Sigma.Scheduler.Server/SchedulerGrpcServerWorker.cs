﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Grpc.Core.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using sigma.scheduler;
using ILogger = Microsoft.Extensions.Logging.ILogger;
using LogLevel = Grpc.Core.Logging.LogLevel;

namespace Sigma.Scheduler.Server
{
    public class SchedulerGrpcServerWorker : BackgroundService
    {
        private readonly ILogger _Logger;
        private readonly ILoggerFactory _LoggerFactory;
        private Grpc.Core.Server _GrpcServer;
        private IServiceProvider _ServiceProvider;

        public SchedulerGrpcServerWorker (IServiceProvider services)
        {
            _LoggerFactory = services.GetService<ILoggerFactory>();
            _Logger = _LoggerFactory.CreateLogger(typeof (SchedulerGrpcServerWorker));
            _ServiceProvider = services;
        }

        protected override async Task ExecuteAsync (CancellationToken stoppingToken)
        {
            Environment.SetEnvironmentVariable("GRPC_VERBOSITY", "DEBUG");
            GrpcEnvironment.SetLogger(new LogLevelFilterLogger(new SchedulerGrpcServerLogger(_Logger), LogLevel.Debug));
            if (stoppingToken.IsCancellationRequested)
            {
                await _GrpcServer.ShutdownAsync();
                _Logger.LogInformation("Shutting down gGRPC Scheduler server worker");
                return;
            }
            // TODO: Unified config.
            _Logger.LogInformation("Starting gGRPC Scheduler server worker");
            string rootCert = File.ReadAllText(SigmaSchedulerServerConfiguration.GrpcRootCertPath);
            string cert = File.ReadAllText(SigmaSchedulerServerConfiguration.GrpcCertPath);
            string certKey = File.ReadAllText(SigmaSchedulerServerConfiguration.GrpcCertKeyPath);
            var keyPair = new KeyCertificatePair(cert, certKey);
            var sslCredentials = new SslServerCredentials(new List<KeyCertificatePair> { keyPair }, rootCert, true);
            _GrpcServer = new Grpc.Core.Server
                          {
                              Services =
                              {
                                  SigmaSchedulerGrpc.BindService(new BiReportEmailService(_ServiceProvider)).Intercept(new GrpcRequestLoggerInterceptor(_LoggerFactory.CreateLogger(typeof (GrpcRequestLoggerInterceptor))))
                              },
                              Ports = { new ServerPort(SigmaSchedulerServerConfiguration.GrpcHost, SigmaSchedulerServerConfiguration.GrpcPort, sslCredentials) }
                          };
            _GrpcServer.Start();
            _Logger.LogInformation("Now listening on localhost:52979");
        }
    }
}