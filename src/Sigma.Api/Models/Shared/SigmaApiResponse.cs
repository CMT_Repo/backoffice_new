﻿using System.Collections.Generic;

namespace Sigma.Api.Models.Shared
{
    public interface ISigmaApiResponse
    {
        public bool IsSuccess { get; set; }
        public IEnumerable<string> ErrorMessages { get; set; }
    }

    public class SigmaApiResponse : ISigmaApiResponse
    {
        public bool IsSuccess { get; set; }
        public IEnumerable<string> ErrorMessages { get; set; }

        public static SigmaApiResponse Failed (IEnumerable<string> errorMessages)
        {
            return new SigmaApiResponse
                   {
                       IsSuccess = false,
                       ErrorMessages = errorMessages
                   };
        }

        public static SigmaApiResponse Failed (string errorMessage)
        {
            return new SigmaApiResponse
                   {
                       IsSuccess = false,
                       ErrorMessages = new[] { errorMessage }
                   };
        }

        public static SigmaApiResponse Success()
        {
            return new SigmaApiResponse { IsSuccess = true };
        }
    }
}