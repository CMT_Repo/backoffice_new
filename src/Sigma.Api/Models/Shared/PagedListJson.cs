﻿using X.PagedList;

namespace Sigma.Api.Models.Shared
{
    public class PagedListJson
    {
        public object Data { get; set; }
        public PagedListMetaData MetaData { get; set; }
    }
}