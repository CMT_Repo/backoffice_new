﻿using System.Collections.Generic;

namespace Sigma.Api.Models.Users
{
    public class PermissionUpdateModel
    {
        public string Username { get; set; }
        public IEnumerable<int> Permissions { get; set; }
    }
}