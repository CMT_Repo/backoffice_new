﻿using System.ComponentModel.DataAnnotations;

namespace Sigma.Api.Models.Users
{
    public class UserRoleModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string RoleName { get; set; }
    }
}