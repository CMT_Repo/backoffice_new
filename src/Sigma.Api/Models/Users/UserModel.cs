﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sigma.Api.Models.Users
{
    public class UserModel
    {
        public string Id { get; set; }
        [EmailAddress]
        public string UserName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string Password { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}