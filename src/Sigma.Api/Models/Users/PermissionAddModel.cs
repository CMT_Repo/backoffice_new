﻿using System.ComponentModel.DataAnnotations;

namespace Sigma.Api.Models.Users
{
    public class PermissionAddModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public int Permission { get; set; }
    }
}