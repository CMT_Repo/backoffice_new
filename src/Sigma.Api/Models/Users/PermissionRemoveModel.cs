﻿using System.ComponentModel.DataAnnotations;

namespace Sigma.Api.Models.Users
{
    public class PermissionRemoveModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public int Permission { get; set; }
    }
}