﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sigma.Api.Models.Users
{
    public class RoleModel
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public string PermissionsString { get; set; }
    }
}