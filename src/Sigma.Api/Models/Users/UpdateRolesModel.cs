﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sigma.Api.Models.Users
{
    public class UpdateRolesModel
    {
        [Required]
        public string Username { get; set; }
        public IEnumerable<string> AddRoles { get; set; }
        public IEnumerable<string> RemoveRoles { get; set; }
    }
}