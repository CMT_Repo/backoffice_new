﻿using System;

namespace Sigma.Api.Models.EventCmt
{
    public class EventCmtModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UrlName { get; set; }
        public string ThankYouUrl { get; set; }
        public string EmailThankYouSubject { get; set; }
        public string EmailThankYouBody { get; set; }
        public string EmailApprovedSubject { get; set; }
        public string EmailApprovedBody { get; set; }
        public string EmailRejectedSubject { get; set; }
        public string EmailRejectedBody { get; set; }
        public string SmtpServerName { get; set; }

        public DateTime DateCreated { get; set; }
    }
}