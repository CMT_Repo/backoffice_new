﻿using System;

namespace Sigma.Api.Models.EventCmt
{
    public class EventCmtRegisterModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public int NumberOfSeats { get; set; }
        public int A { get; set; }
        public string SubAffiliate { get; set; }
        public string Cxd { get; set; }
        public string Vvar10 { get; set; }
        public string Language { get; set; }
        public string EventName { get; set; }
        public DateTime DateCreated { get; set; }
    }
}