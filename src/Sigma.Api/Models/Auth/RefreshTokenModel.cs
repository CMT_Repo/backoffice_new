﻿namespace Sigma.Api.Models.Auth
{
    public class RefreshTokenModel
    {
        public bool Succeeded { get; set; }
        public string Jwt { get; set; }

        public static RefreshTokenModel Failed()
        {
            return new RefreshTokenModel { Succeeded = false };
        }

        public static RefreshTokenModel Success(string jwt)
        {
            return new RefreshTokenModel { Succeeded = true, Jwt = jwt };
        }
    }
}