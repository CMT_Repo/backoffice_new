﻿namespace Sigma.Api.Models.SeoRedirect
{
    public class SeoRedirectModel
    {
        public int Id { get; set; }
        public int WebSiteId { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public string Result { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsMulti { get; set; }
        public bool IsPermanent { get; set; }
        public bool IsRegex { get; set; }
    }
}