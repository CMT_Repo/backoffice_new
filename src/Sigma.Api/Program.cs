using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sigma.Api.Extensions;

namespace Sigma.Api
{
    public class Program
    {
        public static void Main (string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder (string[] args) => Host.CreateDefaultBuilder(args)
                                                                            .ConfigureLogging(loggingBuilder => {
                                                                                                  loggingBuilder.ClearProviders();
                                                                                                  loggingBuilder.AddConfiguration(ConfigurationOptionsSigmaApi.LoggingConfiguration);
                                                                                                  loggingBuilder.AddConsole();
                                                                                                  loggingBuilder.AddSentryNormalized(); })
                                                                            .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}