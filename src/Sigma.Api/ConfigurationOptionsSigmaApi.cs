﻿using System.Linq;
using System.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting.Internal;
using Sigma.Core;

namespace Sigma.Api
{
    public static class ConfigurationOptionsSigmaApi
    {
        /// <summary>
        ///   Environment of the app Dev/Prod.
        /// </summary>
        public static HostingEnvironment Environment => ConfigurationHelper.GetEnvironment();

        public static IConfigurationRoot ConfigurationRoot { get; }

        /// <summary>
        ///   Indicates whether the app is running inside of a docker.
        /// </summary>
        public static bool IsDocker => ConfigurationHelper.IsDocker();
        /// <summary>
        ///   Sigma repository connection string of the database.
        /// </summary>
        public static string SigmaRepositoryConnectionString => ConfigurationRoot["SigmaRepository:ConnectionString"];

        /// <summary>
        ///   Sigma repository connection string of the database.
        /// </summary>
        public static bool IsInitRedis => bool.Parse(ConfigurationRoot["SigmaRepository:IsInitRedis"]);

        /// <summary>
        ///   Redis endpoint address.
        /// </summary>
        public static string RedisAddress => ConfigurationRoot["Redis:EndPoints:Default:Address"];

        /// <summary>
        ///   Redis endpoint port.
        /// </summary>
        public static int RedisPort => int.Parse(ConfigurationRoot["Redis:EndPoints:Default:Port"]);

        /// <summary>
        ///   Redis endpoint.
        /// </summary>
        public static EndPoint RedisEndPoint => IPAddress.TryParse(RedisAddress, out IPAddress ipAddress) ? (EndPoint)new IPEndPoint(ipAddress, RedisPort) : new DnsEndPoint(RedisAddress, RedisPort);

        /// <summary>
        ///   The Data Source Name of a given project in Sentry.
        /// </summary>
        public static string SentryDsn => ConfigurationRoot["Sentry:Dsn"];

        /// <summary>
        ///   The release version of the application.
        /// </summary>
        public static string Release => ConfigurationRoot["Api:Release"];

        /// <summary>
        ///   The API version.
        /// </summary>
        public static string ApiVersion => ConfigurationRoot["Api:Version"];

        /// <summary>
        ///   The allowed CORS domains.
        /// </summary>
        public static string[] Cors => ConfigurationRoot.GetSection("Cors").GetChildren().Select(config => config.Value).ToArray();

        /// <summary>
        ///   Logging configuration.
        /// </summary>
        public static IConfigurationSection LoggingConfiguration => ConfigurationRoot.GetSection("Logging");

        /// <summary>
        ///   Identity configuration.
        /// </summary>
        public static IConfigurationSection IdentityConfiguration => ConfigurationRoot.GetSection("Identity");

        /// <summary>
        ///   Forex gRPC host.
        /// </summary>
        public static string ForexGrpcHost => ConfigurationRoot["ForexGrpc:Host"];

        /// <summary>
        ///   Forex gRPC host.
        /// </summary>
        public static bool ForexGrpcIsSecured => bool.Parse(ConfigurationRoot["ForexGrpc:IsSecured"]);

        /// <summary>
        ///   Forex gRPC port.
        /// </summary>
        public static int ForexGrpcPort => int.Parse(ConfigurationRoot["ForexGrpc:Port"]);

        /// <summary>
        ///   Forex gRPC root cert path.
        /// </summary>
        public static string ForexGrpcRootCertPath => ConfigurationRoot["ForexGrpc:RootCertPath"];

        /// <summary>
        ///   Forex gRPC cert path.
        /// </summary>
        public static string ForexGrpcCertPath => ConfigurationRoot["ForexGrpc:CertPath"];

        /// <summary>
        ///   Forex gRPC cert key.
        /// </summary>
        public static string ForexGrpcCertKeyPath => ConfigurationRoot["ForexGrpc:CertKeyPath"];

        /// <summary>
        ///   Scheduler gRPC host.
        /// </summary>
        public static string SchedulerClientHost => ConfigurationRoot["SchedulerClient:Host"];

        /// <summary>
        ///   Scheduler gRPC host.
        /// </summary>
        public static bool SchedulerClientSecured => bool.Parse(ConfigurationRoot["SchedulerClient:IsSecured"]);

        /// <summary>
        ///   Scheduler gRPC port.
        /// </summary>
        public static int SchedulerClientPort => int.Parse(ConfigurationRoot["SchedulerClient:Port"]);

        /// <summary>
        ///   Scheduler gRPC root cert path.
        /// </summary>
        public static string SchedulerRootCertPath => ConfigurationRoot["SchedulerClient:RootCertPath"];

        /// <summary>
        ///   Scheduler gRPC cert path.
        /// </summary>
        public static string SchedulerCertPath => ConfigurationRoot["SchedulerClient:CertPath"];

        /// <summary>
        ///   Scheduler gRPC cert key.
        /// </summary>
        public static string SchedulerCertKeyPath => ConfigurationRoot["SchedulerClient:CertKeyPath"];

        static ConfigurationOptionsSigmaApi()
        {
            ConfigurationRoot = ConfigurationHelper.InitConfigurationRoot();
        }
    }
}