﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Forex.Grpc.DataTypes;
using Google.Protobuf.WellKnownTypes;
using Sigma.Api.Models.EventCmt;
using Sigma.Api.Models.SeoRedirect;
using Sigma.Api.Models.Users;
using Sigma.Identity;
using X.PagedList;

namespace Sigma.Api.Helpers
{
    internal static class ObjectMapper
    {
        public static IMapper Mapper { get; }

        static ObjectMapper()
        {
            var config = new MapperConfiguration(cfg => {
                                                     cfg.CreateMap<SigmaUser, UserModel>().ReverseMap();
                                                     cfg.CreateMap<IPagedList<SigmaUser>, IPagedList<UserModel>>().ConvertUsing(pagedList => new StaticPagedList<UserModel>(Mapper.Map<IEnumerable<UserModel>>(pagedList), pagedList.GetMetaData()));
                                                     cfg.CreateMap<EventCmtModel, ForexGrpcEventCmt>()
                                                        .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id ?? string.Empty))
                                                        .ForMember(d => d.DateCreated, opt => opt.MapFrom(src => Timestamp.FromDateTime(DateTime.SpecifyKind(src.DateCreated, DateTimeKind.Utc))))
                                                        .ReverseMap();
                                                     cfg.CreateMap<EventCmtRegisterModel, ForexGrpcEventCmtRegister>()
                                                        .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id ?? string.Empty))
                                                        .ForMember(d => d.Cxd, opt => opt.MapFrom(src => src.Cxd ?? string.Empty))
                                                        .ForMember(d => d.Language, opt => opt.MapFrom(src => src.Language ?? string.Empty))
                                                        .ForMember(d => d.Vvar10, opt => opt.MapFrom(src => src.Vvar10 ?? string.Empty))
                                                        .ForMember(d => d.SubAffiliate, opt => opt.MapFrom(src => src.SubAffiliate ?? string.Empty))
                                                        .ForMember(d => d.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber ?? string.Empty))
                                                        .ForMember(d => d.DateCreated, opt => opt.MapFrom(src => Timestamp.FromDateTime(DateTime.SpecifyKind(src.DateCreated, DateTimeKind.Utc))))
                                                        .ReverseMap();
                                                     cfg.CreateMap<SeoRedirectModel, ForexGrpcSeoRedirect>()
                                                        .ReverseMap();
                                                 });
            Mapper = config.CreateMapper();
        }
    }
}