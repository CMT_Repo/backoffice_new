﻿using System;
using Microsoft.Extensions.Logging;

namespace Sigma.Api.Extensions
{
    public static class LoggingBuilderExtension
    {
        /// <summary>
        ///   Adds the Sentry logging integration, normalized to sigma config.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <returns></returns>
        public static ILoggingBuilder AddSentryNormalized (this ILoggingBuilder builder)
        {
            return builder.AddSentry(options => {
                                         // TODO: Add minimum log level from inside of SigmaConfig.
                                         options.MinimumEventLevel = LogLevel.Information;
                                         options.Dsn = ConfigurationOptionsSigmaApi.SentryDsn;
                                         options.Release = ConfigurationOptionsSigmaApi.Release;
                                         options.Environment = ConfigurationOptionsSigmaApi.Environment.EnvironmentName;
                                         options.MaxQueueItems = 100;
                                         options.ShutdownTimeout = TimeSpan.FromSeconds(60);
                                     });
        }
    }
}