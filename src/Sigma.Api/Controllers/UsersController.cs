﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Sigma.Api.Models.Users;
using Sigma.Core.Domain;
using Sigma.Core.Extensions;
using Sigma.Identity;
using X.PagedList;

namespace Sigma.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        //private readonly SigmaRoleManager _RoleManager;
        private readonly SigmaUserManager _UserManager;

        public UsersController (SigmaUserManager userManager, SigmaRoleManager roleManager)
        {
            _UserManager = userManager;
        }

        [HttpPost("Add")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> AddAsync (UserModel model)
        {
            if (!ModelState.IsValid)
                return new StatusCodeResult(400);
            var user = new SigmaUser
                       {
                           Email = model.Email,
                           UserName = model.UserName,
                           FirstName = model.FirstName,
                           LastName = model.LastName
                       };
            IdentityResult identityResult = await _UserManager.CreateAsync(user, model.Password);
            return new JsonResult(identityResult) { StatusCode = 200 };
        }

        [HttpGet("Get")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> GetAsync (string userName)
        {
            SigmaUser user = await _UserManager.FindByNameAsync(userName);
            if (user == null)
                return new StatusCodeResult(500);
            return new JsonResult(user) { StatusCode = 200 };
        }

        [HttpPost("Update")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> UpdateAsync (UserModel model)
        {
            var user = new SigmaUser
                       {
                           Id = model.Id,
                           Email = model.Email,
                           UserName = model.UserName,
                           FirstName = model.FirstName,
                           LastName = model.LastName
                       };
            IdentityResult identityResult = await _UserManager.UpdateAsync(user, model.Password);
            return new JsonResult(identityResult) { StatusCode = 200 };
        }

        [HttpGet("PagedList")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<JsonResult> UsersPagedListAsync (int pageNumber, int pageSize, string sort, SortDirection? sortDirection)
        {
            IPagedList<SigmaUser> userPagedList = await _UserManager.SortedPagedListAsync(pageNumber, pageSize, sort, sortDirection);
            return userPagedList == null ? new JsonResult(400) : new JsonResult(new { data = userPagedList, metaData = userPagedList.GetMetaData() }) { StatusCode = 200 };
        }

        [HttpPost("addPermission")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> AddPermissionAsync (PermissionAddModel model)
        {
            if (!ModelState.IsValid)
                return new StatusCodeResult(400);
            // Check whether such claim exists.
            if (!Enum.IsDefined(typeof (SigmaPermission), model.Permission))
                return new StatusCodeResult(400);
            SigmaUser user = await _UserManager.FindByNameAsync(model.Username);
            if (user == null)
                return new StatusCodeResult(400);
            IdentityResult result = await _UserManager.AddPermissionAsync(user, model.Permission);
            if (result.Succeeded)
                return new StatusCodeResult(204);
            return new JsonResult(result.Errors) { StatusCode = 500 };
        }

        [HttpDelete("RemovePermission")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> RemovePermissionAsync (PermissionRemoveModel model)
        {
            if (!ModelState.IsValid)
                return new StatusCodeResult(400);
            // Check whether such claim exists.
            if (!Enum.IsDefined(typeof (SigmaPermission), model.Permission))
                return new StatusCodeResult(400);
            SigmaUser user = await _UserManager.FindByNameAsync(model.Username);
            if (user == null)
                return new StatusCodeResult(400);
            IdentityResult result = await _UserManager.RemovePermissionAsync(user, model.Permission);
            if (result.Succeeded)
                return new StatusCodeResult(204);
            return new JsonResult(result.Errors) { StatusCode = 500 };
        }

        [HttpPost("UpdatePermission")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> UpdateUserPermissionAsync (PermissionUpdateModel model)
        {
            // Check if all permissions exists, if even one permissions doesn't, nothing will be updated.
            foreach (int permissionInt in model.Permissions)
                if (!Enum.IsDefined(typeof (SigmaPermission), permissionInt))
                    return new StatusCodeResult(400);
            SigmaUser user = await _UserManager.FindByNameAsync(model.Username);
            if (user == null)
                return new StatusCodeResult(400);
            IdentityResult identityResult = await _UserManager.UpdatePermissionsAsync(user, model.Permissions);
            return new JsonResult(identityResult) { StatusCode = 200 };
        }

        [HttpDelete("Delete")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> DeleteUserAsync (string userName)
        {
            SigmaUser user = await _UserManager.FindByNameAsync(userName);
            return new JsonResult(await _UserManager.DeleteAsync(user)) { StatusCode = 200 };
        }

        [HttpGet("Permissions")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> PermissionsAsync (string name)
        {
            SigmaUser user = await _UserManager.FindByNameAsync(name);
            if (user == null)
                return new StatusCodeResult(400);
            Claim permissionsClaim = await _UserManager.GetPermissionClaimAsync(user);
            return new JsonResult(permissionsClaim.Value) { StatusCode = 200 };
        }

        [HttpGet("ClaimPermission")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> ClaimPermissionAsync (string userName)
        {
            SigmaUser user = await _UserManager.FindByNameAsync(userName);
            Claim permissionClaim = await _UserManager.GetPermissionClaimAsync(user);
            return new JsonResult(permissionClaim.Value.ToIntArray()) { StatusCode = 200 };
        }

        [HttpGet("sessions")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> SessionsAsync (string userName)
        {
            SigmaUser user = await _UserManager.FindByNameAsync(userName);
            IEnumerable<RefreshToken> refreshTokens = await _UserManager.GetRefreshTokensAsync(user);
            ICollection<dynamic> dynamicRefreshToken = new List<dynamic>();
            // Remove unnecessary data.
            foreach (RefreshToken refreshToken in refreshTokens)
                dynamicRefreshToken.Add(new
                                        {
                                            userAgent = refreshToken.UserAgent,
                                            ipAddress = refreshToken.IpAddress,
                                            dateCreated = refreshToken.DateCreated,
                                            dateLastUsed = refreshToken.DateLastUsed,
                                            signature = refreshToken.Signature
                                        });
            return new JsonResult(dynamicRefreshToken) { StatusCode = 200 };
        }

        [HttpGet("SessionsPagedList")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> SessionsPagedListAsync (string userName, int pageNumber, int pageSize, string sort, SortDirection? sortDirection)
        {
            SigmaUser user = await _UserManager.FindByNameAsync(userName);
            IPagedList<RefreshToken> refreshTokensPagedList = await _UserManager.GetRefreshTokensPagedListAsync(user, pageNumber, pageSize, sort, sortDirection);
            ICollection<dynamic> dynamicRefreshToken = new List<dynamic>();
            // Remove unnecessary data.
            foreach (RefreshToken refreshToken in refreshTokensPagedList)
                dynamicRefreshToken.Add(new
                                        {
                                            userAgent = refreshToken.UserAgent,
                                            ipAddress = refreshToken.IpAddress,
                                            dateCreated = refreshToken.DateCreated,
                                            dateLastUsed = refreshToken.DateLastUsed,
                                            signature = refreshToken.Signature
                                        });
            return new JsonResult(new { data = dynamicRefreshToken, metaData = refreshTokensPagedList.GetMetaData() }) { StatusCode = 200 };
        }

        [HttpDelete("sessionsRemove")]
        [SigmaIdentityAuthorize(SigmaPermission.Admin)]
        public async Task<IActionResult> SessionsRemoveAsync (string signature)
        {
            // Replace empty space with the plus sign.
            signature = signature.Replace(' ', '+');
            return new JsonResult(await _UserManager.RemoveRefreshTokenAsync(signature)) { StatusCode = 200 };
        }

        [HttpPost("AddRoleToUser")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleUserAdd })]
        public async Task<IActionResult> AddRoleToUserAsync (UserRoleModel model)
        {
            if (!ModelState.IsValid)
                return new StatusCodeResult(400);
            SigmaUser user = await _UserManager.FindByNameAsync(model.UserName);
            IdentityResult identityResult = await _UserManager.AddToRoleAsync(user, model.RoleName);
            return new JsonResult(identityResult) { StatusCode = 200 };
        }

        [HttpDelete("RemoveRoleFromUser")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleUserRemove })]
        public async Task<IActionResult> RemoveRoleFromUserAsync (string username, string roleName)
        {
            if (!ModelState.IsValid)
                return new StatusCodeResult(400);
            SigmaUser user = await _UserManager.FindByNameAsync(username);
            IdentityResult identityResult = await _UserManager.RemoveFromRoleAsync(user, roleName);
            return new JsonResult(identityResult) { StatusCode = 200 };
        }

        [HttpGet("GetUsersRoles")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleView })]
        public async Task<IActionResult> GetUsersRolesAsync (string userName)
        {
            if (string.IsNullOrEmpty(userName))
                return new StatusCodeResult(400);
            SigmaUser user = await _UserManager.FindByNameAsync(userName);
            return new JsonResult(await _UserManager.GetRolesAsync(user)) { StatusCode = 200 };
        }

        [HttpPost("UpdateRoles")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleUserUpdate })]
        public async Task<IActionResult> UpdateUserRolesAsync (UpdateRolesModel model)
        {
            if (!ModelState.IsValid)
                return new StatusCodeResult(400);
            SigmaUser user = await _UserManager.FindByNameAsync(model.Username);
            IdentityResult rolesToRemoveResult = await _UserManager.RemoveFromRolesAsync(user, model.RemoveRoles);
            IdentityResult rolesToAddResult = await _UserManager.AddToRolesAsync(user, model.AddRoles);
            if (rolesToAddResult.Succeeded && rolesToRemoveResult.Succeeded)
                return new JsonResult(IdentityResult.Success) { StatusCode = 200 };
            // Collect errors.
            var errors = new List<IdentityError>();
            if (!rolesToRemoveResult.Succeeded)
                errors.AddRange(rolesToRemoveResult.Errors);
            if (!rolesToAddResult.Succeeded)
                errors.AddRange(rolesToAddResult.Errors);
            return new JsonResult(IdentityResult.Failed(errors.ToArray())) { StatusCode = 200 };
        }

        [HttpGet("RoleUsers")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleView })]
        public async Task<IActionResult> GetUsersByRoleAsync (string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
                return new StatusCodeResult(400);
            return new JsonResult(await _UserManager.GetUsersInRoleAsync(roleName)) { StatusCode = 200 };
        }
    }
}