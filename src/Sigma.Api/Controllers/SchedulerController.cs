﻿using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sigma.Api.Models.Shared;
using Sigma.Core.Domain;
using Sigma.Data;
using sigma.scheduler;
using Sigma.Scheduler.Client;
using X.PagedList;

namespace Sigma.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SchedulerController : Controller
    {
        private readonly SigmaRepository _SigmaRepository;
        private readonly SigmaSchedulerGrpcClient _Scheduler;

        public SchedulerController (SigmaRepository sigmaRepository, SigmaSchedulerGrpcClient scheduler)
        {
            _SigmaRepository = sigmaRepository;
            _Scheduler = scheduler;
        }

        [AllowAnonymous]
        [HttpPost("BiReportEmailJobAdd")]
        public async Task<IActionResult> BiReportEmailJobAddAsync (BiReportEmailJob model)
        {
            RepositoryResponse<BiReportEmailJob> createResponse = await _SigmaRepository.SchedulerJobRepositorySet.AddAsync(model);
            if (createResponse.IsSuccess)
            {
                await _Scheduler.BiReportEmailAddOrUpdateAsync(new SigmaRecurringJob
                                                               {
                                                                   Id = createResponse.Data.Id,
                                                                   Cron = model.Cron
                                                               });
                return new JsonResult(SigmaApiResponse.Success()) { StatusCode = 201 };
            }
            return new JsonResult(SigmaApiResponse.Failed(createResponse.ErrorMessage)) { StatusCode = 200 };
        }

        [AllowAnonymous]
        [HttpPatch("BiReportEmailJobUpdate")]
        public async Task<IActionResult> BiReportEmailJobUpdateAsync (BiReportEmailJob model)
        {
            RepositoryResponse response = await _SigmaRepository.SchedulerJobRepositorySet.UpdateAsync(model);
            if (response.IsSuccess)
            {
                await _Scheduler.BiReportEmailAddOrUpdateAsync(new SigmaRecurringJob
                                                               {
                                                                   Id = model.Id,
                                                                   Cron = model.Cron
                                                               });
                return new JsonResult(SigmaApiResponse.Success()) { StatusCode = 200 };
            }
            return new JsonResult(SigmaApiResponse.Failed(response.ErrorMessage)) { StatusCode = 200 };
        }

        [AllowAnonymous]
        [HttpGet("BiReportEmailJobExecute")]
        public async Task<IActionResult> BiReportEmailJobExecuteAsync (string id)
        {
            await _Scheduler.BiReportEmailExecuteAsync(new StringValue { Value = id });
            return new JsonResult(SigmaApiResponse.Success()) { StatusCode = 200 };
        }

        [AllowAnonymous]
        [HttpDelete("BiReportEmailJobDelete")]
        public async Task<IActionResult> BiReportEmailJobDeleteAsync (string id)
        {
            await _SigmaRepository.SchedulerJobRepositorySet.BiReportEmailJobDelete(id);
            await _Scheduler.BiReportEmailDeleteAsync(new StringValue { Value = id });
            return new JsonResult(SigmaApiResponse.Success()) { StatusCode = 200 };
        }

        [AllowAnonymous]
        [HttpGet("BiReportEmailJobPagedList")]
        public async Task<IActionResult> BiReportEmailJobPagedListAsync (int pageNumber, int pageSize)
        {
            RepositoryResponse<IPagedList<BiReportEmailJob>> repositoryResponse = await _SigmaRepository.SchedulerJobRepositorySet.BiReportEmailJobPagedListAsync(pageNumber, pageSize);
            if (repositoryResponse.IsSuccess)
                return new JsonResult(new PagedListJson { Data = repositoryResponse.Data, MetaData = repositoryResponse.Data.GetMetaData() }) { StatusCode = 200 };
            return new JsonResult(repositoryResponse.ErrorMessage) { StatusCode = 200 };
        }
    }
}