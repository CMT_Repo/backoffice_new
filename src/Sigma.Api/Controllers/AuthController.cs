﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sigma.Api.Models.Auth;
using Sigma.Identity;

namespace Sigma.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly SigmaSignInManager _SignInManager;

        public AuthController (SigmaSignInManager signInManager)
        {
            _SignInManager = signInManager;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> LoginAsync ([FromForm] LoginModel model)
        {
            if (!ModelState.IsValid)
                return new StatusCodeResult(400);
            SignInJwtResult signInJwtResult = await _SignInManager.PasswordSignInJwtAsync(model.UserName, model.Password, false);
            return new JsonResult(signInJwtResult) { StatusCode = 200 };
        }

        [HttpPost("logout")]
        [AllowAnonymous]
        public async Task<IActionResult> LogoutAsync ([FromForm] string refreshToken)
        {
            if (refreshToken != "null" && !string.IsNullOrEmpty(refreshToken))
                await _SignInManager.SingOutJwtAsync(refreshToken);
            return new StatusCodeResult(204);
        }

        [HttpPost("refreshToken")]
        [AllowAnonymous]
        public async Task<IActionResult> RefreshTokenAsync ([FromForm] string refreshToken)
        {
            if (string.IsNullOrEmpty(refreshToken))
                return new StatusCodeResult(400);
            string jwtRefresh = await _SignInManager.JwtRefresh(refreshToken);
            if (string.IsNullOrEmpty(jwtRefresh))
                return new JsonResult(RefreshTokenModel.Failed()) { StatusCode = 200 };
            return new JsonResult(RefreshTokenModel.Success(jwtRefresh)) { StatusCode = 200 };
        }
    }
}