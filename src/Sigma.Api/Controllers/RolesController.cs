﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sigma.Api.Models.Users;
using Sigma.Core.Domain;
using Sigma.Identity;
using X.PagedList;

namespace Sigma.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : Controller
    {
        private readonly SigmaRoleManager _RoleManager;

        public RolesController (SigmaRoleManager roleManager)
        {
            _RoleManager = roleManager;
        }

        [HttpPost("add")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleAdd })]
        public async Task<IActionResult> AddRoleAsync (RoleModel model)
        {
            if (!ModelState.IsValid)
                return new StatusCodeResult(400);
            var sigmaRole = new SigmaRole
                            {
                                Name = model.Name,
                                PermissionsString = model.PermissionsString
                            };
            IdentityResult identityResult = await _RoleManager.CreateAsync(sigmaRole);
            return new JsonResult(identityResult) { StatusCode = 200 };
        }

        [HttpGet("role")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleView })]
        public IActionResult RoleAsync (string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
                return new StatusCodeResult(400);
            return new JsonResult(_RoleManager.Roles.FirstOrDefault(role => role.Name == roleName)) { StatusCode = 200 };
        }

        [HttpGet("roles")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleView })]
        public async Task<IActionResult> RolesAsync()
        {
            return new JsonResult(await _RoleManager.Roles.ToListAsync()) { StatusCode = 200 };
        }

        [HttpGet("pagedList")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleView })]
        public async Task<JsonResult> UsersPagedListAsync (int pageNumber, int pageSize, string sort, SortDirection? sortDirection)
        {
            IPagedList<SigmaRole> userPagedList = await _RoleManager.SortedPagedListAsync(pageNumber, pageSize, sort, sortDirection);
            return userPagedList == null ? new JsonResult(400) : new JsonResult(new { data = userPagedList, metaData = userPagedList.GetMetaData() }) { StatusCode = 200 };
        }

        [HttpDelete("remove")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleDelete })]
        public async Task<IActionResult> RemoveRoleAsync (string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
                return new StatusCodeResult(400);
            SigmaRole role = await _RoleManager.FindByNameAsync(roleName);
            return new JsonResult(await _RoleManager.DeleteAsync(role)) { StatusCode = 200 };
        }

        [HttpPost("update")]
        [SigmaIdentityAuthorize(new[] { SigmaPermission.Admin, SigmaPermission.RoleUpdate })]
        public async Task<IActionResult> UpdateRoleAsync (SigmaRole role)
        {
            if (role == null)
                return new StatusCodeResult(400);
            return new JsonResult(await _RoleManager.UpdateAsync(role)) { StatusCode = 200 };
        }
    }
}