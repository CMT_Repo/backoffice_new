﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sigma.Api.Models.Shared;
using Sigma.Core.Domain;
using Sigma.Data;
using X.PagedList;

namespace Sigma.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmtpClientController
    {
        private readonly SigmaRepository _SigmaRepository;

        public SmtpClientController (SigmaRepository sigmaRepository)
        {
            _SigmaRepository = sigmaRepository;
        }

        [AllowAnonymous]
        [HttpPost("Add")]
        public async Task<IActionResult> AddAsync (SmtpClient model)
        {
            RepositoryResponse<SmtpClient> createResponse = await _SigmaRepository.SmtpClientRepositorySet.AddAsync(model);
            if (createResponse.IsSuccess)
                return new JsonResult(SigmaApiResponse.Success()) { StatusCode = 201 };
            return new JsonResult(SigmaApiResponse.Failed(createResponse.ErrorMessage)) { StatusCode = 200 };
        }

        [AllowAnonymous]
        [HttpPatch("Update")]
        public async Task<IActionResult> UpdateAsync (SmtpClient model)
        {
            RepositoryResponse response = await _SigmaRepository.SmtpClientRepositorySet.UpdateAsync(model);
            if (response.IsSuccess)
                return new JsonResult(SigmaApiResponse.Success()) { StatusCode = 200 };
            return new JsonResult(SigmaApiResponse.Failed(response.ErrorMessage)) { StatusCode = 200 };
        }

        [AllowAnonymous]
        [HttpDelete("Delete")]
        public async Task<IActionResult> DeleteAsync (string id)
        {
            RepositoryResponse response = await _SigmaRepository.SmtpClientRepositorySet.Delete(id);
            if(response.IsSuccess)
                return new JsonResult(SigmaApiResponse.Success()) { StatusCode = 200 };
            return new JsonResult(SigmaApiResponse.Failed(response.ErrorMessage)) { StatusCode = 200 };
        }

        [AllowAnonymous]
        [HttpGet("PagedList")]
        public async Task<IActionResult> PagedListAsync (int pageNumber, int pageSize)
        {
            RepositoryResponse<IPagedList<SmtpClient>> repositoryResponse = await _SigmaRepository.SmtpClientRepositorySet.PagedListAsync(pageNumber, pageSize);
            if (repositoryResponse.IsSuccess)
                return new JsonResult(new PagedListJson { Data = repositoryResponse.Data, MetaData = repositoryResponse.Data.GetMetaData() }) { StatusCode = 200 };
            return new JsonResult(repositoryResponse.ErrorMessage) { StatusCode = 200 };
        }

        [AllowAnonymous]
        [HttpGet("SmtpClientIdNameList")]
        public async Task<IActionResult> SmtpClientIdNameListAsync ()
        {
            RepositoryResponse<Dictionary<string, string>> repositoryResponse = await _SigmaRepository.SmtpClientRepositorySet.SmtpClientIdNameListAsync();
            if (repositoryResponse.IsSuccess)
                return new JsonResult(repositoryResponse.Data) { StatusCode = 200 };
            return new JsonResult(repositoryResponse.ErrorMessage) { StatusCode = 200 };
        }
    }
}