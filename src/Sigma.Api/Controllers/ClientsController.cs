﻿using System.Threading.Tasks;
using Forex.Grpc.UserManager;
using Microsoft.AspNetCore.Mvc;
using Sigma.Forex.Grpc;
using Sigma.Identity;

namespace Sigma.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientsController : Controller
    {
        private readonly ForexGrpcUserManagerClient _ForexGrpcUserManagerClient;

        public ClientsController (ForexGrpcUserManagerClient forexGrpcUserManagerClient)
        {
            _ForexGrpcUserManagerClient = forexGrpcUserManagerClient;
        }

        [HttpGet("ForexGetFullContactDetails")]
        [SigmaIdentityAuthorize(SigmaPermission.ClientsCrmGetFullContactDetails)]
        public async Task<JsonResult> ForexGetFullContactDetailsAsync (string searchQuery)
        {
            ForexGrpcGetFullContactDetailsResponse forexGrpcGetFullContactDetailsResponse = await _ForexGrpcUserManagerClient.GetFullContactDetailsAsync(new ForexGrpcGetFullContactDetailsRequest { SearchQuery = searchQuery });
            return new JsonResult(forexGrpcGetFullContactDetailsResponse) { StatusCode = 200 };
        }
    }
}