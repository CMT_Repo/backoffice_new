﻿using System;
using System.Threading.Tasks;
using Forex.Grpc.DataTypes;
using Forex.Grpc.DepositLog;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Mvc;
using Sigma.Api.Models.Shared;
using Sigma.Core.Extensions;
using Sigma.Forex.Grpc;
using Sigma.Identity;
using X.PagedList;

namespace Sigma.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepositLogController : Controller
    {
        private readonly ForexGrpcDepositLogClient _DepositLogClient;

        public DepositLogController (ForexGrpcDepositLogClient depositLogClient)
        {
            _DepositLogClient = depositLogClient;
        }

        [HttpGet("MetaData")]
        [SigmaIdentityAuthorize(SigmaPermission.DepositLogView)]
        public async Task<IActionResult> MetaDataAsync()
        {
            return new JsonResult(await _DepositLogClient.MetaDataAsync(new Empty())) { StatusCode = 200 };
        }

        [HttpGet("PagedList")]
        [SigmaIdentityAuthorize(SigmaPermission.DepositLogView)]
        public async Task<IActionResult> PagedListAsync (int pageNumber, int pageSize)
        {
            ForexGrpcDepositLogPagedListResponse grpcDepositLogPagedListResponse = await _DepositLogClient.PagedListAsync(new ForexGrpcDepositLogPagedListRequest { PageNumber = pageNumber, PageSize = pageSize });
            var staticPagedList = new StaticPagedList<ForexGrpcDepositAttempt>(grpcDepositLogPagedListResponse.DepositAttemps,
                                                                               grpcDepositLogPagedListResponse.PageNumber,
                                                                               grpcDepositLogPagedListResponse.PageSize,
                                                                               grpcDepositLogPagedListResponse.ItemCount);
            return new JsonResult(new PagedListJson { Data = staticPagedList, MetaData = staticPagedList.GetMetaData() }) { StatusCode = 200 };
        }

        [SigmaIdentityAuthorize(SigmaPermission.DepositLogView)]
        [HttpGet("DepositAttemptPspResults")]
        public async Task<IActionResult> DepositLogDepositAttemptPspResultsAsync (int depositAttemptId)
        {
            return new JsonResult(await _DepositLogClient.DepositAttemptResultsByDepositAttemptAsync(new Int32Value { Value = depositAttemptId })) { StatusCode = 200 };
        }

        [HttpGet("Filter")]
        [SigmaIdentityAuthorize(SigmaPermission.DepositLogView)]
        public async Task<IActionResult> FilterAsync (string id, string mt4Account, string mt4Order, string amount, string firstName,
                                                      string lastName, string email, string crmTransactionId, string currencies,
                                                      string paymentProviders, string creditCardAssociations, string countryISOs, bool isSuccessfulOnly, int pageNumber, int pageSize)
        {
            var depositLogFilterRequest = new ForexGrpcDepositLogFilterRequest
                                          {
                                              Id = id.ToInt(),
                                              Mt4Account = mt4Account.ToInt(),
                                              Mt4Order = mt4Order.ToInt(),
                                              Amount = amount.IsNullOrEmptySafe(),
                                              FirstName = firstName.IsNullOrEmptySafe(),
                                              LastName = lastName.IsNullOrEmptySafe(),
                                              Email = email.IsNullOrEmptySafe(),
                                              CrmTransactionId = crmTransactionId.IsNullOrEmptySafe(),
                                              Currencies = { currencies.ToIntArray().NullCheckSafe() },
                                              PaymentProviders = { paymentProviders.ToStringArray() },
                                              CreditCardAssociations = { creditCardAssociations.ToIntArray().NullCheckSafe() },
                                              CountryISOs = { countryISOs.ToStringArray() },
                                              IsSuccessfulOnly = isSuccessfulOnly,
                                              PageNumber = pageNumber,
                                              PageSize = pageSize
                                          };
            ForexGrpcDepositLogPagedListResponse depositLogPagedListResponse = await _DepositLogClient.FilterAsync(depositLogFilterRequest);
            var staticPagedList = new StaticPagedList<ForexGrpcDepositAttempt>(depositLogPagedListResponse.DepositAttemps,
                                                                               depositLogPagedListResponse.PageNumber,
                                                                               depositLogPagedListResponse.PageSize,
                                                                               depositLogPagedListResponse.ItemCount);
            return new JsonResult(new PagedListJson { Data = staticPagedList, MetaData = staticPagedList.GetMetaData() }) { StatusCode = 200 };
        }

        [HttpPost("DodUpdate")]
        [SigmaIdentityAuthorize(SigmaPermission.DepositLogDodChange)]
        public async Task<IActionResult> DodUpdateAsync ()
        {
            var forexGrpcDepositLogDodValue = new ForexGrpcDepositLogDodValue { Id = Convert.ToInt32(Request.Form["id"]), Value = Convert.ToBoolean(Request.Form["value"]) };
            await _DepositLogClient.DodAsync(forexGrpcDepositLogDodValue);
            return StatusCode(200);
        }
    }
}