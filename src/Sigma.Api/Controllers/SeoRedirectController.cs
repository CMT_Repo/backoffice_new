﻿using System.Threading.Tasks;
using Forex.Grpc.DataTypes;
using Forex.Grpc.SeoRedirects;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Mvc;
using Sigma.Api.Helpers;
using Sigma.Api.Models.SeoRedirect;
using Sigma.Api.Models.Shared;
using Sigma.Forex.Grpc;
using Sigma.Identity;
using X.PagedList;

namespace Sigma.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeoRedirectController : Controller
    {
        private readonly ForexGrpcSeoRedirectClient _SeoRedirectClient;

        public SeoRedirectController (ForexGrpcSeoRedirectClient seoRedirectClient)
        {
            _SeoRedirectClient = seoRedirectClient;
        }

        [HttpGet("PagedList")]
        [SigmaIdentityAuthorize(SigmaPermission.SeoRedirectView)]
        public async Task<JsonResult> SeoRedirectPagedListAsync (int pageNumber, int pageSize)
        {
            ForexGrpcRedirectsPagedListResponse forexGrpcRedirectsPagedListResponse = await _SeoRedirectClient.PagedListAsync(new ForexGrpcRedirectsPagedListRequest { PageNumber = pageNumber, PageSize = pageSize });
            IPagedList<ForexGrpcSeoRedirect> pageOrders = new StaticPagedList<ForexGrpcSeoRedirect>(forexGrpcRedirectsPagedListResponse.SeoRedirects, forexGrpcRedirectsPagedListResponse.PageNumber, forexGrpcRedirectsPagedListResponse.PageSize, forexGrpcRedirectsPagedListResponse.ItemCount);
            return new JsonResult(new PagedListJson { Data = pageOrders, MetaData = pageOrders.GetMetaData() }) { StatusCode = 200 };
        }

        [HttpGet("Websites")]
        [SigmaIdentityAuthorize(SigmaPermission.SeoRedirectView)]
        public async Task<IActionResult> WebSiteListAsync()
        {
            return new JsonResult(await _SeoRedirectClient.WebsitesAsync(new Empty())) { StatusCode = 200 };
        }

        [HttpPost("Add")]
        [SigmaIdentityAuthorize(SigmaPermission.SeoRedirectAdd)]
        public async Task<IActionResult> AddAsync (SeoRedirectModel model)
        {
            if (!ModelState.IsValid)
                return new StatusCodeResult(400);
            await _SeoRedirectClient.AddAsync(new ForexGrpcSeoRedirectAddRequest { SeoRedirect = ObjectMapper.Mapper.Map<SeoRedirectModel, ForexGrpcSeoRedirect>(model) });
            return new JsonResult(true) { StatusCode = 200 };
        }

        [HttpPost("Modify")]
        [SigmaIdentityAuthorize(SigmaPermission.SeoRedirectUpdate)]
        public async Task<IActionResult> ModifyAsync (SeoRedirectModel model)
        {
            if (!ModelState.IsValid)
                return new StatusCodeResult(400);
            await _SeoRedirectClient.ModifyAsync(new ForexGrpcSeoRedirectModifyRequest { SeoRedirect = ObjectMapper.Mapper.Map<SeoRedirectModel, ForexGrpcSeoRedirect>(model) });
            return new JsonResult(true) { StatusCode = 200 };
        }

        [HttpGet("Remove")]
        [SigmaIdentityAuthorize(SigmaPermission.SeoRedirectDelete)]
        public async Task<IActionResult> RemoveAsync (int id)
        {
            return new JsonResult(await _SeoRedirectClient.RemoveAsync(new ForexGrpcSeoRedirectRemoveRequest { SeoRedirectId = id }));
        }
    }
}