﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using CsvHelper;
using Forex.Grpc.DataTypes;
using Forex.Grpc.EventsCmt;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Mvc;
using Sigma.Api.Helpers;
using Sigma.Api.Models.EventCmt;
using Sigma.Api.Models.Shared;
using Sigma.Forex.Grpc;
using Sigma.Identity;
using X.PagedList;

namespace Sigma.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsCmtController : Controller
    {
        private readonly ForexGrpcEventsCmtClient _GrpcEventsCmtClient;

        public EventsCmtController (ForexGrpcEventsCmtClient depositLogClient)
        {
            _GrpcEventsCmtClient = depositLogClient;
        }

        [HttpGet("EventsCmtPagedList")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtView)]
        public async Task<IActionResult> EventsCmtPagedListAsync (int pageNumber, int pageSize)
        {
            ForexGrpcEventsCmtPagedListResponse grpcDepositLogPagedListResponse = await _GrpcEventsCmtClient.EventCmtPagedListAsync(new ForexGrpcEventsCmtPagedListRequest { PageNumber = pageNumber, PageSize = pageSize });
            var staticPagedList = new StaticPagedList<ForexGrpcEventCmt>(grpcDepositLogPagedListResponse.Events,
                                                                         grpcDepositLogPagedListResponse.PageNumber,
                                                                         grpcDepositLogPagedListResponse.PageSize,
                                                                         grpcDepositLogPagedListResponse.ItemCount);
            return new JsonResult(new PagedListJson { Data = staticPagedList, MetaData = staticPagedList.GetMetaData() }) { StatusCode = 200 };
        }

        [HttpPost("EventCmtAdd")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtAdd)]
        public async Task<IActionResult> EventCmtAddAsync (EventCmtModel model)
        {
            return new JsonResult(await EventCmtAddOrUpdateAsync(model)) { StatusCode = 200 };
        }

        [HttpPost("EventCmtUpdate")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtUpdate)]
        public async Task<IActionResult> EventCmtUpdateAsync (EventCmtModel model)
        {
            return new JsonResult(await EventCmtAddOrUpdateAsync(model)) { StatusCode = 200 };
        }

        private async Task<ForexGrpcEventCmtAddResponse> EventCmtAddOrUpdateAsync (EventCmtModel model)
        {
            return await _GrpcEventsCmtClient.EventCmtAddOrUpdateAsync(ObjectMapper.Mapper.Map<EventCmtModel, ForexGrpcEventCmt>(model));
        }

        [HttpGet("EventCmtSmtpServerNames")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtAdd)]
        public async Task<IActionResult> EventCmtSmtpServerNamesAsync()
        {
            ForexGrpcEventCmtSmtpServerNamesResponse smtpServerNames = await _GrpcEventsCmtClient.EventCmtSmtpServerNamesAsync(new Empty());
            return new JsonResult(smtpServerNames.SmtpServerNames) { StatusCode = 200 };
        }

        [HttpGet("EventsCmtNames")]
        public async Task<IActionResult> EventsCmtNamesAsync()
        {
            ForexGrpcEventsCmtNamesResponse grpcDepositLogPagedListResponse = await _GrpcEventsCmtClient.EventCmtNamesAsync(new Empty());
            return new JsonResult(grpcDepositLogPagedListResponse.EventNames) { StatusCode = 200 };
        }

        [HttpDelete("EventsCmtDelete")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtDelete)]
        public async Task<IActionResult> EventsCmtDeleteAsync (string id)
        {
            BoolValue eventCmtDelete = await _GrpcEventsCmtClient.EventCmtDeleteAsync(new StringValue { Value = id });
            return new JsonResult(eventCmtDelete.Value) { StatusCode = 200 };
        }

        [HttpGet("EventsCmtRegistersPagedList")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtRegistersView)]
        public async Task<IActionResult> EventsCmtRegistersPagedListAsync (int pageNumber, int pageSize, string eventName)
        {
            ForexGrpcEventCmtRegisgtersPagedListResponse grpcDepositLogPagedListResponse = await _GrpcEventsCmtClient.EventCmtRegisterPagedListAsync(new ForexGrpcEventCmtRegistersPagedListRequest { PageNumber = pageNumber, PageSize = pageSize, EventName = eventName });
            var staticPagedList = new StaticPagedList<ForexGrpcEventCmtRegister>(grpcDepositLogPagedListResponse.Registers,
                                                                                 grpcDepositLogPagedListResponse.PageNumber,
                                                                                 grpcDepositLogPagedListResponse.PageSize,
                                                                                 grpcDepositLogPagedListResponse.ItemCount);
            return new JsonResult(new PagedListJson { Data = staticPagedList, MetaData = staticPagedList.GetMetaData() }) { StatusCode = 200 };
        }

        [HttpGet("EventsCmtRegistersUpdateStatus")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtRegistersUpdateStatus)]
        public async Task<IActionResult> EventsCmtRegistersUpdateStatusAsync (string id, int status)
        {
            await _GrpcEventsCmtClient.EventCmtRegisterStatusUpdateAsync(new ForexGrpcEventCmtRegisgterUpdateStatusRequest { Id = id, Status = status });
            return new StatusCodeResult(200);
        }

        [HttpGet("EventsCmtRegistersExport")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtRegistersExport)]
        public async Task<FileStreamResult> EventsCmtRegistersExportAsync (string eventName)
        {
            ForexGrpcEventCmtRegisgterTolist cmtRegisterToList = await _GrpcEventsCmtClient.EventCmtRegisterByEventNameToListAsync(new StringValue { Value = eventName });
            var eventRegisters = new List<dynamic>();
            foreach (ForexGrpcEventCmtRegister forexGrpcEventCmtRegister in cmtRegisterToList.Registers)
            {
                string eventStatus = string.Empty;
                switch (forexGrpcEventCmtRegister.EventRegistrationStatus)
                {
                    case 0:
                        eventStatus = "New";
                        break;
                    case 1:
                        eventStatus = "Approved";
                        break;
                    case 2:
                        eventStatus = "Rejected";
                        break;
                }
                eventRegisters.Add(new
                                   {
                                       forexGrpcEventCmtRegister.FullName,
                                       forexGrpcEventCmtRegister.EventName,
                                       Email = forexGrpcEventCmtRegister.EmailAddress,
                                       forexGrpcEventCmtRegister.PhoneNumber,
                                       forexGrpcEventCmtRegister.NumberOfSeats,
                                       EventStatusStatus = eventStatus,
                                       forexGrpcEventCmtRegister.A,
                                       forexGrpcEventCmtRegister.SubAffiliate,
                                       forexGrpcEventCmtRegister.Cxd,
                                       Var10 = forexGrpcEventCmtRegister.Vvar10,
                                       forexGrpcEventCmtRegister.Language,
                                       DateCreated = forexGrpcEventCmtRegister.DateCreated.ToDateTime().ToUniversalTime().ToString("dd.MM.yyyy HH:mm"),
                                       DateModified = forexGrpcEventCmtRegister.DateModified.ToDateTime() == new DateTime(1970, 01, 01) ? string.Empty : forexGrpcEventCmtRegister.DateModified.ToDateTime().ToUniversalTime().ToString("dd.MM/yyyy HH:mm")
                                   });
            }
            //Deserialize model here
            await using var memoryStream = new MemoryStream();
            await using var streamWriter = new StreamWriter(memoryStream);
            using var csvWriter = new CsvWriter(streamWriter, CultureInfo.CurrentCulture);
            csvWriter.WriteRecords(eventRegisters);
            streamWriter.Flush();
            var result = new MemoryStream(memoryStream.ToArray());
            return new FileStreamResult(result, "text/csv") { FileDownloadName = "export.csv" };
        }

        [HttpPost("EventCmtRegisterAdd")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtRegistersAdd)]
        public async Task<IActionResult> EventCmtRegisterAddAsync (EventCmtRegisterModel model)
        {
            return new JsonResult(await EventCmtRegisterAddOrUpdateAsync(model)) { StatusCode = 200 };
        }

        [HttpPost("EventCmtRegisterUpdate")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtRegistersUpdate)]
        public async Task<IActionResult> EventCmtRegisterUpdateAsync (EventCmtRegisterModel model)
        {
            return new JsonResult(await EventCmtRegisterAddOrUpdateAsync(model)) { StatusCode = 200 };
        }

        [HttpDelete("EventsCmtRegisterDelete")]
        [SigmaIdentityAuthorize(SigmaPermission.EventCmtRegistersDelete)]
        public async Task<IActionResult> EventsCmtRegisterDeleteAsync (string id)
        {
            BoolValue eventCmtDelete = await _GrpcEventsCmtClient.EventCmtRegisterDeleteAsync(new StringValue { Value = id });
            return new JsonResult(eventCmtDelete.Value) { StatusCode = 200 };
        }

        private async Task<ForexGrpcEventCmtAddResponse> EventCmtRegisterAddOrUpdateAsync (EventCmtRegisterModel model)
        {
            return await _GrpcEventsCmtClient.EventCmtRegisterAddOrUpdateAsync(ObjectMapper.Mapper.Map<EventCmtRegisterModel, ForexGrpcEventCmtRegister>(model));
        }
    }
}