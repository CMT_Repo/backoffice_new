using System.IO.Compression;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sigma.Data;
using Sigma.Forex.Grpc;
using Sigma.Identity;
using Sigma.Scheduler.Client;

namespace Sigma.Api
{
    public class Startup
    {
        private const string CorsPolicyName = "SigmaApiCors";

        // Use this method to register services.
        public void ConfigureServices (IServiceCollection services)
        {
            // Add controllers.
            services.AddControllers();
            // Add and configure compression.
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);
            services.AddResponseCompression(options => { options.Providers.Add<GzipCompressionProvider>(); });
            // Add sigma data.
            services.Configure<ConfigurationOptionsSigmaData>(options => {
                                                                  options.ConnectionString = ConfigurationOptionsSigmaApi.SigmaRepositoryConnectionString;
                                                                  options.RedisEndpoint = ConfigurationOptionsSigmaApi.RedisEndPoint;
                                                                  options.IsInitRedis = ConfigurationOptionsSigmaApi.IsInitRedis;
                                                              });
            services.AddSigmaData();
            // Add sigma Identity.
            services.Configure<ConfigurationOptionsIdentityConfig>(options => options.ConfigurationSection = ConfigurationOptionsSigmaApi.IdentityConfiguration);
            services.AddSigmaIdentity();
            // Add CORS.
            services.AddCors(options => {
                                 options.AddPolicy(CorsPolicyName, builder => {
                                                                       builder.WithOrigins(ConfigurationOptionsSigmaApi.Cors);
                                                                       builder.AllowAnyMethod();
                                                                       builder.AllowAnyHeader();
                                                                   });
                             });

            // Add forex gRPC.
            services.Configure<ConfigurationOptionsForexGrpc>(options => {
                                                                  options.Host = ConfigurationOptionsSigmaApi.ForexGrpcHost;
                                                                  options.Port = ConfigurationOptionsSigmaApi.ForexGrpcPort;
                                                                  options.IsSecured = ConfigurationOptionsSigmaApi.ForexGrpcIsSecured;
                                                                  options.RootCertPath = ConfigurationOptionsSigmaApi.ForexGrpcRootCertPath;
                                                                  options.CertPath = ConfigurationOptionsSigmaApi.ForexGrpcCertPath;
                                                                  options.CertKeyPath = ConfigurationOptionsSigmaApi.ForexGrpcCertKeyPath;
                                                              });
            services.AddForex();
            // Add Scheduler client.
            services.Configure<ConfigurationOptionsSchedulerClient>(options => {
                                                                        options.Host = ConfigurationOptionsSigmaApi.SchedulerClientHost;
                                                                        options.Port = ConfigurationOptionsSigmaApi.SchedulerClientPort;
                                                                        options.IsSecured = ConfigurationOptionsSigmaApi.SchedulerClientSecured;
                                                                        options.RootCertPath = ConfigurationOptionsSigmaApi.SchedulerRootCertPath;
                                                                        options.CertPath= ConfigurationOptionsSigmaApi.SchedulerCertPath;
                                                                        options.CertKeyPath= ConfigurationOptionsSigmaApi.SchedulerCertKeyPath;
                                                                    });
            services.AddSchedulerClient();
        }

        // Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder applicationBuilder, IHostApplicationLifetime applicationLifetime)
        {
#if DEBUG
            applicationBuilder.UseDeveloperExceptionPage();
#endif
            // Use sigma components.
            applicationBuilder.UseSigmaData();
            applicationBuilder.UseForexGrpc();
            applicationBuilder.UseSchedulerClient();
            applicationBuilder.UseSigmaIdentity();
            // Use CORS.
            applicationBuilder.UseCors(CorsPolicyName);
            // Use static files.
            applicationBuilder.UseStaticFiles();
            // Use register routing logic.
            applicationBuilder.UseRouting();
            // Use auth.
            applicationBuilder.UseAuthentication();
            applicationBuilder.UseAuthorization();
            // Use execute routing logic.
            applicationBuilder.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            // Use compression.
            applicationBuilder.UseResponseCompression();
            // TODO: Register rest of the services EOL.
            // Cleanup operation :P
            applicationLifetime.ApplicationStopped.Register(ForexGrpcClientServiceCollectionExtensions.ShutDown);
            applicationLifetime.ApplicationStopped.Register(SchedulerGrpcClientCollectionExtensions.ShutDown);
        }
    }
}