﻿using Forex.Grpc.DepositLog;
using Forex.Grpc.EventsCmt;
using Forex.Grpc.SeoRedirects;
using Forex.Grpc.UserManager;

namespace Sigma.Forex.Grpc
{
    public class ForexGrpcSeoRedirectClient : ForexGrpcSeoRedirectsService.ForexGrpcSeoRedirectsServiceClient
    {
        public ForexGrpcSeoRedirectClient() : base(ForexGrpcClientServiceCollectionExtensions.Channel) { }
    }

    public class ForexGrpcUserManagerClient : ForexGrpcUserManagerService.ForexGrpcUserManagerServiceClient
    {
        public ForexGrpcUserManagerClient() : base(ForexGrpcClientServiceCollectionExtensions.Channel) { }
    }

    public class ForexGrpcDepositLogClient : ForexGrpcDepositLogService.ForexGrpcDepositLogServiceClient
    {
        public ForexGrpcDepositLogClient() : base(ForexGrpcClientServiceCollectionExtensions.Channel) { }
    }

    public class ForexGrpcEventsCmtClient : ForexGrpcEventsCmtService.ForexGrpcEventsCmtServiceClient
    {
        public ForexGrpcEventsCmtClient() : base(ForexGrpcClientServiceCollectionExtensions.Channel) { }
    }
}