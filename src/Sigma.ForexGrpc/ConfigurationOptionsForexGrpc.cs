﻿namespace Sigma.Forex.Grpc
{
    public class ConfigurationOptionsForexGrpc
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool IsSecured { get; set; }
        public string RootCertPath { get; set; }
        public string CertPath { get; set; }
        public string CertKeyPath { get; set; }
    }
}