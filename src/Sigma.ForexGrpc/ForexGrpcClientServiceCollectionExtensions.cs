﻿using System.IO;
using Grpc.Core;
using Grpc.Core.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Sigma.Forex.Grpc
{
    public static class ForexGrpcClientServiceCollectionExtensions
    {
        internal static Channel Channel;

        public static void AddForex (this IServiceCollection services)
        {
            services.AddScoped<ForexGrpcSeoRedirectClient>();
            services.AddScoped<ForexGrpcUserManagerClient>();
            services.AddScoped<ForexGrpcDepositLogClient>();
            services.AddScoped<ForexGrpcEventsCmtClient>();
        }

        public static void UseForexGrpc (this IApplicationBuilder applicationBuilder)
        {
            ConfigurationOptionsForexGrpc config = applicationBuilder.ApplicationServices.GetService<IOptions<ConfigurationOptionsForexGrpc>>().Value;
            if (config.IsSecured)
            {
                // TODO: Unified config.
                string cacert = File.ReadAllText(config.RootCertPath);
                string clientcert = File.ReadAllText(config.CertPath);
                string clientkey = File.ReadAllText(config.CertKeyPath);
                var sslCredentials = new SslCredentials(cacert, new KeyCertificatePair(clientcert, clientkey));
                Channel = new Channel(config.Host, config.Port, sslCredentials);
            }
            else
                Channel = new Channel(config.Host, config.Port, ChannelCredentials.Insecure);
            // TODO: Sentry Logger.
            GrpcEnvironment.SetLogger(new ConsoleLogger());
        }

        public static void ShutDown()
        {
            Channel.ShutdownAsync().Wait();
        }
    }
}