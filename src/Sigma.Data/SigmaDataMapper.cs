﻿using AutoMapper;
using Sigma.Core.Domain;
using Sigma.DbContext.Entities;

namespace Sigma.Data
{
    internal static class SigmaDataMapper
    {
        public static IMapper Mapper { get; }

        static SigmaDataMapper()
        {
            var config = new MapperConfiguration(cfg => {
                                                     cfg.CreateMap<SmtpClient, SmtpClientEntity>().ReverseMap();
                                                     cfg.CreateMap<BiReportEmailJobEntity, BiReportEmailJob>()
                                                        .ForMember(d => d.SmtpClient, opt => opt.MapFrom(src => Mapper.Map<SmtpClient>(src.SmtpClient)))
                                                        .ReverseMap()
                                                        .ForMember(d => d.SmtpClient, opt => opt.MapFrom(src => Mapper.Map<SmtpClientEntity>(src.SmtpClient)));
                                                 });
            Mapper = config.CreateMapper();
        }
    }
}