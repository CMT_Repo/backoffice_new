﻿using System;

namespace Sigma.Data
{
    /// <summary>
    ///   The direction in which to sequence elements.
    /// </summary>
    public enum Order
    {
        /// <summary>
        ///   Ordered from low values to high values.
        /// </summary>
        Ascending,
        /// <summary>
        ///   Ordered from high values to low values.
        /// </summary>
        Descending
    }

    /// <summary>
    ///   Convert sigma enum to redis enum, as means of abstraction.
    /// </summary>
    internal static class OrderExtensions
    {
        internal static StackExchange.Redis.Order ToRedis (this Order order)
        {
            switch (order)
            {
                case Order.Ascending:  return StackExchange.Redis.Order.Ascending;
                case Order.Descending: return StackExchange.Redis.Order.Descending;
                default:               throw new Exception("Unknown order enum.");
            }
        }
    }
}