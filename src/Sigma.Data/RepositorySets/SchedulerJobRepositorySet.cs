﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sigma.Core.Domain;
using Sigma.Core.Extensions;
using Sigma.DbContext.Entities;
using X.PagedList;

namespace Sigma.Data.RepositorySets
{
    public class SchedulerJobRepositorySet : RepositorySetBase<SchedulerJobRepositorySet>
    {
        public SchedulerJobRepositorySet (string connectionString, bool isInitRedis) : base(connectionString, isInitRedis) { }

        public override void InitRedis() { }

        public override void SetRedisSorts() { }

        public async Task<RepositoryResponse<BiReportEmailJob>> AddAsync (BiReportEmailJob biReportEmailJob)
        {
            // Check whether such name already exists;
            if (CheckIfBiReportEmailJobExists(biReportEmailJob.Name))
                return RepositoryResponse<BiReportEmailJob>.Failed("Duplicate event name");
            BiReportEmailJobEntity biReportEmailJobEntity = SigmaDataMapper.Mapper.Map<BiReportEmailJobEntity>(biReportEmailJob);
            DbContext.Entry(biReportEmailJobEntity.SmtpClient).State = EntityState.Unchanged;
            DbContext.BiReportEmailJobs.Add(biReportEmailJobEntity);
            await DbContext.SaveChangesAsync();
            biReportEmailJob = SigmaDataMapper.Mapper.Map<BiReportEmailJob>(biReportEmailJobEntity);
            return RepositoryResponse<BiReportEmailJob>.Success(biReportEmailJob);
        }

        public async Task<RepositoryResponse> UpdateAsync (BiReportEmailJob biReportEmailJob)
        {
            BiReportEmailJobEntity biReportEmailJobEntityOriginal = DbContext.BiReportEmailJobs.FirstOrDefault(x => x.Id == biReportEmailJob.Id);
            if (biReportEmailJobEntityOriginal.Name != biReportEmailJob.Name)
                if (CheckIfBiReportEmailJobExists(biReportEmailJob.Name))
                    return RepositoryResponse.Failed("Duplicate event name");
            BiReportEmailJobEntity biReportEmailJobEntity = SigmaDataMapper.Mapper.Map<BiReportEmailJobEntity>(biReportEmailJob);
            DbContext.Entry(biReportEmailJobEntity.SmtpClient).State = EntityState.Unchanged;
            DbContext.Entry(biReportEmailJobEntity).State = EntityState.Modified;
            await DbContext.SaveChangesAsync();
            return RepositoryResponse.Success();
        }

        private bool CheckIfBiReportEmailJobExists (string name)
        {
            return DbContext.BiReportEmailJobs.Any(x => x.Name.ToLower() == name.ToLower());
        }

        public async Task<RepositoryResponse<IPagedList<BiReportEmailJob>>> BiReportEmailJobPagedListAsync (int pageNumber, int pageSize)
        {
            IPagedList<BiReportEmailJob> biReportEmailJobs = (await DbContext.BiReportEmailJobs.Include(x => x.SmtpClient).OrderByDescending(x => x.DateCreated).ToPagedListAsync(pageNumber, pageSize)).ToMappedPagedList<BiReportEmailJobEntity, BiReportEmailJob>(SigmaDataMapper.Mapper);
            return RepositoryResponse<IPagedList<BiReportEmailJob>>.Success(biReportEmailJobs);
        }

        public async Task<RepositoryResponse> BiReportEmailJobDelete (string id)
        {
            var biReportEmailJobEntity = new BiReportEmailJobEntity { Id = id };
            DbContext.BiReportEmailJobs.Remove(biReportEmailJobEntity);
            await DbContext.SaveChangesAsync();
            return RepositoryResponse.Success();
        }

        public RepositoryResponse<BiReportEmailJob> BiReportEmailJobFindById (string id)
        {
            BiReportEmailJobEntity biReportEmailJobEntity = DbContext.BiReportEmailJobs.Include(x => x.SmtpClient).FirstOrDefault(x => x.Id == id);
            return RepositoryResponse<BiReportEmailJob>.Success(SigmaDataMapper.Mapper.Map<BiReportEmailJob>(biReportEmailJobEntity));

        }
    }
}