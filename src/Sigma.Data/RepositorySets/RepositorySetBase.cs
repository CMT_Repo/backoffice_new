﻿using System;
using Microsoft.Extensions.Logging;
using Sigma.DbContext;
using Sigma.Redis;

namespace Sigma.Data.RepositorySets
{
    public abstract class RepositorySetBase <T>
    {
        private readonly Lazy<SigmaDbContext> _DbContext;

        protected bool IsInitRedis;
        protected RedisStore Redis => new RedisStore(SigmaRepository.ConnectionMultiplexer);
        protected SigmaDbContext DbContext => _DbContext.Value;
        protected ILogger Logger => SigmaRepository.LoggerFactory.CreateLogger(typeof (T).ToString());

        protected RepositorySetBase (string connectionString, bool isInitRedis)
        {
            _DbContext = new Lazy<SigmaDbContext>(() => new SigmaDbContext(connectionString));
            IsInitRedis = isInitRedis;
        }

        public abstract void InitRedis();

        public abstract void SetRedisSorts();
    }
}