﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sigma.Core.Domain;
using Sigma.Core.Extensions;
using Sigma.DbContext.Entities;
using X.PagedList;

namespace Sigma.Data.RepositorySets
{
    public class SmtpClientRepositorySet : RepositorySetBase<SmtpClientRepositorySet>
    {
        public SmtpClientRepositorySet (string connectionString, bool isInitRedis) : base(connectionString, isInitRedis) { }

        public override void InitRedis() { }

        public override void SetRedisSorts() { }

        public async Task<RepositoryResponse<SmtpClient>> AddAsync (SmtpClient smtpClient)
        {
            // Check whether such name already exists;
            if (CheckIfExists(smtpClient.Name))
                return RepositoryResponse<SmtpClient>.Failed("Duplicate SMTP client name");
            SmtpClientEntity smtpClientEntity = SigmaDataMapper.Mapper.Map<SmtpClientEntity>(smtpClient);
            DbContext.SmtpClients.Add(smtpClientEntity);
            await DbContext.SaveChangesAsync();
            smtpClient = SigmaDataMapper.Mapper.Map<SmtpClient>(smtpClientEntity);
            return RepositoryResponse<SmtpClient>.Success(smtpClient);
        }

        public async Task<RepositoryResponse> UpdateAsync (SmtpClient smtpClient)
        {
            SmtpClientEntity smtpClientEntityOriginal = DbContext.SmtpClients.FirstOrDefault(x => x.Id == smtpClient.Id);
            if (smtpClientEntityOriginal.Name != smtpClient.Name)
                if (CheckIfExists(smtpClient.Name))
                    return RepositoryResponse.Failed("Duplicate SMTP client name");
            SmtpClientEntity smtpClientEntity = SigmaDataMapper.Mapper.Map<SmtpClientEntity>(smtpClient);
            DbContext.Entry(smtpClientEntity).State = EntityState.Modified;
            await DbContext.SaveChangesAsync();
            return RepositoryResponse.Success();
        }

        private bool CheckIfExists (string name)
        {
            return DbContext.SmtpClients.Any(x => x.Name.ToLower() == name.ToLower());
        }

        public async Task<RepositoryResponse<IPagedList<SmtpClient>>> PagedListAsync (int pageNumber, int pageSize)
        {
            IPagedList<SmtpClient> smtpClients = (await DbContext.SmtpClients.OrderByDescending(x => x.DateCreated).ToPagedListAsync(pageNumber, pageSize)).ToMappedPagedList<SmtpClientEntity, SmtpClient>(SigmaDataMapper.Mapper);
            return RepositoryResponse<IPagedList<SmtpClient>>.Success(smtpClients);
        }

        public async Task<RepositoryResponse> Delete (string id)
        {
            var smtpClient = new SmtpClientEntity { Id = id };
            if (DbContext.BiReportEmailJobs.Any(x => x.SmtpClientId == id))
                return RepositoryResponse.Failed("Can't be deleted, as there are active references");
            DbContext.SmtpClients.Remove(smtpClient);
            await DbContext.SaveChangesAsync();
            return RepositoryResponse.Success();
        }

        public RepositoryResponse<SmtpClient> FindById (string id)
        {
            SmtpClientEntity smtpClient = DbContext.SmtpClients.FirstOrDefault(x => x.Id == id);
            return RepositoryResponse<SmtpClient>.Success(SigmaDataMapper.Mapper.Map<SmtpClient>(smtpClient));
        }

        public async Task<RepositoryResponse<Dictionary<string, string>>> SmtpClientIdNameListAsync()
        {
            return RepositoryResponse<Dictionary<string, string>>.Success(await DbContext.SmtpClients.ToDictionaryAsync(smtpClientEntity => smtpClientEntity.Id, smtpClientEntity => smtpClientEntity.Name));
        }
    }
}