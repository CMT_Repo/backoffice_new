﻿using System.Net;

namespace Sigma.Data
{
    public class ConfigurationOptionsSigmaData
    {
        public string ConnectionString { get; set; }
        public EndPoint RedisEndpoint { get; set; }
        public bool IsInitRedis { get; set; }
    }
}