﻿using System;
using System.Reflection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sigma.Data.RepositorySets;
using StackExchange.Redis;

namespace Sigma.Data
{
    public class SigmaRepository
    {
        internal static ILoggerFactory LoggerFactory;
        internal static ConnectionMultiplexer ConnectionMultiplexer;

        private readonly Lazy<SchedulerJobRepositorySet> _SchedulerJobRepositorySet;
        private readonly Lazy<SmtpClientRepositorySet> _SmtpClientRepositorySet;

        public SchedulerJobRepositorySet SchedulerJobRepositorySet => _SchedulerJobRepositorySet.Value;
        public SmtpClientRepositorySet SmtpClientRepositorySet => _SmtpClientRepositorySet.Value;

        public SigmaRepository (IOptions<ConfigurationOptionsSigmaData> config)
        {
            _SchedulerJobRepositorySet = LazyInitRepositorySet<SchedulerJobRepositorySet>(config.Value.ConnectionString, config.Value.IsInitRedis);
            _SmtpClientRepositorySet = LazyInitRepositorySet<SmtpClientRepositorySet>(config.Value.ConnectionString, config.Value.IsInitRedis);
        }

        private Lazy<T> LazyInitRepositorySet <T> (string connectionString, bool isInitRedis) where T : RepositorySetBase<T>
        {
            ConstructorInfo constructor = typeof (T).GetConstructor(new[] { typeof (string), typeof (bool) });
            return new Lazy<T>(() => constructor.Invoke(new object[] { connectionString, isInitRedis }) as T);
        }
    }
}