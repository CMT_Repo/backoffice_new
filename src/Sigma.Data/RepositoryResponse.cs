﻿using System.IO;
using System.Runtime.CompilerServices;

namespace Sigma.Data
{
    public class RepositoryResponse
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsExists { get; set; }

        public static RepositoryResponse Success()
        {
            var result = new RepositoryResponse
                         {
                             IsSuccess = true,
                             IsExists = true
                         };
            return result;
        }

        public static RepositoryResponse NotFound()
        {
            var result = new RepositoryResponse
                         {
                             IsSuccess = false,
                             IsExists = false
                         };
            return result;
        }

        public static RepositoryResponse Failed (string errorMessage)
        {
            var result = new RepositoryResponse
                         {
                             IsSuccess = false,
                             IsExists = false,
                             ErrorMessage = errorMessage
                         };
            return result;
        }
    }

    public class RepositoryResponse <T> : RepositoryResponse
    {
        public T Data { get; set; }

        public static RepositoryResponse<T> Success (T data)
        {
            return new RepositoryResponse<T>
                   {
                       IsSuccess = true,
                       IsExists = true,
                       Data = data
                   };
        }

        public new static RepositoryResponse<T> NotFound()
        {
            var result = new RepositoryResponse<T>
                         {
                             IsSuccess = false,
                             IsExists = false
                         };
            return result;
        }

        public new static RepositoryResponse<T> Failed (string errorMessage)
        {
            var result = new RepositoryResponse<T>
                         {
                             IsSuccess = false,
                             IsExists = false,
                             ErrorMessage = errorMessage

                         };
            return result;
        }
    }
}