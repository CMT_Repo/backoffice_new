﻿using Sigma.Core.Domain;
using Sigma.Redis;

namespace Sigma.Data
{
    internal static class SigmaDataRedisProtoBuffModels
    {
        public static void Init()
        {
            RedisSerializationTypes.Add<BaseDomainObject>(new []{typeof (SmtpClient), typeof (BiReportEmailJob)});
            RedisSerializationTypes.Add<SmtpClient>();
            RedisSerializationTypes.Add<BiReportEmailJob>();
        }
    }
}