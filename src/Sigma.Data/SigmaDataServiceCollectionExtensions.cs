﻿using System;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sigma.Data.RepositorySets;
using Sigma.DbContext;
using StackExchange.Redis;

namespace Sigma.Data
{
    public static class SigmaDataServiceCollectionExtensions
    {
        public static void AddSigmaData (this IServiceCollection services)
        {
            services.AddScoped<SigmaRepository>();
        }

        public static void UseSigmaData (this IApplicationBuilder applicationBuilder)
        {
            // Get ConfigurationOptionsSigmaData.
            ConfigurationOptionsSigmaData configurationOptionsSigmaData = applicationBuilder.ApplicationServices.GetService<IOptions<ConfigurationOptionsSigmaData>>().Value;
            // Init logger
            SigmaRepository.LoggerFactory = applicationBuilder.ApplicationServices.GetService<ILoggerFactory>();
            // Init redis connection multiplexer.
            if (configurationOptionsSigmaData.RedisEndpoint == null)
                throw new Exception("RedisEndpoint can't be empty");
            var connectionMultiplexer = ConnectionMultiplexer.Connect(new ConfigurationOptions { EndPoints = { configurationOptionsSigmaData.RedisEndpoint }, AllowAdmin = true });
            SigmaRepository.ConnectionMultiplexer = connectionMultiplexer;
            if (string.IsNullOrEmpty(configurationOptionsSigmaData.ConnectionString))
                throw new Exception("Sigma repository connection string can't be empty");
            // Flush redis.
            connectionMultiplexer.GetServer(configurationOptionsSigmaData.RedisEndpoint).FlushAllDatabases();
            // Init ProtoBuf models.
            SigmaDataRedisProtoBuffModels.Init();
            // Init migrations.
            var sigmaDbContext = new SigmaDbContext(configurationOptionsSigmaData.ConnectionString);
            sigmaDbContext.Database.Migrate();
            sigmaDbContext.Database.EnsureCreated();

            // Find all objects which inherit from RepositorySetBase, and run their InitRedis and SetRedisSort methods.
            // Disable code bellow if you are during migration, as you don't want to seed redis during such operation.
            // Can't use any args params due to the following issue https://github.com/aspnet/EntityFrameworkCore/issues/8332.
            foreach (Type type in Assembly.GetExecutingAssembly().GetTypes())
                //if (type.IsSubclassOf(typeof(RepositorySetBase)))
                if (type.BaseType.IsGenericType && typeof (RepositorySetBase<>).IsAssignableFrom(type.BaseType.GetGenericTypeDefinition()))
                {
                    ConstructorInfo constructor = type.GetConstructor(new[] { typeof (string), typeof (bool) });
                    object instance = constructor.Invoke(new object[] { configurationOptionsSigmaData.ConnectionString, configurationOptionsSigmaData.IsInitRedis });
                    MethodInfo setRedisSortMethodInfo = type.GetMethod("SetRedisSorts");
                    setRedisSortMethodInfo.Invoke(instance, new object[] { });
                    MethodInfo initRedisMethodInfo = type.GetMethod("InitRedis");
                    initRedisMethodInfo.Invoke(instance, new object[] { });
                }
        }
    }
}