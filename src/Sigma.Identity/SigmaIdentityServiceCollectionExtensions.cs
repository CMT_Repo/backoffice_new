﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Sigma.Identity
{
    public static class SigmaIdentityServiceCollectionExtensions
    {
        public static void AddSigmaIdentity (this IServiceCollection services)
        {
            ServiceProvider serviceProvider = services.BuildServiceProvider();
            ConfigurationOptionsIdentityConfig configurationOptionsIdentityConfig = serviceProvider.GetService<IOptions<ConfigurationOptionsIdentityConfig>>().Value;
            // Init migrations.
            var optionsBuilder = new DbContextOptionsBuilder<SigmaIdentityDbContext>();
            optionsBuilder.UseNpgsql(configurationOptionsIdentityConfig.ConnectionString);
            var context = new SigmaIdentityDbContext(optionsBuilder.Options);
            context.Database.Migrate();
            context.Database.EnsureCreated();
            // AesCredentials setup.
            var aesCredentials = new AesCredentials { Key = configurationOptionsIdentityConfig.RefreshTokenAesKey, Vector = configurationOptionsIdentityConfig.RefreshTokenAesVector };
            // Init redis ProtoBuff models.
            SigmaIdentityRedisProtoBuffModels.Init();
            // DB context use to modify database, using dotnet ef.
            services.AddDbContext<SigmaIdentityDbContext>(options => options.UseNpgsql(configurationOptionsIdentityConfig.ConnectionString));
            // Identity config.
            services
                // Identity user.
                .AddIdentity<SigmaUser, SigmaRole>(identityOptions => {
                                                       identityOptions.User.RequireUniqueEmail = true;
                                                   })
                // Add custom role store.
                .AddRoleStore<SigmaRoleStore>()
                // Add custom user store.
                .AddUserStore<SigmaUserStore>()
                // Add custom claim factory.
                .AddClaimsPrincipalFactory<SigmaClaimsPrincipalFactory>()
                // Add custom role manager.
                .AddRoleManager<SigmaRoleManager>()
                // Add custom signin store.
                .AddSignInManager<SigmaSignInManager>()
                // Add custom user manager.
                .AddUserManager<SigmaUserManager>()
                // Add EF sigma identity Store.
                .AddEntityFrameworkStores<SigmaIdentityDbContext>().AddDefaultTokenProviders();
            // JWT config.
            services.AddAuthentication(configureOptions => {
                                           configureOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                                           configureOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                                       })
                    .AddJwtBearer(configureOptions => {
                                      configureOptions.RequireHttpsMetadata = false;
                                      configureOptions.SaveToken = true;
                                      configureOptions.ClaimsIssuer = configurationOptionsIdentityConfig.IssuerName;
                                      configureOptions.TokenValidationParameters = new TokenValidationParameters
                                                                                   {
                                                                                       ValidateIssuer = true,
                                                                                       ValidIssuer = configurationOptionsIdentityConfig.IssuerName,
                                                                                       ValidateAudience = true,
                                                                                       ValidAudience = configurationOptionsIdentityConfig.AudienceName,
                                                                                       IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configurationOptionsIdentityConfig.IssuerSigningKey)),
                                                                                       RequireExpirationTime = true,
                                                                                       ValidateLifetime = true,
                                                                                       ClockSkew = TimeSpan.Zero
                                                                                   };
                                      configureOptions.Events = new JwtBearerEvents
                                                                {
                                                                    OnAuthenticationFailed = authenticationFailedContext => {
                                                                                                 if (authenticationFailedContext.Exception.GetType() == typeof (SecurityTokenExpiredException))
                                                                                                     authenticationFailedContext.Response.Headers.Add("Token-Expired", "true");
                                                                                                 return Task.CompletedTask;
                                                                                             }
                                                                };
                                  });
            services.AddTransient<SigmaRoleStore>();
            services.AddTransient<SigmaUserStore>();
            services.AddTransient<IAuthorizationPolicyProvider, SigmaIdentityAuthPolicy>();
            services.AddTransient<SigmaClaimsPrincipalFactory>();
            services.AddSingleton<IAuthorizationHandler, SigmaIdentityAuthHandler>();
            // Add token factory.
            services.AddSingleton(serviceProvider => new TokenFactory(configurationOptionsIdentityConfig.IssuerName,
                                                                      configurationOptionsIdentityConfig.IssuerSigningKey,
                                                                      configurationOptionsIdentityConfig.AudienceName,
                                                                      aesCredentials,
                                                                      configurationOptionsIdentityConfig.JwtExpirationSeconds));
            services.AddAuthorization();
        }

        // Init admin user.
        public static void UseSigmaIdentity (this IApplicationBuilder applicationBuilder)
        {
            ConfigurationOptionsIdentityConfig configurationOptionsIdentityConfig = applicationBuilder.ApplicationServices.GetService<IOptions<ConfigurationOptionsIdentityConfig>>().Value;
            using IServiceScope scope = applicationBuilder.ApplicationServices.CreateScope();
            var userManager = (SigmaUserManager)scope.ServiceProvider.GetService(typeof (SigmaUserManager));
            SigmaUser admin = userManager.FindByNameAsync(configurationOptionsIdentityConfig.AdminUsername).Result;
            if (admin == null)
            {
                // Create admin user.
                admin = new SigmaUser
                        {
                            Email = configurationOptionsIdentityConfig.AdminUsername,
                            UserName = configurationOptionsIdentityConfig.AdminUsername,
                            FirstName = "admin",
                            LastName = "admin"
                        };
                userManager.CreateAsync(admin, configurationOptionsIdentityConfig.AdminPassword).Wait();
            }
            else
                // Set password from appsettings.
                userManager.SetPassword(admin, configurationOptionsIdentityConfig.AdminPassword).Wait();
            // Check if admin user claims contain admin and administration permission claim.
            List<SigmaPermission> adminPermissions = userManager.GetPermissionsEnumAsync(admin).Result;
            if (adminPermissions.Count == 0)
            {
                userManager.AddPermissionAsync(admin, SigmaPermission.Admin).Wait();
                userManager.AddPermissionAsync(admin, SigmaPermission.Administration).Wait();
            }
            // Add admin permission.
            if (!adminPermissions.Contains(SigmaPermission.Admin))
                userManager.AddPermissionAsync(admin, SigmaPermission.Admin).Wait();
            // Add administration permission.
            if (!adminPermissions.Contains(SigmaPermission.Administration))
                userManager.AddPermissionAsync(admin, SigmaPermission.Administration).Wait();
        }
    }
}