﻿using System;
using Microsoft.Extensions.Configuration;

namespace Sigma.Identity
{
    public class ConfigurationOptionsIdentityConfig
    {
        public string ConnectionString => ConfigurationSection["ConnectionString"];
        public string IssuerName => ConfigurationSection["IssuerName"];
        public string IssuerSigningKey => ConfigurationSection["IssuerSigningKey"];
        public string AudienceName => ConfigurationSection["AudienceName"];
        public string RefreshTokenAesKey => ConfigurationSection["RefreshTokenAesKey"];
        public string RefreshTokenAesVector => ConfigurationSection["RefreshTokenAesVector"];
        public double JwtExpirationSeconds => Convert.ToDouble(ConfigurationSection["JwtExpirationSeconds"]);
        public string AdminUsername => ConfigurationSection["AdminUsername"];
        public string AdminPassword => ConfigurationSection["AdminPassword"];
        public TimeSpan RefreshTokenRedisTtlMinutes => TimeSpan.FromMinutes(Convert.ToDouble(ConfigurationSection["RefreshTokenRedisTtlMinutes"]));
        public IConfigurationSection ConfigurationSection { get; set; }
    }
}
//TODO: Add logger.