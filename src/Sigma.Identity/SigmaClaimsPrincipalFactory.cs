﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Sigma.Core.Extensions;

namespace Sigma.Identity
{
    public class SigmaClaimsPrincipalFactory : UserClaimsPrincipalFactory<SigmaUser, SigmaRole>
    {
        public new SigmaUserManager UserManager { get; }

        public SigmaClaimsPrincipalFactory (SigmaUserManager userManager, SigmaRoleManager roleManager, IOptions<IdentityOptions> options) : base(userManager, roleManager, options)
        {
            UserManager = userManager;
        }

        // Add claims at this point. This is Authorization process.
        public override async Task<ClaimsPrincipal> CreateAsync (SigmaUser user)
        {
            string userId = await UserManager.GetUserIdAsync(user);
            string userName = await UserManager.GetUserNameAsync(user);
            // REVIEW: Used to match Application scheme
            var claimsIdentity = new ClaimsIdentity("Identity.Application", Options.ClaimsIdentity.UserNameClaimType, Options.ClaimsIdentity.RoleClaimType);
            claimsIdentity.AddClaim(new Claim(Options.ClaimsIdentity.UserIdClaimType, userId));
            claimsIdentity.AddClaim(new Claim(Options.ClaimsIdentity.UserNameClaimType, userName));
            if (UserManager.SupportsUserSecurityStamp)
                claimsIdentity.AddClaim(new Claim(Options.ClaimsIdentity.SecurityStampClaimType, await UserManager.GetSecurityStampAsync(user)));
            if (!UserManager.SupportsUserClaim)
                return new ClaimsPrincipal(claimsIdentity);
            // Construct permission clam.
            foreach (Claim claim in await UserManager.GetClaimsAsync(user))
            {
                // Permissions claim.
                if (claim.Type == SigmaIdentityConstants.PermissionsClaimName)
                {
                    IEnumerable<SigmaPermission> rolesPermissions = await UserManager.GetRolesPermissionsAsync(user);
                    // Get permissions from roles if there are any.
                    if (rolesPermissions.Any())
                    {
                        // Convert permissions string to int list.
                        var permissionClaimInt = claim.Value.ToIntArray().ToList();
                        // Add permissions from roles to permissions claim.
                        permissionClaimInt.AddRange(rolesPermissions.Cast<int>());
                        claimsIdentity.AddClaim(new Claim(SigmaIdentityConstants.PermissionsClaimName, permissionClaimInt.PermissionClaimToString()));
                    }
                    else
                        claimsIdentity.AddClaim(claim);
                }
                else
                    claimsIdentity.AddClaim(claim);
            }
            // Add Custom claims.
            ICollection<Claim> claims = new List<Claim>
                                        {
                                            new Claim("FirstName", user.FirstName),
                                            new Claim("LastName", user.LastName)
                                        };
            claimsIdentity.AddClaims(claims);
            return new ClaimsPrincipal(claimsIdentity);
        }
    }
}