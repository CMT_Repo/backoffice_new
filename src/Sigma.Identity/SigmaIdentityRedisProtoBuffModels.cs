﻿using Sigma.Redis;

namespace Sigma.Identity
{
    internal static class SigmaIdentityRedisProtoBuffModels
    {
        public static void Init()
        {
            RedisSerializationTypes.Add<SigmaUser>();
            RedisSerializationTypes.Add<RefreshToken>();
        }
    }
}