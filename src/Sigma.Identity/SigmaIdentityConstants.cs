﻿namespace Sigma.Identity
{
    public static class SigmaIdentityConstants
    {
        public static readonly string PermissionsClaimName = "Permissions";
    }
}