﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Sigma.Identity
{
    public class SigmaUserStore : UserStore<SigmaUser, SigmaRole, SigmaIdentityDbContext>
    {
        private DbSet<RefreshToken> RefreshTokensSet => Context.Set<RefreshToken>();

        /// <summary>
        ///   A navigation property for the RefreshTokens the store contains.
        /// </summary>
        public virtual IQueryable<RefreshToken> RefreshTokens => RefreshTokensSet;

        public SigmaUserStore (SigmaIdentityDbContext context, IdentityErrorDescriber describer = null) : base(context, describer) { }

        /// <summary>
        ///   Add new <see cref="RefreshToken" /> to store.
        /// </summary>
        /// <param name="refreshToken">Refresh token to save.</param>
        /// <returns>
        ///   The <see cref="Task" /> that represents the asynchronous operation, containing the <see cref="RefreshToken" /> of the
        ///   creation operation.
        /// </returns>
        public virtual async Task<bool> AddRefreshTokenAsync (RefreshToken refreshToken)
        {
            ThrowIfDisposed();
            if (refreshToken == null)
                throw new ArgumentNullException(nameof(refreshToken));
            await Context.RefreshTokens.AddAsync(refreshToken);
            return Convert.ToBoolean(await Context.SaveChangesAsync());
        }

        /// <summary>
        ///   Update <see cref="RefreshToken" /> last used date.
        /// </summary>
        /// <param name="refreshToken">Refresh token.</param>
        /// <returns>
        ///   The <see cref="Task" /> that represents the asynchronous operation, containing the <see cref="RefreshToken" /> to
        ///   update.
        /// </returns>
        public virtual async Task<bool> UpdateRefreshTokenLastUsedDateAsync (RefreshToken refreshToken)
        {
            ThrowIfDisposed();
            if (refreshToken == null)
                throw new ArgumentNullException(nameof(refreshToken));
            // Check if exists before updating.
            string tokenId = await Context.RefreshTokens.Where(token => token.Id == refreshToken.Id).Select(token => token.Id).SingleOrDefaultAsync();
            if (string.IsNullOrEmpty(tokenId))
                return false;
            Context.RefreshTokens.Attach(refreshToken);
            refreshToken.DateLastUsed = DateTime.UtcNow;
            return await Context.SaveChangesAsync() >= 1;
        }

        /// <summary>
        ///   Update <see cref="RefreshToken" />.
        /// </summary>
        /// <param name="refreshToken">Refresh token.</param>
        /// <returns>
        ///   The <see cref="Task" /> that represents the asynchronous operation, containing the <see cref="RefreshToken" /> to
        ///   update.
        /// </returns>
        public virtual async Task UpdateRefreshTokenAsync (RefreshToken refreshToken)
        {
            ThrowIfDisposed();
            if (refreshToken == null)
                throw new ArgumentNullException(nameof(refreshToken));
            Context.RefreshTokens.Attach(refreshToken);
            await Context.SaveChangesAsync();
        }

        /// <summary>
        ///   Update many <see cref="RefreshToken" />.
        /// </summary>
        /// <param name="refreshTokens">Refresh tokens.</param>
        /// <returns>
        ///   The <see cref="Task" /> that represents the asynchronous operation, containing the <see cref="RefreshToken" /> to
        ///   update.
        /// </returns>
        public virtual async Task UpdateRefreshTokensAsync (IEnumerable<RefreshToken> refreshTokens)
        {
            ThrowIfDisposed();
            if (refreshTokens == null)
                throw new ArgumentNullException(nameof(refreshTokens));
            ThrowIfDisposed();
            Context.RefreshTokens.AttachRange(refreshTokens);
            await Context.SaveChangesAsync();
        }

        /// <summary>
        ///   Remove <see cref="RefreshToken" /> from store.
        /// </summary>
        /// <param name="refreshTokenSignature">Refresh token signature.</param>
        /// <returns>
        ///   The <see cref="Task" /> that represents the asynchronous operation, containing the removal of
        ///   <see cref="RefreshToken" />.
        /// </returns>
        public virtual async Task<bool> RemoveRefreshTokenAsync (string refreshTokenSignature)
        {
            ThrowIfDisposed();
            if (refreshTokenSignature == null)
                throw new ArgumentNullException(nameof(refreshTokenSignature));
            // Check if exists before updating.
            string tokenId = await Context.RefreshTokens.Where(token => token.Signature == refreshTokenSignature).Select(token => token.Id).SingleOrDefaultAsync();
            if (string.IsNullOrEmpty(tokenId))
                return false;
            RefreshToken refreshToken = await Context.RefreshTokens.FirstOrDefaultAsync(token => token.Signature == refreshTokenSignature);
            Context.RefreshTokens.Remove(refreshToken);
            return await Context.SaveChangesAsync() >= 1;
        }

        /// <summary>
        ///   Delete <see cref="RefreshToken" /> by user.
        /// </summary>
        /// <param name="user">User.</param>
        /// <returns>
        ///   The <see cref="Task" /> that represents the asynchronous operation, containing the removal get of
        ///   <see cref="RefreshToken" /> by user.
        /// </returns>
        public virtual async Task DeleteRefreshTokenByUserAsync (SigmaUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            var tokensToRemove = Context.RefreshTokens.Where(token => token.User == user).ToList();
            if (tokensToRemove.Count == 0)
                return;
            Context.RefreshTokens.RemoveRange(tokensToRemove);
            await Context.SaveChangesAsync();
        }

    }
}