﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.Extensions.Options;
using Sigma.Core.Extensions;

namespace Sigma.Identity
{
    public class SigmaIdentityAuthorize : AuthorizeAttribute
    {
        /// <summary>
        ///   Specifies that the class or method that this attribute is applied to requires the specified authorization.
        /// </summary>
        public SigmaIdentityAuthorize (bool admin = true, bool isTestIgnored = true)
        {
            Policy = $"{int.MinValue}.{admin}.{isTestIgnored}";
        }

        /// <summary>
        ///   Specifies that the class or method that this attribute is applied to requires the specified authorization.
        /// </summary>
        public SigmaIdentityAuthorize (SigmaPermission permission, bool admin = true, bool isTestIgnored = true)
        {
            Policy = $"{(int)permission}.{admin}.{isTestIgnored}";
        }

        /// <summary>
        ///   Specifies that the class or method that this attribute is applied to requires the specified authorization.
        /// </summary>
        public SigmaIdentityAuthorize (SigmaPermission[] permissions, bool admin = true, bool isTestIgnored = true)
        {
            int[] permissionsIntArr = permissions.Cast<int>().ToArray();
            Policy = $"{permissionsIntArr.PermissionClaimToString()}.{admin}.{isTestIgnored}";
        }
    }

    public class SigmaIdentityAuthorizationRequirement : IAuthorizationRequirement
    {
        public IEnumerable<int> Permissions { get; }

        public bool Admin { get; }

        public bool IsTestIgnored { get; }

        public SigmaIdentityAuthorizationRequirement (IEnumerable<int> permissions, bool admin, bool isTestIgnored)
        {
            Permissions = permissions;
            Admin = admin;
            IsTestIgnored = isTestIgnored;
        }
    }

    public class SigmaIdentityAuthHandler : AuthorizationHandler<SigmaIdentityAuthorizationRequirement>
    {
        protected override Task HandleRequirementAsync (AuthorizationHandlerContext context, SigmaIdentityAuthorizationRequirement identityAuthorizationRequirement)
        {
            // Check if normal authorization used.
            if (identityAuthorizationRequirement.Permissions.First() == -2147483648)
                context.Succeed(identityAuthorizationRequirement);
            // Get permissions claim.
            Claim permissionClaim = context.User.Claims.FirstOrDefault(claim => claim.Type == SigmaIdentityConstants.PermissionsClaimName);
            if (permissionClaim == null)
                return Task.CompletedTask;
            // Check whether the right permissions exists in users permissions claim.
            IEnumerable<int> userPermissions = permissionClaim.Value.ToIntArray();
            foreach (int requiredPermission in identityAuthorizationRequirement.Permissions)
                if (userPermissions.Contains(requiredPermission))
                    context.Succeed(identityAuthorizationRequirement);
            // Check if admin access.
            if (identityAuthorizationRequirement.Admin)
                // Check if has admin permission.
                if (userPermissions.Contains((int)SigmaPermission.Admin))
                    context.Succeed(identityAuthorizationRequirement);
            return Task.CompletedTask;
        }
    }

    public class SigmaIdentityAuthPolicy : IAuthorizationPolicyProvider
    {
        public DefaultAuthorizationPolicyProvider FallbackPolicyProvider { get; }

        public SigmaIdentityAuthPolicy (IOptions<AuthorizationOptions> options)
        {
            FallbackPolicyProvider = new DefaultAuthorizationPolicyProvider(options);
        }

        public Task<AuthorizationPolicy> GetPolicyAsync (string policyName)
        {
            try
            {
                string[] subStringPolicy = policyName.Split(new[] { '.' });
                // Cast to SigmaPermission enum and then to int array.
                var policy = new AuthorizationPolicyBuilder();
                // Add Requirements.
                policy.AddRequirements(new DenyAnonymousAuthorizationRequirement());
                policy.AddRequirements(new SigmaIdentityAuthorizationRequirement(subStringPolicy[0].ToIntArray(), bool.Parse(subStringPolicy[1]), bool.Parse(subStringPolicy[2])));
                return Task.FromResult(policy.Build());
            }
            catch
            {
                //TODO: Log exception.
                return FallbackPolicyProvider.GetPolicyAsync(policyName);
            }
        }

        public Task<AuthorizationPolicy> GetDefaultPolicyAsync() => FallbackPolicyProvider.GetDefaultPolicyAsync();

        public Task<AuthorizationPolicy> GetFallbackPolicyAsync() => FallbackPolicyProvider.GetFallbackPolicyAsync();
    }
}