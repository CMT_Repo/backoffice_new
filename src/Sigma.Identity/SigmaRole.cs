﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Sigma.Core.Domain;
using Sigma.Core.Extensions;

namespace Sigma.Identity
{
    public class SigmaRole : IdentityRole, IBaseDomainObject
    {
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public string PermissionsString { get; set; }
        [NotMapped]
        public IEnumerable<SigmaPermission> Permissions
        {
            get
                {
                    int[] intPermissions = PermissionsString.ToIntArray();
                    return intPermissions.Cast<SigmaPermission>();
                }
        }
    }
}