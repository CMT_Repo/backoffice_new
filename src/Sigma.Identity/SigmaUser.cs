﻿using System;
using Microsoft.AspNetCore.Identity;
using Sigma.Core.Domain;

namespace Sigma.Identity
{
    public class SigmaUser : IdentityUser, IBaseDomainObject
    {
        [ProtectedPersonalData]
        public string FirstName { get; set; }
        [ProtectedPersonalData]
        public string LastName { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}