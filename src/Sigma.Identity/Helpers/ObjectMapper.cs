﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;

namespace Sigma.Identity.Helpers
{
    internal static class ObjectMapper
    {
        internal static IMapper Mapper { get; }

        static ObjectMapper()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<SignInJwtResult, SignInResult>().ReverseMap());
            Mapper = config.CreateMapper();
        }
    }
}