﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Sigma.Identity
{
    public class SigmaRoleStore : RoleStore<SigmaRole, SigmaIdentityDbContext>
    {
        public SigmaRoleStore (SigmaIdentityDbContext context, IdentityErrorDescriber describer = null) : base(context, describer) { }
    }
}