﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using Sigma.Core.Domain;

namespace Sigma.Identity
{
    //TODO: Interception for cache uses, once 3.0.0 is out.

    public class SigmaIdentityDbContext : IdentityDbContext<SigmaUser, SigmaRole, string>
    {
        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }
        public SigmaIdentityDbContext (DbContextOptions<SigmaIdentityDbContext> options) : base(options) { }

        public override int SaveChanges (bool acceptAllChangesOnSuccess)
        {
            UpdateDates();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync (bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            UpdateDates();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void UpdateDates()
        {
            foreach (EntityEntry entityEntry in ChangeTracker.Entries().ToList())
            {
                switch (entityEntry.State)
                {
                    case EntityState.Added:
                        {
                            if (entityEntry.Entity is IBaseDomainObject baseDomainObject)
                                baseDomainObject.DateCreated = DateTime.UtcNow;
                            break;
                        }
                    case EntityState.Modified:
                        {
                            if (entityEntry.Entity is IBaseDomainObject baseDomainObject)
                                if (baseDomainObject is SigmaUser user)
                                {
                                    // Check if user has permission claim, as that claim is added right after user is created and doesn't count as valid argument for dateModified update.
                                    IdentityUserClaim<string> userPermissionClaim = ((SigmaIdentityDbContext)ChangeTracker.Context).UserClaims.FirstOrDefault(identityUserClaim => identityUserClaim.UserId == user.Id && identityUserClaim.ClaimType == SigmaIdentityConstants.PermissionsClaimName);
                                    if (userPermissionClaim == null)
                                        continue;
                                    user.DateModified = DateTime.UtcNow;
                                    continue;
                                }
                                else
                                    baseDomainObject.DateModified = DateTime.UtcNow;
                            break;
                        }
                }
                // Update users modified date where entity have a foreign key associated with user.
                // Also don't update user modified date on a refreshToken.
                if (entityEntry.Entity.GetType() == typeof (RefreshToken))
                    continue;
                foreach (PropertyEntry propertyEntry in entityEntry.Properties)
                {
                    if (!propertyEntry.Metadata.IsForeignKey())
                        continue;
                    foreach (IForeignKey foreignKey in propertyEntry.Metadata.GetContainingForeignKeys())
                    {
                        // Check if foreign key is to sigmaUser.
                        if (foreignKey.PrincipalEntityType.ClrType != typeof (SigmaUser))
                            continue;
                        SigmaUser sigmaUser = ((SigmaIdentityDbContext)ChangeTracker.Context).Users.FirstOrDefault(user => user.Id == propertyEntry.CurrentValue.ToString());
                        sigmaUser.DateModified = DateTime.UtcNow;
                    }
                }
            }
        }
    }
}