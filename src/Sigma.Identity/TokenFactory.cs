﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Sigma.Core.Extensions;

namespace Sigma.Identity
{
    public sealed class TokenFactory
    {
        private readonly string _AudienceName;
        private readonly string _IssuerName;
        private readonly string _IssuerSigningKey;
        private readonly JwtSecurityTokenHandler _JwtSecurityTokenHandler;
        private readonly AesCredentials _RefreshTokenAesCredentials;
        private readonly double _JwtExpirationSeconds;

        /// <summary>
        ///   Init TokenFactory.
        /// </summary>
        public TokenFactory (string issuerName, string issuerSigningKey, string audienceName, AesCredentials refreshTokenAesCredentials, double jwtExpirationSeconds)
        {
            _IssuerName = issuerName;
            _IssuerSigningKey = issuerSigningKey;
            _AudienceName = audienceName;
            _RefreshTokenAesCredentials = refreshTokenAesCredentials;
            _JwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            _JwtExpirationSeconds = jwtExpirationSeconds;
            // Init redis ConnectionMultiplexer.
        }

        /// <summary>
        ///   Creates both JWT and refresh token. First item is for JWT, second is for refreshToken.
        /// </summary>
        public Tuple<string, RefreshToken> CreateTokens (IEnumerable<Claim> claims, SigmaUser user, string ipAddress, string userAgent)
        {
            return new Tuple<string, RefreshToken>(CreateJwt(claims), CreateRefreshToken(claims, user, ipAddress, userAgent));
        }

        /// <summary>
        ///   Create new JWT, based on claims.
        /// </summary>
        public string CreateJwt (IEnumerable<Claim> claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_IssuerSigningKey));
            var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            DateTime expires = DateTime.UtcNow.AddSeconds(_JwtExpirationSeconds);
            var token = new JwtSecurityToken(_IssuerName, _AudienceName, claims, expires: expires, signingCredentials: signingCredentials);
            return _JwtSecurityTokenHandler.WriteToken(token);
        }

        public string RefreshJwt (RefreshToken refreshToken)
        {
            return CreateJwt(refreshToken.Claims.FromStringToClaims());
        }

        public string RefreshJwt (IEnumerable<Claim> claims)
        {
            return CreateJwt(claims);
        }

        /// <summary>
        ///   Create new refresh token.
        /// </summary>
        public RefreshToken CreateRefreshToken (IEnumerable<Claim> claims, SigmaUser user, string ipAddress, string userAgent)
        {
            var refreshToken = new RefreshToken
                               {
                                   IpAddress = ipAddress,
                                   UserAgent = userAgent,
                                   Claims = claims.ClaimsToString(),
                                   User = user,
                                   UserId = user.Id
                               };
            // Generate signature.
            string signature = $"{refreshToken.IpAddress}|{Guid.NewGuid()}";
            // Encrypt signature.
            refreshToken.Signature = EncryptSignature(signature);
            return refreshToken;
        }

        /// <summary>
        ///   Encrypt signature.
        /// </summary>
        private string EncryptSignature (string plainText)
        {
            return AesWrapper.EncryptStringToString(plainText, _RefreshTokenAesCredentials);
        }

        /// <summary>
        ///   Decrypt signature.
        /// </summary>
        private string DecryptSignature (string secret)
        {
            return AesWrapper.DecryptStringFromString(secret, _RefreshTokenAesCredentials);
        }
    }
}