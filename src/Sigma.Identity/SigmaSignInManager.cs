﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Sigma.Identity
{
    public class SigmaSignInManager : SignInManager<SigmaUser>
    {
        private readonly IHttpContextAccessor _HttpContextAccessor;
        private readonly SigmaUserManager _UserManager;
        private readonly TokenFactory _TokenFactory;
        public new SigmaClaimsPrincipalFactory ClaimsFactory;

        public SigmaSignInManager (SigmaUserManager userManager, IHttpContextAccessor contextAccessor, SigmaClaimsPrincipalFactory claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<SignInManager<SigmaUser>> logger, IAuthenticationSchemeProvider schemes, IUserConfirmation<SigmaUser> confirmation,TokenFactory tokenFactory) :
            base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes, confirmation)
        {
            _UserManager = userManager;
            _HttpContextAccessor = contextAccessor;
            _TokenFactory = tokenFactory;
            ClaimsFactory = claimsFactory;
        }

        /// <summary>
        ///   Attempts a password signin for a user.
        /// </summary>
        public async Task<SignInJwtResult> PasswordSignInJwtAsync (string userName, string password, bool lockoutOnFailure)
        {
            SigmaUser sigmaUser = await _UserManager.FindByNameAsync(userName);
            if (sigmaUser == null)
                return SignInJwtResult.Failed(SignInResult.Failed);
            // Returns a flag indicating whether the given password is valid for the given user.
            SignInResult signInResult = await base.CheckPasswordSignInAsync(sigmaUser, password, lockoutOnFailure);
            if (!signInResult.Succeeded)
                return SignInJwtResult.Failed(signInResult);
            ClaimsPrincipal claimsPrincipal = await base.CreateUserPrincipalAsync(sigmaUser);
            string ipAddress = string.Empty;
            if(_HttpContextAccessor.HttpContext.Connection.RemoteIpAddress != null)
                ipAddress = _HttpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            string userAgent = _HttpContextAccessor.HttpContext.Request.Headers["User-Agent"];
            // Create JWT and RefreshToken.
            Tuple<string, RefreshToken> tokens = _TokenFactory.CreateTokens(claimsPrincipal.Claims, sigmaUser, ipAddress, userAgent);
            //Save refresh token to DB.
            bool saveToDb = await _UserManager.AddRefreshTokenAsync(tokens.Item2);
            return saveToDb ? SignInJwtResult.Success(tokens.Item1, tokens.Item2.Signature) : SignInJwtResult.Failed();
        }

        /// <summary>
        ///   Attempts to refresh JWT based on refresh token.
        /// </summary>
        public async Task<string> JwtRefresh (string refreshTokenSignature)
        {
            RefreshToken refreshToken = await _UserManager.GetRefreshTokenAsync(refreshTokenSignature);
            // If null, kick out user.
            if (refreshToken == null)
                    return string.Empty;
            // Update refresh token DateLastUsed.
            if (!await _UserManager.UpdateRefreshTokenLastUsedDateAsync(refreshToken))
                return string.Empty;
            // Get ClaimsIdentity.
            ClaimsPrincipal claimsIdentity = await ClaimsFactory.CreateAsync(await _UserManager.FindByIdAsync(refreshToken.UserId));
            return _TokenFactory.RefreshJwt(claimsIdentity.Claims);
        }

        /// <summary>
        ///   Sign-out the current user out of the application.
        /// </summary>
        public async Task SingOutJwtAsync (string refreshTokenSignature)
        {
            await base.SignOutAsync();
            if (!string.IsNullOrEmpty(refreshTokenSignature))
            {
                await _UserManager.RemoveRefreshTokenAsync(refreshTokenSignature);
            }
        }
    }
}