﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sigma.Core.Domain;
using Sigma.Core.Extensions;
using X.PagedList;

namespace Sigma.Identity
{
    public class SigmaUserManager : UserManager<SigmaUser>
    {
        /// <summary>
        ///   Store the manager operates over.
        /// </summary>
        /// <value>The persistence store the manager operates over.</value>
        private new SigmaUserStore Store { get; }

        /// <summary>
        ///   Returns an IQueryable of SigmaRoles.
        /// </summary>
        public virtual IQueryable<RefreshToken> RefreshTokens => Store.RefreshTokens;

        public SigmaUserManager (SigmaUserStore store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<SigmaUser> passwordHasher, IEnumerable<IUserValidator<SigmaUser>> userValidators, IEnumerable<IPasswordValidator<SigmaUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services,
                                 ILogger<SigmaUserManager> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            Store = store;
        }

        /// <summary>
        ///   Creates the specified SigmaUser in the backing store with given password, as an asynchronous operation.
        /// </summary>
        public override async Task<IdentityResult> CreateAsync (SigmaUser user, string password)
        {
            ThrowIfDisposed();
            IdentityResult identityResult = await base.CreateAsync(user, password);
            if (identityResult.Succeeded)
                // Add default permission claim.
                await base.AddClaimAsync(user, new Claim(SigmaIdentityConstants.PermissionsClaimName, ""));
            return identityResult;
        }

        /// <summary>
        ///   Gets a list of user permission in enums form.
        /// </summary>
        public async Task<List<SigmaPermission>> GetPermissionsEnumAsync (SigmaUser user)
        {
            ThrowIfDisposed();
            IEnumerable<int> permissionsInt = await GetPermissionIntAsync(user);
            return permissionsInt?.Select(i => (SigmaPermission)i).ToList();
        }

        /// <summary>
        ///   Gets a list of user permission in int array form.
        /// </summary>
        public async Task<int[]> GetPermissionIntAsync (SigmaUser user)
        {
            ThrowIfDisposed();
            Claim permissionsClaim = await GetPermissionClaimAsync(user);
            return permissionsClaim?.Value.ToIntArray();
        }

        /// <summary>
        ///   Gets users permission claim.
        /// </summary>
        public async Task<Claim> GetPermissionClaimAsync (SigmaUser user)
        {
            ThrowIfDisposed();
            IList<Claim> userClaim = await base.GetClaimsAsync(user);
            return userClaim.FirstOrDefault(claim => claim.Type == SigmaIdentityConstants.PermissionsClaimName);
        }

        /// <summary>
        ///   Adds the specified permission to user.
        /// </summary>
        public async Task<IdentityResult> AddPermissionAsync (SigmaUser user, int permission)
        {
            ThrowIfDisposed();
            Claim userPermissionClaim = await GetPermissionClaimAsync(user);
            IEnumerable<int> userPermissions = userPermissionClaim.Value.ToIntArray();
            // Check if permission claim contains new permission, as if it does return success.
            if (userPermissions.Contains(permission))
                return IdentityResult.Success;
            var newUserPermissions = userPermissions.ToList();
            newUserPermissions.Add(permission);
            string newPermissionClaimValue = newUserPermissions.PermissionClaimToString();
            return await base.ReplaceClaimAsync(user, userPermissionClaim, new Claim(SigmaIdentityConstants.PermissionsClaimName, newPermissionClaimValue));
        }

        /// <summary>
        ///   Add the specified permission to user.
        /// </summary>
        public async Task<IdentityResult> AddPermissionAsync (SigmaUser user, SigmaPermission permission)
        {
            ThrowIfDisposed();
            return await AddPermissionAsync(user, (int)permission);
        }

        /// <summary>
        ///   Update user permissions.
        /// </summary>
        public async Task<IdentityResult> UpdatePermissionsAsync (SigmaUser user, IEnumerable<int> permissions)
        {
            ThrowIfDisposed();
            Claim userPermissionClaim = await GetPermissionClaimAsync(user);
            return await base.ReplaceClaimAsync(user, userPermissionClaim, new Claim(SigmaIdentityConstants.PermissionsClaimName, permissions.PermissionClaimToString()));
        }

        /// <summary>
        ///   Removes the specified permission to user.
        /// </summary>
        public async Task<IdentityResult> RemovePermissionAsync (SigmaUser user, int permission)
        {
            ThrowIfDisposed();
            Claim userPermissionClaim = await GetPermissionClaimAsync(user);
            IEnumerable<int> userPermissions = userPermissionClaim.Value.ToIntArray();
            // Check if permission claim contains permission, as if it does not return success.
            if (!userPermissions.Contains(permission))
                return IdentityResult.Success;
            var newUserPermissions = userPermissions.ToList();
            newUserPermissions.Remove(permission);
            string newPermissionClaimValue = newUserPermissions.PermissionClaimToString();
            return await base.ReplaceClaimAsync(user, userPermissionClaim, new Claim(SigmaIdentityConstants.PermissionsClaimName, newPermissionClaimValue));
        }

        /// <summary>
        ///   Adds the specified permission to user
        /// </summary>
        public async Task<IdentityResult> RemovePermissionAsync (SigmaUser user, SigmaPermission permission)
        {
            ThrowIfDisposed();
            return await RemovePermissionAsync(user, (int)permission);
        }

        /// <summary>
        ///   Returns paged list with the passed down params.
        /// </summary>
        public async Task<IPagedList<SigmaUser>> SortedPagedListAsync (int pageNumber, int pageSize, string sort, SortDirection? sortDirection)
        {
            ThrowIfDisposed();
            switch (sort)
            {
                case "userName":
                    if (sortDirection == SortDirection.Desc)
                        return await Users.OrderByDescending(sigmaUser => sigmaUser.UserName).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Users.OrderBy(sigmaUser => sigmaUser.UserName).ToPagedListAsync(pageNumber, pageSize);
                case "email":
                    if (sortDirection == SortDirection.Desc)
                        return await Users.OrderByDescending(sigmaUser => sigmaUser.Email).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Users.OrderBy(sigmaUser => sigmaUser.Email).ToPagedListAsync(pageNumber, pageSize);
                case "firstName":
                    if (sortDirection == SortDirection.Desc)
                        return await Users.OrderByDescending(sigmaUser => sigmaUser.FirstName).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Users.OrderBy(sigmaUser => sigmaUser.FirstName).ToPagedListAsync(pageNumber, pageSize);
                case "lastName":
                    if (sortDirection == SortDirection.Desc)
                        return await Users.OrderByDescending(sigmaUser => sigmaUser.LastName).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Users.OrderBy(sigmaUser => sigmaUser.LastName).ToPagedListAsync(pageNumber, pageSize);
                case "dateCreated":
                    if (sortDirection == SortDirection.Desc)
                        return await Users.OrderByDescending(sigmaUser => sigmaUser.DateCreated).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Users.OrderBy(sigmaUser => sigmaUser.DateCreated).ToPagedListAsync(pageNumber, pageSize);
                case "dateModified":
                    if (sortDirection == SortDirection.Desc)
                        return await Users.OrderByDescending(sigmaUser => sigmaUser.DateModified).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Users.OrderBy(sigmaUser => sigmaUser.DateModified).ToPagedListAsync(pageNumber, pageSize);
                default: return null;
            }
        }

        /// <summary>
        ///   Updates the specified in the user store.
        /// </summary>
        public async Task<IdentityResult> UpdateAsync (SigmaUser user, string newPassword)
        {
            ThrowIfDisposed();
            try
            {
                SigmaUser userUpdate = await base.FindByIdAsync(user.Id);
                if (userUpdate == null)
                    throw new ArgumentNullException(nameof(user.Id));
                // UpdatePermissionsAsync values.
                userUpdate.UserName = user.UserName;
                userUpdate.FirstName = user.FirstName;
                userUpdate.LastName = user.LastName;
                userUpdate.Email = user.Email;
                if (!string.IsNullOrEmpty(newPassword))
                    if (!(await SetPassword(userUpdate, newPassword)).Succeeded)
                        return IdentityResult.Failed();
                return await base.UpdateAsync(userUpdate);
            }
            catch (Exception exception)
            {
                return IdentityResult.Failed(new IdentityError { Code = "0", Description = exception.Message });
            }
        }

        /// <summary>
        ///   Set password.
        /// </summary>
        public async Task<IdentityResult> SetPassword (SigmaUser user, string newPassword)
        {
            ThrowIfDisposed();
            IdentityResult validate = await ValidatePasswordAsync(user, newPassword);
            if (!validate.Succeeded)
                return validate;
            IdentityResult updateHashPasswordResult = await base.UpdatePasswordHash(user, newPassword, false);
            if (updateHashPasswordResult.Succeeded)
                return await base.UpdateAsync(user);
            return updateHashPasswordResult;
        }

        /// <summary>
        ///   Add new refresh token.
        /// </summary>
        public virtual async Task<bool> AddRefreshTokenAsync (RefreshToken refreshToken)
        {
            ThrowIfDisposed();
            return await Store.AddRefreshTokenAsync(refreshToken);
        }

        /// <summary>
        ///   Get <see cref="RefreshToken" />.
        /// </summary>
        /// <param name="refreshTokenSignature">Refresh token signature.</param>
        /// <returns>
        ///   The <see cref="Task" /> that represents the asynchronous operation, containing the <see cref="RefreshToken" /> of the
        ///   get operation.
        /// </returns>
        public virtual async Task<RefreshToken> GetRefreshTokenAsync (string refreshTokenSignature)
        {
            ThrowIfDisposed();
            if (refreshTokenSignature == null)
                throw new ArgumentNullException(nameof(refreshTokenSignature));
            return await RefreshTokens.FirstOrDefaultAsync(refreshToken => refreshToken.Signature == refreshTokenSignature);
        }

        /// <summary>
        ///   Get refresh tokens.
        /// </summary>
        public virtual async Task<IEnumerable<RefreshToken>> GetRefreshTokensAsync (SigmaUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            return await RefreshTokens.Where(refreshToken => refreshToken.User == user).ToListAsync();
        }

        /// <summary>
        ///   Update refresh token last used date.
        /// </summary>
        public virtual async Task<bool> UpdateRefreshTokenLastUsedDateAsync (RefreshToken refreshToken)
        {
            ThrowIfDisposed();
            return await Store.UpdateRefreshTokenLastUsedDateAsync(refreshToken);
        }

        /// <summary>
        ///   Remove refresh token.
        /// </summary>
        public virtual async Task<bool> RemoveRefreshTokenAsync (string refreshTokenSignature)
        {
            ThrowIfDisposed();
            return await Store.RemoveRefreshTokenAsync(refreshTokenSignature);
        }

        /// <summary>
        ///   Get refresh tokens.
        /// </summary>
        public async Task<IPagedList<RefreshToken>> GetRefreshTokensPagedListAsync (SigmaUser user, int pageNumber, int pageSize, string sort, SortDirection? sortDirection)
        {
            ThrowIfDisposed();
            switch (sort)
            {
                case "userAgent":
                    if (sortDirection == SortDirection.Desc)
                        return await Store.RefreshTokens.OrderByDescending(token => token.UserAgent).Where(token => token.User == user).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Store.RefreshTokens.OrderBy(token => token.UserAgent).Where(token => token.User == user).ToPagedListAsync(pageNumber, pageSize);
                case "ipAddress":
                    if (sortDirection == SortDirection.Desc)
                        return await Store.RefreshTokens.OrderByDescending(token => token.IpAddress).Where(token => token.User == user).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Store.RefreshTokens.OrderBy(token => token.IpAddress).Where(token => token.User == user).ToPagedListAsync(pageNumber, pageSize);
                case "dateCreated":
                    if (sortDirection == SortDirection.Desc)
                        return await Store.RefreshTokens.OrderByDescending(token => token.DateCreated).Where(token => token.User == user).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Store.RefreshTokens.OrderBy(token => token.DateCreated).Where(token => token.User == user).ToPagedListAsync(pageNumber, pageSize);
                case "dateLastUsed":
                    if (sortDirection == SortDirection.Desc)
                        return await Store.RefreshTokens.OrderByDescending(token => token.DateLastUsed).Where(token => token.User == user).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Store.RefreshTokens.OrderBy(token => token.DateLastUsed).Where(token => token.User == user).ToPagedListAsync(pageNumber, pageSize);
                case "signature":
                    if (sortDirection == SortDirection.Desc)
                        return await Store.RefreshTokens.OrderByDescending(token => token.Signature).Where(token => token.User == user).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Store.RefreshTokens.OrderBy(token => token.Signature).Where(token => token.User == user).ToPagedListAsync(pageNumber, pageSize);
                default: return null;
            }
        }

        /// <summary>
        ///   Deletes the specified <paramref name="user" /> from the backing store.
        /// </summary>
        public override async Task<IdentityResult> DeleteAsync (SigmaUser user)
        {
            ThrowIfDisposed();
            // Remove refresh tokens.
            await Store.DeleteRefreshTokenByUserAsync(user);
            return await base.DeleteAsync(user);
        }

        public async Task<IEnumerable<SigmaPermission>> GetRolesPermissionsAsync(SigmaUser user)
        {
            IQueryable<IEnumerable<SigmaPermission>> rolesPermissionsQuery = from userRole in Store.Context.UserRoles
                                                   join role in Store.Context.Roles on userRole.RoleId equals role.Id
                                                   where userRole.UserId.Equals(user.Id)
                                                   select role.Permissions;
            List<IEnumerable<SigmaPermission>> rolesPermissions = await rolesPermissionsQuery.ToListAsync();
            var permissions = new List<SigmaPermission>();
            foreach (IEnumerable<SigmaPermission> rolePermissions in rolesPermissions)
                permissions.AddRange(rolePermissions);
            return permissions;
        }
    }
}