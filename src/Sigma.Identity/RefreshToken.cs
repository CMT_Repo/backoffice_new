﻿using System;
using Sigma.Core.Domain;

namespace Sigma.Identity
{
    public class RefreshToken : IBaseDomainObject
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public SigmaUser User { get; set; }
        public string UserAgent { get; set; }
        public string IpAddress { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateLastUsed { get; set; }
        public string Signature { get; set; }
        public string Claims { get; set; }

        public RefreshToken()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
//TODO: Consider removing claims string.