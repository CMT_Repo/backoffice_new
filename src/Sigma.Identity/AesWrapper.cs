﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Sigma.Identity
{
    // Generate new key and vector using the following code, then convert to base64.
    // using (AesManaged aesManaged = new AesManaged())
    //{
    //aesManaged.BlockSize = 128;
    //aesManaged.KeySize = 256;
    //aesManaged.GenerateKey();
    //string newKey = Convert.ToBase64String(aesManaged.Key);
    //string newVector = Convert.ToBase64String(aesManaged.IV);
    //}

    internal static class AesWrapper
    {
        private const int c_keySize = 256;
        private const int c_blockSize = 128;

        private static byte[] EncryptString (string plainText, byte[] key, byte[] iv)
        {
            byte[] encrypted;
            try
            {
                // Create an AesManaged object
                // with the specified key and iv.
                using (var aesManaged = new AesManaged())
                {
                    aesManaged.BlockSize = c_blockSize;
                    aesManaged.KeySize = c_keySize;
                    aesManaged.Key = key;
                    aesManaged.IV = iv;
                    // Create an cryptoTransform to perform the stream transform.
                    ICryptoTransform cryptoTransform = aesManaged.CreateEncryptor(aesManaged.Key, aesManaged.IV);
                    // Create the streams used for encryption.
                    using (var memoryStream = new MemoryStream())
                        using (var cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Write))
                        {
                            using (var streamWriter = new StreamWriter(cryptoStream))
                                // Write all data to the stream.
                                streamWriter.Write(plainText);
                            encrypted = memoryStream.ToArray();
                        }
                }
            }
            catch
            {
                return null;
            }
            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }

        private static string DecryptString (byte[] cipheredStringBytes, byte[] key, byte[] iv)
        {
            // Declare the string used to hold the decrypted text.
            string plaintext;
            // Create an AesManaged object with the specified key and iv.
            using (var aesManaged = new AesManaged())
            {
                aesManaged.BlockSize = c_blockSize;
                aesManaged.KeySize = c_keySize;
                aesManaged.Key = key;
                aesManaged.IV = iv;
                // Create a cryptoTransform to perform the stream transform.
                ICryptoTransform cryptoTransform = aesManaged.CreateDecryptor(aesManaged.Key, aesManaged.IV);
                // Create the streams used for decryption.
                try
                {
                    using (var memoryStream = new MemoryStream(cipheredStringBytes))
                    {
                        using (var cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Read))
                            using (var srDecrypt = new StreamReader(cryptoStream))
                                // Read the decrypted bytes from the decrypting stream
                                // and place them in a string.
                                plaintext = srDecrypt.ReadToEnd();
                    }
                }
                catch
                {
                    return null;
                }
            }
            return plaintext;
        }

        private static bool InputCheck (string payload, string key, string vector)
        {
            return !string.IsNullOrEmpty(payload) && !string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(vector);
        }

        private static bool InputCheck (byte[] payload, string key, string vector)
        {
            return payload != null && payload.Length > 0 && !string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(vector);
        }

        /// <summary>
        ///   Encrypts string into an encrypted string.
        /// </summary>
        public static string EncryptStringToString (string plainString, AesCredentials aesCredentials)
        {
            if (!InputCheck(plainString, aesCredentials.Key, aesCredentials.Vector))
                return null;
            byte[] keyBytes = Convert.FromBase64String(aesCredentials.Key);
            byte[] vectorBytes = Convert.FromBase64String(aesCredentials.Vector);
            byte[] encryptedBytes = EncryptString(plainString, keyBytes, vectorBytes);
            if (encryptedBytes == null || encryptedBytes.Length <= 0)
                return null;
            return Convert.ToBase64String(encryptedBytes);
        }

        /// <summary>
        ///   Decrypt string from an encrypted string.
        /// </summary>
        public static string DecryptStringFromString (string cryptedString, AesCredentials aesCredentials)
        {
            if (!InputCheck(cryptedString, aesCredentials.Key, aesCredentials.Vector))
                return null;
            byte[] keyBytes = Convert.FromBase64String(aesCredentials.Key);
            byte[] vectorBytes = Convert.FromBase64String(aesCredentials.Vector);
            byte[] cipherStringBytes = Convert.FromBase64String(cryptedString);
            string decryptedString = DecryptString(cipherStringBytes, keyBytes, vectorBytes);
            return string.IsNullOrEmpty(decryptedString) ? null : decryptedString;
        }

        /// <summary>
        ///   Decrypt string from an bytes.
        /// </summary>
        /// <param name="cryptedBytes"> Bytes to decrypt. </param>
        /// <param name="key"> Key </param>
        /// <param name="vector"> Vector </param>
        public static string DecryptStringFromBytes (byte[] cryptedBytes, string key, string vector)
        {
            if (!InputCheck(cryptedBytes, key, vector))
                return null;
            byte[] keyBytes = Convert.FromBase64String(key);
            byte[] vectorBytes = Convert.FromBase64String(vector);
            string decryptedString = DecryptString(cryptedBytes, keyBytes, vectorBytes);
            return string.IsNullOrEmpty(decryptedString) ? null : decryptedString;
        }

        /// <summary>
        ///   Generates new Aes key and vector.
        /// </summary>
        /// <returns></returns>
        public static AesCredentials GenerateCredentials()
        {
            // Generate new key and vector using the following code, then convert to base64.
            using (var aesManaged = new AesManaged())
            {
                aesManaged.BlockSize = 128;
                aesManaged.KeySize = 256;
                aesManaged.GenerateKey();
                string key = Convert.ToBase64String(aesManaged.Key);
                string vector = Convert.ToBase64String(aesManaged.IV);
                return new AesCredentials
                       {
                           Key = key,
                           Vector = vector
                       };
            }
        }
    }

    public class AesCredentials
    {
        public string Key { get; set; }
        public string Vector { get; set; }
    }
}