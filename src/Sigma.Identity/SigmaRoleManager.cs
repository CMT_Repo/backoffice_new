﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Sigma.Core.Domain;
using Sigma.Core.Extensions;
using X.PagedList;

namespace Sigma.Identity
{
    public class SigmaRoleManager : RoleManager<SigmaRole>
    {
        /// <summary>
        ///   Store the manager operates over.
        /// </summary>
        /// <value>The persistence store the manager operates over.</value>
        private new SigmaRoleStore Store { get; }

        public SigmaRoleManager (SigmaRoleStore store, IEnumerable<IRoleValidator<SigmaRole>> roleValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, ILogger<RoleManager<SigmaRole>> logger) : base(store, roleValidators, keyNormalizer, errors, logger)
        {
            Store = store;
        }

        /// <summary>
        /// Updates the specified <paramref name="role"/>.
        /// </summary>
        /// <param name="role">The role to updated.</param>
        /// <returns>
        /// The <see cref="Task"/> that represents the asynchronous operation, containing the <see cref="IdentityResult"/> for the update.
        /// </returns>
        public override async Task<IdentityResult> UpdateAsync (SigmaRole role)
        {
            ThrowIfDisposed();
            if (role == null)
                throw new ArgumentNullException(nameof(role));
            SigmaRole updatedRole = await base.FindByIdAsync(role.Id);
            if(updatedRole == null)
                return IdentityResult.Failed(new IdentityError { Code = "0", Description = "ID cannot be null" });
            updatedRole.PermissionsString = role.PermissionsString.ToIntArray().PermissionClaimToString();
            updatedRole.Name = role.Name;
            updatedRole.NormalizedName = updatedRole.Name.ToUpper();
            return await base.UpdateAsync(updatedRole);
        }

        /// <summary>
        ///   Returns paged list with the passed down params.
        /// </summary>
        public async Task<IPagedList<SigmaRole>> SortedPagedListAsync(int pageNumber, int pageSize, string sort, SortDirection? sortDirection)
        {
            ThrowIfDisposed();
            switch (sort)
            {
                case "name":
                    if (sortDirection == SortDirection.Desc)
                        return await Roles.OrderByDescending(sigmaUser => sigmaUser.Name).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Roles.OrderBy(sigmaUser => sigmaUser.Name).ToPagedListAsync(pageNumber, pageSize);
                case "dateCreated":
                    if (sortDirection == SortDirection.Desc)
                        return await Roles.OrderByDescending(sigmaUser => sigmaUser.DateCreated).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Roles.OrderBy(sigmaUser => sigmaUser.DateCreated).ToPagedListAsync(pageNumber, pageSize);
                case "dateModified":
                    if (sortDirection == SortDirection.Desc)
                        return await Roles.OrderByDescending(sigmaUser => sigmaUser.DateModified).ToPagedListAsync(pageNumber, pageSize);
                    else
                        return await Roles.OrderBy(sigmaUser => sigmaUser.DateModified).ToPagedListAsync(pageNumber, pageSize);
                default: return null;
            }
        }

    }
}