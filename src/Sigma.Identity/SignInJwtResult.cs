﻿using Microsoft.AspNetCore.Identity;
using Sigma.Identity.Helpers;

namespace Sigma.Identity
{
    public class SignInJwtResult : SignInResult
    {
        public string Jwt { get; set; }
        public string RefreshToken { get; set; }

        public new static SignInJwtResult Success (string jwt, string refreshToken)
        {
            return new SignInJwtResult
                   {
                       Succeeded = true,
                       Jwt = jwt,
                       RefreshToken = refreshToken
                   };
        }

        internal new static SignInJwtResult Failed (SignInResult signInResult = null)
        {
            return ObjectMapper.Mapper.Map<SignInJwtResult>(signInResult ?? SignInResult.Failed);
        }
    }
}