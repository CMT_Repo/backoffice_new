﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Sigma.Core;

namespace Sigma.Identity
{
    internal class SigmaIdentityDesignTimeDbContextFactory : IDesignTimeDbContextFactory<SigmaIdentityDbContext>
    {
        public SigmaIdentityDbContext CreateDbContext (string[] args)
        {
            IConfigurationRoot configurationRoot = ConfigurationHelper.InitConfigurationRoot();
            string connectionString = configurationRoot["Identity:ConnectionString"];
            IDesignTimeDbContextFactoryHelper.WriteInConsoleConnectionStringUsed(connectionString);
            var optionsBuilder = new DbContextOptionsBuilder<SigmaIdentityDbContext>();
            optionsBuilder.UseNpgsql(connectionString);
            return new SigmaIdentityDbContext(optionsBuilder.Options);
        }
    }
}