﻿using System;
using Sigma.Core.Domain;
using Sigma.Redis;
using Xunit;

namespace Sigma.Test.Redis
{
    public class KeyGenTest
    {
        private class PocoClass : IBaseDomainObject
        {
            public string Id { get; set; }
            public DateTime DateCreated { get; set; }
            public DateTime? DateModified { get; set; }
        }

        private double SortFunc (PocoClass arg)
        {
            var random = new Random();
            return random.NextDouble() * (999999999 - 9999) + 9999;
        }

        [Fact]
        public void KeyStringTest()
        {
            var id = Guid.NewGuid().ToString();
            string key = KeyGen.Key<PocoClass>(id);
            Assert.Equal($"pococlass:object:{id.ToLower()}", key);
        }

        [Fact]
        public void KeyGuidTest()
        {
            var id = Guid.NewGuid();
            string key = KeyGen.Key<PocoClass>(id);
            Assert.Equal($"pococlass:object:{id.ToString().ToLower()}", key);
        }

        [Fact]
        public void KeyDomainObjectTest()
        {
            var foo = new PocoClass()
                      {
                          Id = Guid.NewGuid().ToString()
                      };
            string key = KeyGen.Key<PocoClass>(foo);
            Assert.Equal($"pococlass:object:{foo.Id.ToLower()}", key);
        }

        [Fact]
        public void SortNameObjTest()
        {
            var foo = new RedisSort<PocoClass>("mySort", SortFunc);
            string key = KeyGen.SortName(foo);
            Assert.Equal("pococlass:sort:mysort", key);
        }

        [Fact]
        public void SortNameStringTest()
        {
            string key = KeyGen.SortName<PocoClass>("mySORT");
            Assert.Equal("pococlass:sort:mysort", key);
        }

        [Fact]
        public void IndexTest()
        {
            string key = KeyGen.Index<PocoClass>();
            Assert.Equal("pococlass:index", key);
        }

        [Fact]
        public void RefreshToken()
        {
            var id = Guid.NewGuid().ToString();
            string key = KeyGen.RefreshToken(id);
            Assert.Equal($"refreshToken:{id}", key);
        }
    }
}