﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using Sigma.Identity;

namespace Sigma.Test
{
    public static class TestUtil
    {
        public static async Task<SigmaIdentityDbContext> CreateTestContextAsync()
        {
            DbContextOptions<SigmaIdentityDbContext> dbContextOptions = new DbContextOptionsBuilder<SigmaIdentityDbContext>().UseSqlite($"Data Source=D{Guid.NewGuid()}.db").Options;
            var dbContext = new SigmaIdentityDbContext(dbContextOptions);
            await dbContext.Database.EnsureCreatedAsync();
            return dbContext;
        }

        public static SigmaUser CreateTestUser (string namePrefix = "", string email = "", string phoneNumber = "", bool lockoutEnabled = false, DateTimeOffset? lockoutEnd = default, bool useNamePrefixAsUserName = false)
        {
            return new SigmaUser
                   {
                       UserName = useNamePrefixAsUserName ? namePrefix : $"{namePrefix}{Guid.NewGuid()}",
                       Email = email,
                       PhoneNumber = phoneNumber,
                       LockoutEnabled = lockoutEnabled,
                       LockoutEnd = lockoutEnd,
                   };
        }

        public static RefreshToken CreateTestRefreshToken (string userId = "", SigmaUser user = null, string userAgent = null, string ipAddress = null, string signature = null, string claims = "", DateTime dateLastUsed = default)
        {
            return new RefreshToken
                   {
                       User = user,
                       UserId = userId,
                       UserAgent = userAgent ?? $"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/{RandomIntGen(100000, 200000)} Firefox/68.0",
                       Signature = signature ?? Guid.NewGuid().ToString(),
                       IpAddress = ipAddress ?? GetRandomIpAddress(),
                       Claims = claims,
                       DateLastUsed = dateLastUsed,
                   };
        }

        public static IQueryable<RefreshToken> CreateRefreshTokenQueryable(RefreshToken refreshToken = null, List<RefreshToken> refreshTokens = null)
        {
            if (refreshToken != null)
                return new List<RefreshToken> { refreshToken }.AsQueryable().BuildMock().Object;
            if (refreshTokens != null)
                return refreshTokens.AsQueryable().BuildMock().Object;
            return new List<RefreshToken> { CreateTestRefreshToken() }.AsQueryable().BuildMock().Object;
        }

        private static string GetRandomIpAddress()
        {
            var random = new Random();
            return $"{random.Next(1, 255)}.{random.Next(0, 255)}.{random.Next(0, 255)}.{random.Next(0, 255)}";
        }

        private static int RandomIntGen (int minBound, int maxBound)
        {
            var rnd = new Random();
            return rnd.Next(minBound, maxBound);
        }
    }
}