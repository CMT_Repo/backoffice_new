﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Sigma.Identity;

namespace Sigma.Test
{
    public static class MockHelpers
    {
        public static SigmaUserManager TestUserManager (SigmaUserStore store = null)
        {
            store = store ?? new Mock<SigmaUserStore>(new SigmaIdentityDbContext(new DbContextOptions<SigmaIdentityDbContext>()), null).Object;
            var options = new Mock<IOptions<IdentityOptions>>();
            var idOptions = new IdentityOptions { Lockout = { AllowedForNewUsers = false } };
            options.Setup(o => o.Value).Returns(idOptions);
            var userValidators = new List<IUserValidator<SigmaUser>>();
            var validator = new Mock<IUserValidator<SigmaUser>>();
            userValidators.Add(validator.Object);
            var pwdValidators = new List<PasswordValidator<SigmaUser>> { new PasswordValidator<SigmaUser>() };
            var userManager = new SigmaUserManager(store, options.Object, new PasswordHasher<SigmaUser>(),
                                                   userValidators, pwdValidators, new UpperInvariantLookupNormalizer(),
                                                   new IdentityErrorDescriber(), null,
                                                   new Mock<ILogger<SigmaUserManager>>().Object);
            validator.Setup(v => v.ValidateAsync(userManager, It.IsAny<SigmaUser>())).Returns(Task.FromResult(IdentityResult.Success)).Verifiable();
            return userManager;
        }

        public static DbSet<T> GetQueryableMockDbSet <T> (List<T> sourceList) where T : class
        {
            IQueryable<T> queryable = sourceList.AsQueryable();
            var dbSet = new Mock<DbSet<T>>();
            dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());
            dbSet.Setup(d => d.Add(It.IsAny<T>())).Callback<T>((s) => sourceList.Add(s));
            return dbSet.Object;
        }

        public static Mock<SigmaRoleManager> MockRoleManager (SigmaRoleStore store = null)
        {
            store = store ?? new Mock<SigmaRoleStore>(new SigmaIdentityDbContext(new DbContextOptions<SigmaIdentityDbContext>()), null).Object;
            var roles = new List<IRoleValidator<SigmaRole>> { new RoleValidator<SigmaRole>() };
            return new Mock<SigmaRoleManager>(store, roles, new UpperInvariantLookupNormalizer(), new IdentityErrorDescriber(), null);
        }
    }
}