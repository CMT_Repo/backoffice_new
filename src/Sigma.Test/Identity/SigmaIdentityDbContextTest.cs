﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Sigma.Identity;
using Xunit;

namespace Sigma.Test.Identity
{
    public class SigmaIdentityDbContextTest
    {
        [Fact]
        public async Task DateCreatedTest()
        {
            using (SigmaIdentityDbContext dbContext = await TestUtil.CreateTestContextAsync())
            {
                // Setup.
                SigmaUser user = TestUtil.CreateTestUser();
                // Act.
                await dbContext.AddAsync(user);
                await dbContext.SaveChangesAsync();
                // Assert.
                Assert.True(user.DateCreated != DateTime.MinValue && user.DateCreated != null);
                Assert.True((DateTime.UtcNow - user.DateCreated) < new TimeSpan(0, 1, 0));
            }
        }

        [Fact]
        public async Task DateNotModifiedRefreshTokenTest()
        {
            using (SigmaIdentityDbContext dbContext = await TestUtil.CreateTestContextAsync())
            {
                // Setup.
                SigmaUser user = TestUtil.CreateTestUser();
                await dbContext.Users.AddAsync(user);
                RefreshToken refreshToken = TestUtil.CreateTestRefreshToken(user: user, userId: user.Id);
                await dbContext.RefreshTokens.AddAsync(refreshToken);
                await dbContext.AddAsync(refreshToken);
                await dbContext.SaveChangesAsync();
                // Act.
                refreshToken.Signature = "foooo";
                await dbContext.SaveChangesAsync();
                // Assert.
                Assert.Null(user.DateModified);
            }
        }

        [Fact]
        public async Task DateNotModifiedUserNoPermissionClaimTest()
        {
            using (SigmaIdentityDbContext dbContext = await TestUtil.CreateTestContextAsync())
            {
                // Setup.
                SigmaUser user = TestUtil.CreateTestUser();
                await dbContext.AddAsync(user);
                await dbContext.SaveChangesAsync();
                // Act.
                user.FirstName = "Meow";
                user.LastName = "Moo";
                await dbContext.SaveChangesAsync();
                // Assert.
                Assert.Null(user.DateModified);
            }
        }

        [Fact]
        public async Task DateModifiedUserTest()
        {
            using (SigmaIdentityDbContext dbContext = await TestUtil.CreateTestContextAsync())
            {
                // Setup.
                SigmaUser user = TestUtil.CreateTestUser();
                await dbContext.AddAsync(user);
                await dbContext.SaveChangesAsync();
                var claim = new IdentityUserClaim<string>
                            {
                                UserId = user.Id,
                                ClaimType = "foo",
                                ClaimValue = "bar"
                            };
                // Act.
                await dbContext.UserClaims.AddAsync(claim);
                await dbContext.SaveChangesAsync();
                // Assert.
                Assert.True(user.DateModified != DateTime.MinValue && user.DateModified != null);
                Assert.True((DateTime.UtcNow - user.DateModified) < new TimeSpan(0, 1, 0));
            }
        }
    }
}