﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Moq;
using Sigma.Identity;
using Xunit;

namespace Sigma.Test.Identity
{
    public class SigmaSignInManagerTest
    {
        private static SigmaSignInManager SetupSignInManager (SigmaUserManager manager, HttpContext context, ILogger logger = null, IdentityOptions identityOptions = null, IAuthenticationSchemeProvider schemeProvider = null, TokenFactory tokenFactory = null)
        {
            var contextAccessor = new Mock<IHttpContextAccessor>();
            contextAccessor.Setup(a => a.HttpContext).Returns(context);
            Mock<SigmaRoleManager> roleManager = MockHelpers.MockRoleManager();
            identityOptions = identityOptions ?? new IdentityOptions();
            var options = new Mock<IOptions<IdentityOptions>>();
            options.Setup(a => a.Value).Returns(identityOptions);
            var claimsFactory = new SigmaClaimsPrincipalFactory(manager, roleManager.Object, options.Object);
            schemeProvider = schemeProvider ?? new Mock<IAuthenticationSchemeProvider>().Object;
            var sm = new SigmaSignInManager(manager, contextAccessor.Object, claimsFactory, options.Object, null, schemeProvider, new DefaultUserConfirmation<SigmaUser>(), tokenFactory) { Logger = logger ?? NullLogger<SignInManager<SigmaUser>>.Instance };
            return sm;
        }

        [Fact]
        public async Task GoodPasswordSignInAsync()
        {
            // Setup.
            var password = "password";
            SigmaUser user = TestUtil.CreateTestUser();
            var userStore = new Mock<SigmaUserStore>(new SigmaIdentityDbContext(new DbContextOptions<SigmaIdentityDbContext>()), null);
            var tokenFactory = new Mock<TokenFactory>(null, null, null, null);
            tokenFactory.Setup(factory => factory.CreateTokens(It.IsAny<IEnumerable<Claim>>(), user, "", null)).Returns((new Tuple<string, RefreshToken>("foo", new RefreshToken())));
            var userManager = new Mock<SigmaUserManager>(userStore.Object, null, null, null, null, null, null, null, null);
            userManager.Object.UserValidators.Add(new UserValidator<SigmaUser>());
            userManager.Object.PasswordValidators.Add(new PasswordValidator<SigmaUser>());
            userManager.Setup(manager => manager.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            userManager.Setup(manager => manager.FindByIdAsync(user.Id)).ReturnsAsync(user);
            userManager.Setup(manager => manager.GetUserIdAsync(user)).ReturnsAsync(user.Id);
            userManager.Setup(manager => manager.GetUserNameAsync(user)).ReturnsAsync(user.UserName);
            userManager.Setup(manager => manager.SupportsUserLockout).Returns(true).Verifiable();
            userManager.Setup(manager => manager.IsLockedOutAsync(user)).ReturnsAsync(false).Verifiable();
            userManager.Setup(manager => manager.CheckPasswordAsync(user, password)).ReturnsAsync(true).Verifiable();
            userManager.Setup(manager => manager.AddRefreshTokenAsync(It.IsAny<RefreshToken>())).ReturnsAsync(true);
            var context = new DefaultHttpContext();
            SigmaSignInManager signInManager = SetupSignInManager(userManager.Object, context, null, null, null, tokenFactory.Object);
            // Act.
            SignInJwtResult result = await signInManager.PasswordSignInJwtAsync(user.UserName, password, false);
            // Assert.
            Assert.True(result.Succeeded);
            userStore.Verify();
            userManager.Verify();
            tokenFactory.Verify();
        }

        [Fact]
        public async Task BadPasswordSignInTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            var userStore = new Mock<SigmaUserStore>(new SigmaIdentityDbContext(new DbContextOptions<SigmaIdentityDbContext>()), null);
            var tokenFactory = new Mock<TokenFactory>(null, null, null, null);
            tokenFactory.Setup(factory => factory.CreateTokens(It.IsAny<IEnumerable<Claim>>(), user, "", null)).Returns((new Tuple<string, RefreshToken>("foo", new RefreshToken())));
            var userManager = new Mock<SigmaUserManager>(userStore.Object, null, null, null, null, null, null, null, null);
            userManager.Object.UserValidators.Add(new UserValidator<SigmaUser>());
            userManager.Object.PasswordValidators.Add(new PasswordValidator<SigmaUser>());
            userManager.Setup(manager => manager.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            userManager.Setup(manager => manager.FindByIdAsync(user.Id)).ReturnsAsync(user);
            userManager.Setup(manager => manager.GetUserIdAsync(user)).ReturnsAsync(user.Id);
            userManager.Setup(manager => manager.GetUserNameAsync(user)).ReturnsAsync(user.UserName);
            userManager.Setup(manager => manager.SupportsUserLockout).Returns(true).Verifiable();
            userManager.Setup(manager => manager.IsLockedOutAsync(user)).ReturnsAsync(false).Verifiable();
            userManager.Setup(manager => manager.AddRefreshTokenAsync(It.IsAny<RefreshToken>())).ReturnsAsync(true);
            var context = new DefaultHttpContext();
            SigmaSignInManager signInManager = SetupSignInManager(userManager.Object, context, null, null, null, tokenFactory.Object);
            // Act.
            SignInJwtResult result = await signInManager.PasswordSignInJwtAsync(user.UserName, "badPassword", false);
            // Assert.
            Assert.False(result.Succeeded);
            userStore.Verify();
            userManager.Verify();
            tokenFactory.Verify();
        }

        [Fact]
        public async Task JwtRefreshTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            var refreshTokenSignature = "refreshTokenSignature";
            RefreshToken refreshToken = TestUtil.CreateTestRefreshToken(user.Id,signature:refreshTokenSignature);
            var userStore = new Mock<SigmaUserStore>(new SigmaIdentityDbContext(new DbContextOptions<SigmaIdentityDbContext>()), null);
            userStore.Setup(store => store.RefreshTokens).Returns(TestUtil.CreateRefreshTokenQueryable(refreshToken));
            var tokenFactory = new Mock<TokenFactory>(null, null, null, null);
            tokenFactory.Setup(factory => factory.RefreshJwt(It.IsAny<IEnumerable<Claim>>())).Returns("foo");
            var userManager = new Mock<SigmaUserManager>(userStore.Object, null, null, null, null, null, null, null, null);
            userManager.Setup(manager => manager.UpdateRefreshTokenLastUsedDateAsync(It.IsAny<RefreshToken>())).ReturnsAsync(true);
            userManager.Setup(manager => manager.RefreshTokens).Returns(TestUtil.CreateRefreshTokenQueryable(refreshToken));
            userManager.Setup(manager => manager.FindByIdAsync(user.Id)).ReturnsAsync(user);
            userManager.Setup(manager => manager.GetUserIdAsync(user)).ReturnsAsync(user.Id);
            userManager.Setup(manager => manager.GetUserNameAsync(user)).ReturnsAsync(user.UserName);
            userManager.Setup(manager => manager.GetRefreshTokenAsync(refreshTokenSignature)).ReturnsAsync(refreshToken);
            var context = new DefaultHttpContext();
            SigmaSignInManager signInManager = SetupSignInManager(userManager.Object, context, null, null, null, tokenFactory.Object);
            // Act.
            string result = await signInManager.JwtRefresh(refreshTokenSignature);
            // Assert.
            Assert.Equal("foo", result);
            userStore.Verify();
            userManager.Verify();
            tokenFactory.Verify();
        }

        [Fact]
        public async Task SignOutTestAsync()
        {
            var refreshTokenSignature = "refreshTokenSignature";
            // Setup.
            var userStore = new Mock<SigmaUserStore>(new SigmaIdentityDbContext(new DbContextOptions<SigmaIdentityDbContext>()), null);
            var tokenFactory = new Mock<TokenFactory>(null, null, null, null);
            var userManager = new Mock<SigmaUserManager>(userStore.Object, null, null, null, null, null, null, null, null);
            userManager.Setup(manager => manager.RemoveRefreshTokenAsync(refreshTokenSignature)).ReturnsAsync(true);
            var context = new DefaultHttpContext();
            var auth = new Mock<IAuthenticationService>();
            context.RequestServices = new ServiceCollection().AddSingleton(auth.Object).BuildServiceProvider();
            auth.Setup(a => a.SignOutAsync(context, IdentityConstants.ApplicationScheme, It.IsAny<AuthenticationProperties>())).Returns(Task.FromResult(0)).Verifiable();
            auth.Setup(a => a.SignOutAsync(context, IdentityConstants.TwoFactorUserIdScheme, It.IsAny<AuthenticationProperties>())).Returns(Task.FromResult(0)).Verifiable();
            auth.Setup(a => a.SignOutAsync(context, IdentityConstants.ExternalScheme, It.IsAny<AuthenticationProperties>())).Returns(Task.FromResult(0)).Verifiable();
            SigmaSignInManager signInManager = SetupSignInManager(userManager.Object, context, null, userManager.Object.Options);
            // Act.
            await signInManager.SignOutAsync();
            // Assert
            userStore.Verify();
            userManager.Verify();
            tokenFactory.Verify();
            auth.Verify();
        }
    }
}