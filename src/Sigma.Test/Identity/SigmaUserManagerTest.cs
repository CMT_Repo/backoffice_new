﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Moq;
using Sigma.Core.Domain;
using Sigma.Identity;
using X.PagedList;
using Xunit;

namespace Sigma.Test.Identity
{
    public class SigmaUserManagerTest
    {
        private Mock<SigmaUserStore> MockUserStore(SigmaIdentityDbContext sigmaIdentityDbContext = null)
        {
            if (sigmaIdentityDbContext == null)
                return new Mock<SigmaUserStore>(new SigmaIdentityDbContext(new DbContextOptions<SigmaIdentityDbContext>()), null);
            return new Mock<SigmaUserStore>(sigmaIdentityDbContext, null);
        }

        [Fact]
        public async Task SigmaUserManagerMethodsThrowWhenDisposedTestAsync()
        {
            SigmaUserManager userManager = MockHelpers.TestUserManager(new SigmaUserStore(new SigmaIdentityDbContext(new DbContextOptionsBuilder<SigmaIdentityDbContext>().Options)));
            userManager.Dispose();
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.CreateAsync(null, null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.GetPermissionsEnumAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.GetPermissionIntAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.GetPermissionClaimAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.AddPermissionAsync(null, int.MinValue));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.UpdatePermissionsAsync(null, null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.RemovePermissionAsync(null, int.MinValue));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.RemovePermissionAsync(null, SigmaPermission.Admin));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.SortedPagedListAsync(int.MinValue, int.MinValue, null, null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.UpdateAsync(null, null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.SetPassword(null, null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.AddRefreshTokenAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.GetRefreshTokenAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.GetRefreshTokensAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.UpdateRefreshTokenLastUsedDateAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.RemoveRefreshTokenAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.GetRefreshTokensPagedListAsync(null, int.MinValue, int.MinValue, null, null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await userManager.DeleteAsync(null));
        }

        [Fact]
        public async Task CreateTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.CreateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            userStore.Setup(store => store.GetUserNameAsync(user, CancellationToken.None)).Returns(Task.FromResult(user.UserName));
            userStore.Setup(store => store.SetNormalizedUserNameAsync(user, user.UserName.ToUpperInvariant(), CancellationToken.None)).Returns(Task.FromResult(false));
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult result = await userManager.CreateAsync(user, "Password1!");
            // Assert.
            Assert.True(result.Succeeded);
            userStore.Verify(store => store.AddClaimsAsync(user, It.Is<IEnumerable<Claim>>(claims => claims.Any(claim => claim.Type == SigmaIdentityConstants.PermissionsClaimName)), CancellationToken.None));
            userStore.VerifyAll();
        }

        [Fact]
        public async Task CreateBadPasswordTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            SigmaUserManager userManager = MockHelpers.TestUserManager();
            // Act.
            IdentityResult result = await userManager.CreateAsync(user, "foo");
            // Assert.
            Assert.False(result.Succeeded);
            Assert.Collection(result.Errors,
                              item => Assert.Contains(item.Code, "PasswordTooShort"),
                              item => Assert.Contains(item.Code, "PasswordRequiresNonAlphanumeric"),
                              item => Assert.Contains(item.Code, "PasswordRequiresDigit"),
                              item => Assert.Contains(item.Code, "PasswordRequiresUpper"));
        }

        [Fact]
        public async Task GetPermissionClaimTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            var claim = new Claim(SigmaIdentityConstants.PermissionsClaimName, "1,2,3");
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.GetClaimsAsync(user, CancellationToken.None)).ReturnsAsync(new List<Claim>() { claim });
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            Claim permissionClaim = await userManager.GetPermissionClaimAsync(user);
            // Assert.
            Assert.NotNull(permissionClaim);
            Assert.Equal(permissionClaim, claim);
        }

        [Fact]
        public async Task GetPermissionIntTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            var permissionClaim = new Claim(SigmaIdentityConstants.PermissionsClaimName, "1,2,3");
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.GetClaimsAsync(user, CancellationToken.None)).ReturnsAsync(new List<Claim>() { permissionClaim });
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            int[] permissionClaimInt = await userManager.GetPermissionIntAsync(user);
            // Assert.
            Assert.NotNull(permissionClaimInt);
            Assert.Equal(permissionClaimInt, new[] { 1, 2, 3 });
        }

        [Fact]
        public async Task GetPermissionsEnumTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            var permissionClaim = new Claim(SigmaIdentityConstants.PermissionsClaimName, "1001");
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.GetClaimsAsync(user, CancellationToken.None)).ReturnsAsync(new List<Claim>() { permissionClaim });
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            List<SigmaPermission> permissionClaimEnum = await userManager.GetPermissionsEnumAsync(user);
            // Assert.
            Assert.NotNull(permissionClaimEnum);
            Assert.Equal(permissionClaimEnum, new[] { SigmaPermission.Admin });
        }

        [Fact]
        public async Task AddPermissionIntTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            const string permissions = "1,2,3";
            const string newPermissions = "1,2,3,4";
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.GetClaimsAsync(user, CancellationToken.None)).ReturnsAsync(new[] { new Claim(SigmaIdentityConstants.PermissionsClaimName, permissions) });
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            userStore.Setup(store => store.UpdateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult result = await userManager.AddPermissionAsync(user, 4);
            // Assert.
            Assert.True(result.Succeeded);
            userStore.Verify(store => store.ReplaceClaimAsync(user, It.Is<Claim>(claims => claims.Value == permissions), It.Is<Claim>(claims => claims.Value == newPermissions), CancellationToken.None));
            userStore.VerifyAll();
        }

        [Fact]
        public async Task AddPermissionEnumTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            const string permissions = "1,2,3";
            const string newPermissions = "1,2,3,1001";
            Mock<SigmaUserStore> userStore = MockUserStore(await TestUtil.CreateTestContextAsync());
            userStore.Setup(store => store.GetClaimsAsync(user, CancellationToken.None)).ReturnsAsync(new[] { new Claim(SigmaIdentityConstants.PermissionsClaimName, permissions) });
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            userStore.Setup(store => store.UpdateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult result = await userManager.AddPermissionAsync(user, SigmaPermission.Admin);
            // Assert.
            Assert.True(result.Succeeded);
            userStore.Verify(store => store.ReplaceClaimAsync(user, It.Is<Claim>(claims => claims.Value == permissions), It.Is<Claim>(claims => claims.Value == newPermissions), CancellationToken.None));
            userStore.VerifyAll();
        }

        [Fact]
        public async Task AddPermissionDuplicateTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            const string permissions = "1,2,3,4";
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.GetClaimsAsync(user, CancellationToken.None)).ReturnsAsync(new[] { new Claim(SigmaIdentityConstants.PermissionsClaimName, permissions) });
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult result = await userManager.AddPermissionAsync(user, 4);
            // Assert.
            Assert.True(result.Succeeded);
            userStore.Verify(store => store.ReplaceClaimAsync(user, It.IsAny<Claim>(), It.IsAny<Claim>(), CancellationToken.None), Times.Never);
            userStore.VerifyAll();
        }

        [Fact]
        public async void RemovePermissionIntTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            const string permissions = "1,2,3,4";
            const string newPermissions = "1,2,3";
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.GetClaimsAsync(user, CancellationToken.None)).ReturnsAsync(new[] { new Claim(SigmaIdentityConstants.PermissionsClaimName, permissions) });
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            userStore.Setup(store => store.UpdateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult result = await userManager.RemovePermissionAsync(user, 4);
            // Assert.
            Assert.True(result.Succeeded);
            userStore.Verify(store => store.ReplaceClaimAsync(user, It.Is<Claim>(claims => claims.Value == permissions), It.Is<Claim>(claims => claims.Value == newPermissions), CancellationToken.None));
            userStore.VerifyAll();
        }

        [Fact]
        public async void RemovePermissionEnumTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            const string permissions = "1,2,3,1001";
            const string newPermissions = "1,2,3";
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.GetClaimsAsync(user, CancellationToken.None)).ReturnsAsync(new[] { new Claim(SigmaIdentityConstants.PermissionsClaimName, permissions) });
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            userStore.Setup(store => store.UpdateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult result = await userManager.RemovePermissionAsync(user, SigmaPermission.Admin);
            // Assert.
            Assert.True(result.Succeeded);
            userStore.Verify(store => store.ReplaceClaimAsync(user, It.Is<Claim>(claims => claims.Value == permissions), It.Is<Claim>(claims => claims.Value == newPermissions), CancellationToken.None));
            userStore.VerifyAll();
        }

        [Fact]
        public async void UpdatePermissionsTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            const string permissions = "1,2,3,1001";
            const string newPermissions = "1";
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.GetClaimsAsync(user, CancellationToken.None)).ReturnsAsync(new[] { new Claim(SigmaIdentityConstants.PermissionsClaimName, permissions) });
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            userStore.Setup(store => store.UpdateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult result = await userManager.UpdatePermissionsAsync(user, new[] { 1 });
            // Assert.
            Assert.True(result.Succeeded);
            userStore.Verify(store => store.ReplaceClaimAsync(user, It.Is<Claim>(claims => claims.Value == permissions), It.Is<Claim>(claims => claims.Value == newPermissions), CancellationToken.None));
            userStore.VerifyAll();
        }

        [Fact]
        public async Task SortedPagedUserListDefaultTestAsync()
        {
            // Setup.
            Mock<SigmaUserStore> userStore = MockUserStore();
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IPagedList<SigmaUser> result = await userManager.SortedPagedListAsync(1, 5, string.Empty, null);
            // Assert.
            userStore.VerifyAll();
            Assert.Null(result);
        }

        //TODO: Change search param strings to enums.
        [Fact]
        public async Task SortedPagedUserListTestAsync()
        {
            DateTime baseTime = DateTime.Now;
            // Setup.
            var sigmaUsers = new List<SigmaUser>()
                             {
                                 new SigmaUser()
                                 {
                                     UserName = "alfa",
                                     Email = "golf@delta.com",
                                     FirstName = "delta",
                                     LastName = "juliet",
                                     DateCreated = baseTime.AddSeconds(1),
                                     DateModified = baseTime.AddSeconds(10),
                                 },
                                 new SigmaUser()
                                 {
                                     UserName = "bravo",
                                     Email = "hotel@echo.com",
                                     FirstName = "echo",
                                     LastName = "kilo",
                                     DateCreated = baseTime.AddSeconds(2),
                                     DateModified = baseTime.AddSeconds(20),
                                 },
                                 new SigmaUser()
                                 {
                                     UserName = "charlie",
                                     Email = "india@foxtrot.com",
                                     FirstName = "foxtrot",
                                     LastName = "Lima",
                                     DateCreated = baseTime.AddSeconds(3),
                                     DateModified = baseTime.AddSeconds(30),
                                 }
                             };
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.Users).Returns(MockHelpers.GetQueryableMockDbSet(sigmaUsers));
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act - username.
            IPagedList<SigmaUser> resultUsernameAsc = await userManager.SortedPagedListAsync(1, 2, "userName", SortDirection.Asc);
            IPagedList<SigmaUser> resultUsernameDesc = await userManager.SortedPagedListAsync(1, 2, "userName", SortDirection.Desc);
            // Act - email.
            IPagedList<SigmaUser> resultEmailAsc = await userManager.SortedPagedListAsync(1, 2, "email", SortDirection.Asc);
            IPagedList<SigmaUser> resultEmailDesc = await userManager.SortedPagedListAsync(1, 2, "email", SortDirection.Desc);
            // Act - firstName.
            IPagedList<SigmaUser> resultFirstNameAsc = await userManager.SortedPagedListAsync(1, 2, "firstName", SortDirection.Asc);
            IPagedList<SigmaUser> resultFirstNameDesc = await userManager.SortedPagedListAsync(1, 2, "firstName", SortDirection.Desc);
            // Act - lastName.
            IPagedList<SigmaUser> resultLastNameAsc = await userManager.SortedPagedListAsync(1, 2, "lastName", SortDirection.Asc);
            IPagedList<SigmaUser> resultLastNameDesc = await userManager.SortedPagedListAsync(1, 2, "lastName", SortDirection.Desc);
            // Act - dateCreated.
            IPagedList<SigmaUser> resultDateCreatedAsc = await userManager.SortedPagedListAsync(1, 2, "dateCreated", SortDirection.Asc);
            IPagedList<SigmaUser> resultDateCreatedDesc = await userManager.SortedPagedListAsync(1, 2, "dateCreated", SortDirection.Desc);
            // Act - dateModified.
            IPagedList<SigmaUser> resultDateModifiedAsc = await userManager.SortedPagedListAsync(1, 2, "dateModified", SortDirection.Asc);
            IPagedList<SigmaUser> resultDateModifiedDesc = await userManager.SortedPagedListAsync(1, 2, "dateModified", SortDirection.Desc);
            // Assert.
            userStore.VerifyAll();
            // Assert - username.
            Assert.True(resultUsernameAsc != null && resultUsernameAsc.PageCount == 2 && resultUsernameAsc.TotalItemCount == 3);
            Assert.True(resultUsernameDesc != null && resultUsernameDesc.PageCount == 2 && resultUsernameDesc.TotalItemCount == 3);
            Assert.True(sigmaUsers[0].UserName == resultUsernameAsc[0].UserName);
            Assert.True(sigmaUsers[2].UserName == resultUsernameDesc[0].UserName);
            Assert.True(sigmaUsers[1].UserName == resultUsernameAsc[1].UserName && sigmaUsers[1].UserName == resultUsernameDesc[1].UserName);
            // Assert - email.
            Assert.True(resultEmailAsc != null && resultEmailAsc.PageCount == 2 && resultEmailAsc.TotalItemCount == 3);
            Assert.True(resultEmailDesc != null && resultEmailDesc.PageCount == 2 && resultEmailDesc.TotalItemCount == 3);
            Assert.True(sigmaUsers[0].Email == resultEmailAsc[0].Email);
            Assert.True(sigmaUsers[2].Email == resultEmailDesc[0].Email);
            Assert.True(sigmaUsers[1].Email == resultEmailAsc[1].Email && sigmaUsers[1].Email == resultEmailDesc[1].Email);
            // Assert - firstName.
            Assert.True(resultFirstNameAsc != null && resultFirstNameAsc.PageCount == 2 && resultFirstNameAsc.TotalItemCount == 3);
            Assert.True(resultFirstNameDesc != null && resultFirstNameDesc.PageCount == 2 && resultFirstNameDesc.TotalItemCount == 3);
            Assert.True(sigmaUsers[0].FirstName == resultFirstNameAsc[0].FirstName);
            Assert.True(sigmaUsers[2].FirstName == resultFirstNameDesc[0].FirstName);
            Assert.True(sigmaUsers[1].FirstName == resultFirstNameAsc[1].FirstName && sigmaUsers[1].FirstName == resultFirstNameDesc[1].FirstName);
            // Assert - lastName.
            Assert.True(resultLastNameAsc != null && resultLastNameAsc.PageCount == 2 && resultLastNameAsc.TotalItemCount == 3);
            Assert.True(resultLastNameDesc != null && resultLastNameDesc.PageCount == 2 && resultLastNameDesc.TotalItemCount == 3);
            Assert.True(sigmaUsers[0].LastName == resultLastNameAsc[0].LastName);
            Assert.True(sigmaUsers[2].LastName == resultLastNameDesc[0].LastName);
            Assert.True(sigmaUsers[1].LastName == resultLastNameAsc[1].LastName && sigmaUsers[1].LastName == resultLastNameDesc[1].LastName);
            // Assert - dateCreated.
            Assert.True(resultDateCreatedAsc != null && resultDateCreatedAsc.PageCount == 2 && resultDateCreatedAsc.TotalItemCount == 3);
            Assert.True(resultDateCreatedDesc != null && resultDateCreatedDesc.PageCount == 2 && resultDateCreatedDesc.TotalItemCount == 3);
            Assert.True(sigmaUsers[0].DateCreated == resultDateCreatedAsc[0].DateCreated);
            Assert.True(sigmaUsers[2].DateCreated == resultDateCreatedDesc[0].DateCreated);
            Assert.True(sigmaUsers[1].DateCreated == resultDateCreatedAsc[1].DateCreated && sigmaUsers[1].DateCreated == resultDateCreatedDesc[1].DateCreated);
            // Assert - dateModified.
            Assert.True(resultDateModifiedAsc != null && resultDateModifiedAsc.PageCount == 2 && resultDateModifiedAsc.TotalItemCount == 3);
            Assert.True(resultDateModifiedDesc != null && resultDateModifiedDesc.PageCount == 2 && resultDateModifiedDesc.TotalItemCount == 3);
            Assert.True(sigmaUsers[0].DateCreated == resultDateModifiedAsc[0].DateCreated);
            Assert.True(sigmaUsers[2].DateCreated == resultDateModifiedDesc[0].DateCreated);
            Assert.True(sigmaUsers[1].DateCreated == resultDateModifiedAsc[1].DateCreated && sigmaUsers[1].DateCreated == resultDateModifiedDesc[1].DateCreated);
        }

        [Fact]
        public async Task UpdateTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            SigmaUser userUpdated = user;
            userUpdated.FirstName = "Charlie";
            userUpdated.LastName = "Bravo";
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.FindByIdAsync(userUpdated.Id, CancellationToken.None)).ReturnsAsync(user);
            userStore.Setup(store => store.UpdateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult update = await userManager.UpdateAsync(userUpdated, "");
            // Assert.
            userStore.VerifyAll();
            Assert.True(update.Succeeded);
        }

        [Fact]
        public async Task UpdateWUserNameTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            SigmaUser userUpdated = user;
            userUpdated.FirstName = "Charlie";
            userUpdated.LastName = "Bravo";
            userUpdated.UserName = "newUserName";
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.FindByIdAsync(userUpdated.Id, CancellationToken.None)).ReturnsAsync(user);
            userStore.Setup(store => store.UpdateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult update = await userManager.UpdateAsync(userUpdated, "");
            // Assert.
            userStore.VerifyAll();
            Assert.True(update.Succeeded);
        }

        [Fact]
        public async Task UpdateWPasswordTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            SigmaUser userUpdated = user;
            userUpdated.FirstName = "Charlie";
            userUpdated.LastName = "Bravo";
            userUpdated.PasswordHash = "MySecretPassword1!";
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.FindByIdAsync(userUpdated.Id, CancellationToken.None)).ReturnsAsync(user);
            userStore.Setup(store => store.UpdateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult update = await userManager.UpdateAsync(userUpdated, "");
            // Assert.
            userStore.VerifyAll();
            Assert.True(update.Succeeded);
        }

        [Fact]
        public async Task SetPasswordTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.UpdateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult result = await userManager.SetPassword(user, "Password1!");
            // Assert.
            Assert.True(result.Succeeded);
        }

        [Fact]
        public async Task SetPasswordBadTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.UpdateAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            userStore.Setup(store => store.GetSecurityStampAsync(user, CancellationToken.None)).ReturnsAsync("fakeSecurityStamp");
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult result = await userManager.SetPassword(user, "foo");
            // Assert.
            Assert.False(result.Succeeded);
            Assert.Collection(result.Errors,
                              item => Assert.Contains(item.Code, "PasswordTooShort"),
                              item => Assert.Contains(item.Code, "PasswordRequiresNonAlphanumeric"),
                              item => Assert.Contains(item.Code, "PasswordRequiresDigit"),
                              item => Assert.Contains(item.Code, "PasswordRequiresUpper"));
        }

        [Fact]
        public async Task AddRefreshTokenTestAsync()
        {
            // Setup.
            RefreshToken refreshToken = TestUtil.CreateTestRefreshToken();
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.AddRefreshTokenAsync(refreshToken)).ReturnsAsync(true);
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act
            bool result = await userManager.AddRefreshTokenAsync(refreshToken);
            // Assert.
            Assert.True(result);
        }

        [Fact]
        public async Task GetRefreshTokenBySignatureTestAsync()
        {
            // Setup.
            RefreshToken refreshToken = TestUtil.CreateTestRefreshToken();
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.RefreshTokens).Returns(TestUtil.CreateRefreshTokenQueryable(refreshToken));
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            RefreshToken result = await userManager.GetRefreshTokenAsync(refreshToken.Signature);
            // Assert.
            Assert.NotNull(result);
            Assert.Equal(result, refreshToken);
        }

        [Fact]
        public async Task GetRefreshTokensTestAsync()
        {
            SigmaUser user = TestUtil.CreateTestUser();
            // Setup.
            var refreshTokens = new List<RefreshToken>()
                                {
                                    TestUtil.CreateTestRefreshToken(user.Id, user),
                                    TestUtil.CreateTestRefreshToken(user.Id, user),
                                };
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.RefreshTokens).Returns(TestUtil.CreateRefreshTokenQueryable(refreshTokens:refreshTokens));
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IEnumerable<RefreshToken> result = await userManager.GetRefreshTokensAsync(user);
            // Assert.
            Assert.NotNull(result);
            Assert.Equal(result, refreshTokens.ToList());
        }

        [Fact]
        public async Task UpdateRefreshTokenLastUsedDateTestAsync()
        {
            // Setup.
            RefreshToken refreshToken = TestUtil.CreateTestRefreshToken();
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.UpdateRefreshTokenLastUsedDateAsync(refreshToken)).ReturnsAsync(true);
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            bool result = await userManager.UpdateRefreshTokenLastUsedDateAsync(refreshToken);
            // Assert.
            Assert.True(result);
        }

        [Fact]
        public async Task RemoveRefreshTokenTestAsync()
        {
            // Setup.
            RefreshToken refreshToken = TestUtil.CreateTestRefreshToken();
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.RemoveRefreshTokenAsync(refreshToken.Signature)).ReturnsAsync(true);
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            bool result = await userManager.RemoveRefreshTokenAsync(refreshToken.Signature);
            // Assert.
            Assert.True(result);
        }

        //TODO: Change search param strings to enums.
        [Fact]
        public async Task GetRefreshTokensPagedListTestAsync()
        {
            DateTime baseTime = DateTime.Now;
            SigmaUser user = TestUtil.CreateTestUser();
            // Setup.
            var refreshTokens = new List<RefreshToken>
                                {
                                    new RefreshToken
                                    {
                                        User = user,
                                        UserId = user.Id,
                                        UserAgent = "Alfa",
                                        IpAddress = "1.0.0.1",
                                        Signature = "echo ",
                                        DateCreated = baseTime.AddSeconds(1),
                                        DateLastUsed = baseTime.AddSeconds(10),
                                    },
                                    new RefreshToken
                                    {
                                        User = user,
                                        UserId = user.Id,
                                        UserAgent = "Bravo",
                                        IpAddress = "10.0.0.10",
                                        Signature = "foxtrot",
                                        DateCreated = baseTime.AddSeconds(2),
                                        DateLastUsed = baseTime.AddSeconds(20),
                                    },
                                    new RefreshToken
                                    {
                                        User = user,
                                        UserId = user.Id,
                                        UserAgent = "Charlie",
                                        IpAddress = "100.0.0.100",
                                        Signature = "golf",
                                        DateCreated = baseTime.AddSeconds(3),
                                        DateLastUsed = baseTime.AddSeconds(30),
                                    },
                                };
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.RefreshTokens).Returns(MockHelpers.GetQueryableMockDbSet(refreshTokens));
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act - userAgent.
            IPagedList<RefreshToken> resultUserAgentAsc = await userManager.GetRefreshTokensPagedListAsync(user, 1, 2, "userAgent", SortDirection.Asc);
            IPagedList<RefreshToken> resultUserAgentDesc = await userManager.GetRefreshTokensPagedListAsync(user, 1, 2, "userAgent", SortDirection.Desc);
            // Act - ipAddress.
            IPagedList<RefreshToken> resultIpAddressAsc = await userManager.GetRefreshTokensPagedListAsync(user, 1, 2, "ipAddress", SortDirection.Asc);
            IPagedList<RefreshToken> resultIpAddressDesc = await userManager.GetRefreshTokensPagedListAsync(user, 1, 2, "ipAddress", SortDirection.Desc);
            // Act - signature.
            IPagedList<RefreshToken> resultSignatureAsc = await userManager.GetRefreshTokensPagedListAsync(user, 1, 2, "ipAddress", SortDirection.Asc);
            IPagedList<RefreshToken> resultSignatureDesc = await userManager.GetRefreshTokensPagedListAsync(user, 1, 2, "ipAddress", SortDirection.Desc);
            // Act - dateCreated.
            IPagedList<RefreshToken> resultDateCreatedAsc = await userManager.GetRefreshTokensPagedListAsync(user, 1, 2, "dateCreated", SortDirection.Asc);
            IPagedList<RefreshToken> resultDateCreatedDesc = await userManager.GetRefreshTokensPagedListAsync(user, 1, 2, "dateCreated", SortDirection.Desc);
            // Act - dateLastUsed.
            IPagedList<RefreshToken> resultDateLastUsedAsc = await userManager.GetRefreshTokensPagedListAsync(user, 1, 2, "dateLastUsed", SortDirection.Asc);
            IPagedList<RefreshToken> resultDateLastUsedDesc = await userManager.GetRefreshTokensPagedListAsync(user, 1, 2, "dateLastUsed", SortDirection.Desc);
            // Assert.
            userStore.VerifyAll();
            // Assert - userAgent.
            Assert.True(resultUserAgentAsc != null && resultUserAgentAsc.PageCount == 2 && resultUserAgentAsc.TotalItemCount == 3);
            Assert.True(resultUserAgentDesc != null && resultUserAgentDesc.PageCount == 2 && resultUserAgentDesc.TotalItemCount == 3);
            Assert.True(refreshTokens[0] == resultUserAgentAsc[0]);
            Assert.True(refreshTokens[2].UserAgent == resultUserAgentDesc[0].UserAgent);
            Assert.True(refreshTokens[1].UserAgent == resultUserAgentAsc[1].UserAgent && refreshTokens[1].UserAgent == resultUserAgentDesc[1].UserAgent);
            // Assert - ipAddress.
            Assert.True(resultIpAddressAsc != null && resultIpAddressAsc.PageCount == 2 && resultIpAddressAsc.TotalItemCount == 3);
            Assert.True(resultIpAddressDesc != null && resultIpAddressDesc.PageCount == 2 && resultIpAddressDesc.TotalItemCount == 3);
            Assert.True(refreshTokens[0] == resultIpAddressAsc[0]);
            Assert.True(refreshTokens[2].IpAddress == resultIpAddressDesc[0].IpAddress);
            Assert.True(refreshTokens[1].IpAddress == resultIpAddressAsc[1].IpAddress && refreshTokens[1].IpAddress == resultIpAddressDesc[1].IpAddress);
            // Assert - signature.
            Assert.True(resultSignatureAsc != null && resultSignatureAsc.PageCount == 2 && resultSignatureAsc.TotalItemCount == 3);
            Assert.True(resultSignatureDesc != null && resultSignatureDesc.PageCount == 2 && resultSignatureDesc.TotalItemCount == 3);
            Assert.True(refreshTokens[0] == resultSignatureAsc[0]);
            Assert.True(refreshTokens[2].IpAddress == resultSignatureDesc[0].IpAddress);
            Assert.True(refreshTokens[1].IpAddress == resultSignatureAsc[1].IpAddress && refreshTokens[1].IpAddress == resultSignatureDesc[1].IpAddress);
            // Assert - dateCreated.
            Assert.True(resultDateCreatedAsc != null && resultDateCreatedAsc.PageCount == 2 && resultDateCreatedAsc.TotalItemCount == 3);
            Assert.True(resultDateCreatedDesc != null && resultDateCreatedDesc.PageCount == 2 && resultDateCreatedDesc.TotalItemCount == 3);
            Assert.True(refreshTokens[0] == resultDateCreatedAsc[0]);
            Assert.True(refreshTokens[2].IpAddress == resultDateCreatedDesc[0].IpAddress);
            Assert.True(refreshTokens[1].IpAddress == resultDateCreatedAsc[1].IpAddress && refreshTokens[1].IpAddress == resultDateCreatedDesc[1].IpAddress);
            // Assert - dateLastUsed.
            Assert.True(resultDateLastUsedAsc != null && resultDateLastUsedAsc.PageCount == 2 && resultDateLastUsedAsc.TotalItemCount == 3);
            Assert.True(resultDateLastUsedDesc != null && resultDateLastUsedDesc.PageCount == 2 && resultDateLastUsedDesc.TotalItemCount == 3);
            Assert.True(refreshTokens[0] == resultDateLastUsedAsc[0]);
            Assert.True(refreshTokens[2].IpAddress == resultDateLastUsedDesc[0].IpAddress);
            Assert.True(refreshTokens[1].IpAddress == resultDateLastUsedAsc[1].IpAddress && refreshTokens[1].IpAddress == resultDateLastUsedDesc[1].IpAddress);
        }

        [Fact]
        public async Task DeleteTestAsync()
        {
            // Setup.
            SigmaUser user = TestUtil.CreateTestUser();
            Mock<SigmaUserStore> userStore = MockUserStore();
            userStore.Setup(store => store.DeleteRefreshTokenByUserAsync(user)).Returns(Task.CompletedTask);
            userStore.Setup(store => store.DeleteAsync(user, CancellationToken.None)).ReturnsAsync(IdentityResult.Success);
            SigmaUserManager userManager = MockHelpers.TestUserManager(userStore.Object);
            // Act.
            IdentityResult result = await userManager.DeleteAsync(user);
            // Assert.
            userStore.VerifyAll();
            Assert.True(result.Succeeded);
        }

    }
}