﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sigma.Identity;
using Xunit;

namespace Sigma.Test.Identity
{
    public class SigmaUserStoreTest
    {
        [Fact]
        public async Task SqlUserStoreMethodsThrowWhenDisposedTestAsync()
        {
            var store = new SigmaUserStore(new SigmaIdentityDbContext(new DbContextOptionsBuilder<SigmaIdentityDbContext>().Options));
            store.Dispose();
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await store.AddRefreshTokenAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await store.UpdateRefreshTokenLastUsedDateAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await store.UpdateRefreshTokenAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await store.UpdateRefreshTokensAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await store.RemoveRefreshTokenAsync(null));
            await Assert.ThrowsAsync<ObjectDisposedException>(async () => await store.DeleteRefreshTokenByUserAsync(null));
        }

        [Fact]
        public async Task UserStorePublicNullCheckTestAsync()
        {
            Assert.Throws<ArgumentNullException>("context", () => new SigmaUserStore(null));
            var store = new SigmaUserStore(new SigmaIdentityDbContext(new DbContextOptionsBuilder<SigmaIdentityDbContext>().Options));
            await Assert.ThrowsAsync<ArgumentNullException>("refreshToken", async () => await store.AddRefreshTokenAsync(null));
            await Assert.ThrowsAsync<ArgumentNullException>("refreshToken", async () => await store.UpdateRefreshTokenLastUsedDateAsync(null));
            await Assert.ThrowsAsync<ArgumentNullException>("refreshToken", async () => await store.UpdateRefreshTokenAsync(null));
            await Assert.ThrowsAsync<ArgumentNullException>("refreshTokens", async () => await store.UpdateRefreshTokensAsync(null));
            await Assert.ThrowsAsync<ArgumentNullException>("refreshTokenSignature", async () => await store.RemoveRefreshTokenAsync(null));
            await Assert.ThrowsAsync<ArgumentNullException>("user", async () => await store.DeleteRefreshTokenByUserAsync(null));
        }

        [Fact]
        public async Task AddRefreshTokenTestAsync()
        {
            using (var userStore = new SigmaUserStore(await TestUtil.CreateTestContextAsync()))
            {
                // Setup.
                SigmaUser user = TestUtil.CreateTestUser();
                await userStore.CreateAsync(user);
                RefreshToken refreshToken = TestUtil.CreateTestRefreshToken(user: user, userId: user.Id);
                // Act.
                bool result = await userStore.AddRefreshTokenAsync(refreshToken);
                // Assert.
                Assert.True(result);
            }
        }

        [Fact]
        public virtual async Task UpdateRefreshTokenLastUsedDateTestAsync()
        {
            using (var userStore = new SigmaUserStore(await TestUtil.CreateTestContextAsync()))
            {
                // Setup.
                DateTime dateLastUsed = DateTime.UtcNow;
                SigmaUser user = TestUtil.CreateTestUser();
                await userStore.CreateAsync(user);
                RefreshToken refreshToken = TestUtil.CreateTestRefreshToken(user: user, userId: user.Id, dateLastUsed: dateLastUsed);
                await userStore.AddRefreshTokenAsync(refreshToken);
                // Act.
                bool result = await userStore.UpdateRefreshTokenLastUsedDateAsync(refreshToken);
                // Assert.
                Assert.True(result);
                RefreshToken getRefreshToken = await userStore.RefreshTokens.FirstOrDefaultAsync(token => token.Signature == refreshToken.Signature);
                Assert.True(getRefreshToken.DateLastUsed > dateLastUsed);
                Assert.True(getRefreshToken.DateModified > DateTime.MinValue);
            }
        }

        [Fact]
        public async Task UpdateRefreshTokenTestAsync()
        {
            using (var userStore = new SigmaUserStore(await TestUtil.CreateTestContextAsync()))
            {
                // Setup.
                SigmaUser user = TestUtil.CreateTestUser();
                await userStore.CreateAsync(user);
                RefreshToken refreshToken = TestUtil.CreateTestRefreshToken(user: user, userId: user.Id);
                await userStore.AddRefreshTokenAsync(refreshToken);
                // Act.
                refreshToken.Claims = "myNewClaims";
                await userStore.UpdateRefreshTokenAsync(refreshToken);
                // Assert.
                RefreshToken getRefreshToken = await userStore.RefreshTokens.FirstOrDefaultAsync(token => token.Signature == refreshToken.Signature);
                Assert.True(getRefreshToken.Claims == refreshToken.Claims);
                Assert.True(getRefreshToken.DateModified > DateTime.MinValue);
            }
        }

        [Fact]
        public async Task UpdateRefreshTokensTestAsync()
        {
            // Setup.
            SigmaIdentityDbContext dbContext = await TestUtil.CreateTestContextAsync();
            using (var userStore = new SigmaUserStore(dbContext))
            {
                SigmaUser user = TestUtil.CreateTestUser();
                await userStore.CreateAsync(user);
                var refreshTokens = new List<RefreshToken> { TestUtil.CreateTestRefreshToken(user: user, userId: user.Id), TestUtil.CreateTestRefreshToken(user: user, userId: user.Id) };
                foreach (RefreshToken refreshToken in refreshTokens)
                    await userStore.AddRefreshTokenAsync(refreshToken);
                // Act.
                var newClaims = "my new claims";
                for (var i = 0; i < refreshTokens.Count; i++)
                    refreshTokens.ElementAt(i).Claims = newClaims;
                await userStore.UpdateRefreshTokensAsync(refreshTokens);
                // Assert.
                foreach (RefreshToken refreshToken in refreshTokens)
                {
                    RefreshToken getRefreshToken = await userStore.RefreshTokens.FirstOrDefaultAsync(token => token.Signature == refreshToken.Signature);
                    Assert.True(getRefreshToken.Claims == newClaims);
                    Assert.True(getRefreshToken.DateModified > DateTime.MinValue);
                }
            }
        }

        [Fact]
        public async Task RemoveRefreshTokenTestAsync()
        {
            using (var userStore = new SigmaUserStore(await TestUtil.CreateTestContextAsync()))
            {
                // Setup.
                SigmaUser user = TestUtil.CreateTestUser();
                await userStore.CreateAsync(user);
                RefreshToken refreshToken = TestUtil.CreateTestRefreshToken(user: user, userId: user.Id);
                await userStore.AddRefreshTokenAsync(refreshToken);
                // Act.
                bool result = await userStore.RemoveRefreshTokenAsync(refreshToken.Signature);
                // Assert.
                Assert.True(result);
                Assert.Null(await userStore.RefreshTokens.FirstOrDefaultAsync(token => token.Signature == refreshToken.Signature));
            }
        }

        [Fact]
        public async Task GetRefreshTokenTestAsync()
        {
            using (var userStore = new SigmaUserStore(await TestUtil.CreateTestContextAsync()))
            {
                // Setup.
                SigmaUser user = TestUtil.CreateTestUser();
                await userStore.CreateAsync(user);
                RefreshToken refreshToken = TestUtil.CreateTestRefreshToken(user: user, userId: user.Id);
                await userStore.AddRefreshTokenAsync(refreshToken);
                // Act.
                RefreshToken getRefreshToken = await userStore.RefreshTokens.FirstOrDefaultAsync(token => token.Signature == refreshToken.Signature);
                // Assert.
                Assert.Equal(refreshToken, getRefreshToken);
            }
        }

        [Fact]
        public async Task RemoveRefreshTokenByUserTestAsync()
        {
            using (var userStore = new SigmaUserStore(await TestUtil.CreateTestContextAsync()))
            {
                // Setup.
                SigmaUser user = TestUtil.CreateTestUser();
                await userStore.CreateAsync(user);
                RefreshToken refreshToken = TestUtil.CreateTestRefreshToken(user: user, userId: user.Id);
                await userStore.AddRefreshTokenAsync(refreshToken);
                // Act.
                await userStore.DeleteRefreshTokenByUserAsync(user);
                // Assert.
                Assert.Null(await userStore.RefreshTokens.FirstOrDefaultAsync(token => token.Signature == refreshToken.Signature));
                Assert.True((await userStore.RefreshTokens.Where(token => token.User == user).ToListAsync()).Count == 0);
            }
        }
    }
}