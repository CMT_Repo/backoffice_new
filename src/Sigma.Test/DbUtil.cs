﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Sigma.Identity;

namespace Sigma.Test
{
    internal static class DbUtil
    {
        private static IServiceCollection ConfigureDbServices <TContext> (DbConnection connection, IServiceCollection services = null) where TContext : Microsoft.EntityFrameworkCore.DbContext
        {
            if (services == null)
            {
                services = new ServiceCollection();
            }
            services.AddHttpContextAccessor();
            services.AddDbContext<TContext>(options => {
                                                options
                                                    .ConfigureWarnings(b => b.Log(CoreEventId.ManyServiceProvidersCreatedWarning))
                                                    .UseSqlite(connection);
                                            });
            return services;
        }

        internal static TContext Create <TContext> (DbConnection connection, IServiceCollection services = null) where TContext : Microsoft.EntityFrameworkCore.DbContext
        {
            ServiceProvider serviceProvider = ConfigureDbServices<TContext>(connection, services).BuildServiceProvider();
            return serviceProvider.GetRequiredService<TContext>();
        }

        internal static async Task<SigmaIdentityDbContext> CreateSigmaIdentityContextAsync (bool delete = false)
        {
            var sqliteConnection = new SqliteConnection($"DataSource=D{Guid.NewGuid()}.db");
            SigmaIdentityDbContext dbContext = Create<SigmaIdentityDbContext>(sqliteConnection);
            if (delete)
                await dbContext.Database.EnsureDeletedAsync();
            // In-memory database only exists while the connection is open
            sqliteConnection.Open();
            await dbContext.Database.EnsureCreatedAsync();
            return dbContext;
        }
    }
}