CREATE SCHEMA data_development
CREATE TABLE data_development."__EFMigrationsHistory" ( "MigrationId" text NOT NULL,
                                                                           "ProductVersion" text NOT NULL,
                                                                                                 CONSTRAINT "PK_HistoryRow" PRIMARY KEY ("MigrationId"));


CREATE SCHEMA data_production
CREATE TABLE data_production."__EFMigrationsHistory" ( "MigrationId" text NOT NULL,
                                                                          "ProductVersion" text NOT NULL,
                                                                                                CONSTRAINT "PK_HistoryRow" PRIMARY KEY ("MigrationId"));


CREATE SCHEMA identity_development
CREATE TABLE identity_development."__EFMigrationsHistory" ( "MigrationId" text NOT NULL,
                                                                               "ProductVersion" text NOT NULL,
                                                                                                     CONSTRAINT "PK_HistoryRow" PRIMARY KEY ("MigrationId"));


CREATE SCHEMA identity_production;


CREATE TABLE identity_production."__EFMigrationsHistory" ( "MigrationId" text NOT NULL,
                                                                              "ProductVersion" text NOT NULL,
                                                                                                    CONSTRAINT "PK_HistoryRow" PRIMARY KEY ("MigrationId"));