﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting.Internal;

namespace Sigma.Core
{
    public class ConfigurationHelper
    {
        public static IConfigurationRoot InitConfigurationRoot()
        {
            bool isDocker = IsDocker();
            HostingEnvironment environment = GetEnvironment();
            // Check whether ASPNETCORE_ENVIRONMENT is present, otherwise fill it with an empty string.
            if (string.IsNullOrEmpty(environment.EnvironmentName))
                environment.EnvironmentName = string.Empty;
            string appSettingsFileName;
            // Add staging.
            switch (environment.EnvironmentName.ToLower())
            {
                case "development":
                    environment.EnvironmentName = "development";
                    appSettingsFileName = isDocker ? "appsettings.docker.development.json" : "appsettings.development.json";
                    break;
                default:
                    // Default to Production if no environment provided.
                    environment.EnvironmentName = "production";
                    appSettingsFileName = isDocker ? "appsettings.docker.json" : "appsettings.json";
                    break;
            }
            IConfigurationRoot configurationRoot = new ConfigurationBuilder().AddJsonFile(appSettingsFileName, false, false).Build();
            // Print config being used.
            Console.WriteLine("=============================================== CONFIG USED ================================================");
            foreach (IConfigurationProvider provider in configurationRoot.Providers)
                Console.WriteLine(((FileConfigurationProvider)provider).Source.Path);
            Console.WriteLine("============================================================================================================");
            return configurationRoot;
        }

        public static bool IsDocker()
        {
            return Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER") == "true";
        }

        public static HostingEnvironment GetEnvironment()
        {
            return new HostingEnvironment
                   {
                       EnvironmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"),
                       ApplicationName = AppDomain.CurrentDomain.FriendlyName,
                       ContentRootPath = AppDomain.CurrentDomain.BaseDirectory
                   };
        }
    }
}