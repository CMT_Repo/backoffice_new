﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Sigma.Core.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        ///   Calculate the numeric value of a string, symbols ignored.
        /// </summary>
        public static double ToNumericValue (this string str)
        {
            return str.Sum(t => char.IsLetter(t) ? t : char.IsDigit(t) ? t : 0);
        }

        /// <summary>
        ///   Create int array.
        /// </summary>
        public static int[] ToIntArray (this string str)
        {
            return string.IsNullOrEmpty(str) ? Array.Empty<int>() : str.Split(',').Select(value => Convert.ToInt32(value)).ToArray();
        }

        /// <summary>
        ///   Create string array.
        /// </summary>
        public static string[] ToStringArray (this string str)
        {
            return string.IsNullOrEmpty(str) ? new string[0] : str.Split(',');
        }

        /// <summary>
        ///   Cast string to claim array.
        /// </summary>
        public static IEnumerable<Claim> FromStringToClaims (this string str)
        {
            string[] separatedStringClaims = str.Split('|');
            ICollection<Claim> claims = new List<Claim>();
            for (var i = 0; i < separatedStringClaims.Count(); i++)
            {
                string[] extractedStringClaim = separatedStringClaims[i].Split('~');
                if (extractedStringClaim.Length > 1)
                    // Remove tailoring whitespace at the beginning.
                    claims.Add(new Claim(extractedStringClaim[0], extractedStringClaim[1]));
            }
            return claims;
        }

        /// <summary>
        ///   Construct updated claim string with the updated claim.
        /// </summary>
        public static string ConstructUpdatedStringClaim (this string str, Claim claim)
        {
            var claims = str.FromStringToClaims().ToList();
            for (var i = 0; i < claims.Count(); i++)
            {
                if (claims[i].Type != claim.Type)
                    continue;
                claims.RemoveAt(i);
                claims.Add(claim);
                break;
            }
            return claims.ClaimsToString();
        }

        /// <summary>
        ///   Convert string to int, in case of an exception zero is returned.
        /// </summary>
        public static int ToInt (this string str)
        {
            return string.IsNullOrEmpty(str) ? 0 : Convert.ToInt32(str);
        }

        /// <summary>
        ///   Check whether the string is null or empty, in case it's return empty string.
        /// </summary>
        public static string IsNullOrEmptySafe (this string str)
        {
            return string.IsNullOrEmpty(str) ? string.Empty : str;
        }
    }
}