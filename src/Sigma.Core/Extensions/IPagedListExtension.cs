﻿using System.Collections.Generic;
using AutoMapper;
using X.PagedList;

namespace Sigma.Core.Extensions
{
    public static class IPagedListExtension
    {
        public static IPagedList<TDestination> ToMappedPagedList <TSource, TDestination> (this IPagedList<TSource> list, IMapper mapper)
        {
            IEnumerable<TDestination> sourceList = mapper.Map<IEnumerable<TSource>, IEnumerable<TDestination>>(list);
            return new StaticPagedList<TDestination>(sourceList, list.GetMetaData());
        }
    }
}