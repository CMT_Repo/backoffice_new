﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Sigma.Core.Extensions
{
    public static class EnumerableExtension
    {
        /// <summary>
        ///   Determine whether a sequence contains a null element.
        /// </summary>
        public static bool AnyNull <T> (this IEnumerable<T> data)
        {
            for (var i = 0; i < data.Count(); i++)
            {
                if (data.ElementAt(i) != null)
                    continue;
                return true;
            }
            return false;
        }

        /// <summary>
        ///   Get null Indexes.
        /// </summary>
        public static IEnumerable<int> GetNullIndexes <T> (this IEnumerable<T> data)
        {
            for (var i = 0; i < data.Count(); i++)
                if (data.ElementAt(i) == null)
                    yield return i;
        }

        /// <summary>
        ///   Get new array without nulls.
        /// </summary>
        public static IEnumerable<T> SpliceNulls <T> (this IEnumerable<T> data)
        {
            return data.Where(value => value != null);
        }

        /// <summary>
        ///   Convert Enum IEnumerable to it's value in a string arr format.
        /// </summary>
        public static string EnumToValueString (this IEnumerable<Enum> data)
        {
            int[] intArr = data.Cast<int>().ToArray();
            return string.Join(",", intArr.Select(p => p.ToString()).ToArray());
        }

        /// <summary>
        ///   Convert int IEnumerable to string.
        /// </summary>
        public static string PermissionClaimToString (this IEnumerable<int> data)
        {
            return string.Join(",", data.Select(p => p.ToString()).ToArray());
        }

        /// <summary>
        ///   Convert claims to string.
        /// </summary>
        public static string ClaimsToString (this IEnumerable<Claim> claims)
        {
            var stringBuilder = new StringBuilder();
            foreach (Claim claim in claims)
                stringBuilder.Append($"{claim.Type}~{claim.Value}|");
            return stringBuilder.ToString();
        }

        /// <summary>
        ///   Check whether IEnumerable is null, in case it's return new IEnumerable.
        /// </summary>
        public static IEnumerable<T> NullCheckSafe <T> (this IEnumerable<T> data)
        {
            return data ?? new List<T>();
        }
    }
}