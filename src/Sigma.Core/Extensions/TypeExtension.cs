﻿using System;
using System.Linq;
using System.Reflection;

namespace Sigma.Core.Extensions
{
    public static class TypeExtension
    {
        /// <summary>
        /// Adds all of the public facing properties.
        /// </summary>
        public static string[] GetDeclaredProperties <T> (this T t) where T : Type
        {
            return t.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).Select(param => param.Name).ToArray();
        }
    }
}