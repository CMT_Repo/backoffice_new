﻿using System;

namespace Sigma.Core
{
    public class IDesignTimeDbContextFactoryHelper
    {
        public static void WriteInConsoleConnectionStringUsed (string connectionString)
        {
            Console.WriteLine("====================================================== Connection string used ===================================================================");
            Console.WriteLine(connectionString);
            Console.WriteLine("=================================================================================================================================================");
        }
    }
}