﻿using System;

namespace Sigma.Core.Domain
{
    public interface ISmtpClient : IBaseDomainObject
    {
        string Name { get; set; }
        string Host { get; set; }
        int Port { get; set; }
        bool IsEnabled { get; set; }
        bool IsSsl { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string From { get; set; }
    }

    public class SmtpClient : ISmtpClient
    {
        public string Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public string Name { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsSsl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string From { get; set; }
    }
}