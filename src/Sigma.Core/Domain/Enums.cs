﻿namespace Sigma.Core.Domain
{
    public enum SortDirection
    {
        Asc,
        Desc
    }
}