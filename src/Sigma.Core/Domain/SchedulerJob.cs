﻿namespace Sigma.Core.Domain
{
    public interface ISchedulerJob
    {
        public string Cron { get; set; }
    }
}