﻿using System;

namespace Sigma.Core.Domain
{
    public interface IBaseDomainObject {
        string Id { get; set; }
        DateTime DateCreated { get; set; }
        DateTime? DateModified { get; set; }
    }

    public class BaseDomainObject : IBaseDomainObject
    {
        public string Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}