﻿using System;
using System.Collections.Generic;

namespace Sigma.Core.Domain
{
    public interface IBiReportEmailJob <T> : ISchedulerJob, IBaseDomainObject where T : ISmtpClient
    {
        string Name { get; set; }
        string Title { get; set; }
        List<string> Recipients { get; set; }
        List<string> ReportUrls { get; set; }
        T SmtpClient { get; set; }
    }

    public class BiReportEmailJob : IBiReportEmailJob<SmtpClient>
    {
        public string Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public List<string> Recipients { get; set; }
        public List<string> ReportUrls { get; set; }
        public SmtpClient SmtpClient { get; set; }
        public string Cron { get; set; }
    }
}