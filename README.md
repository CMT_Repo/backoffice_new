# Sigma

### This repository holds the source code for Sigma.

# Migrations

### The following commands should be ran from within DbContext location.

#### Speify Environment settings for appsettings.json when doing applying migration/updating db using powershell

##### $env:ASPNETCORE_ENVIRONMENT='development'
##### $env:ASPNETCORE_ENVIRONMENT='production'

Read Environment Variable
##### gci env:ASPNETCORE_ENVIRONMENT

##### Add migration:

```
dotnet ef migrations add [migration name] -v -s ../Sigma.Api/Sigma.Api.csproj --context SigmaDbContext
dotnet ef migrations add [migration name] -v -s ../Sigma.Api/Sigma.Api.csproj --context SigmaIdentityDbContext
```

##### Apply migration:

```
dotnet ef database update [migration name] -v -s ../Sigma.Api/Sigma.Api.csproj --context SigmaDbContext
dotnet ef database update [migration name] -v -s ../Sigma.Api/Sigma.Api.csproj --context SigmaIdentityDbContext
```

##### Remove migration:

```
dotnet ef migrations remove -v -s ../Sigma.Api/Sigma.Api.csproj --context SigmaDbContext
dotnet ef migrations remove -v -s ../Sigma.Api/Sigma.Api.csproj --context SigmaIdentityDbContext
```

##### Revert migration:

```
dotnet ef database update -v -s ../Sigma.Api/Sigma.Api.csproj --context SigmaDbContext
dotnet ef database update -v -s ../Sigma.Api/Sigma.Api.csproj --context SigmaIdentityDbContext
```

##### Run angular using angular cli:

```
ng serve or npm start
```

##### In case migrations tabel is missing use the following script:

```
CREATE TABLE [schemaNameGoesHere]."__EFMigrationsHistory" (
"MigrationId" text NOT NULL,
"ProductVersion" text NOT NULL,
CONSTRAINT "PK_HistoryRow" PRIMARY KEY ("MigrationId"));
```
